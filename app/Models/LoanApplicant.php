<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;


class LoanApplicant extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level', 'exp', 'facebook_account_name', 'emergency_contact_no', 'user_id', 'is_blacklisted'
    ];


    public function user()
    {
        return $this->belongsToMany(User::class, 'user_id');
    }
}
