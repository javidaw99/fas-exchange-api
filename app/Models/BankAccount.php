<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BankCountry;
use App\Services\ActivityLogService;

class BankAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'bank_accounts';

    protected $fillable = [
        'bank_id', 'bank_country_id', 'acc_no', 'acc_name', 'country_id', 'currency_id', 'status' , 'minimum_amount', 'maximum_amount'
    ];

    public function bankCountry()
    {
        return $this->belongsTo(BankCountry::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    protected static function booted()
    {
        static::created(function ($model) {

            ActivityLogService::createActivityLog([
                'subject' => 'bank_accounts',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create bank account',
                'description' => "New bank account created",
            ]);
        });

        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => []
            ];

            $numberColumns = ['minimum_amount', 'maximum_amount'];

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'bank_accounts',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit bank account',
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                ]
            );
        });
    }
}
