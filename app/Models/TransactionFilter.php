<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Country;


class TransactionFilter extends Model
{
    use SoftDeletes;

    protected $table = 'transaction_filters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'currency_id', 'amount', 'amount_label'
    // ];

    public function filterable()
    {
        return $this->morphTo(__FUNCTION__, 'reference_type', 'reference_id');
    }

}
