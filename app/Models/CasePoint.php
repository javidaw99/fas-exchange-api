<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Account;
use App\Models\Transaction;
use App\Models\BankAccount;
use App\Models\Bank;

use App\Services\ActivityLogService;

class CasePoint extends Model
{
    use SoftDeletes;

    protected $table = 'case_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'assigned_user_id', 'reference_id', 'reference_table', 'user_account_id', 'status', 'reject_reason'
    ];

    // public function transact()
    // {
    //     return $this->morphTo(__FUNCTION__, 'reference_type', 'ref_id');
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function toAccount()
    {
        return $this->belongsTo(Account::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class);
    }
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
    public function assignedUser()
    {
        return $this->belongsTo(User::class, 'assigned_user_id');
    }

    public function casePoint()
    {
        return $this->morphTo(__FUNCTION__, 'reference_table', 'reference_id');
    }

}
