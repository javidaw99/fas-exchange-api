<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Country;


class CurrencyKeypad extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency_id', 'amount', 'amount_label'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency');
    }
}
