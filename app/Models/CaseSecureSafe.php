<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Cases;

class CaseSecureSafe extends Model
{
    use SoftDeletes;

    protected $table = 'case_secure_safe';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'case_id',
    ];

    public function case()
    {
        return $this->belongsTo(Cases::class);
    }
}
