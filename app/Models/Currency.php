<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Country;
use App\Models\Currency;
use App\Services\ActivityLogService;

class Currency extends Model
{
    use SoftDeletes;

    protected $morphClass = 'currencies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id', 'currency_name', 'iso_code',
        // 'locale',
        'flag', 'minimum_amount','maximum_amount'
    ];

    protected $hidden = array('pivot');

    public function country()
    {
        return $this->belongsToMany(Country::class, 'country_currency');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'currency_id', 'user_id');
    }

    public function filter()
    {
        return $this->morphOne(TransactionFilter::class, 'filterable', 'reference_table', 'reference_id');

    }

    protected static function booted()
    {
        static::created(function ($model) {

            ActivityLogService::createActivityLog([
                'subject' => 'currencies',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create currency',
                'description' => "New currency created"
            ]);
        });

        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => []
            ];

            $numberColumns = ['minimum_amount'];

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'currencies',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit currency',
                    'number_columns' => $numberColumns,
                    'columns' => $columns
                ]
            );
        });
    }
}
