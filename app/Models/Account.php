<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Currency;
use App\Models\Transaction;
use App\Models\Cases;

class Account extends Model
{

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function cases()
    {
        return $this->hasMany(Cases::class);
    }

    public function countries()
    {
        return $this->belongsTo(Country::class);
    }
}
