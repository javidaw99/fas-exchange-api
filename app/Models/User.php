<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\BankCountry;
use App\Models\UserBank;
use App\Models\Cases;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\Country;
use App\Models\Role;
use App\Models\UserAccount;
use App\Models\Notification;
use App\Services\ActivityLogService;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use SoftDeletes, Authenticatable, Authorizable, HasFactory;

    protected $morphClass = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'country_id', 'role_id', 'pin',
    // ];
    protected $fillable = [
        'username', 
        'fullname', 
        'email', 
        'phone_no', 
        'gender', 
        'dob', 
        'id_number', 
        'country_id', 
        'role_id', 
        'pin', 
        'fcm_token', 
        'fcm_token_status', 
        'app_language', 
        'topkash_id', 
        'password', 
        'secret_key', 
        'status', 
        'is_temp_password',
        'user_device_id',
        'money_changer_id'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'pin',
    ];

    public function personalBanks()
    {
        return $this->belongsToMany(BankCountry::class, 'user_bank', 'user_id', 'bank_country_id')
            ->using(UserBank::class)
            ->as('userbank')
            ->withPivot('id', 'acc_no', 'acc_name');
    }

    public function cases()
    {
        return $this->hasMany(Cases::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function userAccounts()
    {
        return $this->hasMany(UserAccount::class, 'user_id', 'id');
    }

    public function wallets()
    {
        return $this->belongsToMany(Currency::class, 'user_accounts', 'user_id', 'currency_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function accountsTransactions()
    {
        return $this->hasManyThrough(Transaction::class, Account::class);
    }

    public function personalBankByBankAccountAndBankBranch($bankAccountNo, $bankCountryId)
    {

        $userBank = $this->with([
            'personalBanks' => function ($query) use ($bankCountryId, $bankAccountNo) {
                $query
                    ->where('user_bank.bank_country_id', '=', $bankCountryId)
                    ->where('user_bank.acc_no', '=', $bankAccountNo)
                    ->limit(1);
            }
        ])->first();

        return $userBank->personalBanks;
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function message()
    {
        return $this->hasMany(Notification::class);
    }

    // public function address()
    // {
    //     return $this->belongsTo(Address::class, 'address_id');
    // }
    public function address()
    {
        return $this->morphOne(Address::class,'addressable','reference_table','reference_id');
    }

    public function maritalStatus()
    {
        return $this->belongsTo(MaritalStatus::class, 'marital_status_id');
    }

    public function activityLogs()
    {
        return $this->hasMany(ActivityLog::class);
    }

    protected static function booted()
    {
        static::created(function ($model) {

            $currentValues = $model->getAttributes();

            ActivityLogService::createActivityLog([
                'subject' => 'users',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create user',
                'description' => "New user created",
                'user_id' => $currentValues['id']
            ]);
        });

        static::updated(function ($model) {

            $columns = [
                'omit' => [
                    'app_pin', 'fcm_token', 'fcm_token_status', 'password', 'is_temp_password',
                    'reset_pwd_timestamp', 'tfa_secret', 'biometric_public_key', 'secret_key',
                    'topkash_id'
                ],
                'include' => []
            ];

            $numberColumns = [];

            $currentValues = $model->getAttributes();

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'users',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit user',
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                    'user_id' => $currentValues['id']
                ]
            );
        });
    }
}
