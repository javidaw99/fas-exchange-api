<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ReferralGroup;
use App\Models\ReferralTree;
use App\Models\Commission;

class Referral extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'referral_tree_id', 'referral_group_id',
    ];

    public function referralTree()
    {
        return $this->belongsTo(ReferralTree::class, 'referral_tree_id');
    }

    public function referralGroup()
    {
        return $this->belongsTo(ReferralGroup::class, 'referral_group_id');
    }

    public function commission()
    {
        return $this->hasMany(Commission::class);
    }
}
