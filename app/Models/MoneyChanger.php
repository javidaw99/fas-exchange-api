<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Address;
use App\Models\Operation;
use App\Models\User;

use App\Services\ActivityLogService;


class MoneyChanger extends Model
{

    use SoftDeletes;

    protected $morphClass = 'money_changers';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'pic', 'email', 'phone', 'address_id', 'currency_available', 'emergency_pic', 'emergency_phone', 'longitude','latitude','google_map','waze_map', 'is_active'
    ];


    // public function address()
    // {
    //     return $this->belongsTo(Address::class, 'address_id');
    // }
    public function address()
    {
        return $this->morphOne(Address::class,'addressable','reference_table','reference_id');
    }

    public function operation()
    {
        return $this->hasMany(Operation::class);
    }



    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function activityLogs()
    {
        return $this->hasMany(ActivityLog::class);
    }
    protected static function booted()
    {
        static::created(function ($model) {

            $currentValues = $model->getAttributes();

            ActivityLogService::createActivityLog([
                'subject' => 'money_changers',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create money changer',
                'description' => "New money changer created",
                'user_id' => $currentValues['id']
            ]);
        });

        
        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => []
            ];

            $numberColumns = [];

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'money_changers',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit money changer',
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                ]
            );
        });
    }

}
