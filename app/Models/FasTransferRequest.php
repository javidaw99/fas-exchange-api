<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\User;
use App\Models\UserBankAccount;
use App\Models\UserAccount;
use App\Models\Currency;
use App\Models\FasTransfer;
use App\Models\FasTransferConvert;

use App\Models\ExchangeRate;
use App\Models\ExchangeMarginRate;

use Illuminate\Database\Eloquent\Model;

class FasTransferRequest extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_bank_account_id', 'user_account_id', 'from_currency_id', 'to_currency_id', 'total_amount', 'total_convert_amount','final_amount', 'status', 'daily_limit', 'exchange_rate_id','exchange_margin_rate_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function user_bank_account()
    {
        return $this->belongsTo(UserBankAccount::class);
    }

    
    public function user_account()
    {
        return $this->belongsTo(UserAccount::class);
    }
    public function to_currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function exchange_rate()
    {
        return $this->belongsTo(ExchangeRate::class);
    }
    public function exchange_margin_rate()
    {
        return $this->belongsTo(ExchangeMarginRate::class);
    }
    public function from_currency()
    {
        return $this->belongsTo(Currency::class);
    }
    public function fas_transfers()
    {
        return $this->hasMany(FasTransfer::class);
    }
    public function fas_transfer_convert()
    {
        return $this->hasMany(FasTransferConvert::class);
    }
    // public function from_currency()
    // {
    //     return $this->belongsTo(Currency::class);
    // }
}
