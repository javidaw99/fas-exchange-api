<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\User;
use App\Models\State;

class Country extends Model
{

    use SoftDeletes;

    protected $morphClass = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id', 'name', 'iso_code_2', 'iso_code_3', 'is_enabled'
    ];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function currency()
    {
        return $this->belongsToMany(Currency::class, 'country_currency');
    }

    public function banks()
    {
        return $this->belongsToMany(Bank::class, 'bank_country', 'country_id', 'bank_id')
            ->as('bank_country')
            ->withPivot('id');
    }
    public function state()
    {
        return $this->hasMany(State::class);
    }

    public function bankCountries()
    {
        return $this->hasMany(BankCountry::class);
    }
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }

    public function filter()
    {
        return $this->morphOne(TransactionFilter::class, 'filterable', 'reference_table', 'reference_id');

    }
}
