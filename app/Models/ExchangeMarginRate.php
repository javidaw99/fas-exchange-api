<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\ActivityLogService;

class ExchangeMarginRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate'
    ];

    public const UPDATED_AT = null;

    protected static function booted()
    {
        static::created(function ($model) {

            ActivityLogService::createActivityLog([
                'subject' => 'exchange_margin_rates',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create exchange margin rate',
                'description' => "New exchange margin rate created",
            ]);
        });

        // static::updated(function ($model) {

        //     $columns = [
        //         'omit' => [],
        //         'include' => []
        //     ];

        //     $numberColumns = [];

        //     ActivityLogService::createActivityLog(
        //         [
        //             'subject' => 'exchange_margin_rates',
        //             'model' => $model,
        //             'action_type' => 'Edit',   //Create or Edit
        //             'action_source' => 'edit exchange margin rate',
        //             'number_columns' => $numberColumns,
        //             'columns' => $columns,
        //         ]
        //     );
        // });
    }
}
