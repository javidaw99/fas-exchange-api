<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class TransactionPoint extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'reference_table', 'reference_id', 'to_user_id' ,'old_balance', 'new_balance', 'amount','is_requested'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function toUser()
    {
        return $this->belongsTo(User::class);
    }

     public function transactionPoint()
    {
        return $this->morphTo(__FUNCTION__, 'reference_table', 'reference_id');
    }
}
