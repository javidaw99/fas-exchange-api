<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDevice extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand', 
        'bundle_id', 
        'carrier', 
        'device_name', 
        'device_type', 
        'device_id',
        'ip_address',
        'mac_address',
        'manufacturer',
        'model',
        'storage',
        'sys_build_id',
        'sys_build_number',
        'sys_name',
        'sys_version',
        'unique_id',
        'user_agent',
        'version',
    ];
}
