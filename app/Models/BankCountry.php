<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Models\Bank;
use App\Models\Country;
use App\Models\Transfer;
use App\Models\BankAccount;

class BankCountry extends Pivot
{
    protected $table = 'bank_country';
    public $incrementing = true;

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class, 'bank_country_id');
    }
}
