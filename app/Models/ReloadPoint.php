<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\ActivityLogService;
use App\Models\TransactionPoint;
use App\Models\User;
use App\Models\CasePoint;
use App\Models\CommissionPoint;

use App\Models\Bank;

use Brick\Money\Money;

class ReloadPoint extends Model
{
    use SoftDeletes;

    protected $morphClass = 'reload_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type','bank_account_id','bank_id', 'user_id', 'amount', 'status'
    ];


    public function transaction()
    {
        // return $this->morphOne(TransactionPoint::class, 'transactionPoint', 'reference_table', 'reference_id');
        return $this->morphOne('reload_points', 'transactionPoint');
    }

    public function commission()
    {
        return $this->morphOne('reload_points', 'commissionPoint');
    }
    
    public function media()
    {
        return $this->morphMany(Media::class, 'mediable', 'reference_table', 'reference_id');
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }
    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



    public function caseTransaction()
    {
        return $this->morphOne(CasePoint::class, 'casePoint', 'reference_table', 'reference_id');
    }


}
