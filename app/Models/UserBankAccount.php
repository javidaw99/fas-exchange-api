<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Bank;
use App\Models\Country;
use App\Models\Currency;

class UserBankAccount extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_bank_accounts';

    protected $fillable = [
        'user_id', 'reference_name', 'reference_bank_account', 'currency_id', 'country_id', 'bank_id', 'type', 'topkash_user_id','maximum_amount'
    ];

    public function bankUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function bankInfo()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function topkashUser()
    {
        return $this->belongsTo(User::class, 'topkash_user_id');
    }
}
