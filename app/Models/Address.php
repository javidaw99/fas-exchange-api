<?php

namespace App\Models;

use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\ActivityLogService;

class Address extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address_1', 'address_2', 'city', 'postcode', 'state_id', 'country_id', 'reference_id', 'reference_table'
    ];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    
    public function addressable()
    {
        return $this->morphTo(__FUNCTION__, 'reference_table', 'reference_id');
    }

    protected static function booted()
    {
        static::created(function ($model) {

            $currentValues = $model->getAttributes();

            ActivityLogService::createActivityLog([
                'subject' => 'addresses',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create address',
                'description' => "New address created",
                'user_id' => $currentValues['id']
            ]);
        });

        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => []
            ];

            $numberColumns = [];

            $currentValues = $model->getAttributes();

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'addresses',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit address',
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                    'user_id' => $currentValues['id']
                ]
            );
        });
    }
}
