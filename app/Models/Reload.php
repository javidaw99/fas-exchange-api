<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Cases;
use App\Models\Media;
use App\Models\Currency;
use App\Models\BankAccount;
use App\Services\ActivityLogService;
use App\Models\User;
use Brick\Money\Money;

class Reload extends Model
{
    use SoftDeletes;

    protected $morphClass = 'reloads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_account_id', 'currency_id', 'user_account_id', 'user_id', 'amount', 'reference_no'
    ];

    protected $appends = ['formatted_amount'];

    public function getFormattedAmountAttribute()
    {
        $currency = $this->currency()->first();

        $amount = $this->attributes['amount'];
        $isoCode = $currency->iso_code;
        $locale = $currency->locale;

        $money = Money::of($amount, $isoCode);

        return $money->formatTo($locale);
    }

    public function transaction()
    {
        return $this->morphOne('reloads', 'transact');
    }

    public function case()
    {
        return $this->morphOne(Cases::class, 'caseable', 'reference_table', 'reference_id');
    }

    public function media()
    {
        return $this->morphMany(Media::class, 'mediable', 'reference_table', 'reference_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    protected static function booted()
    {
        static::created(function ($model) {

            ActivityLogService::createActivityLog([
                'subject' => 'reloads',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create reload',
                'description' => "New reload created"
            ]);
        });


        // Notes: Currently no need updated event because there is no way to update reload record in BO

        // static::updated(function ($model) {

        //     $columns = [
        //         'omit' => [],
        //         'include' => []
        //     ];

        //     $numberColumns = [];

        //     ActivityLogService::createActivityLog(
        //         [
        //             'subject' => 'reloads',
        //             'model' => $model,
        //             'action_type' => 'Edit', //Create or Edit
        //             'action_source' => 'edit reload',
        //             'number_columns' => $numberColumns,
        //             'columns' => $columns
        //         ]
        //     );
        // });
    }
}
