<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SmsLog extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_no', 'otp_code', 'purpose', 'expired_at'
    ];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
