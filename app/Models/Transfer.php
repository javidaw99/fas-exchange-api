<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Currency;
use App\Models\ExchangeRate;
use App\Models\ExchangeMarginRate;
use App\Services\ActivityLogService;
use Brick\Money\Money;

class Transfer extends Model
{
    use SoftDeletes;

    protected $morphClass = 'transfers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 
        'quote_mode', 
        'quote_id', 
        'from_currency_id', 
        'to_currency_id', 
        'user_bank_account_id', 
        'exchange_rate_id', 
        'base_fixed_rate_id', 
        'tiered_percentage_rate_id', 
        'amount', 
        'processing_fee', 
        'converted_amount', 
        'user_id', 
        'to_user_id', 
        'user_account_id', 
        'to_user_account_id', 
        'exchange_rate_margin', 
        'exchange_margin_rate_id',
        'tk_bank_account_id'
    ];

    // protected $appends = ['formatted_amount'];

    // public function getFormattedAmountAttribute()
    // {
    //     $currency = $this->currency()->first();

    //     $amount = $this->attributes['amount'];
    //     $isoCode = $currency->iso_code;
    //     $locale = $currency->locale;

    //     $money = Money::of($amount, $isoCode);

    //     return $money->formatTo($locale);
    // }

    public function transaction()
    {
        return $this->morphOne('transfers', 'transact');
    }

    public function from_currency()
    {
        return $this->belongsTo(Currency::class, 'from_currency_id');
    }

    public function to_currency()
    {
        return $this->belongsTo(Currency::class, 'to_currency_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function to_user()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }

    public function exchange_rate()
    {
        return $this->belongsTo(ExchangeRate::class, 'exchange_rate_id');
    }

    public function exchange_margin_rate()
    {
        return $this->belongsTo(ExchangeMarginRate::class, 'exchange_margin_rate_id');
    }

    public function user_bank_account()
    {
        return $this->belongsTo(UserBankAccount::class, 'user_bank_account_id');
    }

    public function source_bank_account()
    {
        return $this->belongsTo(BankAccount::class, 'tk_bank_account_id');
    }

    // public function recipientBankCountry()
    // {
    //     return $this->belongsTo(BankCountry::class, 'recipient_bank_country_id');
    // }

    public function case()
    {
        return $this->morphOne(Cases::class, 'caseable', 'reference_table', 'reference_id');
    }


    protected static function booted()
    {
        static::created(function ($model) {

            $values = $model->getAttributes();

            ActivityLogService::createActivityLog([
                'subject' => 'transfers',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create transfer',
                'description' => "New ". strtolower($values['type']) ." created"
            ]);
        });

        // Notes: Currently no need updated event because there is no way to update transfer record in BO


        // static::updated(function ($model) {

        //     $columns = [
        //         'omit' => [],
        //         'include' => []
        //     ];

        //     $numberColumns = [];

        //     ActivityLogService::createActivityLog(
        //         [
        //             'subject' => 'transfers',
        //             'model' => $model,
        //             'action_type' => 'Edit', //Create or Edit
        //             'action_source' => 'edit transfer',
        //             'number_columns' => $numberColumns,
        //             'columns' => $columns
        //         ]
        //     );
        // });
    }
}
