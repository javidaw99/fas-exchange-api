<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LocaleMapper extends Model
{
    protected $table = 'locale_mapper';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_key',
        'content_key',
        'dynamic_values',
        'reference_id',
        'reference_table'
    ];
}
