<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProcessingFeeTieredPercentageRate extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_currency_id', 'to_currency_id', 'percentage_rate', 'from_amount', 'to_amount', 'date'
    ];

    public function from_currency()
    {
        return $this->belongsTo(Currency::class, 'from_currency_id');
    }

    public function to_currency()
    {
        return $this->belongsTo(Currency::class, 'to_currency_id');
    }
}
