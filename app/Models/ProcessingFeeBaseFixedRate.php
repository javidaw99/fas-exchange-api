<?php

namespace App\Models;

use App\Services\ActivityLogService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProcessingFeeBaseFixedRate extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_currency_id', 'to_currency_id', 'amount', 'date'
    ];

    public function from_currency()
    {
        return $this->belongsTo(Currency::class, 'from_currency_id');
    }

    public function to_currency()
    {
        return $this->belongsTo(Currency::class, 'to_currency_id');
    }

    protected static function booted()
    {
        static::created(function ($model) {

            ActivityLogService::createActivityLog([
                'subject' => 'processing_fee_base_fixed_rates',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create processing fee base fixed rate',
                'description' => "New processing fee base fixed rate created",
            ]);
        });

        // static::updated(function ($model) {

        //     $columns = [
        //         'omit' => [],
        //         'include' => []
        //     ];

        //     $numberColumns = [];

        //     ActivityLogService::createActivityLog(
        //         [
        //             'subject' => 'processing_fee_base_fixed_rates',
        //             'model' => $model,
        //             'action_type' => 'Edit',   //Create or Edit
        //             'action_source' => 'edit processing fee base fixed rate',
        //             'number_columns' => $numberColumns,
        //             'columns' => $columns,
        //         ]
        //     );
        // });
    }
}
