<?php
 
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
 
class Notification extends Model
{

    use SoftDeletes;

    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'is_read', 'user_id', 'ref_id', 'ref_type'
    ];

    /**
     * The users that belong to the role.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}