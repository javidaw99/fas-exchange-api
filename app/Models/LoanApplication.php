<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\LoanApplicant;
use App\Models\LoanPackage;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\User;
use App\Models\Staff;
use App\Models\Currency;

class LoanApplication extends Model
{
    use SoftDeletes;

    protected $table = 'loan_applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'applicant_id', 'package_id', 'to_bank_id', 'to_bank_account_no', 'from_bank_account_no', 'status', 'assigned_user_id', 'assigned_staff_id', 'loan_daily_penalty_percentage', 'loan_first_time_penalty', 'loan_received_credit', 'loan_start_date', 'loan_due_date', 'loan_status', 'currency_id', 'exp'
    ];


    // public function getStatusAttribute($value)
    // {
    //     switch ($value) {
    //         case 1:
    //             return 'pending';
    //         case 2:
    //             return 'processing';
    //         case 3:
    //             return 'approved';
    //         case 4:
    //             return 'cancelled';
    //         case 5:
    //             return 'rejected';
    //         case 6:
    //             return 'expired';
    //         case 7:
    //             return 'active';
    //         case 8:
    //             return 'inactive';
    //     }
    // }

    public function applicant()
    {
        return $this->belongsTo(LoanApplicant::class);
    }

    public function package()
    {
        return $this->belongsTo(LoanPackage::class);
    }

    public function toBank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function fromBankAccount()
    {
        return $this->belongsTo(BankAccount::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
