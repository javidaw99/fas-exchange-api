<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\TransactionPoint;
use App\Models\User;

use Brick\Money\Money;


class CommissionPoint extends Model
{
    use SoftDeletes;

    protected $morphClass = 'commission_points';

    protected $fillable = [
    
        'amount', 
        'user_id', 
        'from_user_id',
        'commission_date', 
        'reference_table', 
        'reference_id', 
      
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function from_user()
    {
        return $this->belongsTo(User::class);
    }


    public function transaction()
    {
   
        // return $this->morphOne(TransactionPoint::class, 'transactionPoint', 'reference_table', 'reference_id');
        return $this->morphOne('commission_points', 'transactionPoint');

    }
    public function commissionPoint()
    {
   
        return $this->morphTo(__FUNCTION__, 'reference_table', 'reference_id');


    }

}
