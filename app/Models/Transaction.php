<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Account;
use Brick\Money\Money;

class Transaction extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'case_id', 'user_id', 'reference_user_id', 'reference_table', 'reference_id', 'user_account_id', 'old_balance', 'new_balance', 'amount'
    ];

    // protected $appends = ['formatted_old_balance', 'formatted_new_balance'];

    public const PREFIX = [
        1 => 'TB',
        2 => 'SB',
        3 => 'CV',
        4 => 'SM',
        5 => 'TG',
    ];

    public const PREFIX_REVERT = [
        'TB' => 1,
        'SB' => 2,
        'CV' => 3,
        'SM' => 4,
        'TG' => 5,
    ];

    private function formatAmount($amount)
    {
        $currency = $this->userAccount->currency;
        $isoCode = $currency->iso_code;
        $locale = $currency->locale;

        $money = Money::of($amount, $isoCode);

        return $money->formatTo($locale);
    }

    public function getFormattedOldBalanceAttribute()
    {
        $amount = $this->attributes['old_balance'];

        return $this->formatAmount($amount);
    }

    public function getFormattedNewBalanceAttribute()
    {
        $amount = $this->attributes['new_balance'];

        return $this->formatAmount($amount);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function userAccount()
    {
        return $this->belongsTo(UserAccount::class);
    }

    public function case()
    {
        return $this->belongsTo(Cases::class);
    }
}
