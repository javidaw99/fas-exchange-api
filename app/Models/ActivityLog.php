<?php

namespace App\Models;

use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityLog extends Model
{

    public const REVERT = [
        "reloads" => "Top-Ups (Bank)",
        "redeems" => 'Top-Ups (Gift Card)',
        "transfers_SB" => "Sends (Bank)",
        "transfers_CV" => "Conversions",
        "transfers_SM" => "Sends (Member)",
        "users" => 'Users',
        "bank_accounts" => "Bank Accounts",
        "banks" => "Banks",
        "currencies" => "Currencies",
        "exchange_margin_rates" => "Margin",
        "processing_fee_base_fixed_rates" => "Fees",
        "app_user" => 'Members',
        "money_changers" => 'Money Changers',

    ];

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference_table', 'reference_id', 'action_type', 'action_source', 'description', 'created_by'
    ];

    // protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function activityLog()
    {
        return $this->morphTo(__FUNCTION__, 'reference_table', 'reference_id');
    }
}
