<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Referral;

class ReferralGroup extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ancestor_user_id', 'name', 'code', 'exchange_commission_rate', 'withdrawal_commission_rate'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'ancestor_user_id');
    }

    public function referrals()
    {
        return $this->hasMany(Referral::class);
    }
}
