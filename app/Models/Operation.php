<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\ActivityLogService;


class Operation extends Model
{
    use SoftDeletes;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hours', 'days', 'is_closed','money_changer_id'
    ];

    protected static function booted()
    {
        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => []
            ];

            $numberColumns = [];

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'money_changers',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit operations',
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                ]
            );
        });
    }
}
