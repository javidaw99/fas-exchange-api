<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Currency;
use App\Models\User;

class ExchangeRate extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_currency_id', 'to_currency_id', 'created_by', 'rate', 'effective_date'
    ];

    public function to_currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function from_currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
