<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Currency;


class LoanPackage extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'level', 'currency_id', 'loan_amount', 'loan_day', 'received_credit', 'first_time_penalty', 'daily_penalty_percentage', 'exp', 'status'
    ];


    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
