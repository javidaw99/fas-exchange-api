<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\TransactionPoint;
use App\Models\CommissionPoint;

use Brick\Money\Money;

class TransferPoint extends Model
{

    use SoftDeletes;

    protected $morphClass = 'transfer_points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = [
        'type', 
        'point_margin_rate_id', 
        'amount', 
        'converted_amount', 
        'point_rate_margin', 
        'to_user_id', 
        'user_id', 
        'from_transfer_points_id', 
        'user_bank_account_id', 
        'user_account_id', 
        'commission_rate',
        'convert_rate'
    ];

    public function transaction()
    {
   
        // return $this->morphOne(CommissionPoint::class, 'transactionPoint', 'reference_table', 'reference_id');
        return $this->morphOne('transfer_points', 'transactionPoint');

    }

    public function commission()
    {
        return $this->morphOne(CommissionPoint::class, 'commissionPoint', 'reference_table', 'reference_id');

        // return $this->morphOne('transfer_points', 'commissionPoint');

    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    
    public function user_bank_account()
    {
        return $this->belongsTo(UserBankAccount::class, 'user_bank_account_id');
    }

 
}
