<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Referral;

class ReferralTree extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ancestor_user_id', 'descendant_user_id', 'depth'
    ];

    public function parentUser()
    {
        return $this->belongsTo(User::class, 'ancestor_user_id');
    }

    public function childUser()
    {
        return $this->belongsTo(User::class, 'descendant_user_id');
    }

    public function referral()
    {
        return $this->hasOne(Referral::class);
    }
}
