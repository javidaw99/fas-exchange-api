<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Referral;
use App\Models\Cases;
use App\Models\Currency;

class Commission extends Model
{
    use SoftDeletes;
    protected $morphClass = 'commission';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'referral_id', 'case_id', 'amount', 'currency_id'
    ];

    public function transaction()
    {
        return $this->morphOne('commission', 'transact');
    }

    public function referral()
    {
        return $this->belongsTo(Referral::class);
    }

    public function case()
    {
        return $this->belongsTo(Cases::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
