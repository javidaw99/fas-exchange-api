<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Currency;
use App\Models\ExchangeRate;
use App\Models\Account;
use Brick\Money\Money;

class Exchange extends Model
{
    use SoftDeletes; 

    protected $morphClass = 'exchange';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_currency_id', 'to_currency_id', 'exchange_rate_id', 'processing_fee', 'from_account_id', 'to_account_id', 'from_amount', 'to_amount'
    ];

    protected $appends = ['formatted_from_amount', 'formatted_to_amount'];

    private function formatAmount($amount, $currency)
    {

        $isoCode = $currency->iso_code;
        $locale = $currency->locale;

        $money = Money::of($amount, $isoCode);
        
        return $money->formatTo($locale);
    }

    public function getFormattedFromAmountAttribute()
    {
        $currency = $this->fromCurrency()->first();

        $amount = $this->attributes['from_amount'];

        return $this->formatAmount($amount, $currency);
    }

    public function getFormattedToAmountAttribute()
    {
        $currency = $this->toCurrency()->first();

        $amount = $this->attributes['to_amount'];

        return $this->formatAmount($amount, $currency);
    }


    public function transaction()
    {
        return $this->morphOne('exchange', 'transact');
    }

    public function fromCurrency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function toCurrency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function exchangeRate()
    {
        return $this->belongsTo(ExchangeRate::class, 'exchange_rate_id');
    }

    public function fromAccount()
    {
        return $this->belongsTo(Account::class);
    }

    public function toAccount()
    {
        return $this->belongsTo(Account::class);
    }
}
