<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaritalStatus extends Model
{

    use SoftDeletes;
    /**
     *  The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    protected $hidden = array('created_at','updated_at','deleted_at');

}