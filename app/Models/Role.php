<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Role extends Model
{
    public const TYPES = [
        "customer_service" => 1,
        "app_user" => 2,
        "superadmin" => 3,
        "payment_officer" => 4

    ];

    public const REVERT = [
        "1" => "customer_service",
        "2" => "app_user",
        "3" => 'superadmin',
        "4" => "payment_officer"
    ];

    public const REVERT_NAME = [
        "1" => "Customer Service Users",
        "2" => "Members",
        "3" => 'Super Admins',
        "4" => "Payment Officer"

    ];

    public const PERMISSIONS = [
        "superadmin" => [
            [
                "subject" => "DashboardUser",
                "action" => "create"
            ],
            [
                "subject" => "Case",
                "action" => "assign"
            ],
            [
                "subject" => "AdvancedSettings",
                "action" => "manage"
            ],
            [
                "subject" => "Transfer",
                "action" => "manage"
            ],
            [
                "subject" => "Reload",
                "action" => "manage"
            ],
            [
                "subject" => "Redeem",
                "action" => "manage"
            ],
            [
                "subject" => "User",
                "action" => "manage"
            ],
            [
                "subject" => "Transaction",
                "action" => "manage"
            ],
            [
                "subject" => "Report",
                "action" => "manage"
            ],
            [
                "subject" => "ActivityLog",
                "action" => "manage"
            ],
            [
                "subject" => "Setting",
                "action" => "manage"
            ],
            [
                "subject" => "Member",
                "action" => "manage"
            ],
            [
                "subject" => "MemberStatus",
                "action" => "update"
            ],
            [
                "subject" => "Dashboard",
                "action" => "manage"
            ],
            


        ],
        "customer_service" => [
            [
                "subject" => "Transfer",
                "action" => "read"
            ],
            [
                "subject" => "Reload",
                "action" => "read"
            ],
            [
                "subject" => "Exchange",
                "action" => "read"
            ],
            [
                "subject" => "User",
                "action" => "read"
            ],
            [
                "subject" => "Transaction",
                "action" => "read"
            ],
            [
                "subject" => "Report",
                "action" => "read"
            ],
            [
                "subject" => "ActivityLog",
                "action" => "read"
            ],
            // [
            //     "subject" => "Setting",
            //     "action" => "read"
            // ],
            [
                "subject" => "Customer",
                "action" => "read"
            ],
            [
                "subject" => "AdvancedSettings",
                "action" => "read"
            ],
            [
                "subject" => "Redeem",
                "action" => "read"
            ],
            [
                "subject" => "Member",
                "action" => "read"
            ],
            [
                "subject" => "MemberStatus",
                "action" => "update"
            ],


        ],
        "payment_officer" => [
          
            [
                "subject" => "Case",
                "action" => "assign"
            ],
           
            [
                "subject" => "Transfer",
                "action" => "manage"
            ],
            [
                "subject" => "Reload",
                "action" => "manage"
            ],
            [
                "subject" => "Redeem",
                "action" => "manage"
            ],
     
            [
                "subject" => "Transaction",
                "action" => "manage"
            ],
            [
                "subject" => "Report",
                "action" => "manage"
            ],
            [
                "subject" => "ActivityLog",
                "action" => "manage"
            ],
            // [
            //     "subject" => "Member",
            //     "action" => "read"
            // ],
      
    
        ]

    ];

    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
