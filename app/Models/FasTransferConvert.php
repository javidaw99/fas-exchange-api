<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FasTransferRequest;
use App\Models\BankAccount;
use App\Models\UserBankAccount;

class FasTransferConvert extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','fas_transfer_request_id', 'user_bank_account_id', 'amount','currency_id', 'is_active', 'paid_at', 'status', 'day'
    ];

    public function transfers_request()
    {
        return $this->belongsTo(FasTransferRequest::class);
    }

    public function user_bank_account()
    {
        return $this->belongsTo(UserBankAccount::class);
    }
   

}