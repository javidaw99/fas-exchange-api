<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Country;
use App\Models\BankCountry;
use App\Services\ActivityLogService;

class Bank extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    public function countries()
    {
        return $this->belongsToMany(Country::class, 'bank_country', 'bank_id', 'country_id')->using(BankCountry::class)->withPivot('id', 'branch_name');
    }

    public function bankCountries()
    {
        return $this->belongsToMany(Country::class, 'bank_country', 'bank_id', 'country_id')
            ->as('bank_country')
            ->withPivot('id');
    }


    protected static function booted()
    {
        static::created(function ($model) {

            ActivityLogService::createActivityLog([
                'subject' => 'banks',
                'model' => $model,
                'action_type' => 'Create',   //Create or Edit
                'action_source' => 'create bank',
                'description' => "New bank created",
            ]);
        });

        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => []
            ];

            $numberColumns = [];

            ActivityLogService::createActivityLog(
                [
                    'subject' => 'banks',
                    'model' => $model,
                    'action_type' => 'Edit',   //Create or Edit
                    'action_source' => 'edit bank',
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                ]
            );
        });
    }
}
