<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Onboarding;
use App\Models\Media;


class OnboardingDetails extends Model
{
    use SoftDeletes;

  
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'sequence', 'onboarding_id'
    ];

    public function onboardingId()
    {
        return $this->belongsTo(Onboarding::class, 'onboarding_id');
    }

    public function media()
    {
        return $this->morphOne(Media::class, 'mediable','reference_table','reference_id');
    }
}
