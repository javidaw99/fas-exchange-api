<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserExchangeQuote extends Model
{
    use SoftDeletes;

    protected $table = 'user_exchange_quotes';



    public const MODES = [
        "XPF_XER" => "XPF_XER",  // transfer within Topkash environment with same currency
        "XPF_ER" => "XPF_ER", // transfer within Topkash environment with different currency
        "PF_XER" => "PF_XER", // transfer out to favourite bank account with same currency
        "PF_ER" => "PF_ER", // transfer out to favourite bank account with different currency
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_currency_id', 'to_currency_id', 'base_fixed_rate_id', 'tiered_percentage_rate_id', 'exchange_rate_id', 'exchange_margin_rate_id', 'user_id', 'expired_at', 'quote_uuid', 'mode'
    ];

    public function from_currency()
    {
        return $this->belongsTo(Currency::class, 'from_currency_id');
    }

    public function to_currency()
    {
        return $this->belongsTo(Currency::class, 'to_currency_id');
    }

    public function base_fixed_rate_id()
    {
        return $this->belongsTo(ProcessingFee::class, 'base_fixed_rate_id');
    }

    public function tiered_percentage_rate_id()
    {
        return $this->belongsTo(ProcessingFeeTieredPercentageRate::class, 'tiered_percentage_rate_id');
    }

    public function exchange_rate_id()
    {
        return $this->belongsTo(ExchangeRate::class, 'exchange_rate_id');
    }

    public function user_id()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
