<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FasTransferRequest;
use App\Models\BankAccount;
use App\Models\ExchangeRate;

use Illuminate\Database\Eloquent\Model;

class FasTransfer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fas_transfer_request_id', 'bank_account_id', 'amount', 'is_active', 'paid_at', 'status', 'day', 'exchange_rate_id'
    ];

    public function transfers_request()
    {
        return $this->belongsTo(FasTransferRequest::class);
    }

    public function bank_account()
    {
        return $this->belongsTo(BankAccount::class);
    }
    public function media()
    {
        return $this->morphMany(Media::class, 'mediable', 'reference_table', 'reference_id');
    }
  

}
