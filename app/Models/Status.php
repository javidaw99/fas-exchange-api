<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Status extends Model
{
    public const TYPES = [
        1 => 'pending',
        2 => 'processing',
        3 => 'approved',
        4 => 'cancelled',
        5 => 'rejected',
        6 => 'expired'
    ];

    public const TYPES_REVERT = [
        'pending' => 1,
        'processing' => 2,
        'approved' => 3,
        'cancelled' => 4,
        'rejected' => 5,
        'expired' => 6
    ];

    public const TYPES_FOR_USER = [
        1 => 'active',
        2 => 'blocked',
    ];
}
