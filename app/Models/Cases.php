<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Account;
use App\Models\Transaction;
use App\Services\ActivityLogService;

class Cases extends Model
{
    use SoftDeletes;

    protected $table = 'cases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'assigned_user_id', 'reference_id', 'reference_table', 'user_account_id', 'status', 'reject_reason'
    ];


    // public function getStatusAttribute($value)
    // {
    //     switch ($value) {
    //         case 1:
    //             return 'pending';
    //         case 2:
    //             return 'processing';
    //         case 3:
    //             return 'approved';
    //         case 4:
    //             return 'cancelled';
    //         case 5:
    //             return 'rejected';
    //         case 6:
    //             return 'expired';
    //     }
    // }


    public function transact()
    {
        return $this->morphTo(__FUNCTION__, 'reference_type', 'ref_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function toAccount()
    {
        return $this->belongsTo(Account::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function assignedUser()
    {
        return $this->belongsTo(User::class, 'assigned_user_id');
    }

    public function caseable()
    {
        return $this->morphTo(__FUNCTION__, 'reference_table', 'reference_id');
    }

    protected static function booted()
    {
        // static::created(function ($model) {

        //     $currentValues = $model->getAttributes();

        //     ActivityLogService::createActivityLog([
        //         'subject' => $currentValues['reference_table'],
        //         'model' => $model,
        //         'action_type' => 'Create',   //Create or Edit
        //         'action_source' => 'create' . $currentValues['reference_table'],
        //         'description' => 'new' . $currentValues['reference_table'] . ' created'
        //     ]);
        // });

        static::updated(function ($model) {

            $columns = [
                'omit' => [],
                'include' => ['status', 'reject_reason']
            ];

            $numberColumns = [];

            $currentValues = $model->getAttributes();

            ActivityLogService::createActivityLog(
                [
                    'subject' => $currentValues['reference_table'],
                    'model' => $model,
                    'action_type' => 'Edit', //Create or Edit
                    'action_source' => 'edit' . " " . $currentValues['reference_table'],
                    'number_columns' => $numberColumns,
                    'columns' => $columns,
                    'reference_id' => $currentValues['reference_id']
                ]
            );
        });
    }
}
