<?php
 
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;



class Media extends Model
{

    use SoftDeletes;

    protected $table = 'medias';

    public const TYPES = [
        "primary_repo" => 1,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'path', 'extension', 'mime', 'reference_id', 'reference_table', 'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function mediable()
    {
        return $this->morphTo(__FUNCTION__,'reference_table','reference_id');
    }

}
