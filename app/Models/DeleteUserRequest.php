<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DeleteUserRequest extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_valid', 'username', 'topkash_id', 'phone_no', 'status', 'assigned_user_id', 'reason', 'email'
    ];

    // protected static function booted()
    // {

    //     static::updated(function ($model) {

    //         $columns = [
    //             'omit' => [],
    //             'include' => ['status', 'reason']
    //         ];

    //         $numberColumns = [];

    //         $currentValues = $model->getAttributes();

    //         ActivityLogService::createActivityLog(
    //             [
    //                 'subject' => 'delete_user_requests',
    //                 'model' => $model,
    //                 'action_type' => 'Edit', //Create or Edit
    //                 'action_source' => 'edit delete_user_requests',
    //                 'number_columns' => $numberColumns,
    //                 'columns' => $columns,
    //                 'reference_id' => $currentValues['reference_id']
    //             ]
    //         );
    //     });
    // }

}
