<?php

namespace App\Constants;

use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;


class ExcelConstant
{
    const COLUMN_CHARACTERS = [
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
        10 => 'K',
        11 => 'L',
        12 => 'M',
        13 => 'N',
        14 => 'O',
        15 => 'P',
        16 => 'Q',
        17 => 'R',
        18 => 'S',
        19 => 'T',
        20 => 'U',
        21 => 'V',
        22 => 'W',
        23 => 'X',
        24 => 'Y',
        25 => 'Z',
    ];


    // https://phpspreadsheet.readthedocs.io/en/latest/topics/recipes/#styles
    const TITLE_STYLE = [
        'font' => [
            'bold' => true,
            'size' => 20
        ],
    ];

    const TABLE_HEADER_STYLE = [
        'font' => [
            'bold' => true,
        ],
        'borders' => [
            'top' => [
                'borderStyle' => Border::BORDER_THIN,
            ],
            'bottom' => [
                'borderStyle' => Border::BORDER_THIN,
            ],
        ],
    ];

    const HORIZONTAL_ALIGNMENT = [
        'right' => Alignment::HORIZONTAL_RIGHT,
        'left' => Alignment::HORIZONTAL_LEFT
    ];
}
