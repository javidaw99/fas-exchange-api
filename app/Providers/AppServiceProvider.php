<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        Relation::morphMap([
            'reloads' => 'App\Models\Reload',
            'transfers' => 'App\Models\Transfer',
            'redeems' => 'App\Models\Redeem',
            'money_changers'=> 'App\Models\MoneyChanger',
            'users'=> 'App\Models\User',
            'fas_transfers'=> 'App\Models\FasTransfer',
            'reload_points'=> 'App\Models\ReloadPoint',
            'transfer_points'=> 'App\Models\TransferPoint',
            'commission_points'=> 'App\Models\CommissionPoint',
            'case_points'=> 'App\Models\CasePoint'

        ]);
    }
}
