<?php

namespace App\Console\Commands;

use App\Services\CaseService;
use App\Services\NotificationService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PendingNotificationCronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-pending-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pending notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {

         $pendingCases = CaseService::findAllUserCasesWithPendingStatus();
         $pendingReload = $pendingCases['reload_count'];
         $pendingTransfer = $pendingCases['transfer_count'];

         if($pendingReload > 0 || $pendingTransfer > 0) {

            $notification = NotificationService::sendPushNotificationInBulk([
                'title' => 'Pending Requests',
                'body' => 'Top-Ups: ' . $pendingReload . "\nSends: " . $pendingTransfer,
            ], null, null, true);

         }

         Log::debug('Sending web push notification');

        } catch (\Throwable $th) {
            Log::error('Send pending notification failed. '. $th->getMessage());
        }
    }
}
