<?php

namespace App\Http\Middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Models\Role;
use Closure;

class AppRolesChecking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $role = $request->get('role_id');

        if (
            $role == Role::TYPES['customer_service'] ||
            $role == Role::TYPES['superadmin']
        ) {
            return response([
                'message' => 'Unauthorized user to access'
            ], 401);
        }

        return $next($request);
    }
}
