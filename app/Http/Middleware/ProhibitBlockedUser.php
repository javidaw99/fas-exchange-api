<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Models\User;
use Closure;

class ProhibitBlockedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $user_id = $request->get('user_id');

        $user = User::find($user_id);
        $status = $request->get('status');

        if (empty($user) || $user->status == 'blocked') {
            return response([
                'message' => 'Unauthorized user to access'
            ], 401);
        }

        return $next($request);
    }
}
