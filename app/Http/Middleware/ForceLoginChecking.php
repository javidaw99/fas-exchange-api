<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Models\User;
use Closure;

class ForceLoginChecking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $user_id = $request->get('user_id');

        $user = User::find($user_id);
        
        if ($user->role_id != 2 && $user->is_force_login) {
            return response([
                'message' => 'Unauthorized, need to login again'
            ], 401);
        }

        return $next($request);
    }
}
