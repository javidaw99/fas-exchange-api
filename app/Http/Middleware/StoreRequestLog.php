<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class StoreRequestLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $url = $request->fullUrl();
        $ip = $request->ip();
        $all_request = $request->all();

        // Password is supposed to delete or mask before we save it
        if (array_key_exists('password', $all_request)) {
            unset($all_request['password']);
        }

        $user_id = isset($request) ? $request->get('user_id') : null;

        DB::insert('insert into request_logs (user_id, status_code, url, ip, request, response) values (?, ?, ?, ?, ?, ?)', [
            $user_id, $response->status(), $url, $ip, json_encode($all_request), json_encode($response->content())
        ]);
    }
}
