<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;

class RolesChecking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $role = $request->get('role_id');

        if ($role == Role::TYPES['app_user']) {
            return response([
                'message' => 'Unauthorized user to access'
            ], 401);
        }

        return $next($request);
    }
}
