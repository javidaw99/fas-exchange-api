<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $agent = new Agent();

      
        $authorization = isset(getallheaders()["Authorization"]) ? getallheaders()["Authorization"] :null;

        if(!$authorization) {
            $authorization = isset(getallheaders()["authorization"]) ? getallheaders()["authorization"] :null;
        }

        preg_match('/^Bearer/', $authorization, $matches);
   
        if (count($matches)) {
            $jwt = $request->bearerToken();
        } else {
            $jwt = $request->cookie('jwt');
        }


        if (!isset($jwt)) {
            return response(
                [
                    'message' => 'Unauthorized',
                ],
                401
            );
        }

        try {
            /**
             * IMPORTANT!!!!!!!!!!!!!!!!
             * Need to replace with environment variable
             */
            $jwtKey = 'PMjDndKTbDnB2rdE1b5v61';

            // Start decode jwt with key
            $decoded = JWT::decode($jwt, new Key($jwtKey, 'HS256'));

            // Assign user id into request object
            $request->request->add([
                'user_id' => $decoded->sub,
                'role_id' => $decoded->role,
            ]);
        } catch (\Throwable $th) {
            return response(
                [
                    'message' => $th->getMessage(),
                ],
                401
            );
        }

        return $next($request);
    }
}
