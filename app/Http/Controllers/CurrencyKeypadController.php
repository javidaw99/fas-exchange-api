<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use App\Services\AccountService;
use App\Services\CurrencyKeypadService;
use App\Services\UtilityService;

class CurrencyKeypadController extends BaseController
{
    public function getAllCurrencyKeypads($id)
    {
        $currencyKeypad = CurrencyKeypadService::findCurrencyKeypadByCurrencyId(
            $id
        );

        return response()->json($currencyKeypad);
    }


    public function createCurrencyKeypad(Request $request)
    {

        $this->validate($request, [
            'currency_id' => ['required', 'integer'],
            'amount' => ['required', 'numeric'],
            'amount_label' => ['required','string']  
        ]);

        $data['currency_id'] = $request->input('currency_id');
        $data['amount'] = $request->input('amount');
        $data['amount_label'] = $request->input('amount_label');

        $currencyKeypad = CurrencyKeypadService::createCurrencyKeypad(
            $data
        );

        return response()->json($currencyKeypad);
    }
}
