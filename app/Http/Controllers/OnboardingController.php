<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Onboarding;
use App\Services\UtilityService;

class OnboardingController extends BaseController
{
    public function index(Request $request)
    {
        // $records = Onboarding::select('id','name','start_date','end_date','is_published','created_by')->get();
        // $records = FamilyMembers::all();
        $records = Onboarding::with([
            'createdBy:id,username,fullname',
            'updatedBy:id,username,fullname',
            'onboardingDetails:id,title,content,sequence,onboarding_id'
        ]);

        $fields = array(
            'name',
            'start_date',
            'end_date',
            'is_published',
        );

        $records = UtilityService::modelQueryBuilder($records, $request, $fields);
        return response()->json($records);
    }
    public function detail($id)
    {
        try {
            $record = Onboarding::where('id', $id)->with([
                'createdBy:id,username,fullname',
                'updatedBy:id,username,fullname',
                'onboardingDetails:id,title,content,sequence,onboarding_id'
            ])->first();

            if(empty($record)) {
                return response()->json(['message' => 'Onboarding not found'], 400);
            }

            return response()->json($record, 200);
        }
        catch(Throwable $e)
        {
            return response()->json(['message' => 'Not able to get this Onboarding'], 400);
        }

        return response()->json($record);
    }

    public function addOnboarding (Request $request) {

        $this->validate($request, [
            'name' => ['required', 'string'],
            'start_date' => ['required', 'date_format:Y-m-d'],
            'end_date' => ['required', 'date_format:Y-m-d'],  
            
        ]);

        $name = $request->input('name');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $is_published = $request->input('is_published');

        try {
            Onboarding::insert([
                'name' => $name,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'is_published' => $is_published,
                'created_by' => auth()->user()->id
                // 'created_by' => "123"
            ]);

            return response()->json(['message' => 'Successfully add this Onboarding'], 200);
        }
        catch(Throwable $e)
        {
            return response()->json(['message' => 'Not able to add this Onboarding'], 400);
        }
   
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string'],
            'start_date' => ['required', 'date_format:Y-m-d'],
            'end_date' => ['required', 'date_format:Y-m-d'],  
        ]);

        $name = $request->input('name');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $is_published = $request->input('is_published');

        try {
            $onboarding = Onboarding::find($id);
            $onboarding->name = $name;
            $onboarding->start_date = $start_date;
            $onboarding->end_date = $end_date;
            $onboarding->is_published = $is_published;
            $onboarding->updated_by = auth()->user()->id;
            $onboarding->save();
        
            return response()->json(['message' => 'Successfully update this Onboarding'], 200);
        }
        catch(Throwable $e)
        {
            return response()->json(['message' => 'Not able to update this Onboarding'], 400);
        }
    }

    public function delete($id)
    {
        try {

            $record = Onboarding::find($id);

            if(empty($record)) {
                return response()->json(['message' => 'Onboarding not found'], 400);
            }

            $record->delete();
        
            return response()->json(['message' => 'Successfully delete this Onboarding'], 200);
        }
        catch(Throwable $e)
        {
            ExchangeRate::withTrashed()->find($id)->restore();

            return response()->json(['message' => 'Not able to delete this Onboarding'], 400);
        }
    }

}
