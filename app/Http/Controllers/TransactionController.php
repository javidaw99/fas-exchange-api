<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Redeem;
use App\Services\TransactionService;
use App\Services\UtilityService;
use App\Services\CurrencyService;
use App\Services\CountryService;
use App\Services\RedeemService;
use App\Services\ActivityLogService;
use Carbon\Carbon;

class TransactionController extends BaseController
{
    public function activities(Request $request)
    {
        return response()->json([]);
    }

    public function findUserTransactionsById()
    {

        $user = auth()->user();
        $userId = $user->id;

        $transactions = TransactionService::findUserTransactionsById($userId);


        return response()->json($transactions);
    }

    public function findUserTransactionsByReferenceId($id, Request $request)
    {

        $user = auth()->user();
        $userId = $user->id;

        $queryParams = $request->all();

        if (isset($queryParams['type'])) {
            $type = $queryParams['type'];

            $model = null;

            if($type === 'redeem'){
           $model = Redeem::find($id);
         }
             ActivityLogService::createActivityLog([
                'subject' => $type . 's',
                'model' => $model,
                'action_type' => 'View',  
                'action_source' => 'view ' . $type . ' account',
                'description' => 'view details'
            ]);
        } else {
            $type = Null;
        }

        $transactions = TransactionService::findUserTransactionsByReferenceId($id, $type);


        return response()->json($transactions[0]);
    }

    public function findLastestUserTransactionsByUserId($id)
    {
        $transactions = TransactionService::findLastestUserTransactionsByUserId($id);

        foreach ($transactions as $transaction) {

            if (!$transaction->status) {
                
                $transaction->status = '3';
            }
        }

        return response()->json($transactions);
    }

    public function findAllUserTransactions(Request $request)
    {
        $user = auth()->user();

        $queryParams = $request->all();

        
        
        $user = auth()->user();

        if (isset($queryParams['id'])) {
            $id = $queryParams['id'];
        } else {
            $id = Null;
        }

        if (isset($queryParams['topkash_id'])) {
            $topkash_id = $queryParams['topkash_id'];
        } else {
            $topkash_id = Null;
        }

        if (isset($queryParams['username'])) {
            $username = $queryParams['username'];
        } else {
            $username = Null;
        }

        if(isset($queryParams['transaction_type'])){
            $type = explode(',' , $queryParams['transaction_type']);
        } else {
            $type = [];
        }

        if(isset($queryParams['currency'])){
            $currency =  $queryParams['currency'];
        } else {
            $currency = [];
        }

        if (isset($queryParams['created_at'])) {
            $created_at = $queryParams['created_at'];
        } else {
            $created_at = [];
        }

       
        

        $transactions = TransactionService::findAllUserTransactions($id, $topkash_id, $username, $currency, $type, $created_at);

        $transactions = UtilityService::modelQueryBuilder($transactions, $request,['reference_username','reference_table','transaction_type']);

        return response()->json($transactions);
    }

    public function findAllRedeemTransactions(Request $request)
    {
        $queryParams = $request->all();
        //
        $transactions = RedeemService::findAllRedeems();

        if (isset($queryParams['id'])) {
            $transactions->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['topkash_id'])) {
            $transactions->where('customer_topkash_id', 'like', '%' .$queryParams['topkash_id']. '%');
        }

        if (isset($queryParams['username'])) {
            $transactions->where('customer_username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['currency'])) {
            $transactions->whereIn('from_currency_code', $queryParams['currency']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
            $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');

            $transactions->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }


        $transactions = UtilityService::modelQueryBuilder($transactions, $request);

        return response()->json($transactions);
    }

    public function findRedeemById($id)
    {
        $redeem = RedeemService::findRedeemById($id);
        $model = Redeem::find($id);

        if (empty($redeem)) {
            return response()->json(['message' => 'Record not found'], 400);
        }

        ActivityLogService::createActivityLog([
            'subject' => 'redeems',
            'model' => $model,
            'action_type' => 'View',  
            'action_source' => 'view redeem account',
            'description' => 'view details'
        ]);

        return response()->json($redeem);
    }


    public function findAllTransactions(Request $request)
    {
        $queryParams = $request->all();
        //
        $transactions = TransactionService::findAllTransactions();

        if (isset($queryParams['transaction_type'])) {
            $transactions->whereIn('type_id', $queryParams['transaction_type']);
        }

        if (isset($queryParams['country'])) {
            $country = CountryService::findCountryByIsoCode($queryParams['country']);

            $transactions->whereIn('transfer_bank_country_id', $country);
        }

        if (isset($queryParams['from_currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['from_currency']);

            $transactions->whereIn('from_currency_id', $currency);
        }


        if (isset($queryParams['to_currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['to_currency']);

            $transactions->whereIn('to_currency_id', $currency);
        }

        if (isset($queryParams['customer_topkash_id'])) {
            $transactions->where('customer_topkash_id', 'like', '%' .$queryParams['customer_topkash_id']. '%');
        }

        if (isset($queryParams['customer_username'])) {

            $transactions->where('customer_username', 'like', '%' .$queryParams['customer_username']. '%');
        }

        if (isset($queryParams['assigned_to'])) {
            $transactions->whereIn('assigned_to_username', $queryParams['assigned_to']);
        }

    
        if (isset($queryParams['status'])) {
            $transactions->whereIn('status', $queryParams['status']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $transactions->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }


        $transactions = UtilityService::modelQueryBuilder($transactions, $request);

        foreach ($transactions as $transaction) {

            if (!$transaction->status) {
                
                $transaction->status = '3';
            }
        }

        return response()->json($transactions);
    }
}
