<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\OnboardingDetails;
use App\Services\UtilityService;

class OnboardingDetailsController extends BaseController
{
    public function index(Request $request)
    {
        // $records = Onboarding::select('id','name','start_date','end_date','is_published','created_by')->get();
        // $records = FamilyMembers::all();
        $records = OnboardingDetails::with([
            'onboardingId:id,name,start_date,end_date,is_published,created_by,updated_by',
        ]);

        $fields = array(
            'title',
            'content',
            'sequence',
        );

        $records = UtilityService::modelQueryBuilder($records, $request, $fields);
        return response()->json($records);
    }
    public function detail($id)
    {
        try {
            $record = OnboardingDetails::where('id', $id)->with([
                'onboardingId:id,name,start_date,end_date,is_published,created_by,updated_by',
            ])->first();

            if(empty($record)) {
                return response()->json(['message' => 'Onboarding Details not found'], 400);
            }

            return response()->json($record, 200);
        }
        catch(Throwable $e)
        {
            return response()->json(['message' => 'Not able to get this Onboarding Details'], 400);
        }

        return response()->json($record);
    }

    public function addOnboardingDetails (Request $request) {

        $this->validate($request, [
            'title' => ['required', 'string'],   
            'onboarding_id' => ['required', 'string'],   
        ]);

        $title = $request->input('title');
        $content = $request->input('content');
        $sequence = $request->input('sequence');
        $onboarding_id = $request->input('onboarding_id');

        try {
            OnboardingDetails::insert([
                'title' => $title,
                'content' => $content,
                'sequence' => $sequence,
                'onboarding_id' => $onboarding_id,
            ]);

            return response()->json(['message' => 'Successfully add this Onboarding Details'], 200);
        }
        catch(Throwable $e)
        {
            return response()->json(['message' => 'Not able to add this Onboarding Details'], 400);
        }
   
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string'],   
            'onboarding_id' => ['required', 'string'],
        ]);

        $title = $request->input('title');
        $content = $request->input('content');
        $sequence = $request->input('sequence');
        $onboarding_id = $request->input('onboarding_id');

        try {
            $onboarding = OnboardingDetails::find($id);
            $onboarding->title = $title;
            $onboarding->content = $content;
            $onboarding->sequence = $sequence;
            $onboarding->onboarding_id = $onboarding_id;
            $onboarding->save();
        
            return response()->json(['message' => 'Successfully update this Onboarding'], 200);
        }
        catch(Throwable $e)
        {
            return response()->json(['message' => 'Not able to update this Onboarding'], 400);
        }
    }

    public function delete($id)
    {
        try {

            $record = OnboardingDetails::find($id);

            if(empty($record)) {
                return response()->json(['message' => 'Onboarding Details not found'], 400);
            }

            $record->delete();
        
            return response()->json(['message' => 'Successfully delete this Onboarding Details'], 200);
        }
        catch(Throwable $e)
        {
            ExchangeRate::withTrashed()->find($id)->restore();

            return response()->json(['message' => 'Not able to delete this Onboarding Details'], 400);
        }
    }

}
