<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\ExchangeRateService;
use App\Services\ExchangeMarginRateService;
use App\Services\CurrencyService;
use App\Models\ExchangeRate;
use App\Models\ExchangeMarginRate;
use App\Services\UtilityService;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Throwable;

class ExchangeRateController extends BaseController
{
    public function index(Request $request)
    {
        $records = DB::table('exchange_rates as er')
            ->leftJoin('currencies as fc', 'fc.id', '=', 'er.from_currency_id')
            ->leftJoin('currencies as tc', 'tc.id', '=', 'er.to_currency_id')
            ->select(
                'er.id as key',
                'er.from_currency_id',
                'er.to_currency_id',
                'er.rate',
                'er.effective_date',
                'fc.iso_code as from_currency',
                'tc.iso_code as to_currency',
            )
            ->get();

        $modifiedRecords = $records->groupBy('effective_date')->map(function ($group) {
            return $group->groupBy('from_currency');
        });

        $final = [];

        foreach ($modifiedRecords as $date => $item) {
            array_push(
                $final,
                [
                    "key" => $date,
                    "date" => $date,
                    "total_curr" => count($item),
                    "currencies" =>
                    $item->map(function ($value, $key) {
                        return [
                            "key" => $key . $value[0]->effective_date, "from_curr" => $key, "to_curruncies" => $value
                        ];
                    })->values()
                ]
            );
        }

        $response = collect($final)->sortByDesc('date');

        if ($request->has('page') && $request->has('page_size')) {
            $page = $request->query('page');
            $page_size = $request->query('page_size');
            $finalData = $this->paginate($response, $page_size, $page);
        } else {
            $finalData = $this->paginate($response, 5, 1);
        }

        return response()->json($finalData);
    }

    public function list(Request $request)
    {
        $queryParams = $request->all();


        $records = DB::table('exchange_rates as er')
            ->leftJoin('currencies as fc', 'fc.id', '=', 'er.from_currency_id')
            ->leftJoin('currencies as tc', 'tc.id', '=', 'er.to_currency_id')
            ->select(
                'er.id as key',
                'er.from_currency_id',
                'er.to_currency_id',
                'er.rate',
                'er.effective_date',
                'fc.iso_code as from_currency_iso_code',
                'fc.currency_name as from_currency_name',
                'tc.iso_code as to_currency_iso_code',
                'tc.currency_name as to_currency_name',

            );
            
         /**
         * To apply filter from request query params
         */

         if (isset($queryParams['from_currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['from_currency']);

            $records->whereIn('er.from_currency_id',$currency);
        }

        if (isset($queryParams['to_currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['to_currency']);

            $records->whereIn('er.to_currency_id', $currency);
        }

       
        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $records->whereBetween('er.effective_date', [$finalFromDate, $finalToDate]);
        }

        $records->orderBy('effective_date', 'desc');

        $response = UtilityService::modelQueryBuilder($records, $request, [], 'er.effective_date', false);

     

        return response()->json($response);
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values()->all(), $items->count(), $perPage, $page, $options);
    }

    public function detail($id)
    {
        try {
            $record = ExchangeRate::where('id', $id)->with([
                'toCurrency:id,iso_code',
                'fromCurrency:id,iso_code',
                'createdBy:id,username,fullname'
            ])->first();

            if (empty($record)) {
                return response()->json(['message' => 'Record not found'], 400);
            }

            return response()->json($record, 200);
        } catch (Throwable $e) {
            return response()->json(['message' => 'Not able to get this record'], 400);
        }

        return response()->json($record);
    }

    public function createExchangeRate(Request $request)
    {
        $this->validate($request, [
            'from_currency_id' => ['required', 'integer'],
            'to_currency_id' => ['required', 'integer'],
            'rate' => ['required', 'numeric'],
            'date' => ['required', 'date_format:Y-m-d'],
        ]);

        $fromCurrencyId = $request->input('from_currency_id');
        $toCurrencyId = $request->input('to_currency_id');
        $rate = $request->input('rate');
        $date = $request->input('date');

        try {
            if ($fromCurrencyId == $toCurrencyId) {
                return response()->json(['message' => 'Target currency should not be similar to source currency'], 400);
            }

            $payload = [
                'from_currency_id' => $fromCurrencyId,
                'to_currency_id' => $toCurrencyId,
                'date' => $date
            ];

            $record = ExchangeRateService::checkExistingExchangeRateRecord($payload);

            if (count($record) > 0) {
                return response()->json(['message' => 'This record is already created before'], 400);
            }

            ExchangeRate::insert([
                'from_currency_id' => $fromCurrencyId,
                'to_currency_id' => $toCurrencyId,
                'rate' => $rate,
                'date' => $date,
                'created_by' => auth()->user()->id
            ]);

            return response()->json(['message' => 'Successfully add this record'], 200);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function updateRateByApi(Request $request)
    {

        $this->validate($request, [
            'date' => ['date_format:Y-m-d'],
        ]);

        $todayDate = Carbon::now()->format('Y-m-d');

        $inputDate = $request->input('date');

        $date = isset($inputDate) ? $inputDate : $todayDate;


        try {
            ExchangeRateService::updateExchangeRatesByApi($date);


            return response()->json(['message' => 'Successfully update exchange rate'], 200);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'rate' => ['required', 'numeric'],
        ]);

        $rate = $request->input('rate');

        try {
            $exchangeRate = ExchangeRate::find($id);
            $exchangeRate->rate = $rate;
            $exchangeRate->save();

            return response()->json(['message' => 'Successfully update this record'], 200);
        } catch (Throwable $e) {
            return response()->json(['message' => 'Not able to update this record'], 400);
        }
    }

    public function delete($id)
    {
        try {

            $record = ExchangeRate::find($id);

            if (empty($record)) {
                return response()->json(['message' => 'Record not found'], 400);
            }

            $record->delete();

            return response()->json(['message' => 'Successfully delete this record'], 200);
        } catch (Throwable $e) {
            ExchangeRate::withTrashed()->find($id)->restore();

            return response()->json(['message' => 'Not able to delete this record'], 400);
        }
    }

    public function addInBluk(Request $request)
    {
        $this->validate($request, [
            'exchange_rates.*.from_currency_id' => ['required', 'integer'],
            'exchange_rates.*.to_currency_id' => ['required', 'integer'],
            'exchange_rates.*.rate' => ['required', 'numeric'],
            'exchange_rates.*.date' => ['required', 'date_format:Y-m-d'],
        ]);

        $exchangeRates = $request->input('exchange_rates');

        try {

            $records = [];

            foreach ($exchangeRates as $exchangeRate) {
                $record = ExchangeRateService::checkExistingExchangeRateRecord($exchangeRate);

                if (count($record) > 0) {
                    return response()->json([
                        'message' => 'The record(s) is already created before',
                        'existed' => [
                            'from_currency_id' => $exchangeRate['from_currency_id'],
                            'to_currency_id' => $exchangeRate['to_currency_id']
                        ]
                    ], 400);
                }

                array_push($records, [
                    'from_currency_id' => $exchangeRate['from_currency_id'],
                    'to_currency_id' => $exchangeRate['to_currency_id'],
                    'rate' => $exchangeRate['rate'],
                    'date' => $exchangeRate['date'],
                    'created_by' => auth()->user()->id
                ]);
            }

            ExchangeRate::insert($records);

            return response()->json(['message' => 'Successfully add this record'], 200);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    public function findAllExchangeRatesByCurrencyIds(Request $request)
    {

        $this->validate($request, [
            'from_currency_id' => ['required', 'integer'],
            'to_currency_id' => ['required', 'integer'],
        ]);


        $from_currency_id = $request->query('from_currency_id');
        $to_currency_id = $request->query('to_currency_id');

        return ExchangeRateService::findAllExchangeRatesByCurrencyIds($from_currency_id, $to_currency_id);
    }

    public function createExchangeMarginRate(Request $request)
    {
        $this->validate($request, [
            'rate' => ['required', 'numeric'],
        ]);

        $rate = $request->input('rate');

        try {
            ExchangeMarginRateService::createExchangeMarginRate($rate);

            return response()->json(['message' => 'Successfully add this record'], 200);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function findAllExchangeMarginRate(Request $request)
    {
        $queryParams = $request->all();

        $records = ExchangeMarginRateService::findAllExchangeMarginRate($queryParams);

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $records->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        $records->orderBy('id', 'desc');

        $response = UtilityService::modelQueryBuilder($records, $request, [], null, false);

        return response()->json($response);
    }
}
