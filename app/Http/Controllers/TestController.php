<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\TestService;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Throwable;

class TestController extends BaseController
{

    public function getAllUsers()
    {
        $users = TestService::getAllUsers();

        return response()->json($users);
    }


    public function findUserById(Request $request)
    {

        $user_id = $request->query('id');
        $user = TestService::findUserById($user_id);

        return response()->json($user);
    }


    public function createDashboardUser(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'fullname' => 'required',
            'email' => 'required',
            'phone_no' => 'required',
        ]);

        try {

            $data = [];
            $data['username'] = $request->input('username');
            $data['fullname'] = $request->input('fullname');
            $data['email'] = $request->input('email');
            $data['phone_no'] = $request->input('phone_no');


            $user = TestService::createDashboardUser($data);

            return response()->json(['message' => 'Created new account.']);
        } catch (Throwable $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function updateDashboardUser(Request $request)
    {

        $this->validate($request, [
            'id' => 'required',
            'username' => 'required',
            'fullname' => 'required',
            'email' => ['required', 'email'],
            'phone_no' => 'required',
        ]);

        $data = [];
        $data['id'] = $request->input('id');
        $data['username'] = $request->input('username');
        $data['fullname'] = $request->input('fullname');
        $data['email'] = $request->input('email');
        $data['phone_no'] = $request->input('phone_no');


        $user = TestService::updateDashboardUser($data);


        if ($user) {
            return response()->json([
                'message' => "Successfully update dashboard user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }

    public function deleteUserbyId(Request $request)
    {

        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $user = TestService::deleteUserbyId($id);

        if ($user) {
            return response()->json([
                'message' => "User deleted"
            ], 200);
        } else {

            return response()->json([
                'message' => "Something went wrong"
            ], 400);
        }


        return response()->json(['message' => 'Deleted.']);
    }

    public function getAllUsersLeftJoinCountry()
    {

        $users = TestService::getAllUsersLeftJoinCountry();

        return response()->json($users);
    }

    public function getUserByIdLeftJoinCountry($id)
    {

        // $id = $request->query('id');

        $user = TestService::getUserByIdLeftJoinCountry($id);

        return response()->json($user);
    }

    public function getUsersWithCountry()
    {

        $user = TestService::getUsersWithCountry();

        return response()->json($user);
    }
}
