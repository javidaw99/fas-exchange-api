<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Services\NotificationService;

class NotificationController extends BaseController
{
    public function getNotifications()
    {
        $user = auth()->user();

        $records = Notification::select('id','title', 'content', 'is_read', 'created_at')
            ->where('user_id',  $user->id)
            ->orderByDesc('created_at')
            ->paginate(10);


        $count = Notification::where('user_id',  $user->id)
            ->where('is_read', 0)
            ->count();

        $records = collect($records);

        $records->put('total_unread' , $count);

        return response()->json($records);

    }

    public function markAllAsRead()
    {
        $user = auth()->user();

        Notification::where('user_id', $user->id)
        ->where('is_read', 0)
        ->update(['is_read' =>  1]);  

        return response()->json([
            'message' => 'All messages are marked as read',
        ]);

    }

    public function markMessageAsRead(Request $request)
    {
     
        Notification::where('id', $request->input('id'))
        ->update(['is_read' =>  1]);  

        return response()->json([
            'message' => 'Selected message is marked as read',
        ]);

    }   
    public function sendPushNotification(Request $request)
    {
        NotificationService::sendPushNotification([
            'fcm_token' => $request->input('fcm_token'), 
            'title' => $request->input('title'), 
            'body' => $request->input('body'),
            'target_user_id' => $request->input('user_id')
        ]);

        return response('OK');
    }

    public function sendPushNotificationInBulk(Request $request)
    {
        NotificationService::sendPushNotificationInBulk([
            'title' => $request->input('title'), 
            'body' => $request->input('body'),
        ]);

        return response('OK');
    }
 
}
