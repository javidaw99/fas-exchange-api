<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\CountryCurrency;
use App\Services\AccountService;
use App\Services\CountryService;
use App\Services\UtilityService;
use Throwable;

class CountryController extends BaseController
{
    // public function index(Request $request) {

    //     $countries = Country::leftJoin('currencies AS cr', 'countries.id', 'cr.country_id')
    //         ->select([
    //             'countries.*',
    //             'cr.id AS currency_id',
    //             'cr.iso_code as currency_iso'
    //         ])
    //         ->get();

    //     return response()->json($countries);
    // }

    public function countryWithCurrencies(Request $request)
    {

        $type = $request->input('type');

        if ($type === 'all') {
            $countries_currencies = Country::with([
                'currency:id,currency_name,iso_code'
            ])
                ->where('is_enabled', 1)
                ->get();

            return response()->json($countries_currencies);
        } else {
            $countries_currencies = Country::with([
                'currency:id,currency_name,iso_code'
            ]);

            $fields = array(
                'name',
                'iso_code_2',
            );

            $records = UtilityService::modelQueryBuilder($countries_currencies, $request, $fields);

            return response()->json($records);
        }
    }


    public function updateCountry($id, Request $request)
    {

        $this->validate($request, [
            'currencies' => ['required'],
        ]);

        $countryCurrencyExist = AccountService::findCountryCurrencyByCountryId($id);
        $countryArray = $request->input('currencies');

        if ($countryCurrencyExist) {

            $deleteCountryCurrency = CountryCurrency::where('country_id', $id)
                ->whereNull('deleted_at')
                ->delete();

            foreach ($countryArray as $currencyId) {
                $countryCurrency = AccountService::createCountryCurrencyRecord($id, $currencyId);
            }

            return response()->json([
                'message' => 'Successfully update the country'
            ]);
        } else {

            foreach ($countryArray as $currencyId) {
                $countryCurrency = AccountService::createCountryCurrencyRecord($id, $currencyId);
            }

            return response()->json([
                'message' => 'Successfully update the country'
            ]);
        }
    }

    public function updateCountryStatus($id)
    {

        $updated = AccountService::updateCountryEnableStatus($id);

        if ($updated) {

            $country = AccountService::findCountryById($id);

            if ($country->is_enabled == 1) {
                return response()->json([
                    'message' => 'Enabled'
                ]);
            } else if ($country->is_enabled == 0) {
                return response()->json([
                    'message' => 'Disabled'
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Something wrong'
            ], 500);
        }
    }

    public function updateCountryForTransacts($id)
    {

        $updated = AccountService::updateCountryForTransacts($id);

        if ($updated) {

            $country = AccountService::findCountryById($id);

            if ($country->for_transacts == 1) {
                return response()->json([
                    'message' => 'Enabled for transacts'
                ]);
            } else if ($country->for_transacts == 0) {
                return response()->json([
                    'message' => 'Disabled for transacts'
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Something wrong'
            ], 500);
        }
    }

    public function findAllActiveCountries(Request $request)
    {
        try {
            $countries = CountryService::findAllActiveCountriesWithCurrencies();

            $record = UtilityService::modelQueryBuilder($countries, $request);

            return response()->json($record);
        }
        catch(Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function findCountryById($id)
    {
        try {
            $country = CountryService::findCountryWithCurrenciesById($id);

            return response()->json($country);
        }
        catch(Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function updateCountryById($id, Request $request)
    {
        $this->validate($request, [
            'currency_ids' => ['required']
        ]);

        try {
            CountryService::updateCountryCurrenciesById($id, $request->input('currency_ids'));

            return response()->json(['message' => 'Successfully update country']);
        }
        catch(Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function findCountryFilterOptionsByType($type)
    {
        $country = CountryService::findCountryFilterOptionsByType($type);

        if($type == 'all') {

            $country = CountryService::findAllCountryFilterOptions();

            $country = collect($country)->unique('name')->map(function ($value) {
                return [
                    'id' => $value->id,
                    'type' => 'all',
                    'name' => $value->name,
                    'iso_code' => $value->iso_code
                ];
            })->values();

        }
        
        return $country;
    }
}
