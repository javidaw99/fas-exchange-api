<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\MediaService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Services\UtilityService;
use Throwable;

class MediaController extends BaseController
{
    public function CreateBanner(Request $request)
    {
        try {

            $this->validate($request, [
                'name' => 'required',
                'media_id' => 'required',
                'type' => 'required',
                'sequence' => 'required',
                'is_enabled' => 'required',
            ]);

            $name = $request->input('name');
            $mediaId = $request->input('media_id');
            $type = $request->input('type');
            $sequence = $request->input('sequence');
            $isEnabled = $request->input('is_enabled');

            if ($type=='navigate') {
                $this->validate($request, [
                    'navigation_stack' => 'required',
                    'navigation_screen' => 'required',
                ]);
                $navigationStack = $request->input('navigation_stack');
                $navigationScreen = $request->input('navigation_screen');

                MediaService::createBanner($name, $mediaId, $type, $sequence, $isEnabled, $navigationStack, $navigationScreen);
                
            } else if ($type=='website') {
                $this->validate($request, [
                    'website' => 'required',
                ]);
                $website = $request->input('website');
                
                MediaService::createBanner($name, $mediaId, $type, $sequence, $isEnabled, null, null, $website);
            }

            return response()->json([
                'message' => 'Banner Successfully Created'
            ], 200);

        }
        catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 404);
        }
    }

    public function removeMediaById($id)
    {
        try {

            $media = MediaService::findMediaById($id);
print_r($media);
            return response()->json($media);

            if (is_null($media)) {
                return response()->json([
                    'message' => 'Something wrong'
                ], 400);
            } else {
                // $media->delete();

                return response()->json([
                    'message' => 'Successfully delete media'
                ]);
            }
        } catch (Throwable $th) {
            // return response()->json(['message' => 'Not able to delete this record'], 400);
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function createBannerMedia(Request $request)
    {
        try {

            $this->validate($request, [
                'filename' => 'required',
                'path' => 'required',
                'extension' => 'required',
                'mime' => 'required',
                'type' => 'required',
            ]);

            $filename = $request->input('filename');
            $path = $request->input('path');
            $extension = $request->input('extension');
            $mime = $request->input('mime');
            $type = $request->input('type');

            // Add image to S3
            $storage = Storage::disk();
            $filePath = 'banner';
            $file = $request->file('image');
            $upload = $storage->putFileAs($filePath, $file, $filename);

            // Add image to DB
            $item = MediaService::createMedia($filename, $path, $extension, $mime, 0, ' ', $type);

            return response()->json([
                $item->id
            ], 200);

        }
        catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 404);
        }
    }

    public function findAllBanners(Request $request)
    {
        $queryParams = $request->all();

        $records = MediaService::findAllBanners();

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $records->whereBetween('banners.created_at', [$finalFromDate, $finalToDate]);
        }

        $records->orderBy('banners.id', 'desc');

        $response = UtilityService::modelQueryBuilder($records, $request, [], null, false);

        // $banners = $response->get();

        foreach ($response as $banner) {
            
            if (!is_null($banner->path)) {
                $file_url = UtilityService::generateTemporaryFileUrl($banner->path);
                // $file_url = UtilityService::generateTemporaryFileUrl('');
                $banner->file_url = $file_url;
            }
        }

        return response()->json($response);
    }

    public function findBannerById($id) 
    {
        $banner = MediaService::findBannerWithPathById($id);

        if (!is_null($banner->path)) {
            $file_url = UtilityService::generateTemporaryFileUrl($banner->path);
            $banner->file_url = $file_url;
        }

        return response()->json(
            $banner
        );
    }

    public function updateBanner ($id, Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'media_id' => 'required',
            'type' => 'required',
            'sequence' => 'required',
            'is_enabled' => 'required',
        ]);

        try {

            $name = $request->input('name');
            $mediaId = $request->input('media_id');
            $type = $request->input('type');
            $sequence = $request->input('sequence');
            $isEnabled = $request->input('is_enabled');

            $banner = MediaService::findBannerById($id);

            if ($banner) {

                $banner->name = $name;
                $banner->media_id = $mediaId;
                $banner->type = $type;
                $banner->sequence = $sequence;
                $banner->is_enabled = $isEnabled;

                if ($type=='navigate') {
                    $this->validate($request, [
                        'navigation_stack' => 'required',
                        'navigation_screen' => 'required',
                    ]);
                    $navigationStack = $request->input('navigation_stack');
                    $navigationScreen = $request->input('navigation_screen');
    
                    $banner->navigation_stack = $navigationStack;
                    $banner->navigation_screen = $navigationScreen;
                    $banner->website = null; 

                } else if ($type=='website') {
                    $this->validate($request, [
                        'website' => 'required',
                    ]);
                    $website = $request->input('website');

                    $banner->navigation_stack = null;
                    $banner->navigation_screen = null;
                    $banner->website = $website;
                    
                }

                $banner->save();
                return response()->json(['message' => 'Successfully update the banner'], 200);
            } else {
             
                return response()->json([
                    'message' => 'The banner does not exist'
                ], 400);
            }
           
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }  
   
    }

}
