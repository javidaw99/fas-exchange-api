<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Services\UtilityService;
use App\Services\CurrencyService;
use Carbon\Carbon;
use Throwable;

use App\Services\ProcessingFeeBaseFixedRateService;

class ProcessingFeeBaseFixedRateController extends BaseController
{
    public function index(Request $request)
    {
        $records = DB::table('processing_fee_base_fixed_rates as er')
            ->leftJoin('currencies as fc', 'fc.id', '=', 'er.from_currency_id')
            ->leftJoin('currencies as tc', 'tc.id', '=', 'er.to_currency_id')
            ->select(
                'er.id as key',
                'er.from_currency_id',
                'er.to_currency_id',
                'er.amount',
                'er.date',
                'fc.iso_code as from_currency',
                'tc.iso_code as to_currency',
            )
            ->get();

        $modifiedRecords = $records->groupBy('date')->map(function ($group) {
            return $group->groupBy('from_currency');
        });

        $final = [];

        foreach ($modifiedRecords as $date => $item) {
            array_push(
                $final,
                [
                    "key" => $date,
                    "date" => $date,
                    "total_curr" => count($item),
                    "currencies" =>
                    $item->map(function ($value, $key) {
                        return [
                            "key" => $key . $value[0]->date, "from_curr" => $key, "to_curruncies" => $value
                        ];
                    })->values()
                ]
            );
        }

        $response = collect($final)->sortByDesc('date');

        if ($request->has('page') && $request->has('page_size')) {
            $page = $request->query('page');
            $page_size = $request->query('page_size');
            $finalData = $this->paginate($response, $page_size, $page);
        } else {
            $finalData = $this->paginate($response, 5, 1);
        }

        return response()->json($finalData);
    }

    public function list(Request $request)
    {

        $queryParams = $request->all();

        
        
        $records = DB::table('processing_fee_base_fixed_rates as er')
            ->leftJoin('currencies as fc', 'fc.id', '=', 'er.from_currency_id')
            ->leftJoin('currencies as tc', 'tc.id', '=', 'er.to_currency_id')
            ->select(
                'er.id as key',
                'er.from_currency_id',
                'er.to_currency_id',
                'er.amount',
                'er.date',
                'fc.iso_code as from_currency_iso_code',
                'fc.currency_name as from_currency_name',
                'tc.iso_code as to_currency_iso_code',
                'tc.currency_name as to_currency_name',
            );
          
            
           /**
         * To apply filter from request query params
         */

            if (isset($queryParams['from_currency'])) {

                $currency = CurrencyService::findCurrencyByIsoCode($queryParams['from_currency']);

                $records->whereIn('er.from_currency_id',$currency);
            }
    
            if (isset($queryParams['to_currency'])) {

                $currency = CurrencyService::findCurrencyByIsoCode($queryParams['to_currency']);

                $records->whereIn('er.to_currency_id', $currency);
            }
    
           
            if (isset($queryParams['created_at'])) {
                $fromDate = $queryParams['created_at'][0];
                $toDate = $queryParams['created_at'][1];
                $fromCarbon = new Carbon($fromDate);
                $toCarbon = new Carbon($toDate);
                if (isset($queryParams['localTimeOffset'])) {
                    $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                    $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                }
                else 
                {
                    $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                    $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
                }

                $records->whereBetween('er.date', [$finalFromDate, $finalToDate]);
            }

            $records->orderBy('date', 'desc');

            
            // $searchingFields = array('er.from_currency_id', 'er.from_currency_id');
    
            $response = UtilityService::modelQueryBuilder($records, $request, [], 'er.date', false);

       
        return response()->json($response);
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage)->values()->all(), $items->count(), $perPage, $page, $options);
    }

    public function findAllProcessingFeeBaseFixedRates()
    {
        return ProcessingFeeBaseFixedRateService::findAllProcessingFeeBaseFixedRates();
    }

    public function createProcessingFeeBaseFixedRate(Request $request)
    {
        try {
            $this->validate($request, [
                'from_currency_id' => ['required', 'integer'],
                'to_currency_id' => ['required', 'integer'],
                'amount' => ['required', 'numeric'],
                'date' => ['required', 'date'],
            ]);

            $fromCurrencyId = $request->input('from_currency_id');
            $toCurrencyId =  $request->input('to_currency_id');
            $amount =  floatval($request->input('amount'));
            $date =  $request->input('date');

            $processingFeeBaseFixedRateId = ProcessingFeeBaseFixedRateService::createProcessingFeeBaseFixedRate([
                'from_currency_id' => $fromCurrencyId,
                'to_currency_id' => $toCurrencyId,
                'amount' => $amount,
                'date' => $date,
            ]);

            return $processingFeeBaseFixedRateId;
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to add this record'], 400);
        }
    }

    
}
