<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Cases;
use App\Models\Reload;
use App\Models\Status;
use App\Models\Transfer;
use App\Services\CaseService;
use App\Services\AccountService;
use App\Services\CurrencyService;
use App\Services\CountryService;
use App\Services\UserService;
use App\Services\UtilityService;
use App\Services\SocketService;
use App\Services\NotificationService;
use App\Services\ActivityLogService;
use Throwable;

class CaseController extends BaseController
{
    public function userCases(Request $request)
    {

        $now = Carbon::now();

        $month = $request->query('month');
        $year = $request->query('year');

        if (is_null($month) || is_null($year)) {
            $month = $now->month;
            $year = $now->year;
        }

        $cases = CaseService::getUserCasesByMonthAndYear($month, $year);

        return response()->json($cases);
    }

    public function getRecentRecords()
    {

        $records = Cases::with('transact')
            ->select('id', 'user_id', 'ref_type', 'ref_id', 'assigned_user_id', 'status', 'created_at')
            ->orderByDesc('created_at')
            ->limit(10)
            ->get();

        return response()->json($records);
    }

    public function getReloadRecords(Request $request)
    {
        $records = CaseService::getCasesByRefType('reload');

        // DB::enableQueryLog();

        // Temporarily retrieve data order by descending
        $records = UtilityService::modelQueryBuilder($records, $request, [
            'reference_no',
            'bank_branch_name',
            'user_username',
        ], null, true);

        // var_dump(DB::getQueryLog());

        if (count($records) > 0) {
            if (method_exists($records, 'hasPages'))
            //To detect whether the model is a paginate object or normal collection
            {
                $alteredRecords = $records->getCollection()->each(function ($item) {
                    if (!is_null($item->file_path)) {
                        $file_url = UtilityService::generateTemporaryFileUrl($item->file_path);
                        $item->file_url = $file_url;
                        // $item->setAttribute('file_url', $file_url);
                    }
                });

                return $records->setCollection($alteredRecords);
            } else {
                return $records->each(function ($item) {
                    if (!is_null($item->file_path)) {
                        $file_url = UtilityService::generateTemporaryFileUrl($item->file_path);
                        $item->file_url = $file_url;
                        // $item->setAttribute('file_url', $file_url);
                    }
                });
            }
        }

        return response()->json($records);
    }



    public function findReloadCaseById($id, Request $request)
    {
        $mode = $request->query('mode') ?? null;

        $reload = CaseService::findReloadCaseById($id);

        if (empty($reload)) {
            return response()->json(['message' => 'Record not found'], 400);
        }

        if (!is_null($reload->file_path)) {
            $file_url = UtilityService::generateTemporaryFileUrl($reload->file_path);
            $reload->file_url = $file_url;
        }

         $model = Reload::find($id);

        if($mode === 'view'){
            ActivityLogService::createActivityLog([
                'subject' => 'reloads',
                'model' => $model,
                'action_type' => 'View',  
                'action_source' => 'view reload account',
                'description' => 'view details'
            ]);
        } 



        return response()->json($reload);
    }

    public function deleteReloadRecordById($id)
    {
        try {
            $case = CaseService::getCaseByTypeAndRefId('reloads', $id);

            if (empty($case)) {
                return response()->json(['message' => 'Reload id not found'], 400);
            }

            $case->transact()->delete();
            $case->delete();

            return response()->json(['message' => 'Successfully delete record'], 200);
        } catch (Throwable $th) {
            $case->withTrashed()->restore();

            return response()->json(['message' => 'Not able to delete record'], 400);
        }
    }

    public function updateReloadById(Request $request, $id)
    {
        $this->validate($request, [
            'status' => ['required', 'integer']
        ]);

        try {
            $status = $request->input('status');
            $rejectReason = $request->input('reject_reason') ? $request->input('reject_reason') : '-';

            $reload = Reload::find($id);

            if (!empty($reload)) {
                $case = CaseService::getCaseByTypeAndRefId('reloads', $id);

                if (empty($case)) {
                    return response()->json(['message' => 'Case not exist'], 400);
                }

                if (is_null($case->assigned_user_id)) {
                    $case->assigned_user_id = $request->input('user_id');
                }

                if ($case->assigned_user_id != $request->input('user_id')) {
                    return response()->json(['message' => 'This case is already assigned to other user'], 400);
                }


                if ($case->status == 3) {

                    $transaction = CaseService::getTransactionByCaseId($case->id);

                    if ($transaction) {
                        $assignedUser = $case->assignedUser()->first();
                        return response()->json(['message' => 'This record is already approved by ' . $assignedUser->fullname], 400);
                    }
                } else {
                    if ($status == 3) {

                        $case->processed_at = Carbon::now()->format('Y-m-d H:i:s');
                        $case->status = $status;
                        $case->save();

                        /* Temporary remove socket service first */
                        // SocketService::emitSocketData('/remove-reload', [
                        //     'id' => $case->id
                        // ]);

                    } else if (
                        $status == 4 ||
                        $status == 5 ||
                        $status == 6
                    ) {
                        /* Temporary remove socket service first */
                        // SocketService::emitSocketData('/remove-reload', [
                        //     'id' => $case->id
                        // ]);
                        $case->processed_at = Carbon::now()->format('Y-m-d H:i:s');
                        $case->status = $status;
                        $case->reject_reason = $rejectReason;
                        $case->save();
                    } else {
                        $userCase = CaseService::getCasesByRefType('reload', null)->find($case->id);
                        $userCase = UtilityService::appendAttribute(collect([$userCase]), [
                            [
                                'name' => 'formatted_amount',
                                'path' => 'transact.formatted_amount',
                                'hide' => 'transact'
                            ]
                        ]);
                        /* Temporary remove socket service first */
                        // SocketService::emitSocketData('/add-reload', $userCase[0]->toArray());

                        $case->status = $status;
                        $case->reject_reason = $rejectReason;
                        $case->save();
                    }
                }
                // }

                $user = UserService::findUserById($case->user_id);

                $reloadCurrency = CurrencyService::findOneById($reload->currency_id);

                $dynamicValues = [
                    'iso_code' => UtilityService::formatNumberToCurrency($reload->amount, $reloadCurrency->iso_code)
                ];

                $language = !is_null($user->app_language) ? $user->app_language : 'en';

                $locale = UtilityService::loadLocale($language);

                if ($status == 3) {
                    $titleKey = 'notification.title_1';
                    $contentKey = 'notification.description_1';
                } else if ($status == 5) {
                    $titleKey = 'notification.title_1';
                    $contentKey = 'notification.description_2';
                }

                $notification = NotificationService::sendPushNotification([
                    'fcm_token' => $user->fcm_token,
                    'title' => $locale->t($titleKey),
                    'body' => $locale->t($contentKey, $dynamicValues),
                    'target_user_id' => $case->user_id
                ], 'reload', $reload->id);

                UtilityService::createLocaleMapperRecord([
                    'title_key' => 'notification.title_1',
                    'content_key' => 'notification.description_1',
                    'dynamic_values' => json_encode($dynamicValues),
                    'reference_table' => 'notification',
                    'reference_id' => $notification->id
                ]);
            } else {
                return response()->json(['message' => 'Record not exist'], 400);
            }

            // return response()->json(['message' => 'Successfully update this record', 'notification' => $notification], 200);
            return response()->json(['message' => 'Successfully update this record'], 200);
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function getTransferRecords(Request $request)
    {
        $records = CaseService::getCasesByRefType('transfer');
        // Temporarily retrieve data order by descending
        $records = UtilityService::modelQueryBuilder($records, $request, [
            'bank_name',
            'user_fullname',
            'user_username',
            'country_name',
        ], null, true);

        return response()->json($records);
    }

    public function findTransferCaseById($id, Request $request)
    {
        $mode = $request->query('mode') ?? null;

        
        $transfer = CaseService::findTransferCaseById($id);

        if (empty($transfer)) {
            return response()->json(['message' => 'Record not found'], 400);
        }

        $model = Transfer::find($id);

        if($mode === 'view') {
            ActivityLogService::createActivityLog([
                'subject' => 'transfers',
                'model' => $model,
                'action_type' => 'View',  
                'action_source' => 'view transfer account',
                'description' => 'view details'
            ]);
        }

        
       
         
        $toConvertAmountBeforeRoundUp = $transfer->amount - $transfer->processing_fee;
        $toConvertAmount = round($toConvertAmountBeforeRoundUp, 2);

        $transfer->to_convert_amount = $toConvertAmount;

        return response()->json($transfer);
        
    }

    public function deleteTransferRecordById($id)
    {
        try {

            $case = CaseService::getCaseByTypeAndRefId('transfers', $id);

            if (empty($case)) {
                return response()->json(['message' => 'Transfer id not found'], 400);
            }

            $case->transact()->delete();
            $case->delete();

            return response()->json(['message' => 'Successfully delete record'], 200);
        } catch (Throwable $th) {
            $case->withTrashed()->restore();

            return response()->json(['message' => 'Not able to delete record'], 400);
        }
    }

    public function updateTransferById(Request $request, $id)
    {
        $this->validate($request, [
            // 'recipient_bank_id' => ['required', 'integer'],
            // 'recipient_acc_no' => ['required', 'numeric'],
            'status' => ['required', 'integer'],
            'tk_bank_account_id' => ['required_if:status,==,3']
        ]);

        try {
            // $recipientBankId = $request->input('recipient_bank_id');
            // $recipientAccNo = $request->input('recipient_acc_no');
            // $purpose = $request->input('purpose');
            $status = $request->input('status');
            $rejectReason = $request->input('reject_reason') ? $request->input('reject_reason') : '-';
            $topkashBankAccId = $request->input('tk_bank_account_id') ? $request->input('tk_bank_account_id') : null;
            $topkashReferenceNo = $request->input('tk_reference_no') ? $request->input('tk_reference_no') : null;

            $transfer = Transfer::find($id);

            if (!empty($transfer)) {
                // $transfer->recipient_bank_country_id = $recipientBankId;
                // $transfer->recipient_acc_no = $recipientAccNo;
                // $transfer->purpose = $purpose;
                // $transfer->save();

                $case = CaseService::getCaseByTypeAndRefId('transfers', $id);

                if (empty($case)) {
                    return response()->json(['message' => 'Case not exist'], 400);
                }

                if (is_null($case->assigned_user_id)) {
                    $case->assigned_user_id = $request->input('user_id');
                }

                if ($case->assigned_user_id != $request->input('user_id')) {
                    return response()->json(['message' => 'This case is already assigned to other user'], 400);
                }

                if ($case->status == 3) {

                    $transaction = CaseService::getTransactionByCaseId($case->id);

                    if ($transaction) {
                        $assignedUser = $case->assignedUser()->first();
                        return response()->json(['message' => 'This record is already approved by ' . $assignedUser->fullname], 400);
                    }
                } else {

                    // $case->status = $status;
                    // $case->reject_reason_text = $rejectReason;
                    // $case->save();

                    if ($status == 3) {

                        $case->processed_at = Carbon::now()->format('Y-m-d H:i:s');
                        $case->status = $status;
                        $case->save();

                        $transfer->tk_bank_account_id = $topkashBankAccId;
                        $transfer->tk_reference_no = $topkashReferenceNo;
                        $transfer->save();

                        // try {
                        //     $caseSecureSafe = CaseService::createCaseSecureSafe($case->id);

                        //     if ($caseSecureSafe) {
                        //         $transaction = CaseService::getLatestTransactionRecord($case->user_id, $case->account_id);
                        //         if (!empty($transaction)) {
                        //             $currentBalance = $transaction->new_balance;

                        //             if ($currentBalance <= 0) {
                        //                 return response()->json(['message' => 'Not enough balance'], 400);
                        //             }

                        //             $newBalance = $currentBalance - $transfer->amount;

                        //             $payload = [];
                        //             $payload['case_id'] = $case->id;
                        //             $payload['currency_id'] = $transfer->currency_id;
                        //             $payload['user_id'] = $case->user_id;
                        //             $payload['account_id'] = $case->account_id;
                        //             $payload['old_balance'] = $currentBalance;
                        //             $payload['new_balance'] = $newBalance;
                        //             $payload['rate'] = null;

                        //             CaseService::createTransactionRecord($payload);

                        //             /* Temporary remove socket service first */
                        //             // SocketService::emitSocketData('/remove-transfer', [
                        //             //     'id' => $case->id
                        //             // ]);
                        //         } else {
                        //             return response()->json(['message' => 'No transaction record exist'], 400);
                        //         }
                        //     }
                        // } catch (Throwable $th) {
                        //     return response()->json(['message' => 'Something went wrong'], 400);
                        // }
                    } else if (
                        $status == 4 ||
                        $status == 5 ||
                        $status == 6
                    ) {
                        /* Temporary remove socket service first */
                        // SocketService::emitSocketData('/remove-transfer', [
                        //     'id' => $case->id
                        // ]);
                        $case->processed_at = Carbon::now()->format('Y-m-d H:i:s');
                        $case->status = $status;
                        $case->reject_reason = $rejectReason;
                        $case->save();
                    } else {
                        $userCase = CaseService::getCasesByRefType('transfer', null)->find($case->id);
                        $userCase = UtilityService::appendAttribute(collect([$userCase]), [
                            [
                                'name' => 'formatted_amount',
                                'path' => 'transact.formatted_amount',
                                'hide' => 'transact'
                            ]
                        ]);
                        /* Temporary remove socket service first */
                        // SocketService::emitSocketData('/add-transfer', $userCase[0]->toArray());

                        $case->status = $status;
                        $case->reject_reason_text = $rejectReason;
                        $case->save();
                    }
                }

                $user = UserService::findUserById($case->user_id);

                $transferCurrency = CurrencyService::findOneById($transfer->from_currency_id);

                $transferToUserBank = UserService::findUserBankAccountById($transfer->user_bank_account_id);

                $language = !is_null($user->app_language) ? $user->app_language : 'en';

                $locale = UtilityService::loadLocale($language);

                if ($status == 3) {
                    $contentKey = 'notification.description_3';
                    $dynamicValues = [
                        'iso_code' => UtilityService::formatNumberToCurrency($transfer->amount, $transferCurrency->iso_code)
                    ];

                    $bodyText = $locale->t('notification.description_3', $dynamicValues);

                    if ($transferToUserBank->type === 'OTHERS') {
                        $contentKey = 'notification.description_8';
                        $dynamicValues = [
                            'iso_code' => UtilityService::formatNumberToCurrency($transfer->amount, $transferCurrency->iso_code),
                            'reference_name' => $transferToUserBank->reference_name
                        ];

                        $bodyText = $locale->t('notification.description_8', $dynamicValues);
                    }

                    $notification = NotificationService::sendPushNotification([
                        'fcm_token' => $user->fcm_token,
                        'title' => $locale->t('notification.title_2'),
                        'body' => $bodyText,
                        'target_user_id' => $case->user_id
                    ], 'transfer',  $transfer->id);

                    UtilityService::createLocaleMapperRecord([
                        'title_key' => 'notification.title_2',
                        'content_key' => $contentKey,
                        'dynamic_values' => json_encode($dynamicValues),
                        'reference_table' => 'notification',
                        'reference_id' => $notification->id
                    ]);
                } else if ($status == 5) {

                    $contentKey = 'notification.description_4';
                    $dynamicValues = [
                        'iso_code' => UtilityService::formatNumberToCurrency($transfer->amount, $transferCurrency->iso_code)
                    ];

                    $bodyText = $locale->t($contentKey, $dynamicValues);

                    if ($transferToUserBank->type === 'OTHERS') {

                        $contentKey = 'notification.description_9';
                        $dynamicValues = [
                            'iso_code' => UtilityService::formatNumberToCurrency($transfer->amount, $transferCurrency->iso_code),
                            'reference_name' => $transferToUserBank->reference_name
                        ];

                        $bodyText = $locale->t($contentKey, $dynamicValues);
                    }

                    $notification = NotificationService::sendPushNotification([
                        'fcm_token' => $user->fcm_token,
                        'title' => $locale->t('notification.title_2'),
                        'body' => $bodyText,
                        'target_user_id' => $case->user_id
                    ], 'transfer', $transfer->id);

                    UtilityService::createLocaleMapperRecord([
                        'title_key' => 'notification.title_2',
                        'content_key' => $contentKey,
                        'dynamic_values' => json_encode($dynamicValues),
                        'reference_table' => 'notification',
                        'reference_id' => $notification->id
                    ]);
                }
            } else {
                return response()->json(['message' => 'Record not exist'], 400);
            }

            // return response()->json(['message' => 'Successfully update this record', 'notification' => $notification], 200);
            return response()->json(['message' => 'Successfully update this record'], 200);
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function assignCase(Request $request)
    {
        $this->validate($request, [
            'ref_id' => ['required', 'integer'],
            'type' => ['required', 'string'],
            'action' => ['required', 'string', 'in:move,assign'],
            'assigned_user_id' => ['integer', 'required_if:action,move']
        ]);

        $ref_id = $request->input('ref_id');
        $type = $request->input('type');
        $action = $request->input('action');
        $user = auth()->user();

        // $case = CaseService::getCaseByTypeAndRefId($type, $ref_id);

        return CaseService::handleCaseWithLock($type, $ref_id, ['*'], $action, $user);

        // if (empty($case)) {
        //     return response()->json(['message' => 'Case record cannot be found'], 400);
        // }

        // if ($action == 'assign') {
        //     if ($case->assigned_user_id == $user->id) {
        //         return response()->json(['message' => 'This record has been assigned to' . $user->fullname], 200);
        //     }
        //     if (!empty($case->assigned_user_id)) {
        //         return response()->json(['message' => 'This record has been assigned to other user'], 400);
        //     } else {

        //         $case->assigned_user_id = $user->id;
        //         $case->status = 2;
        //         $case->save();
        //     }
        // } else if ($action == 'move') {
        //     $assigned_user_id = $request->input('assigned_user_id');
        //     $user = UserService::findUserById($assigned_user_id);

        //     $case->assigned_user_id = $assigned_user_id;
        //     $case->status = 2;
        //     $case->save();
        // }

        // $socketEndpoint = '/';

        // if ($type == 'reload') {
        //     $socketEndpoint = '/update-reload';
        // } else if ($type == 'transfer') {
        //     $socketEndpoint = '/update-transfer';
        // }

        // SocketService::emitSocketData($socketEndpoint, [
        //     'id' => $case->id,
        //     'data' => [
        //         'assigned_user' => [
        //             'id' => $case->assigned_user_id,
        //             'username' => $user->username
        //         ]
        //     ]
        // ]);

        // return response()->json([
        //     'message' => 'Successfully ' . $action . ' this case to ' . $user->fullname
        // ], 200);
    }

    public function getStatuses()
    {
        $allRecords = Status::all();

        return response()->json($allRecords);
    }

    public function getExchangeRecords(Request $request)
    {
        $records = CaseService::getCasesByRefType('exchange');

        $records = UtilityService::modelQueryBuilder($records, $request, [
            'from_currency.iso_code',
            'to_currency.iso_code',
            'users.fullname'
        ]);

        if (method_exists($records, 'hasPages'))
        //To detect whether the model is a paginate object or normal collection
        {
            $appendedCollection = $records->getCollection()->each(function ($item) {
                $processingFee = $item->processing_fee ? $item->processing_fee : 0;
                $finalRate = round($item->exchange_rate * (1 - ($item->processing_fee / 100)), 4);

                $item->setAttribute('processing_fee', $processingFee);
                $item->setAttribute('final_exchange_rate', $finalRate);
            });

            $records->setCollection($appendedCollection);
        } else {
            $records->each(function ($item) {
                $processingFee = $item->processing_fee ? $item->processing_fee : 0;
                $finalRate = round($item->exchange_rate * (1 - ($item->processing_fee / 100)), 4);

                $item->setAttribute('processing_fee', $processingFee);
                $item->setAttribute('final_exchange_rate', $finalRate);
            });
        }

        $records = UtilityService::appendAttribute($records, [
            [
                'name' => 'formatted_from_amount',
                'path' => 'transact.formatted_from_amount',
                'hide' => 'transact'
            ],
            [
                'name' => 'formatted_to_amount',
                'path' => 'transact.formatted_to_amount',
                'hide' => 'transact'
            ]
        ]);

        return response()->json($records);
    }

    public function findAllUserCasesWithPendingStatus()
    {
        try {
            $pendingCases = CaseService::findAllUserCasesWithPendingStatus();

            return response()->json($pendingCases);
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function findAllTransfers(Request $request)
    {
        $queryParams = $request->all();

        // \DB::enableQueryLog(); 

        $cases = CaseService::getCasesByRefTypeAndStatus('transfer', 'all');
        $cases->where([
            ['type', 'SEND'],
            ['user_bank_account_type', '!=', 'TOPKASH']
        ]);

        /**
         * To apply filter from request query params
         */

        if (isset($queryParams['id'])) {
            $cases->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['username'])) {
            $cases->where('user_username', 'like', '%' . $queryParams['username'] . '%');
        }

        if (isset($queryParams['topkash_id'])) {
            $cases->where('user_topkash_id', 'like', '%' . $queryParams['topkash_id'] . '%');
        }

        if (isset($queryParams['assigned_to'])) {
            $cases->whereIn('assigned_user_username', $queryParams['assigned_to']);
        }

        if (isset($queryParams['currency'])) {

            $cases->whereIn('from_currency_code', $queryParams['currency']);
        }

        if (isset($queryParams['country'])) {

            $country = CountryService::findCountryByIsoCode($queryParams['country']);

            $cases->whereIn('country_id', $country);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }
            
            $cases->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['processed_at'])) {
            $fromDate = $queryParams['processed_at'][0];
            $toDate = $queryParams['processed_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }
            
            $cases->whereBetween('processed_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['status'])) {
            $cases->whereIn('status', $queryParams['status']);
        }

        $cases->orderBy('created_at', 'desc');

        // Temporarily retrieve data order by descending
        $records = UtilityService::modelQueryBuilder($cases, $request);

        // dd(\DB::getQueryLog());

        return response()->json($records);
    }

    public function findAllPendingTransfers(Request $request)
    {
        $queryParams = $request->all();
        $userId = auth()->user()->id;

        // dd($queryParams['created_at']);

        // \DB::enableQueryLog(); 

        $pendingCases = CaseService::getCasesByRefTypeAndStatus('transfer', 1);
        $processingCases = CaseService::getCasesByRefTypeAndStatus('transfer', 2);

        // $processingCases->where('assigned_user_id', $userId);

        /**
         * To apply filter from request query params
         */

        if (isset($queryParams['id'])) {
            $pendingCases->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
            $processingCases->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['username'])) {
            $pendingCases->where('user_username', 'like', '%' . $queryParams['username'] . '%');
            $processingCases->where('user_username', 'like', '%' . $queryParams['username'] . '%');
        }

        if (isset($queryParams['topkash_id'])) {
            $pendingCases->where('user_topkash_id', 'like', '%' . $queryParams['topkash_id'] . '%');
            $processingCases->where('user_topkash_id', 'like', '%' . $queryParams['topkash_id'] . '%');
        }

        if (isset($queryParams['assigned_to'])) {
            $pendingCases->whereIn('assigned_user_username', $queryParams['assigned_to']);
            $processingCases->whereIn('assigned_user_username', $queryParams['assigned_to']);
        }

        if (isset($queryParams['from_currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['from_currency']);

            $pendingCases->whereIn('from_currency_id', $currency);
            $processingCases->whereIn('from_currency_id', $currency);
        }

        if (isset($queryParams['to_currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['to_currency']);

            $pendingCases->whereIn('to_currency_id', $currency);
            $processingCases->whereIn('to_currency_id', $currency);
        }


        if (isset($queryParams['country'])) {

            $country = CountryService::findCountryByIsoCode($queryParams['country']);

            $pendingCases->whereIn('country_id', $country);
            $processingCases->whereIn('country_id', $country);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $pendingCases->whereBetween('created_at', [$finalFromDate, $finalToDate]);
            $processingCases->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        $cases = $pendingCases->union($processingCases)->orderBy('created_at', 'asc');

        /**
         * To apply filter from request query params
         */

        $records = UtilityService::modelQueryBuilder($cases, $request);

        // dd(\DB::getQueryLog());

        return response()->json($records);
    }

    public function findAllReloads(Request $request)
    {
        $queryParams = $request->all();
        //
        // \DB::enableQueryLog(); 

        $cases = CaseService::getCasesByRefTypeAndStatus('reload', 'all');

        /**
         * To apply filter from request query params
         */

        if (isset($queryParams['id'])) {
            $cases->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $cases->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['processed_at'])) {
            $fromDate = $queryParams['processed_at'][0];
            $toDate = $queryParams['processed_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $cases->whereBetween('processed_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['currency'])) {
            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['currency']);

            $cases->whereIn('reload_currency_id', $currency);
        }

        if (isset($queryParams['country'])) {
            $country = CountryService::findCountryByIsoCode($queryParams['country']);

            $cases->whereIn('country_id', $country);
        }

        if (isset($queryParams['bank_account_no'])) {
            $cases->where('bank_account_no', 'like', '%' .$queryParams['bank_account_no']. '%');
        }

        if (isset($queryParams['reference_no'])) {
            $cases->where('reference_no', 'like', '%' . $queryParams['reference_no'] . '%');
        }

        if (isset($queryParams['user_username'])) {
            $cases->where('user_username', 'like', '%' . $queryParams['user_username'] . '%');
        }

        if (isset($queryParams['user_topkash_id'])) {
            $cases->where('user_topkash_id', 'like', '%' . $queryParams['user_topkash_id'] . '%');
        }

        if (isset($queryParams['assigned_to'])) {
            $cases->whereIn('assigned_user_username', $queryParams['assigned_to']);
        }

        if (isset($queryParams['status'])) {
            $cases->whereIn('status', $queryParams['status']);
        }

        $cases->orderBy('created_at', 'desc');

        $records = UtilityService::modelQueryBuilder($cases, $request);

        //  dd(\DB::getQueryLog());

        return response()->json($records);
    }

    public function findAllPendingReloads(Request $request)
    {
        $queryParams = $request->all();
        $userId = auth()->user()->id;
        //

        $pendingCases = CaseService::getCasesByRefTypeAndStatus('reload', 1);
        $processingCases = CaseService::getCasesByRefTypeAndStatus('reload', 2);

        // $processingCases->where('assigned_user_id', $userId);

        /**
         * To apply filter from request query params
         */

        if (isset($queryParams['id'])) {
            $pendingCases->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
            $processingCases->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $pendingCases->whereBetween('created_at', [$finalFromDate, $finalToDate]);
            $processingCases->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['currency'])) {

            $currency = CurrencyService::findCurrencyByIsoCode($queryParams['currency']);

            $pendingCases->whereIn('reload_currency_id', $currency);
            $processingCases->whereIn('reload_currency_id', $currency);
        }

        if (isset($queryParams['country'])) {

            $country = CountryService::findCountryByIsoCode($queryParams['country']);

            $pendingCases->whereIn('country_id', $country);
            $processingCases->whereIn('country_id', $country);
        }

        if (isset($queryParams['bank_account_no'])) {
            $pendingCases->where('bank_account_no', 'like', '%' .$queryParams['bank_account_no']. '%');
            $processingCases->where('bank_account_no', 'like', '%' .$queryParams['bank_account_no']. '%');
        }

        if (isset($queryParams['reference_no'])) {
            $pendingCases->where('reference_no', 'like', '%' . $queryParams['reference_no'] . '%');
            $processingCases->where('reference_no', 'like', '%' . $queryParams['reference_no'] . '%');
        }

        if (isset($queryParams['user_topkash_id'])) {
            $pendingCases->where('user_topkash_id', 'like', '%' . $queryParams['user_topkash_id'] . '%');
            $processingCases->where('user_topkash_id', 'like', '%' . $queryParams['user_topkash_id'] . '%');
        }

        if (isset($queryParams['assigned_to'])) {
            $pendingCases->whereIn('assigned_user_username', $queryParams['assigned_to']);
            $processingCases->whereIn('assigned_user_username', $queryParams['assigned_to']);
        }

        $cases = $pendingCases->union($processingCases)->orderBy('created_at', 'asc');

        /**
         * To apply filter from request query params
         */

        $records = UtilityService::modelQueryBuilder($cases, $request);

        return response()->json($records);
    }
}
