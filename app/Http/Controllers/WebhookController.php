<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\CurrencyService;
use App\Services\RedeemService;
use App\Services\UserService;
use Carbon\Carbon;
use Throwable;

class WebhookController extends BaseController
{
    public function handleGiftpaceRequest(Request $request)
    {
        try {
            $code = $request->input('code');
            $currencyCode = $request->input('currency_code');
            $username = $request->input('username');
            $amount = $request->input('amount');
            $timestamp = $request->input('currentTimestamp');
            $requestHashed = $request->input('hashed');

            $user = UserService::getUserByUsername($username);

            // $signature = $code.'|'.$username.'|'.$user->phone_no.'|'.$timestamp.'|'.$platformCode.'|'.strtoupper($currencyCode).'|'.env('GIFTPACE_SECRET_KEY');
            $signature = $timestamp.'|'.$amount.'|'.$username.'|'.strtoupper($currencyCode).'|'.$code.'|'.env('GIFTPACE_SECRET_KEY');
            $hashed = md5($signature);

            if($requestHashed !== $hashed) {
                return response()->json([
                    'message' => 'Checksum verification failed'
                ], 400);
            }

            $currency = CurrencyService::findOneByCode($currencyCode);

            if(!$user) {
                return response()->json([
                    'message' => 'Invalid user'
                ], 400);
            }

            $redeemRecord = RedeemService::findVoucherRedeemByUserIdAndVoucherCode($user->id, $code, 'success');

            if(!$redeemRecord) {
                $currency = CurrencyService::findOneByCode($currencyCode);

                RedeemService::createVoucherRedeemRecord([
                    'user_id' => $user->id,
                    'voucher_code' => $code,
                    'currency_id' => $currency->id,
                    'amount' => $amount,
                    'claimed_at' => Carbon::createFromTimestamp($timestamp),
                    'status' => 'success'
                ]);

                return response()->json([
                    'message' => 'Successfully redeem voucher with code' .$code
                ]);
            }
            else {
                return response()->json([
                    'message' => 'Successfully redeem voucher with code' .$code
                ]);
            }
        }
        catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 404);
        }
    }
}
