<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Throwable;

use App\Services\ProcessingFeeTieredPercentageRateService;

class ProcessingFeeTieredPercentageRateController extends BaseController
{
    public function findAllProcessingFeeTieredPercentageRates()
    {
        return ProcessingFeeTieredPercentageRateService::findAllProcessingFeeTieredPercentageRates();
    }

    public function createProcessingFeeTieredPercentageRate(Request $request)
    {
        try {
            $this->validate($request, [
                'from_currency_id' => ['required', 'integer'],
                'to_currency_id' => ['required', 'integer'],
                'percentage_rate' => ['required', 'numeric'],
                'from_amount' => ['required', 'numeric'],
                'to_amount' => ['required', 'numeric'],
                'date' => ['required', 'date'],
            ]);


            $fromCurrencyId = $request->input('from_currency_id');
            $toCurrencyId =  $request->input('to_currency_id');
            $percentageRate =  floatval($request->input('percentage_rate'));
            $fromAmount =  floatval($request->input('from_amount'));
            $toAmount =  floatval($request->input('to_amount'));
            $date =  $request->input('date');

            $processingFeeTieredPercentageRateId = ProcessingFeeTieredPercentageRateService::createProcessingFeeTieredPercentageRate([
                'from_currency_id' => $fromCurrencyId,
                'to_currency_id' => $toCurrencyId,
                'percentage_rate' => $percentageRate,
                'from_amount' => $fromAmount,
                'to_amount' => $toAmount,
                'date' => $date,
            ]);

            return $processingFeeTieredPercentageRateId;
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }
}
