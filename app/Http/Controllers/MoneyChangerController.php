<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\MoneyChangerService;
use App\Services\AddressService;
use App\Services\UtilityService;
use App\Services\UserService;

use App\Services\OperationService;


class MoneyChangerController extends BaseController
{
    public function createMoneyChanger(Request $request)
    {

        try{

           
            $name = $request->input('name');
            $is_name_unique = MoneyChangerService::checkUniqueName($name);

            if (!$is_name_unique) {
                return response()->json(['message' => 'Business Name existed'], 400);
            }

            $createdAddressId = null;
            $address = [];

            // print_r( $request->input());
            $data = [];
            $data['name'] = $request->input('name');
            $data['pic'] = $request->input('pic');
            $data['email'] = $request->input('email');
            $data['phone'] = $request->input('phone');
            $data['is_active'] = $request->input('is_active');
            $data['currency_available'] = $request->input('currency_available');
            $data['operation_days'] = $request->input('operation_days');
            $data['operation_hours'] = $request->input('operation_hours');
            $data['emergency_pic'] = $request->input('emergency_pic');
            $data['emergency_phone'] = $request->input('emergency_phone');
            $data['longitude'] = $request->input('longitude');
            $data['latitude'] = $request->input('latitude');
            $data['google_map'] = $request->input('google_map');
            $data['waze_map'] = $request->input('waze_map');
            $data['is_active'] = $request->input('is_active');

            $operationArray = $request->input('operations');
            $assignUserId = $request->input('assign_users');

            $money_changer = MoneyChangerService::createNewMoneyChanger($data);

            if($request->input('address_1') ){
                $address['address_1'] = $request->input('address_1');
                $address['address_2'] = $request->input('address_2');
                $address['city'] = $request->input('city');
                $address['postcode'] = $request->input('postcode');
                $address['state_id'] = $request->input('state');
                $address['country_id'] = $request->input('country');
                $address['reference_table'] = 'money_changers';
                $address['reference_id'] = $money_changer;

                $createdAddressId = AddressService::createAddress($address);

            }
            if($request->input('assign_users')){
                $assignUser = [];
               
                $assignUser['money_changer_id'] = $money_changer;
    
                $updateUser = UserService::updateUserById($assignUser,$assignUserId);
    
            }


            if($request->input('operations')){
                foreach ($operationArray as $operation) {
                    $item = json_decode($operation);
                    $operation = [];
                    $operation['days'] = $item->days;
                    $operation['hours'] = $item->hours;
                    $operation['is_closed'] = $item->is_closed;

                    $countryCurrency = OperationService::createNewOperation($operation,$money_changer);
                }
            }


            return response()->json(['message' => 'Added new money changer'], 200);


        }catch(Throwable $e){
            return response()->json(['message' => $e->getMessage()], 400);

        }
    }
    public function moneyChanger(Request $request) {

        $queryParams = $request->all();


        $moneyChanger = MoneyChangerService::findAllMoneyChanger();

        if (isset($queryParams['name'])) {
            $moneyChanger->where('name', 'like', '%' .$queryParams['name']. '%');
        }

        // print_r($queryParams);
        // if (isset($queryParams['currency'])) {
        //     $moneyChanger->where('currency_available', 'like', '%'.$queryParams['currency'].'%');
        // }

        if (isset($queryParams['is_active'])) {
            $moneyChanger->whereIn('is_active', $queryParams['is_active']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $moneyChanger->whereBetween('money_changers.created_at', [$finalFromDate, $finalToDate]);
        }
        
        

       $records = UtilityService::modelQueryBuilder($moneyChanger, $request);

        return response()->json($records);
    }

    public function findMoneyChangerById($id)
    {
        $moneyChanger = MoneyChangerService::findMoneyChangerById($id);

        return response()->json(
            $moneyChanger
        );
    }
    
    public function updateMoneyChangerById($id, Request $request)
    {

        // print_r($request->input());

        $addressId = $request->input('reference_id');

        $data = [];
        $data['name'] = $request->input('name');
        $data['pic'] = $request->input('pic');
        $data['email'] = $request->input('email');
        $data['phone'] = $request->input('phone');
        $data['is_active'] = $request->input('is_active');
        $data['currency_available'] = $request->input('currency_available');
       
        $data['emergency_pic'] = $request->input('emergency_pic');
        $data['emergency_phone'] = $request->input('emergency_phone');
        $data['longitude'] = $request->input('longitude');
        $data['latitude'] = $request->input('latitude');
        $data['google_map'] = $request->input('google_map');
        $data['waze_map'] = $request->input('waze_map');
        $data['is_active'] = $request->input('is_active');

        $address['address_1'] = $request->input('address_1');
        $address['address_2'] = $request->input('address_2');
        $address['city'] = $request->input('city');
        $address['postcode'] = $request->input('postcode');
        $address['state_id'] = $request->input('state');
        $address['country_id'] = $request->input('country');


        $address['reference_table'] = $request->input('reference_table');

        // $data['assign_user'] = $request->input('assign_user');
        $assignUserId = $request->input('assign_users');
        $operationArray = $request->input('operations');
        $updateAddressId = null;
        $updateUser = null;

        $operations = null;

        print_r($request->input());
        if($addressId){
            $address['reference_id'] = $request->input('reference_id');

            $updateAddressId = AddressService::updateAddressByReferenceIdReferenceTable($address);
            // $updateAddressId = AddressService::updateAddressByIdForActivityLog($address, $addressId);

        }else if(!$addressId && ($request->input('address_1'))) {
            $address['reference_id'] = $id;
            $updateAddressId = AddressService::createAddress($address);
            // $data["address_id"] = $createdAddressId;
        }
        if($request->input('assign_users')){
            $moneyChangerUsers = UserService::findAllUsersByMoneyChangerId($id);
            $assignUser = [];
             foreach ($moneyChangerUsers as $item) {
                $assignUser = [];
                $assignUser['money_changer_id'] = null;

                $updateUser = UserService::updateUserById($assignUser,$item->id);
            }

                $assignUser['money_changer_id'] = $id;

                $updateUser = UserService::updateUserById($assignUser,$assignUserId);

        }


        if($request->input('operations')){
            foreach ($operationArray as $operation) {
                $operationId = $operation['id'];

                $operations[] = OperationService::updateOperationById($operation,$operationId);
            }
        }


        $updated = MoneyChangerService::updateMoneyChangerById($data, $id);


        if ($updated || $updateAddressId || $updateUser|| in_array(true, $operations)) {
            return response()->json([
                'message' => "Successfully update dashboard user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }


}
