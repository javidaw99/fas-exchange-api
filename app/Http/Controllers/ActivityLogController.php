<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\ActivityLog;
use App\Models\Role;
use App\Models\Transaction;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\ActivityLogService;
use App\Services\UserService;
use App\Services\UtilityService;
use Throwable;
use Carbon\Carbon;
use function _\map;

class ActivityLogController extends BaseController
{

    public function activityLogs(Request $request)
    {

        try {
            // DB::enableQueryLog();
            $activityLogs = ActivityLogService::findAllActivityLogs();

            $queryParams = $request->all();

            /**
            * To apply filter from request query params
            */
   
            // if (isset($queryParams['created_by'])) {
            //     $activityLogs->where('created_by', '=', $queryParams['created_by']);
            // }

            if (isset($queryParams['created_by'])) {
                $activityLogs->whereIn('created_by', $queryParams['created_by']);
            }

           if (isset($queryParams['description'])) {
               $activityLogs->where('description', 'like', '%'.$queryParams['description'].'%');
           }

           if (isset($queryParams['section'])) {
                preg_match('/(^transfers)/', $queryParams['section'], $matchedRef);

                if (count($matchedRef) > 0) {
                    $section = preg_replace('/_[A-Z]+/', '', $queryParams['section']);
                    $type = preg_replace('/(^transfers_)/', '', $queryParams['section']);
                    $typeId = Transaction::PREFIX_REVERT[$type];

                    $activityLogs->where([
                        ['activity_logs.reference_table', '=', $section],
                        ['transactions.type_id', $typeId]
                    ]);
                }
                else if($queryParams['section'] == 'app_user' || $queryParams['section'] == 'users') {

                    $roleId = [Role::TYPES['app_user']];
                    
                    if($queryParams['section'] == 'users') {
                        $roleId = [Role::TYPES["customer_service"], Role::TYPES["superadmin"]];
                    }

                    $activityLogs->leftJoin('users', 'activity_logs.reference_id', 'users.id')
                        ->where('activity_logs.reference_table', '=', 'users')
                        ->whereIn('users.role_id', $roleId);
                } 
                else {
                    $activityLogs->where('activity_logs.reference_table', '=', $queryParams['section']);
                } 
            }
                       
           if (isset($queryParams['created_at'])) {
               $fromDate = $queryParams['created_at'][0];
               $toDate = $queryParams['created_at'][1];
               $fromCarbon = new Carbon($fromDate);
               $toCarbon = new Carbon($toDate);
               if (isset($queryParams['localTimeOffset'])) {
                   $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                   $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
               }
               else 
               {
                   $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                   $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
               }
   
               $activityLogs->whereBetween('activity_logs.created_at', [$finalFromDate, $finalToDate]);
           }

            $records = UtilityService::modelQueryBuilder($activityLogs, $request);

            // dd(DB::getQueryLog());

            $alteredData = $records->getCollection()->each(function ($item) {

                preg_match('/(^transfers$)/', $item->reference_table, $matchedRef);

                if(count($matchedRef) > 0) {
                    $type = preg_replace('/[0-9]+/', '', $item->doc_id);
                    $section = $item->reference_table . "_" . $type;

                    $item->section = ActivityLog::REVERT[$section];
                }
                else {
                    $item->section = ActivityLog::REVERT[$item->reference_table];

                    if ($item->reference_table === 'users') {
                        $user = UserService::findOneUserById($item->reference_id);
    
                        if(!empty($user)) {
                            $item->section = Role::REVERT_NAME[$user->role_id];
                        }
                        else {
                            $item->section = 'N/A';
                        }
                    }
                }
            });

            $records->setCollection($alteredData);

            return response()->json($records);
        } catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage(),
            ], 500);
        }


    }

    public function findActivityLogSections() 
    {
        try {

            $section = ActivityLog::REVERT;

            $section = map($section, function($value, $key) {
                return [
                    "label" => $value,
                    "value" => $key
                ];
            });

            // $section = map($section, function($value, $key) {
            //     if($key !== 'users') {
            //         return [
            //             "label" => $value,
            //             "value" => $key
            //         ];
            //     }
            // });

          

            return response()->json($section);
        }
        catch(Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function findAllByReferenceTableReferenceID($referenceTable,  $referenceId)
    {

        try {
            $activityLogs = ActivityLogService::findAllByReferenceTableReferenceID($referenceTable, $referenceId);

            return response()->json($activityLogs);
        } catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function createActivityLog(Request $request)
    {

        $this->validate($request, [
            'reference_table' => ['required', 'string'],
            'reference_id' => ['required', 'integer'],
            'action_type' => ['required', 'string'],
            'action_source' => ['required', 'string'],
            'created_by' => ['required', 'integer'],
            'description' => ['required', 'string'],
        ]);

        $values = $request->all();

        return ActivityLogService::createActivityLog($values);
    }
}
