<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\SocketService;

class SocketController extends BaseController
{
    public function verifyConnection(Request $request)
    {
        $headers = SocketService::verifyOrigin($request->headers->get('origin'));

        $this->validate($request, [
            'token' => ['required'],
            'user' => ['required', 'integer'],
        ]);

        
        try {
            SocketService::verifyToken($request->input('token'), $request->input('user'));
        } catch (\Throwable $th) {
            return response([
                'message' => $th->getMessage()
            ], 400);
        }

        return response('Verified', 200)->withHeaders($headers);
    }
 
}
