<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class SettingController extends BaseController
{
    public function index()
    {
        $settings = DB::table('general_settings')
        ->select('key', 'value', 'type', 'comment')
        ->get();

        $settings = $settings->mapWithKeys(function ($item, $key) {
            return [$item->key => $item];
        });

        return response()->json($settings->all());
    }

    public function updateSettingByKey($key, Request $request)
    {
        $this->validate($request, [
            'value' => 'required'
        ]);

        $value = $request->input('value');

        $setting = DB::table('general_settings')->where('key', $key)->first();

        if(!is_null($setting)) {
            DB::table('general_settings')->where('key', $key)->update([
                'value' => $value
            ]);

            return response()->json(['message' => 'Successfully update this setting'], 200);
        }
        else {
            return response()->json(['message' => 'Key is not found'], 400);
        }
    }
}
