<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\UserAccount;
use App\Services\TransactionService;
use App\Services\UtilityService;
use App\Services\UserService;
use App\Services\MailService;
use App\Services\AccountService;
use App\Services\AddressService;
use Throwable;
use RobThree\Auth\TwoFactorAuth;
use Symfony\Component\HttpFoundation\Cookie;
use Jenssegers\Agent\Agent;
use App\Services\NotificationService;
use Illuminate\Support\Arr;
use App\Services\ActivityLogService;

use function _\every;


class UserController extends BaseController
{
    /**
     * App User
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = UserService::getUserByUsername($request->input('username'));

        if (!empty($user)) {

            if (Role::TYPES['app_user'] !== $user->role_id) {

                if (Hash::check($request->input('password'), $user->password)) {
                    $token = UserService::generateUserToken($user->id, $user->role_id);
                    // $socketToken = UserService::generateUserToken($user->id, $user->role_id, 'socket');

                    $status = UserService::checkTfaStatusById($user->id);

                    $domain = UtilityService::generateCookieDomain($request);

                    $agent = new Agent();

                    // If detected it is from postman, ignore strict rule for cookie
                    if ($agent->isRobot()) {
                        $cookie = Cookie::create('jwt', $token, strtotime("+24 hours"));
                    } else {
                        $cookie = Cookie::create('jwt', $token, strtotime("+24 hours"), '/', $domain, TRUE, TRUE, FALSE, 'None');
                    }

                    if ($user->is_force_login) {

                        UserService::resetForceLoginById($user->id);
                    }

                    $role = UserService::findRoleById($user->role_id);

                    $permissions = Role::PERMISSIONS[$role->code];

                    $user = (object)$user->only(['id', 'username', 'fullname', 'email', 'nationality_id', 'phone_no', 'is_temp_password']);

                    return response()->json([
                        'message' => 'Successfully login',
                        'user' => $user,
                        'tfa_status' => $status,
                        'permissions' => $permissions,
                        'token' => $token
                    ])->cookie($cookie);
                } else {
                    return response()->json(['message' => 'Incorrect password'], 400);
                }
            } else {
                return response()->json(['message' => 'Unauthorized user.'], 400);
            }
        } else {
            return response()->json(['message' => 'Incorrect username'], 400);
        }
    }

    public function logout(Request $request)
    {

        $domain = UtilityService::generateCookieDomain($request);

        $cookie = Cookie::create('jwt', null, 0, '/', $domain, TRUE, TRUE, FALSE, 'None');

        return response()->json(["message" => 'done'])->cookie($cookie);
    }

    public function bankAccounts(Request $request)
    {

        $country = $request->query('country');

        $user = auth()->user();

        if (is_null($country)) {
            $bankAccounts = $user->personalBanks();
        } else {
            $bankAccounts = $user->personalBanks()
                ->whereHas('country', function ($query) use ($country) {
                    $query->where('iso_code_2', $country)
                        ->orWhere('iso_code_3', $country);
                });
        }

        $result = $bankAccounts->get()->pluck('userbank');

        return response()->json($result);
    }

    public function profileAccount(Request $request)
    {

        $user = auth()->user();

        $userId = $user->id;

        $userAccounts = AccountService::getUserCurrencyAccountsWithLatestTransaction($userId);

        return response()->json([
            'profile' => $user->makeHidden(['accounts', 'deleted_at']),
            'accounts' => $userAccounts
        ]);
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'fullname' => ['required'],
            'nationality_id' => ['required', 'integer'],
            'phone_no' => ['required'],
            'email' => ['required', 'email']
        ]);

        $data = [];
        $data['fullname'] = $request->input('fullname');
        $data['nationality_id'] = $request->input('nationality_id');
        $data['phone_no'] = $request->input('phone_no');
        $data['email'] = $request->input('email');

        $userId = $request->input('user_id');

        $updated = UserService::updateUserById($data, $userId);
        $user = UserService::findUserById($userId);

        if ($updated) {
            return response()->json([
                'message' => "Successfully update user info.",
                'user' => [
                    'fullname' => $user->fullname,
                    'nationality_id' => $user->nationality_id,
                    'phone_no' => $user->phone_no,
                    'email' => $user->email
                ]
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }

    public function updateUserDetails(Request $request)
    {

        $this->validate($request, [
            'app_user_id' => ['required', 'integer']
        ]);

        $data = [];

        $data['fullname'] = $request->input('fullname');
        $data['username'] = $request->input('username');
        $data['email'] = $request->input('email');
        $data['dob'] = $request->input('dob');
        $data['phone_no'] = $request->input('phone_no');
        $data['id_type'] = $request->input('id_type');
        $data['id_number'] = $request->input('id_number');
        $data['marital_status_id'] = $request->input('marital_status_id');
        $data['gender'] = $request->input('gender');
        $userId = $request->input('app_user_id');

        $updated = UserService::updateUserById($data, $userId);

        if ($updated) {
            return response()->json([
                'message' => "Successfully update user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }

    public function updateAddressById(Request $request)
    {
        $this->validate($request, [
            'address_1' => ['required'],
            'city' => ['required'],
            'postcode' => ['required'],
            'country_id' => ['required'],
            'address_id' => ['required'],
        ]);
        $address = [];
        $address['address_1'] = $request->input('address_1');
        $address['address_2'] = $request->input('address_2');
        $address['city'] = $request->input('city');
        $address['postcode'] = $request->input('postcode');
        $address['country_id'] = $request->input('country_id');
        $address['address_id'] = $request->input('address_id');

        $addressId = $request->input('address_id');

        $updated = AddressService::updateAddressById($address, $addressId);

        if ($updated) {
            return response()->json([
                'message' => "Successfully update user's address.",

            ], 200);
        } else {
            return response()->json([
                'message' => "Nothing updated",

            ], 202);
        }
    }

    public function updateAppUser(Request $request)
    {

        $this->validate($request, [
            'fullname' => ['required'],
            'country_id' => ['required', 'integer'],
            'phone_no' => ['required'],
            'app_user_id' => ['required', 'integer']
        ]);

        $data = [];
        $data['fullname'] = $request->input('fullname');
        $data['country_id'] = $request->input('country_id');
        $data['phone_no'] = $request->input('phone_no');
        $userId = $request->input('app_user_id');

        $updated = UserService::updateUser($data, $userId);

        if ($updated) {
            return response()->json([
                'message' => "Successfully update user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }



    public function updateDashboardUser(Request $request)
    {

        $this->validate($request, [
            'status' => ['required'],
            'fullname' => ['required'],
            'phone_no' => ['required'],
            'email' => ['required', 'email'],
            'dashboard_user_id' => ['required']

        ]);

        $data = [];
        $data['status'] = $request->input('status');
        $data['fullname'] = $request->input('fullname');
        $data['phone_no'] = $request->input('phone_no');
        $data['email'] = $request->input('email');
        $userId = $request->input('dashboard_user_id');
        $updated = UserService::updateUserById($data, $userId);

        //        //Unique email checking
        //    $email = $request->input('email');
        //    $is_email_unique = UserService::checkUniqueEmail($email);

        //    if (!$is_email_unique) {
        //        return response()->json(['message' => 'Email existed'], 400);
        //    } 

        if ($updated) {
            return response()->json([
                'message' => "Successfully update dashboard user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }

    public function forgetPassword(Request $request)
    {

        $this->validate($request, [
            'email' => 'required'
        ]);

        try {

        $email = $request->input('email');
        $user = UserService::getUserByEmail($email);

        if($user['password'] == null) {
            return response()->json([
                'message' => "New user detected, you're not allowed to proceed with this action",
            ], 400);
        }

        // // Encrypt user detail for onboarding email
        $encryptedUserDetailCipherText = UserService::generateEncryptedUserDetail($user);  
            
            // Sending email 
            $payload = [
                "to" =>  $email,
                'fullname' => $user->fullname,
                'username' => $user->username,
                'url' =>  env('RESET_PASSWORD_URL_LOCAL') . '/' . $encryptedUserDetailCipherText
            ];

            MailService::sendResetPasswordEmail($payload);

            UserService::updateIsResetPasswordById($user['id']);

            return response()->json(['message' => 'Successfully sent to your email'], 200);
            
        } catch (Throwable $e) {
            return response()->json(['message' =>'Successfully sent to your email']);
        

        }
    }


    public function checkResetPasswordLinkExpiry(Request $request)
    {
        try {
            $this->validate($request, [
                'e' => 'required'
            ]);
    
            $link = $request->query('e');
    
            $result = UserService::verifyEncryptedUserDetail($link);

            $user = UserService::getUserByUsername($result['username']);

            // dd($result['username']);

            if(!$result['is_valid'] ) {
                return response()->json([
                    'message' => 'Password link is expired',
                ], 400);
            }

            $current_time = Carbon::now()->timestamp;

            $new_pwd_timestamp = $result['timestamp'];

            $calc_time_passed = $current_time - $new_pwd_timestamp;


            if ($calc_time_passed > 86400) {

                return response()->json(['message' => 'Password link is expired','username' => $user->username],400);
            } else {
                return response()->json(['message' => 'Password link is available', 'username' => $user->username]);
            }

        }
        catch(Throwable $th) {
            return response()->json([
                'message' => 'Invalid link'
            ], 400);
        }
    }

    public function checkNewPasswordLink(Request $request)
    {
        try {
            $this->validate($request, [
                'e' => 'required'
            ]);
    
            $link = $request->query('e');
    
            $result = UserService::verifyEncryptedNewUserDetail($link);

            $user = UserService::getUserByUsername($result['username']);

            if(!$result['is_valid']) {
                return response()->json([
                    'message' => 'Password link is invalid or expired', 'username' => $user->username
                ], 400);
            }

            $current_time = Carbon::now()->timestamp;

            $new_pwd_timestamp = $result['timestamp'];

            $calc_time_passed = $current_time - $new_pwd_timestamp;


            if ($calc_time_passed > 86400) {

                return response()->json(['message' => 'Password link is expired','username' => $user->username],400);
            } else {
                return response()->json(['message' => 'Password link is available', 'username' => $user->username]);
            }

        }
        catch(Throwable $th) {
            return response()->json([
                'message' => 'Invalid link',
            ], 400);
        }
    }


    public function createNewPassword(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
            'e' => 'required',
        ]);

        $encryptedDetail = $request->input('e');
    
        $result = UserService::verifyEncryptedUserDetail($encryptedDetail);

        if($request->input('username') !==  $result['username'] ) {
            return response()->json([
                'message' => 'Invalid Username'
            ], 400);
        }

        $current_time = Carbon::now()->timestamp;

        $new_pwd_timestamp = $result['timestamp'];

        $calc_time_passed = $current_time - $new_pwd_timestamp;


        if ($calc_time_passed > 86400) {

            return response()->json(['message' => 'Password link is expired'],400);
        }

        $user = UserService::getUserByUsername($request->input('username'));

        User::where('id',   $user->id)
            ->update([
                'password' =>  Hash::make($request->input('password')),
            ]);

        return response()->json([
            'message' => 'Successfully create password',
        ]);
    }

    public function resetPassword(Request $request)
    {

        $this->validate($request, [
            'password' => 'required',
            'e' => 'required',
        ]);

        $encryptedDetail = $request->input('e');
        $result = UserService::verifyEncryptedUserDetail($encryptedDetail);
       
        $user = UserService::getUserByUsername($result['username']);

        if($result['is_resetting_pwd'] == 0) {
            return response()->json([
                'message' => 'Password link is expired'
            ], 400);
        }

        User::where('id',   $user->id)
            ->update([
                'password' =>  Hash::make($request->input('password')),
                'is_resetting_pwd' => 0
            ]);

        return response()->json([
            'message' => 'Successfully reset password',
        ]);


    }



    /**
     * Dashboard User
     */

    public function getUsers(Request $request)
    {

        $users = User::with([
            'country' => function ($query) {
                $query->select('id', 'name', 'iso_code_2');
            },
            'address' => function ($query) {
                $query->join('countries', 'countries.id', '=', 'addresses.country_id')->select('addresses.*', 'countries.name as country');
            },
            // ->join('countries', 'addresses.country_id', '')
        ])
            ->join('roles', 'users.role_id', 'roles.id')
            ->select('users.*', 'roles.name as role');
        // ->where('roles.id', '=', 2);

        $searchingFields = array('username', 'phone_no');

        $records = UtilityService::modelQueryBuilder($users, $request, $searchingFields);

        return response()->json($records);
    }

    public function findUserById($id)
    {
        $user = UserService::findOneUserById($id);

        if($user['password'] == null) {

            $current_time = Carbon::now()->timestamp;

            $date_created = Carbon::createFromFormat('Y-m-d H:i:s', $user['created_at'])->timestamp;

            $calc_time_passed = $current_time - $date_created;

            if ($calc_time_passed > 86400) {
                $user->resend_link = true;
            } 
        }

        return response()->json(
            $user
        );
    }

    public function getDashboardUsers(Request $request)
    {

        $users = User::with([
            'country' => function ($query) {
                $query->select('id', 'name', 'iso_code_2');
            }
        ])
            ->join('roles', 'users.role_id', 'roles.id')
            ->select('users.*', 'roles.name as role')
            ->where('role_id', '!=', 2);


        $searchingFields = array('fullname', 'username', 'email', 'phone_no', 'status');

        $records = UtilityService::modelQueryBuilder($users, $request, $searchingFields);
        

        return response()->json($records);
    }

    public function getRoles()
    {

        $roles = Role::select('id', 'name', "code")
            ->whereNull('deleted_at')
            ->where('id', '!=', 2)
            ->get();

        return response()->json($roles);
    }

    public function addUser(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'fullname' => 'required',
            'email' => 'required|email',
            'phone_no' => 'required',
            // 'nationality_id' => 'required'
        ]);

        try {

            $username = $request->input('username');
            $is_username_unique = UserService::checkUniqueUsername($username);

            if (!$is_username_unique) {
                return response()->json(['message' => 'Username existed'], 400);
            } else if (preg_match('/\s/', $username)) {
                return response()->json(['message' => 'No space allowed in username'], 400);
            }

            // $phone_no = UtilityService::sanitisePhoneNumber($request->input('nationality_id'), $request->input('phone_no'));

            $pwd = UserService::generateTemporaryPassword();

            $data = [];
            $data['username'] = $request->input('username');
            $data['fullname'] = $request->input('fullname');
            $data['email'] = $request->input('email');
            $data['phone_no'] = $request->input('phone_no');
            $data['password'] = $pwd['hashed_temp_password'];
            // $data['nationality_id'] = $request->input('nationality_id');
            $data['is_temp_password'] = 1;

            $user_id = UserService::createNewUser($data, 'Customer Service');

            $user = UserService::findUserById($user_id);

            $payload = [
                "to" => $request->input('email'),
                "fullname" => $user->fullname,
                "password" => $pwd['temp_password'],
                'url' => env('SIGN_IN_URL_LOCAL')
            ];

            
            // MailService::sendWelcomeTempPasswordEmail($payload);

            return response()->json(['message' => 'Added new user'], 200);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function changePassword(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required'
        ]);

        $user = UserService::findUserById($request->get('user_id'));

        if (Hash::check($request->input('old_password'), $user->password)) {

            UserService::updatePasswordByUserId(
                [
                    'user_id' => $request->get('user_id'),
                    'new_password' => Hash::make($request->input('new_password'))
                ]
            );

            return response()->json([
                'message' => 'Successfully change password',
            ]);
        } else {
            return response()->json(['message' => 'Your old password is incorrect'], 400);
        }
    }

    public function saveFcmDetails(Request $request)
    {

        $this->validate($request, [
            'fcm_token' => 'required',
        ]);

        $fcmToken = $request->input('fcm_token');

        $saveFcmDetails = UserService::saveFcmDetails($fcmToken);

        if ($saveFcmDetails) {
            return response('OK');
        }

        return response('Fcm token exist');
    }

    public function generateTfaQrCode()
    {

        $tfa = new TwoFactorAuth('Money Exchange');

        $secret = $tfa->createSecret();

        $user = auth()->user();

        $username = $user->username;

        $store_secret = UserService::storeTfaSecret($secret, $user->id);

        if ($store_secret) {

            $qr_code = $tfa->getQRCodeImageAsDataUri($username, $secret);

            return response()->json(['qr_code' => $qr_code]);
        } else {

            return response()->json(['error' => 'Failed to update secret'], 400);
        }
    }

    public function tfaVerification(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
        ]);

        $tfa = new TwoFactorAuth('Money Exchange');

        $user = auth()->user();

        $secret =  UserService::getTfaSecret($user->id);

        $code = $request->input('code');

        $status = UserService::checkTfaStatusById($user->id);

        //Login
        if ($status) {

            if ($tfa->verifyCode($secret, $code) === true) {

                return response()->json(['verification_status' => 1]);
            } else {

                return response()->json(['message' => 'Incorrect Login Verification Code'], 400);
            }
        } else {

            if ($tfa->verifyCode($secret, $code) === true) {

                $enableTfa = UserService::enableTfa($user->id);

                return response()->json(['is_tfa_enabled' => true]);
            } else {

                return response()->json(['message' => 'Incorrect Verification Code'], 400);
            }
        }
    }

    public function checkTfaStatusById(Request $request, $user_id = null)
    {

        $isForLogin = true;
        $user = auth()->user();

        if (is_null($user_id)) {
            $userId = $user->id;
            $isForLogin = false;
        }

        $status = UserService::checkTfaStatusById($userId);
        $secretExist = UserService::checkIsSecretExisted($userId);

        if ($isForLogin) {
            return $status;
        }

        return response()->json([
            'is_tfa_enabled' => $status,
            'is_tfa_secret_enabled' => $secretExist
        ]);
    }

    public function handleTfa(Request $request)
    {

        $user = auth()->user();

        $status = UserService::enableTfa($user->id);

        return response()->json($status);
    }

    public function resetTfa(Request $request)
    {

        $user = auth()->user();

        $status = UserService::resetTfa($user->id);

        return response()->json([
            'is_tfa_enabled' => 0,
            'is_tfa_secret_enabled' => false
        ]);
    }

    public function forceDashboardUserLoginAgain()
    {

        UserService::forceLogin();

        return response()->json([
            'is_force_login' => 1,
            'message' => "Successfully enable force login to all dashboard users"
        ]);
    }




    // public function findUserTransactionsByUserId(Request $request)
    // {
    //     $this->validate($request, [
    //         'app_user_id' => ['required', 'integer'],
    //     ]);


    //     $user = auth()->user();
    //     $userId = $request->query('app_user_id');
    //     if ($request->query('currencies')) {
    //         $currencies = explode(',', $request->query('currencies'));
    //     } else {
    //         $currencies = [];
    //     }

    //     $transactions = TransactionService::findUserTransactionsByUserId($userId, $currencies);

    //     $transactions = UtilityService::modelQueryBuilder($transactions, $request, [], null, false);

    //     return response()->json($transactions);
    // }

    public function findAllUserTransactionsByUserId(Request $request)
    {
        $this->validate($request, [
            'app_user_id' => ['required', 'integer'],
        ]);

        $user = auth()->user();
        $userId = $request->query('app_user_id');

        $transactions = TransactionService::findAllUserTransactionsByUserId($userId);
        return response()->json($transactions->get());
    }

    public function getTransactionsByUserId(Request $request)
    {
        $this->validate($request, [
            'app_user_id' => ['required', 'integer'],
            'currency_ids' => ['string', 'regex:/[0-9]+(,[0-9]+)*$/'],
        ]);

        $id = $request->query('app_user_id');

        $reloads = Transaction::from('transactions as reload_transact')
            ->leftJoin('currencies', 'reload_transact.currency_id', 'currencies.id')
            ->leftJoin('cases', 'reload_transact.case_id', 'cases.id')
            ->leftJoin('reloads', 'cases.ref_id', 'reloads.id')
            ->select(
                'reload_transact.id',
                'reload_transact.account_id',
                'reload_transact.old_balance',
                'reload_transact.new_balance',
                'reloads.amount',
                'reload_transact.rate as exchange_rate',
                'currencies.iso_code as currency_code',
                'currencies.locale as currency_locale',
                'cases.ref_type',
                'reload_transact.created_at as transact_created_at',
            )
            ->where([
                ['reload_transact.user_id', $id],
                ['cases.ref_type', 'reloads']
            ]);

        $transfers = Transaction::from('transactions as transfer_transact')
            ->leftJoin('currencies', 'transfer_transact.currency_id', 'currencies.id')
            ->leftJoin('cases', 'transfer_transact.case_id', 'cases.id')
            ->leftJoin('transfers', 'cases.ref_id', 'transfers.id')
            ->select(
                'transfer_transact.id',
                'transfer_transact.account_id',
                'transfer_transact.old_balance',
                'transfer_transact.new_balance',
                'transfers.amount',
                'transfer_transact.rate as exchange_rate',
                'currencies.iso_code as currency_code',
                'currencies.locale as currency_locale',
                'cases.ref_type',
                'transfer_transact.created_at as transact_created_at',
            )
            ->where([
                ['transfer_transact.user_id', $id],
                ['cases.ref_type', 'transfers']
            ]);

        $exchanges = Transaction::from('transactions as exchange_transact')
            ->leftJoin('currencies', 'exchange_transact.currency_id', 'currencies.id')
            ->leftJoin('cases', 'exchange_transact.case_id', 'cases.id')
            ->leftJoin('exchanges', 'cases.ref_id', 'exchanges.id')
            ->select(
                'exchange_transact.id',
                'exchange_transact.account_id',
                'exchange_transact.old_balance',
                'exchange_transact.new_balance',
                DB::raw(
                    'IF(
                    exchanges.from_account_id = exchange_transact.account_id, 
                    exchanges.from_amount, 
                    exchanges.to_amount
                ) as amount'
                ),
                'exchange_transact.rate as exchange_rate',
                'currencies.iso_code as currency_code',
                'currencies.locale as currency_locale',
                DB::raw(
                    'IF(
                    exchanges.from_account_id = exchange_transact.account_id, 
                    "exchange_out", 
                    "exchange_in"
                ) as ref_type'
                ),
                'exchange_transact.created_at as transact_created_at',
            )
            ->where([
                ['exchange_transact.user_id', $id],
                ['cases.ref_type', 'exchange']
            ]);

        if ($request->has('currency_ids')) {
            $currencyIds = array_map('intval', explode(',', $request->query('currency_ids')));

            if (count($currencyIds) > 0) {
                $reloads->whereIn('currencies.id', $currencyIds);
                $transfers->whereIn('currencies.id', $currencyIds);
                $exchanges->whereIn('currencies.id', $currencyIds);
            }
        }

        $records = $reloads
            ->union($transfers)
            ->union($exchanges)
            ->orderBy('transact_created_at', 'desc')
            ->paginate(20);

        $modifiedCollection = $records->getCollection()->each(function ($item) {
            if (!is_null($item->amount)) {
                $formattedAmount = UtilityService::formatNumberToCurrency(
                    $item->amount,
                    $item->currency_code,
                    $item->currency_locale
                );

                $item->setAttribute('formatted_amount', $formattedAmount);
            }
        });

        $records->setCollection($modifiedCollection);

        return $records;
    }

    public function verifyPinNumber(Request $request)
    {

        $this->validate($request, [
            'pin' => ['required'],
        ]);

        $user = auth()->user();

        if (Hash::check($request->input('pin'), $user->pin)) {

            return response()->json([
                'message' => "Correct PIN number"
            ]);
        } else {
            return response()->json(['message' => 'Incorrect PIN'], 400);
        }
    }

    public function findCountryStatesMaritalStatus()
    {


        $countryData = UserService::findAllCountryWithState();
        $maritalStatusData = UserService::findAllMaritalStatus();

        return response()->json([
            'country' => $countryData,
            'marital_status' => $maritalStatusData
        ]);
    }

   

    // NEW UI API

    public function updateCustomerById($id, Request $request)
    {
        try {
            $info = [];
            $info['fullname'] = $request->input('fullname') ?? null;
            $info['status'] = $request->input('status') ?? null;
            $info['id_type'] = $request->input('id_type') ?? null;
            $info['id_number'] = $request->input('id_number') ?? null;
            $info['dob'] = $request->input('dob') ?? null;
            $info['gender'] = $request->input('gender') ?? null;
            $info['marital_status_id'] = $request->input('marital_status_id') ?? null;
    
            $address = [];
            $address['address_1'] = $request->input('address_line_1') ?? null;
            $address['address_2'] = $request->input('address_line_2') ?? null;
            $address['country_id'] = $request->input('country_id') ?? null;
            $address['state_id'] = $request->input('state_id') ?? null;
            $address['city'] = $request->input('city') ?? null;
            $address['postcode'] = $request->input('postcode') ?? null;
            
            $user = UserService::findOneUserById($id);
    
            if(empty($user)) {
                return response()->json([
                    'message' => "User not found"
                ], 400);
            }

            $addressId = null;
            $addressUpdated = false;

            $existingAddress = AddressService::findAddressByReferenceIdReferenceTable($id, 'users');

            if(empty($existingAddress)) {

                $allEmpty = every($address, function ($value) { return is_null($value); });

                if(!$allEmpty) {
                    $address['reference_table'] = 'users';
                    $address['reference_id'] = $id;
                    $addressId = AddressService::createAddress($address);
                }
            }
            else {
                $address['reference_table'] = 'users';
                $address['reference_id'] = $id;

                $addressUpdated = AddressService::updateAddressByReferenceIdReferenceTable($address);
            }
            
    
            // $info['address_id'] = $addressId;
            
            $updated = UserService::updateUserById($info, $id);
    
            if ($updated || $addressUpdated ||  $addressId) {

                if($addressId) {

                     return response()->json([
                        'message' => "Successfully create user info."
                    ], 200);
                    
                }
                return response()->json([
                    'message' => "Successfully update user info."
                ], 200);
            } else {
                return response()->json([
                    'message' => "Nothing updated"
                ], 202);
            }
        }
        catch(Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function updateCustomerStatusById($id, Request $request)
    {
        try {
            $this->validate($request, [
                'status' => ['required']
            ]);

            $info = [];
            $info['status'] = $request->input('status');

            $user = UserService::findOneUserById($id);
    
            if(empty($user)) {
                return response()->json([
                    'message' => "User not found"
                ], 400);
            }
            
            $updated = UserService::updateUserById($info, $id);
    
            if ($updated) {
                return response()->json([
                    'message' => "Successfully update user status."
                ], 200);
            } else {
                return response()->json([
                    'message' => "Nothing updated"
                ], 202);
            }
        }
        catch(Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function findUserProfile()
    {
        $user = UserService::findOneUserById(auth()->user()->id);

        return response()->json(
            $user
        );
    }

    public function findDashboardUserById($id)
    {
        $user = UserService::findOneUserById($id);

        return response()->json(
            $user
        );
    }

    public function updateDashboardUserById(Request $request)
    {

        $this->validate($request, [
            'fullname' => ['required'],
            'phone_no' => ['required'],
            'email' => ['required', 'email'],
            // 'role_id' => ['required', 'integer'],
            // 'status' => ['required'],
            'dashboard_user_id' => ['required']

        ]);

        $data = [];
        $data['fullname'] = $request->input('fullname');
        $data['phone_no'] = $request->input('phone_no');
        // $data['email'] = $request->input('email');
        $data['role_id'] = $request->input('role_id');
        $userId = $request->input('dashboard_user_id');

        $updated = UserService::updateDashboardUserById($data, $userId);

        if ($updated) {
            return response()->json([
                'message' => "Successfully update dashboard user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }


    public function createUser(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'fullname' => 'required',
            'email' => 'required|email',
            'phone_no' => 'required',
            'role_code' => ['required', 'string'],
            'status' => ['required', 'integer'],
           
        ]);

        try {

            //Unique username checking
            $username = $request->input('username');
            $is_username_unique = UserService::checkUniqueUsername($username);

            if (!$is_username_unique) {
                return response()->json(['message' => 'Username existed'], 400);
            } else if (preg_match('/\s/', $username)) {
                return response()->json(['message' => 'No space allowed in username'], 400);
            }

            //Unique email checking
            $email = $request->input('email');
            $is_email_unique = UserService::checkUniqueEmail($email);

            if (!$is_email_unique) {
                return response()->json(['message' => 'Email existed'], 400);
            } 
            

            // $phone_no = UtilityService::sanitisePhoneNumber($request->input('nationality_id'), $request->input('phone_no'));

            $pwd = UserService::generateTemporaryPassword();

            $data = [];
            $data['username'] = $request->input('username');
            $data['fullname'] = $request->input('fullname');
            $data['email'] = $request->input('email');
            $data['phone_no'] = $request->input('phone_no');
            $data['password'] = $pwd['hashed_temp_password'];
            $data['status'] = $request->input('status');
            $data['is_temp_password'] = 1;

            $user_id = UserService::createNewUser($data, $request->input('role_code'));

            $user = UserService::findUserById($user_id);
           
            // Encrypt user detail for onboarding email
            $encryptedUserDetailCipherText = UserService::generateEncryptedUserDetail($user);  
            
            
            // Sending email 
            $payload = [
                "to" => $request->input('email'),
                "fullname" => $user->fullname,
                "username" => $user->username,
                'url' =>  env('FIRST_LOGIN_URL_LOCAL') . '/' . $encryptedUserDetailCipherText
            ];

            MailService::sendWelcomeTempPasswordEmail($payload);


            return response()->json(['message' => 'Added new user'], 200);
            
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        

        }
    }

    public function findAllDashboardUsers(Request $request)
    {
          
        $queryParams = $request->all();

        $users = UserService::findAllUsersByRoleId([
            Role::TYPES['superadmin'],
            Role::TYPES['customer_service'],
            Role::TYPES['payment_officer']
        ]);

         /**
         * To apply filter from request query params
         */

        
        if (isset($queryParams['id'])) {
            $users->where('users.id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['role_id_selected'])) {
            $users->whereIn('role_id', $queryParams['role_id_selected']);
        }

        if (isset($queryParams['username'])) {
            $users->where('username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['fullname'])) {
            $users->where('fullname', 'like', '%'.$queryParams['fullname'].'%');
        }

        if (isset($queryParams['status'])) {
            $users->whereIn('status', $queryParams['status']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $users->whereBetween('users.created_at', [$finalFromDate, $finalToDate]);
        }
        
        

        $records = UtilityService::modelQueryBuilder($users, $request);
        

        return response()->json($records);
    }

    public function findAllCaseAssignee(Request $request)
    {
          
        $queryParams = $request->all();

        $users = UserService::findAllUsersByRoleId([
            Role::TYPES['superadmin'],
            Role::TYPES['payment_officer']
        ]);

         /**
         * To apply filter from request query params
         */

        
        if (isset($queryParams['id'])) {
            $users->where('users.id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['role_id_selected'])) {
            $users->whereIn('role_id', $queryParams['role_id_selected']);
        }

        if (isset($queryParams['username'])) {
            $users->where('username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['fullname'])) {
            $users->where('fullname', 'like', '%'.$queryParams['fullname'].'%');
        }

        if (isset($queryParams['status'])) {
            $users->whereIn('status', $queryParams['status']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $users->whereBetween('users.created_at', [$finalFromDate, $finalToDate]);
        }
        
        

        $records = UtilityService::modelQueryBuilder($users, $request);
        

        return response()->json($records);
    }

    public function findAllUsersByRoleId($role_id, Request $request)
    {
        // \DB::enableQueryLog(); 
        
        $queryParams = $request->all();

        $users = UserService::findAllUsersByRoleId([$role_id]);

        /**
         * To apply filter from request query params
         */

         if (isset($queryParams['id'])) {
            $users->where('users.id', '=', $queryParams['id']);
        }

        if (isset($queryParams['username'])) {
            $users->where('username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['fullname'])) {
            $users->where('fullname', 'like', '%'.$queryParams['fullname'].'%');
        }

        if (isset($queryParams['status'])) {
            $users->whereIn('status', $queryParams['status']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $users->whereBetween('users.created_at', [$finalFromDate, $finalToDate]);
        }
        

        $records = UtilityService::modelQueryBuilder($users, $request);
        
        // dd(\DB::getQueryLog()); // Show results of log

        return response()->json($records);
    }
    public function findAllCustomersEnabled()
    {
        $users = UserService::findAllUsersByRoleIdEnabled([2]);
        // print_r($users);
        return response()->json(
            $users->get()
        );

    }

    public function findAllCustomers(Request $request)
    {
        // \DB::enableQueryLog(); 
        
        $queryParams = $request->all();

        $users = UserService::findAllUsersByRoleId([2]);

        /**
         * To apply filter from request query params
         */


        if (isset($queryParams['topkash_id'])) {
            $users->where('users.topkash_id', 'like', '%' .$queryParams['topkash_id']. '%');
        }

        if (isset($queryParams['username'])) {
            $users->where('username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['fullname'])) {
            $users->where('fullname', 'like', '%'.$queryParams['fullname'].'%');
        }

        if (isset($queryParams['status'])) {
            $users->whereIn('status', $queryParams['status']);
        }

        if (isset($queryParams['phone_no'])) {
            $users->where('phone_no', 'like', '%'.$queryParams['phone_no'].'%');
        }
        
        //
        $records = UtilityService::modelQueryBuilder($users, $request);
        
        // dd(\DB::getQueryLog()); // Show results of log

        return response()->json($records);
    }

    public function findCustomerById($id, Request $request)
    {
        $user = UserService::findOneUserById($id);
        $model = User::find($id);
        $mode = $request->query('mode') ?? null;
        
        $address = [];

        // if($user->address_id) {
        //     $address = AddressService::findAddressById($user->address_id);
        // }

        // if(!empty($address)) {
        //     $user['address_1'] = $address->address_1;
        //     $user['address_2'] = $address->address_2;
        //     $user['postcode'] = $address->postcode;
        //     $user['city'] = $address->city;
        //     $user['country_id'] = $address->country_id;
        //     $user['state_id'] = $address->state_id;
        // }
        // else {
        //     $user['address_1'] = null;
        //     $user['address_2'] = null;
        //     $user['postcode'] = null;
        //     $user['city'] = null;
        //     $user['country_id'] = null;
        //     $user['state_id'] = null;
        // }

        if($mode === 'view'){
            ActivityLogService::createActivityLog([
                'subject' => 'users',
                'model' => $model,
                'action_type' => 'View',  
                'action_source' => 'view user account',
                'description' => 'view details'
            ]);

        }

        return response()->json(
            $user
        );
    }


    public function resendEmailForPasswordLink($id)
    {
        $user = UserService::findOneUserById($id);

        if(empty($user)) {
            return response()->json(['message' => 'User is not existed'], 400);
        }

        // dd($user->email);

        $encryptedUserDetailCipherText = UserService::generateEncryptedUserDetail($user);  

        $payload = [
            "to" => $user->email,
            "fullname" => $user->fullname,
            "username" => $user->username,
            'url' =>  env('FIRST_LOGIN_URL_LOCAL') . '/' . $encryptedUserDetailCipherText
        ];

        MailService::sendWelcomeTempPasswordEmail($payload);

        return response()->json(['message' => 'Email has been resent'], 200);

    }


    public function findCustomerWalletsById(Request $request, $id)
    {
        // return response()->json(
        //     $request
        // );
        $user = UserService::findOneUserById($id);

        $wallets = $user->wallets;

        foreach ($wallets as $wallet) {
            $latestTransaction = TransactionService::findLatestTransactionByUserAndCurrencyId($user->id, $wallet->id);

            $userAccount = UserAccount::where([
                'user_id' => $user->id,
                'currency_id' => $wallet->id
            ])->first();

            if (!isset($latestTransaction)) {
                $new_balance = 0;
                $user_account_id = $userAccount->id;
            } else {
                $new_balance = $latestTransaction->new_balance;
                $user_account_id = $latestTransaction->user_account_id;
            }

            $walletCountry = $wallet->country->first();

            $wallet->user_account_id = $user_account_id;
            $wallet->amount = $new_balance;
            $wallet->country_id = $walletCountry->id;
            $wallet->country_iso_code = $walletCountry->iso_code_2;
            $wallet->country_name = $walletCountry->name;
        }

        return response()->json([
            'data' => $wallets
        ]);

        // $records = UtilityService::modelQueryBuilder($wallets, $request, [], null, false);

        // return response()->json($records);

        // return response()->json([
        //     'data' => $wallets
        // ]);

        // return response()->json(
        //     $user
        // );
    }

}
