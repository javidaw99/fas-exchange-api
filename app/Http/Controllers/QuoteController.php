<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Services\QuoteService;

class QuoteController extends BaseController
{
    public function calculateRateByCurrencyIds(Request $request)
    {
        $this->validate($request, [
            'from_currency_id' => ['required', 'integer'],
            'to_currency_id' => ['required', 'integer'],
            'amount' => ['required', 'numeric'],
            'user_bank_account_id' => ['integer'],
            // 'user_account_id' => ['prohibited_unless:user_bank_account_id,null', 'required_without:user_bank_account_id', 'integer'],
        ]);

        $values = $request->all();

        return QuoteService::getQuoteByCurrencyIds($values);
    }

    public function verifyQuoteToken(Request $request)
    {
        $this->validate($request, [
            'token' => ['required'],
            'from_currency_id' => ['required', 'integer'],
            'to_currency_id' => ['required', 'integer'],

        ]);

        $values = $request->all();

        return QuoteService::verifyQuoteToken($values);
    }
}
