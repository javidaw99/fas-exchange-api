<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Bank;
use App\Models\BankCountry;
use App\Models\BankAccount;
use App\Services\BankService;
use App\Services\AccountService;
use App\Services\UtilityService;
use App\Services\CurrencyService;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;


use Throwable;

class BankController extends BaseController
{
    public function index(Request $request)
    {
        $banks = Bank::select(
            'id',
            'name',
            'created_at',
        );

        $banks->orderBy('name', 'asc');

        $banks = UtilityService::modelQueryBuilder($banks, $request, ['name']);

        return response()->json($banks);
    }

    public function bankCountryWithBankAccounts(Request $request)
    {

        $this->validate($request, [
            'currency' => ['required', 'integer'],
            'country' => ['required', 'integer']
        ]);

        $currencyId = $request->input('currency');
        $countryId = $request->input('country');

        $bankCountries = BankCountry::where('bank_country.country_id', $countryId)
            ->with('bankAccount', function ($query) use ($currencyId, $countryId) {
                $query->where('bank_accounts.country_id', $countryId)
                    ->where('bank_accounts.currency_id', $currencyId);
            })
            // ->where('bank_accounts.country_id', $countryId)
            // ->where('bank_accounts.currency_id', $currencyId)
            ->get();

        return response()->json($bankCountries);

        // BankAccount::where('currency_id', $currencyId)
        // ->where('country_id', $countryId)
        // ->with('')
        // ->get();
    }

    public function bankCountry(Request $request)
    {

        $currencyId = $request->query('currency');
        $countryId = $request->query('country');

        $bankCountries = BankCountry::where('bank_country.country_id', $countryId)
            ->leftJoin('countries', 'countries.id', 'bank_country.country_id')
            ->leftJoin('country_currency', 'countries.id', 'country_currency.country_id')
            ->where('country_currency.currency_id', $currencyId)
            ->select(
                'bank_country.id',
                'bank_country.country_id',
                'bank_country.bank_id',
                'bank_country.branch_name',
                'bank_country.created_at'
            )
            ->get();

        return response()->json($bankCountries);
    }

    public function addBank(Request $request)
    {

        $this->validate($request, [
            'name' => ['required', 'string'],
            // 'bank_country' => 'required', 
            // 'branch_name' => 'required', 
        ]);

        $bankName = $request->input('name');
        $bankExisted = AccountService::findBankByName($bankName);

        foreach ($bankExisted as $bank) {
            $compareBankName =  strcasecmp($bank["name"], $bankName);
            if ($compareBankName == 0) {
                return response()->json([
                    'message' => 'Bank is already existed'
                ], 400);
            }
        }

        $newbank = AccountService::createNewBank($bankName);

        // $bankInfo['country_id'] = $request->input('bank_country');
        // $bankInfo['branch_name'] = $request->input('branch_name');
        // $bankInfo['bank_id'] = $newbank['id'];



        if ($newbank) {
            return response()->json([
                'message' => 'Successfully add new bank'
            ]);
            // $newBankCountry = AccountService::createBankCountry($bankInfo);
            // if (isset($newBankCountry->wasRecentlyCreated) && $newBankCountry->wasRecentlyCreated === true) {
            //     return response()->json([
            //         'message' => 'Successfully add new bank'
            //     ]);
            // }else{
            //     return response()->json([
            //         'message' => 'Something wrong'
            //     ], 400);
            // }
        }
        return response()->json(['message' => 'Not able to add this record'], 400);
    }

    public function deleteBank($id)
    {

        try {

            $bank = AccountService::findBankById($id);
            $country = $bank->countries()->first();

            if (is_null($bank)) {
                return response()->json([
                    'message' => 'Something wrong'
                ], 400);
            } else {
                $bank->delete();

                if (!is_null($country)) {
                    $bank->countries()->detach($country->id);
                }

                return response()->json([
                    'message' => 'Successfully delete the bank'
                ]);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to delete this record'], 400);
        }
    }

    public function updateBank($id, Request $request)
    {

        $this->validate($request, [
            'name' => ['required', 'string']
        ]);

        try {

            $oldBank = AccountService::findBankById($id);
            $oldBankName = $oldBank->name;
            $bankName = $request->input('name');

            if (strcasecmp($oldBankName, $bankName)==0) {  // no name change
                return response()->json(['message' => 'Successfully update the bank name'], 200);
            }

            $bankExisted = AccountService::findBankByName($bankName);
            foreach ($bankExisted as $bank) {   // name exist for another bank
                $compareBankName =  strcasecmp($bank["name"], $bankName);

                if ($compareBankName == 0) {
                    return response()->json([
                        'message' => 'Bank is already existed'
                    ], 400);
                }
            }

            $bank = AccountService::findBankById($id);
            $bank->name = $request->input('name');;
            $bank->save();
            return response()->json(['message' => 'Successfully update the bank name'], 200);
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to update this record'], 400);
        }
    }

    public function createBankAccount(Request $request)
    {
        $this->validate($request, [
            'acc_name' => ['required', 'string'],
            'acc_no' => ['required', 'integer'],
            'bank_id' => ['required', 'integer'],
            'country_id' => ['required', 'integer'],
            'currency_id' => ['required', 'integer'],
            'status' => ['required', 'integer'],
            'minimum_amount' => ['required','numeric'],
            'maximum_amount' => ['required', 'numeric'],
        ]);

        $data = [];
        $data['acc_name'] = $request->input('acc_name');
        $data['acc_no'] = $request->input('acc_no');
        $data['bank_id'] = $request->input('bank_id');
        $data['country_id'] = $request->input('country_id');
        $data['currency_id'] = $request->input('currency_id');
        $data['status'] = $request->input('status');
        $data['minimum_amount'] = floatval($request->input('minimum_amount'));
        $data['maximum_amount'] = floatval($request->input('maximum_amount'));
        
        
        return BankService::createBankAccount($data);

    }

    public function updateBankAccount(Request $request)
    {

        $this->validate($request, [
            'acc_name' => ['required', 'string'],
            'acc_no' => ['required', 'integer'],
            'bank_account_id' => ['required', 'integer'],
            'bank_country_id' => ['required', 'integer'],
            'currency_id' => ['required', 'integer'],
            'status' => ['required', 'integer']
        ]);
        $accName = $request->input('acc_name');
        $accNo = $request->input('acc_no');
        $bankAccId = $request->input('bank_account_id');
        $bankCountryId = $request->input('bank_country_id');
        $currencyId = $request->input('currency_id');
        $status = $request->input('status');
        try {

            $bankExisted = BankAccount::where('acc_no', $accNo)
                ->where('bank_country_id', $bankCountryId)
                ->where('id', '!=', $bankAccId)
                ->whereNull('deleted_at')
                ->count();

            $bank = AccountService::findBankAccountById($bankAccId);
            $bank->acc_no = $accNo;

            $bank->acc_name = $accName;

            $bank->currency_id = $currencyId;

            $bank->bank_country_id = $bankCountryId;

            $bank->status = $status;

            if ($bank->isClean()) {
                return response()->json([
                    'message' => 'Nothing updated'
                ], 202);
            } else {
                if (!$bankExisted) {
                    $bank->acc_no = $accNo;

                    $bank->acc_name = $accName;

                    $bank->currency_id = $currencyId;

                    $bank->bank_country_id = $bankCountryId;
                    
                    $bank->status = $status;

                    $bank->save();

                    return response()->json(['message' => 'Successfully update bank account number'], 200);
                } else {

                    return response()->json([
                        'message' => 'Account number already existed'
                    ], 400);
                }
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to update this record'], 400);
        }
    }

    public function addBankBranch(Request $request)
    {

        $this->validate($request, [
            'bank_id' => ['required', 'integer'],
            'branch_name' => ['required', 'string'],
            'country_id' => ['required', 'integer']
        ]);

        $data = [];
        $data['bank_id'] = $request->input('bank_id');
        $data['branch_name'] = $request->input('branch_name');
        $data['country_id'] = $request->input('country_id');


        try {

            $branchExisted = BankCountry::where('branch_name', $data['branch_name'])
                ->where('country_id', $data['country_id'])
                ->whereNull('deleted_at')
                ->count();


            if (!$branchExisted) {

                $createNewBranch = AccountService::createNewBankBranch($data);

                return response()->json([
                    'message' => 'Added New Branch '
                ]);
            } else {

                return response()->json([
                    'message' => 'Branch Existed'
                ], 400);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to add this record'], 400);
        }
    }

    public function updateBankBranch(Request $request)
    {

        $this->validate($request, [
            'bank_id' => ['required', 'integer'],
            'bank_country_id' => ['required', 'integer'],
            'branch_name' => ['required', 'string'],
            'country_id' => ['required', 'integer']
        ]);

        $data = [];
        $data['bank_id'] = $request->input('bank_id');
        $data['branch_name'] = $request->input('branch_name');
        $data['country_id'] = $request->input('country_id');

        $bankCountryId = $request->input('bank_country_id');

        try {

            $branchExisted = BankCountry::where('branch_name', $data['branch_name'])
                ->where('country_id', $data['country_id'])
                ->where('id', '!=', $bankCountryId)
                ->whereNull('deleted_at')
                ->count();

            $bankCountry = AccountService::findBankCountryById($bankCountryId);

            if (!$branchExisted) {
                $bankCountry->bank_id = $data['bank_id'];
                $bankCountry->branch_name = $data['branch_name'];
                $bankCountry->country_id = $data['country_id'];

                $bankCountry->save();

                return response()->json(['message' => 'Successfully update bank branch'], 200);
            } else {

                return response()->json([
                    'message' => 'Something went wrong'
                ], 400);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to update this record'], 400);
        }
    }

    public function getBanksByCountry(Request $request)
    {
        $bank = DB::table('bank_country')
            ->leftJoin('banks', 'banks.id', '=', 'bank_country.bank_id')
            ->leftJoin('countries', 'countries.id', '=', 'bank_country.country_id')
            ->leftJoin('bank_accounts', 'bank_accounts.bank_country_id', '=', 'bank_country.id')
            ->leftJoin('country_currency', 'country_currency.country_id', '=', 'countries.id')
            ->select(
                'countries.id as country_id',
                'countries.name as country_name',
                'countries.iso_code_2 as country_code',
                'banks.name as bank_name',
                'bank_country.id as bank_country_id',
                'bank_country.branch_name',
                'bank_accounts.id as bank_account_id',
                'bank_country.country_id as bank_account_country_id',
                'bank_accounts.acc_no',
                'bank_accounts.acc_name',
                'country_currency.currency_id as country_currency_id'
            )
            ->orderBy('bank_country.created_at', 'desc')
            ->orderBy('bank_accounts.created_at', 'desc');

        $bank = UtilityService::modelQueryBuilder($bank, $request, [], null, false);

        if (method_exists($bank, 'hasPages')) {
            $groupedByBankId = $bank->getCollection()->groupBy('bank_country_id');
        } else {
            $groupedByBankId = $bank->groupBy('bank_country_id');
        }

        $finalData = [];

        foreach ($groupedByBankId as $bankAccountId => $bankAccounts) {
            $matchedBank = $bank->first(function ($value) use ($bankAccountId) {
                return $value->bank_country_id == $bankAccountId;
            });

            $accountData = $bankAccounts->map(function ($accounts) {
                return collect($accounts)
                    ->only(['acc_no', 'acc_name', 'bank_country_id', 'bank_account_id'])
                    ->all();
            });

            $finalObject = collect($matchedBank)
                ->only(['branch_name', 'country_id', 'country_name', 'country_code', 'bank_name', 'bank_country_id', 'bank_account_country_id', 'country_currency_id'])
                ->merge(['accounts' => $accountData]);

            array_push($finalData, $finalObject);
        }

        if (method_exists($bank, 'hasPages')) {
            $finalData = collect($finalData);
            $bank = $bank->setCollection($finalData);
        } else {
            $bank = $finalData;
        }

        return response()->json($bank);
    }

    public function findAllBankAvailableCountries(Request $request)
    {
        $country_ids = BankService::findAllBankAvailableCountries();

        return $country_ids;
    }
    public function findAllBankAccountsAvailableCountries(Request $request)
    {
        $country_ids = BankService::findAllBankAccountsAvailableCountries();

        return $country_ids;
    }
    public function findAllBanks()
    {
        $banks = BankService::findAllBanks();

        return $banks;
    }

    public function findAllBankAvailableCountriesByCurrency($currencyId)
    {

        $country_ids = BankService::findAllBankAvailableCountriesByCurrency($currencyId);

        return $country_ids;
    }

    public function findAllBankAccounts(Request $request) 
    {
        $queryParams = $request->all();

        $bank_accounts = BankService::findAllBankAccounts()
        ->select(
            'bank_accounts.id as bank_acc_id', 
            'countries.id as countries_id',
            'countries.name as country_name', 
            'countries.iso_code_2 as country_iso_code', 
            'currencies.id as currency_id',
            'iso_code as currency_code', 
            'bank_country.id as bank_country_id',
            'banks.id as banks_id',
            'banks.name as bank_name',
            'acc_no', 
            'acc_name', 
            'bank_accounts.created_at', 
            'bank_accounts.status',
            'bank_accounts.minimum_amount',
            'bank_accounts.maximum_amount',
        );

        if (isset($queryParams['id'])) {
            $bank_accounts->where('bank_accounts.id', '=', $queryParams['id']);
        }

        if (isset($queryParams['country'])) {
            $bank_accounts->whereIn('countries.iso_code_2', $queryParams['country']);
        }

        if (isset($queryParams['currency_code'])) {
            $bank_accounts->whereIn('currencies.iso_code', $queryParams['currency_code']);
        }

        if (isset($queryParams['bank'])) {
            $bank_accounts->whereIn('banks.name', $queryParams['bank']);
        }

        if (isset($queryParams['acc_no'])) {
            $bank_accounts->where('acc_no', 'like', '%' .$queryParams['acc_no']. '%');
        }

        if (isset($queryParams['acc_name'])) {
            $bank_accounts->where('acc_name', 'like', '%' .$queryParams['acc_name']. '%');
        }

        if (isset($queryParams['status'])) {
            $bank_accounts->whereIn('status', $queryParams['status']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }
            
            $bank_accounts->whereBetween('bank_accounts.created_at', [$finalFromDate, $finalToDate]);
        }

        $response = UtilityService::modelQueryBuilder($bank_accounts, $request);

        return response()->json($response);

    }

    public function findBankAccountById($bankAccId) 
    {
        $bankAcc = BankService::findAllBankAccountsByCurrencyAndCountry($bankAccId);

        return response()->json(
            $bankAcc
        );
    }


    public function updateBankAccountById(Request $request)
    {

        $this->validate($request, [
            'acc_name' => ['required', 'string'],
            'acc_no' => ['required', 'string'],
            'bank_account_id' => ['required', 'integer'],
            'bank_id' => ['required', 'integer'],
            'status' => ['required', 'integer'],
            'minimum_amount' => ['required', 'numeric', 'lt:maximum_amount', 'min:1'],
            'maximum_amount' => ['required','numeric', 'gt:minimum_amount', 'min:1'],
        ]);
        
        $bankAccId = $request->input('bank_account_id');
        $accNo = $request->input('acc_no');
        $accName = $request->input('acc_name');
        $bankId = $request->input('bank_id');
        $status = $request->input('status');
        $minimumAmount = $request->input('minimum_amount');
        $maximumAmount = $request->input('maximum_amount');


        return BankService::updateBankAccountById($bankAccId, $accNo, $accName, $bankId, $status, $minimumAmount, $maximumAmount);


    }

    public function findBankCountryByCountryId(Request $request)
    {
        $this->validate($request, [
            'country_id' => ['required', 'integer'],
        ]);

        $countryId = $request->input('country_id');

        return BankService::findBankCountryByCountryId($countryId);
    }

    public function findBankAccountHighFrequencyAndAmount(Request $request)
    {
        $currencies = CurrencyService::findAll();

        $currencyArray = array();

        foreach ($currencies as $key => $value) {
            $bankAccounts = DB::select('SELECT * FROM bank_account_frequency_amount_view where currency_id = ? order by score desc limit 5', [$value->id]);
           
            array_push($currencyArray, collect($bankAccounts)
                ->flatten()
                ->values());
        }

        $modifiedRecords = collect($currencyArray)->filter(function($value) {
            return count($value);
        })->map(function($value, $key) {
                return [
                    'currency_name' => $value->first()->currency_name,
                    'items' => $value
                ];
            })
            ->values();
        

        return response()->json($modifiedRecords);

    }

    public function findBankAccountLowFrequencyAndAmount(Request $request)
    {
        $currencies = CurrencyService::findAll();

        $currencyArray = array();

        foreach ($currencies as $key => $value) {

            $bankAccounts = BankService::findAllBankAccountsByLowestScore($value->id);
            // $bankAccounts = DB::select('SELECT * FROM bank_account_frequency_amount_view where currency_id = ? order by score limit 3', [$value->id]);
           
            array_push($currencyArray, collect($bankAccounts)
                ->flatten()
                ->values());
        }

        $modifiedRecords = collect($currencyArray)->filter(function($value) {
            return count($value);
        })->map(function($value, $key) {
                return [
                    'currency_name' => $value->first()->currency_name,
                    'items' => $value
                ];
            })
            ->values();
        

        return response()->json($modifiedRecords);

    }
}
