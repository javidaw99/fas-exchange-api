<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\LoanPackage;
use App\Services\LoanService;
use App\Services\UtilityService;

use Throwable;

class LoanController extends BaseController
{

    public function createLoanPackage(Request $request)
    {
        try {

            $this->validate($request, [
                'name' => 'required',
                'type' => 'required',
                'level' => 'required',
                'currency_id' => 'required',
                'loan_amount' => 'required',
                'loan_day' => 'required',
                'received_credit' => 'required',
                'first_time_penalty' => 'required',
                'daily_penalty_percentage' => 'required',
                'exp' => 'required',
                'status' => 'required'
            ]);

            $payload = [
                'name' => $request->input('name'),
                'type' => $request->input('type'),
                'level' =>  $request->input('level'),
                'currency_id' => $request->input('currency_id'),
                'loan_amount' => $request->input('loan_amount'),
                'loan_day' => $request->input('loan_day'),
                'received_credit' => $request->input('received_credit'),
                'first_time_penalty' => $request->input('first_time_penalty'),
                'daily_penalty_percentage' => $request->input('daily_penalty_percentage'),
                'exp' => $request->input('exp'),
                'status' =>  $request->input('status'),
            ];
            $package = LoanService::createLoanPackage($payload);

            if ($package) {
                return response()->json([
                    'message' => "Successfully create loan package.",

                ], 200);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function findAllLoanPackages(Request $request)
    {
        $loanPackages = LoanService::findAllLoanPackages();

        $loanPackages = UtilityService::modelQueryBuilder($loanPackages, $request, ['name']);


        return response()->json($loanPackages);
    }

    public function findOneLoanPackageById($id)
    {
        $loanPackage = LoanService::findOneLoanPackageById($id);

        return response()->json(
            $loanPackage
        );
    }

    public function updateLoanPackageById(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'level' => 'required',
            'currency_id' => 'required',
            'loan_amount' => 'required',
            'loan_day' => 'required',
            'received_credit' => 'required',
            'first_time_penalty' => 'required',
            'daily_penalty_percentage' => 'required',
            'exp' => 'required',
            'status' => 'required'
        ]);

        $data = [
            'name' => $request->input('name'),
            'type' => $request->input('type'),
            'level' =>  $request->input('level'),
            'currency_id' => $request->input('currency_id'),
            'loan_amount' => $request->input('loan_amount'),
            'loan_day' => $request->input('loan_day'),
            'received_credit' => $request->input('received_credit'),
            'first_time_penalty' => $request->input('first_time_penalty'),
            'daily_penalty_percentage' => $request->input('daily_penalty_percentage'),
            'exp' => $request->input('exp'),
            'status' =>  $request->input('status'),
        ];

        $updated = LoanService::updateLoanPackageById($data, $id);

        if ($updated) {
            return response()->json([
                'message' => "Successfully update loan package.",
            ], 200);
        } else {
            return response()->json([
                'message' => "Nothing updated",

            ], 202);
        }
    }

    public function deleteLoanPackageById($id)
    {
        $isDeleted = LoanService::deleteLoanPackageById($id);

        if ($isDeleted) {
            return response()->json(['message' => 'Record deleted']);
        }
        return response()->json(['message' => 'Not able to delete this record'], 400);
    }
}
