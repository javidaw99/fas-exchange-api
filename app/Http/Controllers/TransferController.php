<?php

namespace App\Http\Controllers;

use App\Models\Transfer;
use App\Services\TransferService;
use App\Services\UtilityService;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\ActivityLogService;
use Carbon\Carbon;

class TransferController extends BaseController
{
    public function findAllConverts(Request $request)
    {
        $queryParams = $request->all();

        // \DB::enableQueryLog(); 

        $converts = TransferService::findAllConverts();

        /**
         * To apply filter from request query params
         */

        if (isset($queryParams['id'])) {
            $converts->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['from_currency']) && isset($queryParams['to_currency'])) {
            $converts->whereIn('from_currency_code', $queryParams['from_currency'])->whereIn('to_currency_code', $queryParams['to_currency']);
        }
        else if (isset($queryParams['from_currency'])) {
            $converts->whereIn('from_currency_code', $queryParams['from_currency']);
        }
        else if (isset($queryParams['to_currency'])) {
            $converts->whereIn('to_currency_code', $queryParams['to_currency']);
        }

        if (isset($queryParams['topkash_id'])) {
            $converts->where('user_topkash_id', 'like', '%' .$queryParams['topkash_id']. '%');
        }

        if (isset($queryParams['username'])) {
            $converts->where('user_username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $converts->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        $converts->orderBy('created_at', 'desc');

        // Temporarily retrieve data order by descending
        $records = UtilityService::modelQueryBuilder($converts, $request);

        // dd(\DB::getQueryLog());

        return response()->json($records);
    }

    public function findAllTransfersByType($type, Request $request)
    {
        $queryParams = $request->all();

        if(!in_array($type, ['topkash','others','self'])) {
            return response()->json(['message' => 'Invalid transfer type'], 400);
        }

        // \DB::enableQueryLog(); 

        $transfers = TransferService::findAllTransfersByType($type);
        $transfers->where('is_requested', 1);

        /**
         * To apply filter from request query params
         */

        if (isset($queryParams['id'])) {
            $transfers->where('doc_id', 'like', '%' . $queryParams['id'] . '%');
        }

        if (isset($queryParams['username'])) {
            $transfers->where('user_username', 'like', '%' .$queryParams['username']. '%');
        }

        if (isset($queryParams['topkash_id'])) {
            $transfers->where('user_topkash_id', 'like', '%' .$queryParams['topkash_id']. '%');
        }

        if (isset($queryParams['to_currency'])) {
            $transfers->whereIn('to_currency_code', $queryParams['to_currency']);
        }

        if (isset($queryParams['from_currency'])) {
            $transfers->whereIn('from_currency_code', $queryParams['from_currency']);
        }


        if (isset($queryParams['country'])) {
            $transfers->whereIn('country_id', $queryParams['country']);
        }

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $transfers->whereBetween('created_at', [$finalFromDate, $finalToDate]);
        }

        $transfers->orderBy('created_at', 'desc');

        // Temporarily retrieve data order by descending
        $records = UtilityService::modelQueryBuilder($transfers, $request);

        // dd(\DB::getQueryLog());

        return response()->json($records);
    }

    public function findTransferById($id)
    {
        $transfer = TransferService::findTransferById($id);
        $model = Transfer::find($id);

        if (empty($transfer)) {
            return response()->json(['message' => 'Record not found'], 400);
        }

         ActivityLogService::createActivityLog([
                'subject' => 'transfers',
                'model' => $model,
                'action_type' => 'View',  
                'action_source' => 'view send member account',
                'description' => 'view details'
            ]);
        $toConvertAmountBeforeRoundUp = $transfer->amount - $transfer->processing_fee;
        $toConvertAmount = round($toConvertAmountBeforeRoundUp, 2);

        $transfer->to_convert_amount = $toConvertAmount;

        return response()->json($transfer);
    }
}
