<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use App\Services\AccountService;
use App\Services\CountryService;
use App\Services\UtilityService;
use App\Services\CurrencyService;

use function _\map;

class AppCurrencyController extends BaseController
{

    public function currencies(Request $request)
    {

        $type = $request->input('type');

        if ($type === 'all') {
         
            $currencies = Currency::select(
                'currencies.id',
                'currencies.currency_name',
                'currencies.iso_code as currency_iso',
                'currencies.minimum_amount',
                'currencies.flag'
            )
            ->rightJoin('country_currency', 'country_currency.currency_id', 'currencies.id')
            ->where('country_currency.deleted_at', null)
            ->get();



            $remappedCurrencies = $currencies->map(function ($item, int $key) {
                return [
                    'id' => $item['id'],
                    'currency_name' => $item['currency_name'],
                    'currency_iso' => $item['currency_iso'],
                    'flag' => env('AWS_URL') . $item['flag'],
                    'minimum_amount' => $item['minimum_amount']
                ];
            });

            $records = $remappedCurrencies;
        } else {
            $currencies = Currency::select(
                'id',
                'currency_name',
                'iso_code as currency_iso',
                'minimum_amount',
                'flag'
            )->get();
            // dd($currencies);

            $currencies = map($currencies ,function ($item, int $key) {
                return [
                    'id' => $item['id'],
                    'currency_name' => $item['currency_name'],
                    'currency_iso' => $item['currency_iso'],
                    'flag' => env('AWS_URL') . $item['flag'],
                    'minimum_amount' => $item['minimum_amount'],
                ];
            });

        }

        return response()->json($currencies);
    }

    public function findCountryCurrencyById($id)
    {
        $currencies =  CountryService::findCountryCurrencyById($id);

        return response()->json($currencies);
    }

    
    public function findCurrencyById($id)
    {
        $currencies =  CurrencyService::findOneById($id);

        return response()->json($currencies);
    }
}
