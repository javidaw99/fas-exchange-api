<?php

namespace App\Http\Controllers\ApiV2;

use App\Models\UserAccount;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\TransactionService;
use App\Services\UserService;
use App\Services\UtilityService;

class AppTransactionController extends BaseController
{
    public function activities(Request $request)
    {
        return response()->json([]);
    }

    public function findUserTransactionsById()
    {

        $user = auth()->user();
        $userId = $user->id;

        $transactions = TransactionService::findUserTransactionsById($userId);


        return response()->json($transactions);
    }

    public function findUserTransactionsHistoryByCurrency(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;
        $version = $request->header('version');

        $currencyId = $request->query('currency_id');

        $page = $request->query('page');
        $limit = $request->query('page_size');

        $page = isset($page) ? $page : 1;
        $limit = isset($limit) ? $limit : 10;

        $userAccount = UserService::checkWalletExist($userId, $currencyId);

        $userAccountId = $userAccount->id;

        $transactions = TransactionService::findUserTransactionsHistoryByCurrency($userId, $currencyId, $userAccountId, $limit, $page, $version);

        return response()->json($transactions);
    }

    public function findUserTransactionsHistory(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;
        $version = $request->header('version');

        $page = $request->query('page');
        $limit = $request->query('page_size');

        $page = isset($page) ? $page : 1;
        $limit = isset($limit) ? $limit : 10;

        $transactions = TransactionService::findUserTransactionsHistory($userId, $limit, $page, $version);

        return response()->json($transactions);
    }

    public function findLatestTransactionByUserAndCurrencyId(Request $request)
    {

        $user = auth()->user();
        $userId = $user->id;

        $currencyId = $request->query('currency_id');

        $latestTransaction = TransactionService::findLatestTransactionByUserAndCurrencyId($userId, $currencyId);

        return response()->json($latestTransaction);
    }
}
