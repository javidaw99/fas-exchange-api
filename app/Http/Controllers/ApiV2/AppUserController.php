<?php

namespace App\Http\Controllers\ApiV2;

use App\Models\Currency;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;
use App\Models\UserBankAccount;
use App\Models\UserAccount;
use App\Services\UtilityService;
use App\Services\UserService;
use App\Services\AccountService;
use App\Services\AddressService;
use App\Services\NotificationService;
use App\Services\SmsLogService;
use App\Services\TransactionService;
use Exception;
use Throwable;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use ParagonIE\HiddenString\HiddenString;
use ParagonIE\Halite\Symmetric;
use ParagonIE\Halite\Symmetric\EncryptionKey;
use Faker\Factory;
use App\Services\CurrencyKeypadService;
use App\Services\UserDeviceService;
use App\Services\CurrencyService;


class AppUserController extends BaseController
{

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * App User
     */
    public function appLogin(Request $request)
    {
        $this->validate($request, [
            'email_phone_username' => 'required',
            'password' => 'required',
            'region_code' => 'string'
        ]);


        /**
         * Validate thru few layers
         * 1. First, check whether passed input is phone number or not, if yes we will check with phone no field
         * 2. If it is not valid phone number we will check using other fields (email or username)
         * 3. Next we will check based on username or email using single query
         */
        $loginIdentifier = $request->input('email_phone_username');
        $region = $request->input('region') ?? 'MY';


        // If it is valid phone number we will prioritise phone number checking
        if (UtilityService::validatePhoneNumber($loginIdentifier, $region)) {
            $user = UserService::findOneUserByPhoneNo($loginIdentifier);
        } else if (UtilityService::validateEmailAddress($loginIdentifier)) {
            $user = UserService::getUserByEmail($loginIdentifier);
        } else {
            $user = UserService::getUserByUsername($loginIdentifier);
        }

        if (!empty($user)) {

            if($user->status !== 1){
                return response()->json(['message' => 'This user is no longer active'], 401);
            }

            if (Role::TYPES['app_user'] == $user->role_id) {
                if (Hash::check($request->input('password'), $user->password)) {

                    $token = UserService::generateUserToken($user->id, $user->role_id);

                    $user = (object)$user->only(['id', 'username', 'fullname', 'email', 'phone_no', 'is_temp_password', 'app_language']);

                    $status = UserService::checkTfaStatusById($user->id);

                    return response()->json([
                        'message' => 'Successfully login',
                        'user' => $user,
                        'token' => $token,
                        'app_language' => $user->app_language
                    ]);
                } else {
                    return response()->json(['message' => 'Invalid credentials'], 400);
                }
            } else {
                return response()->json(['message' => 'Unauthorized user.'], 401);
            }
        }

        return response()->json(['message' => 'Invalid credentials'], 400);
    }

    public function createAppRegisterOtp(Request $request)
    {
        $this->validate($request, [
            'country_id' => ['required'],
            'phone_no' => ['required'],
        ]);

        try {

            $userId = $request->input('user_id');
            $phoneNo = $request->input('phone_no');

            $phoneNumberExist = UtilityService::checkIsPhoneNumberExisted($phoneNo);

            if (!$phoneNumberExist) {
                $otpCode = UtilityService::generateOtpCode();

                $sendSms = Http::get(env('ONEWAYSMS_MT_URL'), [
                    'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                    'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                    'senderid' => "INFO",
                    'languagetype' => 1,
                    'mobileno' =>  $phoneNo,
                    'message' => 'TopKash: Your one-time code for registration is ' . $otpCode . '.'
                ]);

                $data = [];
                $data['phone_no'] = $phoneNo;
                $data['otp_code'] = $otpCode;

                $createOtpRecord = UtilityService::createOtpRecord($data);

                if ($createOtpRecord) {
                    return response()->json([
                        'message' => "Successfully update otp info and send message."
                    ], 200);
                } else {
                    return response()->json(['message' => 'Failed to update'], 400);
                }
            } else {
                return response()->json(['message' => 'Phone number is used'], 400);
            }
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function verifyAppUsername(Request $request)
    {
        $this->validate($request, [
            'username' => ['required'],
        ]);

        $user = UserService::getUserByUsername($request->input('username'));

        if (empty($user)) {
            return response()->json(['message' => 'Username is valid'], 200);
        }

        return response()->json(['message' => 'Username existed, please try another username'], 400);
    }

    public function verifyAppEmailAddress(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email:rfc',
        ]);

        $user = UserService::getUserByEmail($request->input('email'));

        if (empty($user)) {
            return response()->json(['message' => 'Email address is valid'], 200);
        }

        return response()->json(['message' => 'Email address existed, please try another email address'], 400);
    }

    public function verifyAppPhoneNumber(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required',
            'region' => 'required|min:2|max:2',
        ]);

        $phoneNo = $request->input('phone_no');
        $region = $request->input('region');

        if (UtilityService::validatePhoneNumber($phoneNo, $region)) {
            $user = UserService::getUserByPhoneNo($phoneNo);
        }

        if (empty($user)) {
            return response()->json(['message' => 'Phone number is valid'], 200);
        }

        return response()->json(['message' => 'Phone number existed, please try another phone number'], 400);
    }

    public function verifyAppInitiateSecureCode(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required',
            'region' => 'required|min:2|max:2',
            'username' => 'required',
            'email' => 'required|email:rfc',
            'password' => 'required'
        ]);

        $phoneNo = $request->input('phone_no');
        $region = $request->input('region');
        $email = $request->input('email');
        $username = $request->input('username');
        $password = Hash::make($request->input('password'));

        if (UtilityService::validatePhoneNumber($phoneNo, $region)) {
            $user = UserService::findOneUserByPhoneNo($phoneNo);
        } else if (UtilityService::validateEmailAddress($email)) {
            $user = UserService::getUserByEmail($email);
        } else {
            $user = UserService::getUserByUsername($username);
        }

        if (empty($user)) {
            // Recursive function to only return unique tac code
            $tacCode = $this->generateTacCode();

            // Define temporary user record
            $newTempUser = array(
                'username' => $username,
                'password' => $password,
                'phone_no' => $phoneNo,
                'email' => $email,
                'tac_code' => $tacCode
            );

            // Check if there is existing temp user record
            $tempUser = UserService::getAppTempUserByEmailPhoneNoUsername($newTempUser);

            // If temp user not existed, create new temp user
            if (empty($tempUser)) {
                UserService::createNewTempAppUser($newTempUser, 'User');
            }
            // Update new tac code for existing temp user
            else {
                // If TAC code just recently sent, e.g. lesser than a minute. We don't allow generate new TAC code
                $currentTime = Carbon::now();
                $diffInSeconds = $currentTime->diffInSeconds($tempUser->updated_at);

                if ($diffInSeconds <= 60) {
                    $remainingSeconds = 60 - $diffInSeconds;
                    return response()->json([
                        // Temporary send seconds back to frontend only since implement countdown for resend code
                        // 'message' => 'Please try again in ' . $remainingSeconds . ' seconds',
                        'seconds' => $remainingSeconds
                    ], 400);
                }

                UserService::updateAppTempUserTacCode($newTempUser);
            }

            // Send SMS to user's phone number
            Http::get(env('ONEWAYSMS_MT_URL'), [
                'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                'senderid' => "INFO",
                'languagetype' => 1,
                'mobileno' =>  $phoneNo,
                'message' => 'TopKash: Your secure code for registration is ' . $tacCode . '.'
            ]);


            return response()->json(['message' => 'Your shall receive TAC code via SMS in a while'], 200);
        }

        return response()->json(['message' => 'Phone number existed, please try another phone number'], 400);
    }

    public function verifyAppSecureCode(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required',
            'region' => 'required|min:2|max:2',
            'username' => 'required',
            'email' => 'required|email:rfc',
            'secure_code' => 'required|numeric|digits:6'
        ]);

        $phoneNo = $request->input('phone_no');
        $email = $request->input('email');
        $username = $request->input('username');
        $tacCode = $request->input('secure_code');

        // Define temporary user record
        $newTempUser = array(
            'username' => $username,
            'phone_no' => $phoneNo,
            'email' => $email,
            'tac_code' => $tacCode
        );

        // Check if there is existing temp user record
        $tempUser = UserService::getAppTempUserByEmailPhoneNoUsername($newTempUser);

        if (empty($tempUser)) {
            return response()->json(['message' => 'Invalid secure code, please try again'], 400);
        }

        // If TAC code is within 60s, TAC code is considered valid. After 60s considered expired, user need to request another time.
        $currentTime = Carbon::now();
        $diffInSeconds = $currentTime->diffInSeconds($tempUser->updated_at);

        if ($diffInSeconds <= 60) {
            if ($tempUser->tac_code === $tacCode) {
                // Soft delete temp user record and create new user if TAC code is matched
                UserService::deleteNewTempAppUser($newTempUser);

                // Then create new user based on temp user table
                $topkashId = $this->generateUniqueTopkashId();
                $tempUser = $tempUser->toArray();
                $secretKey = Str::uuid()->toString();
                $tempUser['secret_key'] = $secretKey;
                $tempUser['topkash_id'] = $topkashId;

                UserService::createNewAppUser($tempUser, 'User');


                return response()->json(['message' => 'Account created!'], 200);
            }

            return response()->json(['message' => 'Invalid secure code, please try again'], 400);
        }

        return response()->json(['message' => 'Secure code expired, please try to request new secure code'], 400);
    }


    public function generateUniqueTopkashId()
    {
        $topkashIdPrefix = "T";
        $topkashIdString = Str::random(8);
        $fullTopkashId = $topkashIdPrefix . strtolower($topkashIdString);

        if (UserService::checkUniqueTopkashId($fullTopkashId)) {
            return $fullTopkashId;
        }

        $this->generateUniqueTopkashId();
    }

    public function updateAppUserPin(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required',
            'pin' => 'required|digits:6'
        ]);

        $pin = Hash::make($request->input('pin'));
        $phoneNo = $request->input('phone_no');


        // Check if there is existing temp user record
        $user = UserService::findOneUserByPhoneNo($phoneNo);

        if (empty($user)) {
            return response()->json(['message' => 'User not found'], 400);
        }

        UserService::changeAppPin(
            [
                'user_id' => $user->id,
                'new_pin' => $pin
            ]
        );
    }

    public function generateTacCode()
    {
        $tacCode = $this->faker->numberBetween(111111, 999999);

        if (UserService::checkTempUserTacCodeUniqueness($tacCode)) {
            return $tacCode;
        }

        $this->generateTacCode();
    }


    public function register(Request $request)
    {

        $this->validate($request, [
            'username' => ['required'],
            'password' => ['required'],
            'nationality_id' => ['required'],
            'phone_no' => ['required'],
            'pin' => ['required', 'integer'],
            'email' => ['required', 'email'],
            'otp_code' => ['required'],
        ]);

        try {
            $data = [];
            $data['username'] = $request->input('username');
            $data['password'] = Hash::make($request->input('password'));
            $data['nationality_id'] = $request->input('nationality_id');
            $data['phone_no'] = $request->input('phone_no');
            $data['pin'] = Hash::make($request->input('pin'));
            $data['email'] = $request->input('email');

            $otpCode = $request->input('otp_code');

            $findSmsLogsByPhoneNo = UtilityService::findSmsLogsByPhoneNo($request->input('phone_no'));
            $phoneNumberExist = UtilityService::checkIsPhoneNumberExisted($request->input('phone_no'));

            if ($findSmsLogsByPhoneNo->otp_code === $otpCode) {

                if (!$phoneNumberExist) {
                    $createNewAppUser = UserService::createNewAppUser($data, 'User');

                    if ($createNewAppUser) {
                        return response()->json([
                            'message' => "OTP code is valid and successfully register."
                        ], 200);
                    } else {
                        return response()->json([
                            'message' => "Something wrong. Unable to create user"
                        ], 400);
                    }
                } else {
                    return response()->json([
                        'message' => 'Phone number is used.'
                    ], 400);
                }
            } else {
                return response()->json(['message' => 'Invalid OTP code'], 400);
            }
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    public function profileAccount(Request $request)
    {
        $user = auth()->user();

        $userId = $user->id;

        $userAccounts = AccountService::getUserCurrencyAccountsWithLatestTransaction($userId);
        $userData = UserService::retrieveUserInformation($userId);

        return response()->json([
            'profile' => $userData->makeHidden(['fcm_token', 'tfa_secret']),
            'accounts' => $userAccounts,
        ]);
    }
    public function findOneUserById(Request $request)
    {
        $user = auth()->user();
        $userId = $user->id;

        $userData = UserService::findOneUserById($userId);

        return response()->json([
            'data' => $userData
        ]);
    }


    public function findCountryStatesMaritalStatus()
    {


        $countryData = UserService::findAllCountryWithState();
        $maritalStatusData = UserService::findAllMaritalStatus();

        return response()->json([
            'country' => $countryData,
            'marital_status' => $maritalStatusData
        ]);
    }

    public function saveFcmDetails(Request $request)
    {

        $this->validate($request, [
            'fcm_token' => 'required',
        ]);

        $fcmToken = $request->input('fcm_token');

        $saveFcmDetails =  UserService::saveFcmDetails($fcmToken);

        if ($saveFcmDetails) {
            return response('OK');
        }

        return response('Fcm token exist');
    }

    public function updateUserById(Request $request)
    {

        // $test = $request->all();
        // dd($test);

        $filterRequest = $request->except(['user_id', 'role_id']);
        $data = [];
        foreach ($filterRequest as $key => $value) {
            $data[$key] = $value;
        }


        // $updated = UserService::updateUserById($data);
        $user = UserService::findOneUserById($request->get('user_id'));

        if ($updated) {
            return response()->json([
                'message' => "Successfully update user info.",
                'user' => $user

            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }

    public function updateProfile(Request $request)
    {

        $user = UserService::findUserById($request->get('user_id'));

        $data = [];
        $data['fullname'] = $request->input('fullname');
        $data['phone_no'] = $request->input('phone_no');
        $data['dob'] = $request->input('dob');
        $data['gender'] = $request->input('gender');
        $data['marital_status_id'] = $request->input('marital_status_id');
        $data['nationality_id'] = $request->input('nationality_id');
        $data['id_type'] = $request->input('id_type');
        $data['id_number'] = $request->input('id_number');

        $address = [];
        $address['address_1'] = $request->input('address_1');
        $address['city'] = $request->input('city');
        $address['postcode'] = $request->input('postcode');
        $address['state_id'] = $request->input('state_id');
        $address['country_id'] = $request->input('country_id');
        $address_id = $user["address_id"];

        $is_null_address_data = collect($address)->every(function ($value) {
            return is_null($value);
        });

        $is_null_user_data = collect($data)->every(function ($value) {
            return is_null($value);
        });


        if (!$is_null_user_data) {

            // update user and create/update address
            if (!$is_null_address_data) {

                if (!$address_id) {


                    $address_id = AddressService::createAddress($address);
                } else {


                    AddressService::updateAddressById($address, $user["address_id"]);

                    $address_id = $user["address_id"];
                }
            }

            // update user only
            $user = UserService::updateUser($data, $address_id);
        } else {

            // create/update address only
            if (!$address_id) {

                $address_id = AddressService::createAddress($address);

                $user = UserService::updateUser(null, $address_id);
            } else {

                AddressService::updateAddressById($address, $user["address_id"]);

                $address_id = $user["address_id"];
            }
        }

        if ($user || !$is_null_address_data) {
            return response()->json([
                'message' => "Successfully update user info."
            ], 200);
        } else {

            return response()->json([
                'message' => "Nothing updated"
            ], 202);
        }
    }

    public function verifyProfilePhoneNumber(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required'
        ]);

        $phoneNo = $request->input('phone_no');


        $user = UserService::findOneUserByPhoneNo($phoneNo);


        if ($user) {

            $tacCode = $this->generateTacCode();

            $newSecureCodeLog = array(
                'user_id' => $user->id,
                'tac_code' => $tacCode,
                'type' => 'reset_pin'
            );
            $secureCodeLog = UserService::findOneSecureCodeLog($newSecureCodeLog);

            $secretKey['secret_key'] =  $tacCode;

            if ($secureCodeLog) {

                $currentTime = Carbon::now();
                $diffInSeconds = $currentTime->diffInSeconds($secureCodeLog->created_at);

                if ($diffInSeconds <= 60) {

                    $remainingSeconds = 60 - $diffInSeconds;
                    return response()->json([
                        'message' => 'Please try again in ' . $remainingSeconds . ' seconds',
                        'seconds' => $remainingSeconds
                    ], 400);
                }
                UserService::deleteSecureCodeLog($newSecureCodeLog);
                UserService::createSecureCodeLog($newSecureCodeLog);
                // UserService::updateUserById($secretKey);
            } else {
                UserService::deleteSecureCodeLog($newSecureCodeLog);
                UserService::createSecureCodeLog($newSecureCodeLog);
                // UserService::updateUserById($secretKey);
            }

            // Send SMS to user's phone number
            Http::get(env('ONEWAYSMS_MT_URL'), [
                'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                'senderid' => "INFO",
                'languagetype' => 1,
                'mobileno' =>  $phoneNo,
                'message' => 'TopKash: Your secure code for registration is ' . $tacCode . '.'
            ]);

            return response()->json(['message' => 'Your shall receive TAC code via SMS in a while'], 200);
        } else {
            return response()->json(['message' => 'Phone number does not match'], 400);
        }
    }

    public function verifyProfileSecureCode(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required',
            'secure_code' => 'required'
        ]);

        $phoneNo = $request->input('phone_no');
        $tacCode = $request->input('secure_code');


        $user = UserService::findOneUserByPhoneNo($phoneNo);
        // Check if there is existing temp user record


        if (empty($user)) {
            return response()->json(['message' => 'Invalid secure code, please try again'], 400);
        }

        $newSecureCodeLog = array(
            'user_id' => $user->id,
            'tac_code' => $tacCode,
            'type' => 'reset_pin'
        );

        $secureCodeLog = UserService::findOneSecureCodeLog($newSecureCodeLog);

        // If TAC code is within 60s, TAC code is considered valid. After 60s considered expired, user need to request another time.
        $currentTime = Carbon::now();
        $diffInSeconds = $currentTime->diffInSeconds($secureCodeLog->created_at);

        if ($secureCodeLog->tac_code === $tacCode) {
            if ($diffInSeconds <= 60) {
                // Soft delete temp user record and create new user if TAC code is matched
                UserService::deleteSecureCodeLog($newSecureCodeLog);

                $secretKey['secret_key'] =  $tacCode;


                // UserService::updateUserById($secretKey);


                return response()->json(['message' => 'Secure Code Match!'], 200);
            }

            return response()->json(['message' => 'Secure code expired, please try to request new secure code'], 400);
        }

        return response()->json(['message' => ' Invalid secure code, please try again'], 400);
    }

    public function checkIsPhoneNoValid(Request $request)
    {

        $this->validate($request, [
            'phone_no' => ['required'],
        ]);

        $phoneNo = $request->input('phone_no');

        $user = UserService::findOneUserByPhoneNo($phoneNo);

        if ($user) {

            if (!$user->username) {
                return response()->json([
                    'message' => "Phone number is existed."
                ], 200);
            } else {
                return response()->json([
                    // For scenario that app user reset password using admin account's phone number 
                    'message' => "Phone number is not valid."
                ], 400);
            }
        } else {
            return response()->json([
                'message' => "Phone number is not existed."
            ], 400);
        }
    }

    public function sendResetPasswordOtp(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required'
        ]);

        $phoneNo = $request->input('phone_no');
        $user = UserService::findOneUserByPhoneNo($phoneNo);

        if ($user) {

            $latestRecord = UtilityService::findSmsLogsByPhoneNo($phoneNo, 'reset_password');

            $currentTimeStamp = Carbon::now()->getTimestamp();

            // If latest record existed, check expired_at
            if ($latestRecord) {

                // If expired, send new OTP, else throw error
                if ($currentTimeStamp > $latestRecord->expired_at) {

                    $otpCode = UtilityService::generateOtpCode();

                    Http::get(env('ONEWAYSMS_MT_URL'), [
                        'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                        'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                        'senderid' => "INFO",
                        'languagetype' => 1,
                        'mobileno' =>  $phoneNo,
                        'message' => 'TopKash: Use ' . $otpCode . ' to reset your password.'
                    ]);

                    $createOtpRecord = UtilityService::createOtpRecord([
                        'phone_no' => $phoneNo,
                        'otp_code' => $otpCode,
                        'purpose' => 'reset_password'

                    ]);

                    if (!$createOtpRecord) {
                        return response()->json(['message' => 'Something went wrong'], 400);
                    }

                    return response()->json(['message' => 'OTP sent', "expired_at" =>  $createOtpRecord], 200);
                } else {
                    return response()->json(['message' => 'Too frequently. Please try again after 1 minute'], 400);
                }
            } else {
                $otpCode = UtilityService::generateOtpCode();

                Http::get(env('ONEWAYSMS_MT_URL'), [
                    'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                    'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                    'senderid' => "INFO",
                    'languagetype' => 1,
                    'mobileno' =>  $phoneNo,
                    'message' => 'TopKash: Use ' . $otpCode . ' to reset your password.'
                ]);

                $createOtpRecord = UtilityService::createOtpRecord([
                    'phone_no' => $phoneNo,
                    'otp_code' => $otpCode,
                    'purpose' => 'reset_password'

                ]);

                if (!$createOtpRecord) {
                    return response()->json(['message' => 'Something went wrong'], 400);
                }

                return response()->json(['message' => 'OTP sent'], 200);
            }

            $currentTimeStamp = Carbon::now()->getTimestamp();
        } else {
            return response()->json(['message' => 'User not found'], 400);
        }
    }

    // public function resetPassword(Request $request)
    // {
    //     $this->validate($request, [
    //         'reset_code' => 'required',
    //         'password' => 'required',
    //     ]);

    //     $userId = $request->query('user_id');
    //     $resetCode = $request->input('reset_code');
    //     $password = $request->input('password');

    //     $user = UserService::findUserById($userId);

    //     if ($user) {
    //         $findSmsLogsByPhoneNo = UtilityService::findSmsLogsByPhoneNo($user->phone_no, 'reset_pin');

    //         if ($findSmsLogsByPhoneNo->otp_code === $resetCode) {

    //             $current_time = Carbon::now();
    //             $resetPwdTimeStamp = Carbon::parse($user->reset_pwd_timestamp);

    //             if ($current_time->timestamp > $resetPwdTimeStamp->timestamp) {
    //                 $minuteDiff = $current_time->diffInSeconds($resetPwdTimeStamp);

    //                 if ($minuteDiff > 60) {
    //                     return response()->json([
    //                         'message' => 'Reset code has expired. Please request for new reset code to proceed',
    //                     ], 400);
    //                 } else {
    //                     $foundUser = User::find($user->id);
    //                     $foundUser->password = Hash::make($password);
    //                     $foundUser->reset_pwd_timestamp = NULL;

    //                     if ($foundUser->isClean()) {
    //                         return response()->json([
    //                             'message' => 'Old password should not be same as new password',
    //                         ], 400);
    //                     } else {
    //                         $foundUser->save();
    //                         return response()->json([
    //                             'message' => 'Successfully reset password',
    //                         ], 200);
    //                     }
    //                 }
    //             }
    //         } else {
    //             return response()->json([
    //                 'message' => 'Invalid reset code',
    //             ], 400);
    //         }
    //     } else {
    //         return response()->json(['message' => 'User not found'], 400);
    //     }
    // }

    public function resetPassword(Request $request)
    {

        $this->validate($request, [
            'new_password' => 'required',
            'phone_no' => 'required'
        ]);

        $phoneNo = $request->input('phone_no');

        $newPassword = Hash::make($request->input('new_password'));

        UserService::updatePasswordByPhoneNo(
            $phoneNo,
            $newPassword
        );

        return response()->json([
            'message' => 'Successfully change password',
        ]);
    }

    public function updatePasswordByUserId(Request $request)
    {

        $this->validate($request, [
            'current_password' => 'required',
            'new_password' => 'required'
        ]);

        $user = UserService::findOneUserById($request->get('user_id'));


        if (Hash::check($request->input('current_password'), $user->password)) {

            UserService::updatePasswordByUserId(
                [
                    'user_id' => $request->get('user_id'),
                    'new_password' => Hash::make($request->input('new_password'))
                ]
            );

            return response()->json([
                'message' => 'Successfully change password',
            ]);
        } else {

            return response()->json(['message' => 'Your current password is incorrect'], 400);
        }
    }

    public function changePassword(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required'
        ]);

        $user = UserService::findUserById($request->get('user_id'));

        if (Hash::check($request->input('old_password'), $user->password)) {

            UserService::updatePasswordByUserId(
                [
                    'user_id' => $request->get('user_id'),
                    'new_password' => Hash::make($request->input('new_password'))
                ]
            );

            return response()->json([
                'message' => 'Successfully change password',
            ]);
        } else {
            return response()->json(['message' => 'Your old password is incorrect'], 400);
        }
    }

    public function updatePinByUserId(request $request)
    {

        $user = UserService::findOneUserById($request->get('user_id'));

        if ($request->has('is_reset')) {


            UserService::updatePinByUserId(
                [
                    'user_id' => $request->get('user_id'),
                    'new_pin' => Hash::make($request->input('new_pin'))
                ]
            );
            return response()->json([
                'message' => 'Successfully update pin number'
            ]);
        } else {

            $this->validate($request, [
                'current_pin' => 'required',
                'new_pin' => 'required'
            ]);


            if (Hash::check($request->input('current_pin'), $user->app_pin)) {

                UserService::updatePinByUserId(
                    [
                        'user_id' => $request->get('user_id'),
                        'new_pin' => Hash::make($request->input('new_pin'))
                    ]
                );
                return response()->json([
                    'message' => 'Successfully change pin number'
                ]);
            } else {
                return response()->json([
                    'message' => 'Your current pin number is incorrect'
                ], 400);
            }
        }
    }
    public function changePin(request $request)
    {

        $this->validate($request, [
            'current_pin' => 'required',
            'new_pin' => 'required'
        ]);

        $user = UserService::findUserById($request->get('user_id'));

        if (Hash::check($request->input('current_pin'), $user->pin)) {

            UserService::updatePinByUserId(
                [
                    'user_id' => $request->get('user_id'),
                    'new_pin' => Hash::make($request->input('new_pin'))
                ]
            );
            return response()->json([
                'message' => 'Successfully change pin number'
            ]);
        } else {
            return response()->json([
                'message' => 'Your old pin number is incorrect'
            ], 400);
        }
    }

    public function findMainWallet(){

            $user = auth()->user();
    
            $wallets = $user->wallets;
    

            $filtered  =$wallets->where('iso_code', 'MYR');

            // $filtered = $wallets->filter(function ($value, int $key) {
            //     print_r($value->iso_code);

            //     return $value->iso_code == 'MYR';
            // });

            // print_r($filtered->toJson());

            // Prepare flag image from S3 bucket
            foreach ($wallets as $wallet) {
                $latestTransaction = TransactionService::findLatestTransactionByUserAndCurrencyId($user->id, $wallet->id);
                $userAcount = UserAccount::where([
                    'user_id' => $user->id,
                    'currency_id' => $wallet->id
                ])->first();
    
                if (!isset($latestTransaction)) {
                    $new_balance = 0;
                    $user_account_id = $userAcount->id;
                } else {
                    $new_balance = $latestTransaction->new_balance;
                    $user_account_id = $latestTransaction->user_account_id;
                }
    
                $currencyKeypads = CurrencyKeypadService::findCurrencyKeypadByCurrencyId(
                    $wallet->id
                );
    
                $walletCountry = $wallet->country->first();
    
                
                $wallet->user_account_id = $user_account_id;
                $wallet->flag = env('AWS_URL') . $wallet->flag;
                $wallet->amount = $new_balance;
                $wallet->country_id = $walletCountry->id;
                $wallet->country_iso_code = $walletCountry->iso_code_2;
                $wallet->country_name = $walletCountry->name;
                $wallet->keypads = $currencyKeypads;
    
                $wallet->daily_limit = $userAcount->daily_limit;
    
            }
            return response()->json([
                'data' => $filtered->first()
            ]);
        
    
    }
    public function findAllWallets()
    {
        $user = auth()->user();

        $wallets = $user->wallets;

        // Prepare flag image from S3 bucket
        foreach ($wallets as $wallet) {
            $latestTransaction = TransactionService::findLatestTransactionByUserAndCurrencyId($user->id, $wallet->id);
            $userAcount = UserAccount::where([
                'user_id' => $user->id,
                'currency_id' => $wallet->id
            ])->first();

            if (!isset($latestTransaction)) {
                $new_balance = 0;
                $user_account_id = $userAcount->id;
            } else {
                $new_balance = $latestTransaction->new_balance;
                $user_account_id = $latestTransaction->user_account_id;
            }

            $currencyKeypads = CurrencyKeypadService::findCurrencyKeypadByCurrencyId(
                $wallet->id
            );

            $walletCountry = $wallet->country->first();

            
            $wallet->user_account_id = $user_account_id;
            $wallet->flag = env('AWS_URL') . $wallet->flag;
            $wallet->amount = $new_balance;
            $wallet->country_id = $walletCountry->id;
            $wallet->country_iso_code = $walletCountry->iso_code_2;
            $wallet->country_name = $walletCountry->name;
            $wallet->keypads = $currencyKeypads;

            $wallet->daily_limit = $userAcount->daily_limit;

        }
        return response()->json([
            'data' => $wallets
        ]);
    }

    public function findTopkashUserAllWallets($topkash_user_id)
    {
        $user = UserService::findOneByTopkashUserId($topkash_user_id);

        $wallets = $user->wallets;

        // Prepare flag image from S3 bucket
        foreach ($wallets as $wallet) {

            $latestTransaction = TransactionService::findLatestTransactionByUserAndCurrencyId($user->id, $wallet->id);
            $userAcount = UserAccount::where([
                'user_id' => $user->id,
                'currency_id' => $wallet->id
            ])->first();

            if (!isset($latestTransaction)) {
                $new_balance = 0;
                $user_account_id = $userAcount->id;
            } else {
                $new_balance = $latestTransaction->new_balance;
                $user_account_id = $latestTransaction->user_account_id;
            }

            $wallet->user_account_id = $user_account_id;
            $wallet->flag = env('AWS_URL') . $wallet->flag;
            $wallet->amount = $new_balance;
        }
        return response()->json([
            'data' => $wallets
        ]);
    }

    public function createWallet(Request $request)
    {
        $this->validate($request, [
            'currency_id' => 'required'
        ]);

        $user = auth()->user();
        $userId = $user->id;
        $currencyId = $request->input('currency_id');

        $isCurrencyExisted = Currency::where([
            'id' => $currencyId
        ])->count();

       
        if (!$isCurrencyExisted) {
            return response()->json([
                'message' => "Invalid currency selected"
            ], 400);
        }

        $currency =  Currency::find($currencyId);
        $dailyLimit = $currency->daily_limit;

        $isUserAccountExisted = UserAccount::where([
            'user_id' => $userId,
            'currency_id' => $currencyId
        ])->count();

        if ($isUserAccountExisted) {
            return response()->json([
                'message' => "Wallet with selected currency existed"
            ], 400);
        }

        UserAccount::create([
            'user_id' => $userId,
            'currency_id' => $currencyId,
            'daily_limit' => $dailyLimit
        ]);

        return response()->json([
            'message' => 'Created new wallet'
        ]);
    }

    public function updateBiometricPublicKey(Request $request)
    {
        $user = auth()->user();
        $publicKey = $request->input('public_key');
        $updateUserBiometricPublicKey = UserService::updateBiometricPublicKey($user->id, $publicKey);

        if ($updateUserBiometricPublicKey) {
            return response()->json([
                'message' => 'Successfully update biometric public key'
            ]);
        } else {
            return response()->json([
                'message' => 'Something went wrong'
            ], 400);
        }
    }

    public function verifyBiometricPublickey(Request $request)
    {
        $this->validate($request, [
            'payload_string' => ['required', 'string'],
            'signature' => ['required', 'string']
        ]);

        $user = auth()->user();
        $userPublicKey = $user->biometric_public_key;
        $userSecretKey = $user->secret_key;

        $pem = "-----BEGIN PUBLIC KEY-----\n";
        $formattedKey = chunk_split($userPublicKey, 64, "\n");

        $pem .= $formattedKey;
        $pem .= "-----END PUBLIC KEY-----\n";

        $formattedPublicKey = openssl_pkey_get_public($pem);
        $payloadString = $request->get('payload_string');
        $signature = $request->get('signature');

        $result = openssl_verify($payloadString, base64_decode($signature), $formattedPublicKey, OPENSSL_ALGO_SHA256);

        if ($result) {


            $timeStampKey =  UserService::generateTimeStampKey($userSecretKey);

            return response()->json([
                'message' =>  'Authentication Success',
                'key' => $timeStampKey
            ]);
        } else {
            return response()->json([
                'message' => 'Authentication failed'
            ], 400);
        }

        return $result;
    }

    public function createOrUpdateAppPinCode(Request $request)
    {
        $this->validate($request, [
            'app_pin' => 'required|digits:6'
        ]);

        $appPin = Hash::make($request->input('app_pin'));
        $username = $request->input('username') ? $request->input('username') : null;

        if ($username) {

            $user = UserService::getUserByUsername($username);
        } else {
            $user = auth()->user();
        }

        $updateAppPinCode = UserService::updateAppPinCode($appPin, $user);

        if ($updateAppPinCode) {
            return response()->json([
                'message' => 'Successfully update app pin'
            ]);
        } else {
            return response()->json([
                'message' => 'Failed to update app pin'
            ], 400);
        }
    }

    public function verifyAppPinCode(Request $request)
    {
        $this->validate($request, [
            'app_pin' => ['required', 'string'],
        ]);

        $user = auth()->user();
        $userSecretKey = $user->secret_key;


        if (Hash::check($request->input('app_pin'), $user->app_pin)) {

            $timeStampKey =  UserService::generateTimeStampKey($userSecretKey);



            return response()->json([
                'message' =>  'Authentication Success',
                'key' => $timeStampKey
            ]);
        } else {

            return response()->json([
                'message' => 'Authentication failed'
            ], 400);
        }
    }

    public function walletSessionVerify(Request $request)
    {
        $this->validate($request, [
            'biometric_expiry_token' => ['required', 'string'],
        ]);

        $user = auth()->user();
        $userSecretKey = $user->secret_key;
        $userSecretKey = str_replace('-', '', $userSecretKey);
        $secretKey = new HiddenString($userSecretKey);
        $secretKey = new EncryptionKey($secretKey);

        $biometricExpiryToken = $request->input('biometric_expiry_token');

        $currentTimeStamp = Carbon::now('UTC');

        $decryptedBiometricExpiryToken =  Symmetric\Crypto::decrypt(
            $biometricExpiryToken,
            $secretKey
        );

        $decryptedBiometricExpiryToken = $decryptedBiometricExpiryToken->getString();

        $timeStamp = json_decode($decryptedBiometricExpiryToken)->current_time;

        $hourDiff = $currentTimeStamp->diffInMinutes($timeStamp);


        if ($hourDiff < 60) {

            return response()->json(['message' => 'Your token is valid']);
        } else {
            return response()->json(['message' => 'Your token is expired, Please do biometric authentication again'], 400);
        }
    }

    public function findAllBankAccounts(Request $request)
    {
        $user = auth()->user();

        $userId = $user->id;

        $type = $request->query('type');
        $currencyId = $request->query('currency_id') ?? null;
        // print_r($request->query());

        $bankAccounts = UserService::findAllBankAccounts($userId, $type, $currencyId);

        return response()->json([
            'bank_accounts' => $bankAccounts,
        ]);
    }


    public function deleteBankAccountByBankAccountId($id)
    {
        try {

            $record = UserBankAccount::find($id);

            if (empty($record)) {
                return response()->json(['message' => 'Record not found'], 400);
            }

            $record->delete();

            return response()->json(['message' => 'Record deleted']);
        } catch (Throwable $e) {
            return response()->json(['message' => 'Not able to delete this record'], 400);
        }
    }

    public function createUserBankAccount(Request $request)
    {

        $this->validate($request, [
            'reference_name' => ['required'],
            'reference_bank_account' => ['required'],
            'currency_id' => ['required', 'integer'],
            'country_id' => ['required', 'integer'],
            'bank_id' => ['required', 'integer'],
            'type' => ['required'],
        ]);

        $user = auth()->user();
        $userId = $user->id;

        $currencyId = $request->input('currency_id');
        $values = $request->all();

        $currencies =  CurrencyService::findOneById($currencyId);

        $values["maximum_amount"] = $currencies->maximum_amount;


        UserService::createUserBankAccount($values, $userId);

        return response()->json(['message' => 'Successfully created new bank account'], 200);
    }

    public function createTopkashUserBankAccount(Request $request)
    {
        $this->validate($request, [
            'topkash_user_id' => ['required'],
        ]);

        $user = auth()->user();
        $userId = $user->id;

        $topkashUserId = $request->input('topkash_user_id');

        $topkashUser = UserService::findOneByTopkashUserId($topkashUserId);

        if (!empty($topkashUser)) {
            $isSuccess = UserService::createTopkashUserBankAccount($userId, $topkashUserId);

            if ($isSuccess) {
                return response()->json(['message' => 'Successfully added new TopKash user'], 200);
            }
        }

        return response()->json(['message' => 'Topkash account is already existed'], 400);
    }

    public function findBankAccountByBankAccountId($id)
    {

        $userbankAccount = UserService::findBankAccountByBankAccountId($id);

        return response()->json(
            $userbankAccount
        );
    }
    public function verifyTopkashId(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'topkash_id' => 'required',
        ]);

        $user = auth()->user();
        $userId = $user->id;
        $username = $request->input('username');
        $topkashId = $request->input('topkash_id');

        $topkashUser = UserService::findUserByTopkashIdAndUsername($username, $topkashId);

        if (!empty($topkashUser)) {

            if ($topkashUser->id === $userId) {

                return response()->json([
                    'message' => 'You cannot add yourself.',
                ], 400);
            } else {
                return response()->json([
                    'message' => 'This username/ Topkash ID exists.',
                    'topkash_user_id' => $topkashUser->id
                ], 200);
            }
        } else {
            return response()->json([
                'message' => 'This username/ Topkash ID does not exist.',
            ], 400);
        }
    }


    public function sendResetPinOtp()
    {

        $user = auth()->user();

        $phoneNo = $user->phone_no;

        try {
            // Find Sms Log's latest record using phone number
            $latestRecord = UtilityService::findSmsLogsByPhoneNo($phoneNo, 'reset_pin');

            $currentTimeStamp = Carbon::now()->getTimestamp();

            // If latest record existed, check expired_at
            if ($latestRecord) {

                // If expired, send new OTP, else throw error
                if ($currentTimeStamp > $latestRecord->expired_at) {

                    $otpCode = UtilityService::generateOtpCode();

                    Http::get(env('ONEWAYSMS_MT_URL'), [
                        'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                        'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                        'senderid' => "INFO",
                        'languagetype' => 1,
                        'mobileno' =>  $phoneNo,
                        'message' => 'TopKash: Use ' . $otpCode . ' to reset your pin.'
                    ]);

                    $createOtpRecord = UtilityService::createOtpRecord([
                        'phone_no' => $phoneNo,
                        'otp_code' => $otpCode,
                        'purpose' => 'reset_pin'

                    ]);

                    if (!$createOtpRecord) {
                        return response()->json(['message' => 'Something went wrong'], 400);
                    }

                    return response()->json(['message' => 'OTP sent', "expired_at" => $createOtpRecord], 200);
                } else {
                    return response()->json(['message' => 'Too frequently. Please try again after 1 minute'], 400);
                }
            } else {
                $otpCode = UtilityService::generateOtpCode();

                Http::get(env('ONEWAYSMS_MT_URL'), [
                    'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                    'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                    'senderid' => "INFO",
                    'languagetype' => 1,
                    'mobileno' =>  $phoneNo,
                    'message' => 'TopKash: Use ' . $otpCode . ' to reset your pin.'
                ]);

                $createOtpRecord = UtilityService::createOtpRecord([
                    'phone_no' => $phoneNo,
                    'otp_code' => $otpCode,
                    'purpose' => 'reset_pin'

                ]);

                if (!$createOtpRecord) {
                    return response()->json(['message' => 'Something went wrong'], 400);
                }

                return response()->json(['message' => 'OTP sent', "expired_at" => $createOtpRecord], 200);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Something went wrong'], 400);
        }
    }

    public function verifyResetOtpCode(Request $request)
    {
        $this->validate($request, [
            'otp_code' => ['required'],
            'purpose' => ['required'],
        ]);

        $user = auth()->user();

        $phoneNo =  isset($user->phone_no) ? $user->phone_no : $request->input('phone_no');
        $otpCode = $request->input('otp_code');
        $purpose = $request->input('purpose');

        $latestRecord = UtilityService::findSmsLogsByPhoneNo($phoneNo, $purpose);
        $currentTimeStamp = Carbon::now()->getTimestamp();

        if ($currentTimeStamp < $latestRecord->expired_at) {

            $verifyOtp = UtilityService::verifyOtpCode($phoneNo, $otpCode);

            if ($verifyOtp) {

                return response()->json(['message' => 'Verified'], 200);
            }

            return response()->json(['message' => 'Wrong OTP', 'status_code' => 5], 400);
        } else {
            return response()->json(['message' => 'OTP Expired. Please request again', 'status_code' => 6], 400);
        }
    }

    public function updateAppLanguage(Request $request) 
    {
        try {
            $this->validate($request, [
                'language' => ['required', 'string']
            ]);
    
            $user = auth()->user();
            
            UserService::updateAppLanguage($user->id, $request->input('language'));

            return response()->json(['message' => 'App language updated'], 200); 
        }
        catch(Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function findOrCreateUserDeviceById(Request $request) 
    {
        try {    
            $user = auth()->user();

            $data = [];
            $data['user_id'] = $user->id;
            $data['brand'] = $request->input('brand');
            $data['bundle_id'] = $request->input('bundle_id');
            $data['carrier'] = $request->input('carrier');
            $data['device_name'] = $request->input('device_name');
            $data['device_type'] = $request->input('device_type');
            $data['device_id'] = $request->input('device_id');
            $data['ip_address'] = $request->input('ip_address');
            $data['mac_address'] = $request->input('mac_address');
            $data['manufacturer'] = $request->input('manufacturer');
            $data['model'] = $request->input('model');
            $data['storage'] = $request->input('storage');
            $data['sys_build_id'] = $request->input('sys_build_id');
            $data['sys_build_number'] = $request->input('sys_build_number');
            $data['sys_name'] = $request->input('sys_name');
            $data['sys_version'] = $request->input('sys_version');
            $data['unique_id'] = $request->input('unique_id');
            $data['user_agent'] = $request->input('user_agent');
            $data['version'] = $request->input('version');
            
            UserDeviceService::findOrCreateUserDevice($data);

            return response()->json(['message' => 'Device info is created or updated'], 200); 
        }
        catch(Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function findUplineUser(Request $request){
        $user = UserService::findUplineUser();

        return response()->json(
            $user
        );
    }

    public function findDownlineUser(Request $request){
        $users = UserService::findDownlineUser();

        return response()->json(
            $users
        );
    }
}
