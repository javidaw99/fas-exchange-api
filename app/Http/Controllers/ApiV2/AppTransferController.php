<?php

namespace App\Http\Controllers\ApiV2;

use App\Services\CaseService;
use App\Services\TransferService;
use App\Services\CurrencyService;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Services\UserService;
use App\Services\NotificationService;
use App\Services\TransactionService;
use App\Models\Status;
use App\Services\ActivityLogService;
use App\Services\UtilityService;
use App\Services\BankService;
use App\Services\ExchangeRateService;
use App\Services\FasTransferRequestService;
use App\Services\FasTransferService;
use App\Services\FasTransferConvertService;
use App\Services\MediaService;
use App\Services\ExchangeMarginRateService;
use App\Models\CountryCurrency;


use Throwable;

use function _\map;

class AppTransferController extends BaseController
{
    public function transfer(Request $request)
    {

        $this->validate($request, [
            'type' => ['required'],
            'amount' => ['required', 'numeric'],
            'q_key' => ['required'],  //encrypted quote token
            'to_user_id' => ['integer'],
            'user_account_id' => ['required', 'integer'],
            'to_user_account_id' => ['prohibits:currency_id', 'required_without_all:currency_id,user_bank_account_id,to_user_id'],
            'user_bank_account_id' => ['prohibits:currency_id,', 'required_without_all:currency_id,to_user_account_id,to_user_id'],
            'currency_id' => ['prohibits:to_user_account_id,user_bank_account_id,to_user_id', 'required_without_all:to_user_account_id,user_bank_account_id,to_user_id'],
        ]);
    
        $values = $request->all();
        $currencyId = isset($values['currency_id']) ? $values['currency_id'] : null;
        $toUserAccountId = isset($values['to_user_account_id']) ? $values['to_user_account_id'] : null;

        $user = auth()->user();
        $userId = $user->id;

        if ($values['type'] === 'CONVERT') {
            $values['to_user_id'] =  $userId;
        }

        if (!$toUserAccountId) {

            if ($currencyId) {

                $existedWallet = UserService::checkWalletExist($userId, $currencyId);

                if (empty($existedWallet)) {

                    $wallet = UserService::addWallet([
                        'user_id' => $userId,
                        'currency_id' => $currencyId
                    ]);

                    if ($wallet) {
                        $values['to_user_account_id'] = $wallet->id;
                    }
                } else {
                    $values['to_user_account_id'] = $existedWallet->id;
                }
            };
        }

        $transfer = TransferService::transfer($values, $userId);

        if (!$transfer) {
            return response()->json(['message' => 'Something went wrong'], 400);
        }

        if ($transfer->to_user_id && $transfer->user_id !==  $transfer->to_user_id) {

            $fromCurrency = CurrencyService::findOneById($transfer->from_currency_id);
            $toCurrency = CurrencyService::findOneById($transfer->to_currency_id);
            $receiver = UserService::findOneUserById($transfer->to_user_id);

            $language = !is_null($user->app_language) ? $user->app_language : 'en';

            $locale = UtilityService::loadLocale($language);

            $senderDynamicValues = [
                'from_iso_code' => $fromCurrency->iso_code,
                'transfer_amount' => $transfer->amount,
                'username' => $receiver->username
            ];

            $senderNotification =  NotificationService::sendPushNotification([
                'fcm_token' => $user->fcm_token,
                'title' => $locale->t('notification.title_2'),
                'body' => $locale->t('notification.description_5', $senderDynamicValues),
                'target_user_id' => $transfer->user_id
            ], 'transfer', $transfer->id);

            UtilityService::createLocaleMapperRecord([
                'title_key' => 'notification.title_2',
                'content_key' => 'notification.description_5',
                'dynamic_values' => json_encode($senderDynamicValues),
                'reference_table' => 'notification',
                'reference_id' => $senderNotification->id
            ]);

            if ($receiver->fcm_token) {

                $language = !is_null($receiver->app_language) ? $receiver->app_language : 'en';

                $locale = UtilityService::loadLocale($language);

                $receiverDynamicValues = [
                    'to_iso_code' => $toCurrency->iso_code,
                    'convert_amount' => $transfer->converted_amount,
                    'username' => $user->username
                ];

                $receiverNotification = NotificationService::sendPushNotification([
                    'fcm_token' => $receiver->fcm_token,
                    'title' => $locale->t('notification.title_3'),
                    'body' => $locale->t('notification.description_6', $receiverDynamicValues),
                    'target_user_id' => $receiver->id
                ], 'transfer', $transfer->id);

                UtilityService::createLocaleMapperRecord([
                    'title_key' => 'notification.title_3',
                    'content_key' => 'notification.description_6',
                    'dynamic_values' => json_encode($receiverDynamicValues),
                    'reference_table' => 'notification',
                    'reference_id' => $receiverNotification->id
                ]);
            }
        } else if ($transfer->user_id && floatval($transfer->amount) > 0 && $transfer->type == 'SEND') {

            $notification = NotificationService::sendPushNotificationInBulk([
                'title' => 'New Transfer Request',
                'body' => 'New Transfer Request',
            ], 'transfer', $transfer->id);
        }

        return $transfer;

    }

    public function scanAndTransfer(Request $request)
    {

        $this->validate($request, [
            'type' => ['required'],
            'amount' => ['required', 'numeric'],
            'q_key' => ['required'],  //encrypted quote token
            'to_user_id' => ['required', 'integer'],
            'user_account_id' => ['required', 'integer'],
            'to_user_account_id' => ['integer'],
            'user_bank_account_id' => ['integer'],
            'currency_id' => ['integer'],
        ]);

        $values = $request->all();
        $currencyId = isset($values['currency_id']) ? $values['currency_id'] : null;
        $toUserAccountId = isset($values['to_user_account_id']) ? $values['to_user_account_id'] : null;

        $user = auth()->user();
        $userId = $user->id;

        $transfer = TransferService::transfer($values, $userId);


        if (!$transfer) {
            return response()->json(['message' => 'Something went wrong'], 400);
        }

        if ($transfer->to_user_id && $transfer->user_id !==  $transfer->to_user_id) {

            $fromCurrency = CurrencyService::findOneById($transfer->from_currency_id);
            $toCurrency = CurrencyService::findOneById($transfer->to_currency_id);
            $receiver = UserService::findOneUserById($transfer->to_user_id);

            $language = !is_null($user->app_language) ? $user->app_language : 'en';

            $locale = UtilityService::loadLocale($language);

            $senderDynamicValues = [
                'from_iso_code' => $fromCurrency->iso_code,
                'transfer_amount' => $transfer->amount,
                'username' => $receiver->username
            ];

            $senderNotification =  NotificationService::sendPushNotification([
                'fcm_token' => $user->fcm_token,
                'title' => $locale->t('notification.title_2'),
                'body' => $locale->t('notification.description_5', $senderDynamicValues),
                'target_user_id' => $transfer->user_id
            ], 'transfer', $transfer->id);

            UtilityService::createLocaleMapperRecord([
                'title_key' => 'notification.title_2',
                'content_key' => 'notification.description_5',
                'dynamic_values' => json_encode($senderDynamicValues),
                'reference_table' => 'notification',
                'reference_id' => $senderNotification->id
            ]);

            if ($receiver->fcm_token) {

                $language = !is_null($receiver->app_language) ? $receiver->app_language : 'en';

                $locale = UtilityService::loadLocale($language);

                $receiverDynamicValues = [
                    'to_iso_code' => $toCurrency->iso_code,
                    'convert_amount' => $transfer->converted_amount,
                    'username' => $user->username
                ];

                $receiverNotification = NotificationService::sendPushNotification([
                    'fcm_token' => $receiver->fcm_token,
                    'title' => $locale->t('notification.title_3'),
                    'body' => $locale->t('notification.description_6', $receiverDynamicValues),
                    'target_user_id' => $receiver->id
                ], 'transfer', $transfer->id);

                UtilityService::createLocaleMapperRecord([
                    'title_key' => 'notification.title_3',
                    'content_key' => 'notification.description_6',
                    'dynamic_values' => json_encode($receiverDynamicValues),
                    'reference_table' => 'notification',
                    'reference_id' => $receiverNotification->id
                ]);
            }
        } 

        return $transfer;
    }

    public function findTransferRecordById($id)
    {
        $user = auth()->user();
        $transfer = TransferService::findTransferById($id);
        
        $transferLogs = [];

        if($transfer->user_bank_account_type !== 'TOPKASH') {
            $transferLogs = ActivityLogService::findAllByReferenceTableReferenceID("transfers", $id);
            
            $transferLogs = $transferLogs->filter(function ($item) {
                return $item->action_type !== 'View';
            });

            $language = !is_null($user->app_language) ? $user->app_language : 'en';
            $locale = UtilityService::loadLocale($language);

            $transferLogs = map($transferLogs, function($item) use ($locale) {

                $description = "";
                $title = "";
                
                preg_match_all('/\d+/', $item->description, $numbers);
                $status = end($numbers[0]);

                if($item->action_type == "Create") {
                    $status = 1;
                    $title = $locale->t('transaction_timeline.title_1');
                    $description = $locale->t('transaction_timeline.description_1');
                }
                else if($item->action_type == "Edit") {
                    if($status == 2) {
                        $title = $locale->t('transaction_timeline.title_2');
                        $description = $locale->t('transaction_timeline.description_2');
                    }
                    else if ($status == 3) {
                        $title = $locale->t('transaction_timeline.title_3');
                        $description = $locale->t('transaction_timeline.description_3');
                    }
                    else if ($status == 5) {
                        $title = $locale->t('transaction_timeline.title_4');
                        $description = $locale->t('transaction_timeline.description_4');
                    }
                }

                return [
                    "date" => $item->created_at,
                    "status" => $status,
                    "title" => $title,
                    "description" => $description
                ];
            });
        }

        if (empty($transfer)) {
            return response()->json(['message' => 'Record not found'], 400);
        }

        if ($transfer->user_bank_account_type === 'TOPKASH') {
            $user = UserService::findOneUserById($transfer->to_user_id);

            $transfer->to_topkash_id = $user->topkash_id;
            $transfer->to_username = $user->username;

            $sender = UserService::findOneUserById($transfer->user_id);

            $transfer->sender_topkash_id = $sender->topkash_id;
            $transfer->sender_username = $sender->username;
        }

        $case = CaseService::getCaseByTypeAndRefId('transfers', $transfer->id);

        if ($case) {
            $transfer->status = Status::TYPES[$case->status];
            $transfer->reject_reason = $case->reject_reason;
        } else {
            $transfer->status = Status::TYPES['3'];
            $transfer->reject_reason = null;
        }

        $transfer->logs = $transferLogs;

        return response()->json($transfer);
    }

    public function createFasTransferRequest(Request $request){

        $days = 0;
        $i = 0;
        $limit = $request->input('limit');
        $total_amount = $request->input('total_amount');
        // $exchange_rate_id = $request->input('exchange_rate_id');

        $country_id = $request->input('country_id');
        $from_currency_id = $request->input('from_currency_id');
        $to_currency_id = $request->input('to_currency_id');
        // $user_bank_account_id = $request->input('user_bank_account_id');
        $user_account_id = $request->input('user_account_id');
        $userId = $request->input('user_id');
        $type = 'OTHERS';

        $country_ids = CountryCurrency::with([
            'country' => function ($query) {
                $query->select('id', 'name', 'iso_code_2');
            },
            'currency' => function ($query) {
                $query->select('id', 'currency_name', 'iso_code');
            },

        ])->whereHas('country', function ($query) {
            $query->where('is_enabled', true);
        })->where('currency_id', $to_currency_id)
          ->get();



        $bankAccounts = BankService::findAllBankAccountsByCurrencyAndCountry($from_currency_id, $country_id);

        $exchangeRate = ExchangeRateService::findOneByCurrencyIds($from_currency_id, $to_currency_id);

        // $useBanksAccounts = UserService::findAllBankAccounts($userId, $type, $to_currency_id);
        $toCurrency = CurrencyService::findOneById($to_currency_id);

        $exchangeMarginRateItem = ExchangeMarginRateService::findLatestExchangeMarginRate();

        $marginRate = $exchangeMarginRateItem->rate;

        $fromCurrency = CurrencyService::findOneById($from_currency_id);

        if($bankAccounts->isEmpty() || $bankAccounts->count() < 5){
            return response()->json([
                'message' => "Sorry, our $fromCurrency->iso_code currency bank account do not support at this moment"
            ], 400);
        }
        // if($useBanksAccounts->isEmpty()){

        //     return response()->json([
        //         'message' => "Sorry Dear, you dont have any $toCurrency->iso_code currency bank account"
        //     ], 400);
        // }
        // if($useBanksAccounts->count() < 4){
        //     return response()->json([
        //         'message' => "Sorry Dear, at least need 5 $toCurrency->iso_code currency bank account to convert"
        //     ], 400);
        // }
    
        // $useBanksAccounts = $useBanksAccounts->toArray();
        $exchangeRateId = $exchangeRate->id;
        $rate = $exchangeRate->rate;
        // dd($exchangeRate->rate);


        $bankAccounts = $bankAccounts->toArray();
        $bankLength = count($bankAccounts);
        $days = ceil($total_amount/$limit);

        $dayObject;
        $dayArray = [];
        $amount_balance = $total_amount;

        while($i<$days){
            $the_amount = 0;
            if($amount_balance - $limit < 0){
                $the_amount = $amount_balance;

                $amount_balance = 0 ;
            }else{
                $the_amount = $limit;
                $amount_balance -= $limit;
            }

            $loop = true;
            $acc_amount = $the_amount;
            $accObject;
            $accArray = [];
            $total_acc_amount = 0;
            $bankAccountsList = $bankAccounts;

            while($loop) {
                $bankAccountListLength = count($bankAccountsList);

                $randomize = rand(5000, 20000);
                $randomBankInt = rand(1,$bankAccountListLength);

                // print_r($bankAccountsList[$randomBankInt-1]);
                // echo '<br>';
                if(!isset($bankAccountsList[$randomBankInt-1])){
                    return response()->json([
                        'message' => "too big amount our banks account can't support"
                    ], 400);
                }
                $randomBank = $bankAccountsList[$randomBankInt-1];

                array_splice( $bankAccountsList, $randomBankInt-1, 1);

                // print_r($bankAccountsList);
                if($acc_amount - $randomize > 0){
                    // echo($randomize . "<br>");
    
                }else{
                    // echo $acc_amount;
                    $randomize = $acc_amount;
                }

                // dd($randomBank['bank_country']['bank_name']);

                $total_acc_amount += $randomize;

                $accObject = (object) array(
                    'acc_name' => $randomBank['acc_name'],
                    'acc_no' => $randomBank['acc_no'],
                    'bank_acc_id'=>$randomBank['id'],

                    'bank_name'=>$randomBank['bank_country']['bank_name'],
                    'amount' => $randomize,
                );
    
                array_push($accArray,  $accObject);
                $acc_amount -=  $randomize;
    
                if( $acc_amount <= 0){
                    $loop = false;
    
                }
              }
            //   $convert_amount = ceil($total_acc_amount * $rate);
            //   $countUserBankAccount = count($useBanksAccounts);
            //   $min = ceil($convert_amount/$countUserBankAccount);
            //   $max= $convert_amount;
            //   $loopConvert = true;
            //   $convObject;
            //   $convArray = [];

            //   $userBankAccountList = $useBanksAccounts;

            //   $rand = rand();
            //   dd($rand);
            // print_r($max);

            //   while($loopConvert) {
            //     $countUserBankAccountList = count($userBankAccountList);

            //     $randomAmountConvert = rand($min, $max);
            //     $randomUserBankInt = rand(1,$countUserBankAccountList);

            //     $randomUserBank = $userBankAccountList[$randomUserBankInt-1];
            //     array_splice( $userBankAccountList, $randomUserBankInt-1, 1);

            //     // print_r($randomUserBank);

            //     if($convert_amount - $randomAmountConvert > 0){
            //     }else{
            //         $randomAmountConvert = $convert_amount;
            //     }
            //     // print_r($randomUserBank);
            //     // echo'<br>';

            //     $convObject = (object) array(
            //         'acc_name' => $randomUserBank['reference_name'],
            //         'acc_no' => $randomUserBank['reference_bank_account'],
            //         'bank_acc_id'=>$randomUserBank['id'],

            //         'bank_name'=>$randomUserBank['bank_info']['name'],
            //         'amount' => $randomAmountConvert,
            //     );
            //     array_push($convArray,  $convObject);
            //     $convert_amount -=  $randomAmountConvert;


            //     if( $convert_amount <= 0){
            //         $loopConvert = false;
    
            //     }
            //   }
            //   print_r($convArray);

              $dayObject = (object) array(
                'day' => $i + 1,
                'accounts'=> $accArray,
                // 'user_accounts'=>$convArray,
                'total_account_amount'=>$total_acc_amount,
                'amount' => $the_amount,
                // 'convert_amount'=> ceil(($total_acc_amount*$rate) - )
            );
            
            array_push($dayArray,  $dayObject);


            $i++;
        }
        // print_r($dayArray);


        
        $data= [];
        $data['user_id'] = $request->input('user_id');
        // $data['user_bank_account_id'] = $request->input('user_bank_account_id');

        $data['user_account_id'] = $request->input('user_account_id');

        $data['from_currency_id'] = $request->input('from_currency_id');
        $data['to_currency_id'] = $request->input('to_currency_id');
        $data['total_amount'] = $request->input('total_amount');
        $data['total_convert_amount'] = ceil($total_amount*$rate);
        $data['exchange_margin_rate_id'] = $exchangeMarginRateItem->id;
        $data['final_amount'] = round(($total_amount * $rate) - ($total_amount * $marginRate * $rate)) ;
        $data['status'] = 1;
        $data['daily_limit'] = $request->input('limit');
        $data['exchange_rate_id'] = $exchangeRateId;
        // print_r($data);

        $fasExchangeRequest = FasTransferRequestService::createFasTransferRequest($data);
        // print_r($fasExchangeRequest);


        foreach($dayArray as $value){
            foreach($value->accounts as $accValue){
                // print_r($accValue);
                $item =[];
                $item['fas_transfer_request_id'] = $fasExchangeRequest;
                $item['bank_account_id'] = $accValue->bank_acc_id;
                $item['amount'] = $accValue->amount;
                $item['day'] = $value->day;
                FasTransferService::createFasTransfer($item);


            }

            // foreach($value->user_accounts as $userValue){
            //     // print_r($accValue);
            //     $itemUser =[];
            //     $itemUser['fas_transfer_request_id'] = $fasExchangeRequest;
            //     $itemUser['user_bank_account_id'] = $userValue->bank_acc_id;
            //     $itemUser['currency_id'] = $to_currency_id;
            //     $itemUser['amount'] = $userValue->amount;
            //     $itemUser['day'] = $value->day;
            //     FasTransferConvertService::createFasTransferConvert($itemUser);


            // }
        }
            // print_r($data);

        return response()->json([
            'message' => "Successfully create transfer request.",
            'id' =>  $fasExchangeRequest
            
        ], 200);
        return $fasExchangeRequest;
    }
    public function createFasTransferConvert(Request $request){


            $transferRequestId = $request->input('transfer_request');
            $transferDay = $request->input('day');
            $fasExchange = FasTransferRequestService::findOneFasTransferRequest($transferRequestId);
            $amount = $request->input('amount');
            
            $totalSumConvert = 0;
            $totalsum = 0;
            $totalConvert = 0;
            $marginRate = $fasExchange->exchange_margin_rate->rate;
            $rate = $fasExchange->exchange_rate->rate;

            // $marginRate = $fasExchange->exc
            $fasTransfer = $fasExchange->fas_transfers->groupBy('day');
            $fasTransfer = $fasTransfer->toArray();

            // $fastransfer = $fasExchange->fas_transfers->toArray();
            foreach($fasTransfer[$transferDay] as $value){
                $totalsum += floatval($value['amount']);
             }

             $totalConvert = round(($totalsum * $rate) - ($totalsum * $marginRate * $rate)) ;

            if( count($fasExchange->fas_transfer_convert)>0){
                $fasExchangeConvert = $fasExchange->fas_transfer_convert->groupBy('day');
                $fasExchangeConvert = $fasExchangeConvert->toArray();
                // print_r($fasExchangeConvert[$transferDay]);
                if(isset($fasExchangeConvert[$transferDay])){
                    foreach($fasExchangeConvert[$transferDay] as $value){
                        $totalSumConvert += floatval($value['amount']);
                     }
                }
                
            }
            if ($totalConvert < $totalSumConvert+$amount ){
                return response()->json([
                    'message' => "Amount is exceeded"
                ], 400);
            }

            $data = [];
            $data["fas_transfer_request_id"] = $request->input('transfer_request');
            $data["user_bank_account_id"] = $request->input('user_bank_account_id');
            $data["currency_id"] = $request->input('currency_id');
            $data["amount"] = $request->input('amount');
            $data["day"] = $request->input('day');


            $fasTransferConvert = FasTransferConvertService::createFasTransferConvert($data);
            return response()->json([
                'message' => "Successfully added bank to convert amount.",
                'id' =>  $fasTransferConvert
                
            ], 200);
    }

    public function findAllFasTransferRequest(Request $request){

        $user_id = $request->input('user_id');
        $user_account_id = $request->input('user_account');
        $fasExchange = FasTransferRequestService::findAllFasTransferRequest($user_id,$user_account_id);


        // print_r($fasExchange);
        $fasExchange->map(function($item) {

             $item->status = Status::TYPES[$item->status];

            return $item;
        });

        // dd('hi');
        return response()->json($fasExchange);

    }

    public function findOneFasTransferRequest($id){

        // $requestId = $request->input('request_id');
        $fasExchange = FasTransferRequestService::findOneFasTransferRequest($id);
        $fasExchange->fas_transfers->map(function($item) {
            if(isset($item->media[0])){
                $file_url = UtilityService::generateTemporaryFileUrl($item->media[0]->path);
        
                // $item->file = $file_url;
                $file = [];
                $file['uid'] = $item->media[0]->id;
                $file['name'] = $item->media[0]->filename;
                $file['url'] = $file_url;
                $item->file= [$file];
            }

            $item->status = Status::TYPES[$item->status];


            return $item;
        });

        $fasExchange->fas_transfer_convert->map(function($item) {


            $item->status = Status::TYPES[$item->status];


            return $item;
        });


// dd($fasExchange);

        // foreach ($fasAccounts as $account) {
        //     // print_r($account['media'][0]);

        //     if (isset($account['media'][0])) {
        //         $file_url = UtilityService::generateTemporaryFileUrl($account['media'][0]['path']);
        //         // $file_url = UtilityService::generateTemporaryFileUrl('');
        //         // $account->file_url = $file_url;
        //         $account['file_url']=$file_url;
        //         dd($account);
        //     }
        // }
        // $fasExchange->fas_transfers = $fasAccounts;     
        // dd($account);

        return response()->json($fasExchange);

    }
    public function uploadReceipt(Request $request)
    {
        try {

            $this->validate($request, [
                'filename' => 'required',
                'path' => 'required',
                'extension' => 'required',
                'mime' => 'required',
                'type' => 'required',
            ]);
            $user = auth()->user();

            $filename = $request->input('filename');
            $path = $request->input('path');
            $extension = $request->input('extension');
            $mime = $request->input('mime');
            $type = $request->input('type');
            $reference_id = $request->input('referenceid');

            // Add image to S3
            $storage = Storage::disk();
            // $filePath = 'media' . '/' . $user->id . '/' . 'reload';

            $filePath = 'banner';
            // $file = $request->file('file');
            $file = $request->file('image');

            $upload = $storage->putFileAs($filePath, $file, $filename);

            // Add image to DB
            $item = MediaService::createMedia($filename, $path, $extension, $mime, $reference_id, 'fas_transfers', $type);

            // FasTransferService::updateFasTransferStatus($reference_id,2);
            return response()->json([
                $item->id
            ], 200);

        }
        catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 404);
        }
    }

    public function removeReceiptById($id)
    {
        try {
            // print_r($id);

            $media = MediaService::findMediaById($id);

            $fasTransferId =  $media->reference_id;
            // return response()->json($media);

            if (is_null($media)) {
                return response()->json([
                    'message' => 'Something wrong'
                ], 400);
            } else {
                $media->delete();
                FasTransferService::updateFasTransferStatus($fasTransferId,1);

                return response()->json([
                    'message' => 'Successfully delete media'
                ]);
            }
        } catch (Throwable $th) {
            // return response()->json(['message' => 'Not able to delete this record'], 400);
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function removeUserBankAccount($id){
        $fasTransferConvert = FasTransferConvertService::findFasTransferConvertById($id);
        $fasTransferConvert->delete();
        return response()->json([
            'message' => "Successfully delete bank account",
            
        ], 200);
    }

    public function updateUserBankAccount(Request $request){

        $transferDay = $request->input('day');
        $requestId = $request->input('request_id');

        $fasExchange = FasTransferRequestService::findOneFasTransferRequest($requestId);
        $fasExchangeConvert = $fasExchange->fas_transfer_convert->groupBy('day');

       $fasArray = $fasExchangeConvert[$transferDay]->toArray();

       foreach($fasArray as $value){
            // $totalsum += floatval($value['amount']);
            // print_r($value['id']);
            FasTransferConvertService::updateFasTransferConvertStatus($value['id'],2);

        }
        // FasTransferConvertService::updateFasTransferConvertStatus($id,2);
        return response()->json([
            'message' => 'Successfully update bank account'
        ]);
    }

    public function exchangeMarginRate(){
        $exchangeMarginRateItem = ExchangeMarginRateService::findLatestExchangeMarginRate();
        return response()->json(
            $exchangeMarginRateItem
        , 200);
    }
    public function updateAllFasTransferStatusByDay(Request $request){

        $transferDay = $request->input('day');
        $requestId = $request->input('request_id');

        $fasExchange = FasTransferRequestService::findOneFasTransferRequest($requestId);
        $fasExchange = $fasExchange->fas_transfers->groupBy('day');

        $fasArray = $fasExchange[$transferDay]->toArray();

        foreach($fasArray as $value){
            FasTransferService::updateFasTransferStatus($value['id'],2);
        }
        return response()->json([
            'message' => 'Successfully update all transaction'
        ]);
    }

}
