<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\ExchangeRate;
use App\Services\ExchangeRateService;
use App\Services\SettingService;
use Carbon\Carbon;
use function _\map;

class AppExchangeRateController extends BaseController
{
    public function appExchangeRateList(Request $request, $is_from_api = true)
    {
        $fromCurrencyId = $request->query('from_currency_id');
        $currentDate = Carbon::now()->format('Y-m-d');

        $records = ExchangeRate::leftJoin('currencies as from_currency', 'exchange_rates.from_currency_id', 'from_currency.id')
            ->leftJoin('currencies as to_currency', 'exchange_rates.to_currency_id', 'to_currency.id')
            ->select([
                'exchange_rates.id',
                'rate',
                'effective_date',
                'from_currency_id',
                'from_currency.iso_code as from_currency_iso_code',
                'to_currency_id',
                'to_currency.iso_code as to_currency_iso_code',
            ])
            ->whereDate('effective_date', $currentDate)
            ->where(function ($query) use ($fromCurrencyId) {
                $query->where('from_currency_id', $fromCurrencyId)
                    ->orWhere('to_currency_id', $fromCurrencyId);
            })
            ->get();

        $processingFee = SettingService::extractGeneralSettingValueByKey('processing_fee');
        $processingFee = 1 - ($processingFee / 100);

        $finalData = map($records, function ($item) use ($fromCurrencyId, $processingFee) {

            if ($item['from_currency_id'] != $fromCurrencyId) {

                $fromCurrency = [
                    'id' => $item['to_currency_id'],
                    'iso_code' => $item['to_currency_iso_code']
                ];

                $toCurrency = [
                    'id' => $item['from_currency_id'],
                    'iso_code' => $item['from_currency_iso_code']
                ];

                $rate = round(1 / $item['rate'] * $processingFee, 4);

                return [
                    'id' =>  $item['id'],
                    'effective_date' => $item['effective_date'],
                    'from_currency_id' => $fromCurrency['id'],
                    'from_currency_iso_code' => $fromCurrency['iso_code'],
                    'to_currency_id' => $toCurrency['id'],
                    'to_currency_iso_code' => $toCurrency['iso_code'],
                    'exchange_rate' => '1 ' . $fromCurrency['iso_code'] . ' = ' . $rate . ' ' . $toCurrency['iso_code']
                ];
            } else {
                $rate = round($item['rate'] * $processingFee, 4);

                $item['exchange_rate'] = '1 ' . $item['from_currency_iso_code'] . ' = ' . $rate . ' ' . $item['to_currency_iso_code'];

                return $item;
            }
        });

        if ($is_from_api) {
            return response()->json($finalData, 200);
        } else {
            return collect($finalData);
        }
    }

    public function findAllExchangeRatesByCurrencyIds(Request $request)
    {

        $from_currency_id = $request->query('from_currency_id');
        $to_currency_id = $request->query('to_currency_id');

        return ExchangeRateService::findAllExchangeRatesByCurrencyIds($from_currency_id, $to_currency_id);
    }

    public function findOneByCurrencyIdsAndDate(Request $request)
    {
        $this->validate($request, [
            'from_currency_id' => 'required',
            'to_currency_id' => 'required',
            'effective_date' => 'required',
        ]);

        $from_currency_id = $request->query('from_currency_id');
        $to_currency_id = $request->query('to_currency_id');
        $effective_date = $request->query('effective_date');

        return ExchangeRateService::findOneByCurrencyIdsAndDate($from_currency_id, $to_currency_id, urlencode($effective_date));
    }

    public function findOneByCurrencyIds(Request $request)
    {
        $this->validate($request, [
            'from_currency_id' => 'required',
            'to_currency_id' => 'required',
        ]);

        $from_currency_id = $request->query('from_currency_id');
        $to_currency_id = $request->query('to_currency_id');

        return ExchangeRateService::findOneByCurrencyIds($from_currency_id, $to_currency_id);
    }
}
