<?php

namespace App\Http\Controllers\ApiV2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TransactionPointService;


class AppTransactionPointController extends Controller
{
    public function findLatestTransactionPointByUserId(Request $request){

        $user = auth()->user();
        $userId = $user->id;
        $latestPoint = TransactionPointService::findLatestTransactionPointByUserId($userId);

        return $latestPoint->new_balance;

    }

    public function findTransactionPointByUserId(Request $request){

        $user = auth()->user();
        $userId = $user->id;
        $transactionList = TransactionPointService::findTransactionPointByUserId($userId);
        return response()->json($transactionList);


    }
}
