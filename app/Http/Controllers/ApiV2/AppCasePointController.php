<?php

namespace App\Http\Controllers\ApiV2;

use App\Services\CasePointService;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Services\UtilityService;

use Throwable;

use function _\map;
use function _\count;

class AppCasePointController extends BaseController
{
    public function findCaseByUserId()
    {
        $user = auth()->user();
        $userId = $user->id;
        $case = CasePointService::findCaseByUserId('reload_points', $userId);

        if($case){
            $count =  ($case->casePoint->media->count());

            if($count>0){
                $count =  ($case->casePoint->media->count());
                $item = $case->casePoint->media[$count-1];

                $file_url = UtilityService::generateTemporaryFileUrl($item->path);
        
                // $item->file = $file_url;
                $file = [];
                $file['uid'] = $item->id;
                $file['name'] = $item->filename;
                $file['url'] = $file_url;
                $case->casePoint->file= [$file];
            }
        }



        return  response()->json($case);

    }

    public function updateCaseStatueById()
    {
        $user = auth()->user();
        $userId = $user->id;
        return CasePointService::updateCaseStatueById($userId);

    }

}
