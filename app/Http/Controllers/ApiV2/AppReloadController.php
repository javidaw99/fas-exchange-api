<?php

namespace App\Http\Controllers\ApiV2;

use App\Services\CaseService;
use App\Services\ReloadService;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\UtilityService;
use App\Services\ActivityLogService;
use App\Models\Status;

use Faker\Factory;
use function _\map;

class AppReloadController extends BaseController
{
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function findReloadRecordById($id)
    {
        $user = auth()->user();
        $reload = ReloadService::findReloadRecordById($id);
        $reload->file_url = UtilityService::generateTemporaryFileUrl($reload->path);
        $reloadLogs = ActivityLogService::findAllByReferenceTableReferenceID("reloads", $id);

        $reloadLogs = $reloadLogs->filter(function ($item) {
            return $item->action_type !== 'View';
        });

        $language = !is_null($user->app_language) ? $user->app_language : 'en';
        $locale = UtilityService::loadLocale($language);

        $reloadLogs = map($reloadLogs, function($item) use ($locale) {

            $description = "";
            $title = "";
            
            preg_match_all('/\d+/', $item->description, $numbers);
            $status = end($numbers[0]);

            if($item->action_type == "Create") {
                $status = 1;
                $title = $locale->t('transaction_timeline.title_1');
                $description = $locale->t('transaction_timeline.description_1');
            }
            else if($item->action_type == "Edit") {
                if($status == 2) {
                    $title = $locale->t('transaction_timeline.title_2');
                    $description = $locale->t('transaction_timeline.description_2');
                }
                else if ($status == 3) {
                    $title = $locale->t('transaction_timeline.title_3');
                    $description = $locale->t('transaction_timeline.description_3');
                }
                else if ($status == 5) {
                    $title = $locale->t('transaction_timeline.title_4');
                    $description = $locale->t('transaction_timeline.description_4');
                }
            }

            return [
                "date" => $item->created_at,
                "status" => $status,
                "title" => $title,
                "description" => $description
            ];
        });

        if (empty($reload)) {
            return response()->json(['message' => 'Record not found'], 400);
        }

        $case = CaseService::getCaseByTypeAndRefId('reloads', $reload->id);

        if ($case) {
            $reload->status = Status::TYPES[$case->status];
            $reload->reject_reason = $case->reject_reason;
        } else {
            $reload->status = Status::TYPES['3'];
            $reload->reject_reason = null;
        }

        $reload->logs = $reloadLogs;

        return response()->json($reload);
    }

    public function initiateReloadsAction()
    {
        $referenceNumber = $this->generateUniqueReferenceNumber();

        return response()->json([
            'message' => 'Generated new reference number',
            'reference_number' => $referenceNumber
        ]);
    }

    public function generateUniqueReferenceNumber()
    {
        $newReferenceNumber = $this->faker->regexify('[abcdefghijkmnpqrstuvwxyz1-9]{10}');

        if (ReloadService::checkReloadReferenceNumberUniqueness($newReferenceNumber)) {
            return $newReferenceNumber;
        }

        $this->generateUniqueReferenceNumber();
    }
}
