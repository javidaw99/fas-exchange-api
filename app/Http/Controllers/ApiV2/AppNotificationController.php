<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Services\NotificationService;
use App\Services\CaseService;
use App\Services\UtilityService;
use App\Models\Cases;
use App\Models\Status;

class AppNotificationController extends BaseController
{
    public function getNotifications(Request $request)
    {
        $user = auth()->user();
        $userId = $user-> id;
        $pageSize = 10;

        $appLang = !is_null($user->app_language) ? $user->app_language : 'en';

        $locale = UtilityService::loadLocale($appLang);

        $records = Notification::select(
                'notifications.id',
                'title', 
                'content', 
                'is_read', 
                'ref_type', 
                'ref_id', 
                'notifications.created_at',
                'locale_mapper.title_key',
                'locale_mapper.content_key',
                'locale_mapper.dynamic_values',
            )
            ->leftJoin('locale_mapper', 'notifications.locale_mapper_id', 'locale_mapper.id')
            ->where('user_id',  $userId)
            // ->orderByDesc('created_at')
            ->orderBy('created_at', 'desc')
            ->paginate($pageSize);
            
        $alteredRecords = $records->getCollection()->each(function($item) use ($locale) {

            if(!is_null($item['title_key']) && !is_null($item['title_key'])) {
                $item['title'] = $locale->t($item['title_key']);

                if(!is_null($item['dynamic_values'])) {
                    $variables = json_decode($item['dynamic_values'], true); // Convert JSON to PHP array
                    $item['content'] = $locale->t($item['content_key'], $variables);
                }
                else {
                    $item['content'] = $locale->t($item['content_key']);
                }
            }

            if($item->ref_type == 'reload') {
                
                $case = CaseService::getCaseStatusWithStatusName('reloads', $item->ref_id);
                $item->setAttribute('status', $case->status_name);

            } else if($item->ref_type == 'transfers') {
                
                $case = CaseService::getCaseStatusWithStatusName('transfers', $item->ref_id);

                if($case) {
                    $item->setAttribute('status', $case->status_name);

                } else {
                    $item->setAttribute('status',  Status::TYPES['3']);

                }

            } else if($item->ref_type == 'exchange') {
                
                $case = CaseService::getCaseStatusWithStatusName('exchange', $item->ref_id);
                $item->setAttribute('status', $case->status_name);

            }
        });

        $records->setCollection($alteredRecords);

        $count = Notification::where('user_id',  $userId)
            ->where('is_read', 0)
            ->count();

        $records = collect($records);

        $records->put('total_unread' , $count);

        return response()->json($records);

    }

    public function markMessageAsRead(Request $request)
    {
     
        Notification::where('id', $request->input('id'))
        ->update(['is_read' =>  1]);  

        return response()->json([
            'message' => 'Selected message is marked as read',
        ]);

    }   
 
}
