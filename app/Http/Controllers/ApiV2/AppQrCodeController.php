<?php

namespace App\Http\Controllers\ApiV2;

use App\Services\UserService;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Throwable;

class AppQrCodeController extends BaseController
{
    public function generateQrCode() 
    {
        try {
            $user = auth()->user();

            $encryptedQrCode = UserService::generateEncryptedQrCode($user->id);

            return response()->json(['qrcode' => $encryptedQrCode], 200);
        }
        catch(Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }
    }

    public function verifyQrCode(Request $request)
    {
        try {
            $this->validate($request, [
                'code' => 'required'
            ]);
    
            $code = $request->input('code');
    
            $result = UserService::verifyEncryptedQrCode($code);

            if(!$result['is_valid']) {
                return response()->json([
                    'message' => 'Invalid QRCode'
                ], 400);
            }

            return response()->json(['user' => $result['user']], 200);
        }
        catch(Throwable $th) {
            return response()->json([
                'message' => 'Invalid QRCode'
            ], 400);
        }
    }
}
