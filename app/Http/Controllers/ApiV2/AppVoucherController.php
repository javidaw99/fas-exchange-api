<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\UserService;
use App\Services\CurrencyService;
use App\Services\RedeemService;
use Illuminate\Support\Facades\Http;
use Throwable;
use App\Services\NotificationService;
use App\Services\UtilityService;

class AppVoucherController extends BaseController
{
    public function redeemVoucherByCode(Request $request)
    {

        // Add logic to check if user have account
        try {
            $this->validate($request, [
                'code' => ['required', 'string'],
                'currency_code' => ['required', 'string']
            ]);

            $userId = auth()->user()->id;

            $user = UserService::findOneUserById($userId);

            if (empty($user)) {
                return response()->json(['message' => 'User not found'], 400);
            }

            $platform_code = env('GIFTPACE_PLATFORM_CODE');
            $code = $request->input('code');
            $currency_code = $request->input('currency_code');
            $username = $user->username;
            $fullname = $user->fullname ? $user->fullname : $user->username;
            $email = $user->email;
            $phone = $user->phone_no;
            $timestamp = Carbon::now()->timestamp;

            $signature = strtoupper($code) . '|' . $username . '|' . $phone . '|' . $timestamp . '|' . $platform_code . '|' . strtoupper($currency_code) . '|' . env('GIFTPACE_SECRET_KEY');
            $hashed = md5($signature);

            $payload = [
                "code" => $code,
                "currency_code" => $currency_code,
                "username" => $username,
                "fullname" => $fullname,
                "email" => $email,
                "phone" => $phone,
                "timestamp" => $timestamp,
                "hashed" => $hashed
            ];

            $res = Http::post(env('GIFTPACE_API_URL') . '/v1/vouchers/' . $platform_code . '/redeem', $payload);

            $result = $res->json();

            $currency = CurrencyService::findOneByCode($currency_code);

            if ($res->failed()) {
                $errorMessage = 'Fail to redeem voucher with code ' . $code;

                if ($res->status() == 400) {
                    if ($result['error_code'] == '203') {
                        $errorMessage = 'Invalid voucher code';
                    } else if ($result['error_code'] == '204') {
                        $errorMessage = 'Voucher already redeemed before';
                    }

                    RedeemService::createVoucherRedeemRecord([
                        'user_id' => $userId,
                        'voucher_code' => $code,
                        'currency_id' => $currency->id,
                        'status' => 'failed'
                    ]);

                    return response()->json([
                        'code' => $result['error_code'],
                        'message' => $errorMessage
                    ], $res->status());
                }

                RedeemService::createVoucherRedeemRecord([
                    'user_id' => $userId,
                    'voucher_code' => $code,
                    'currency_id' => $currency->id,
                    'status' => 'failed'
                ]);

                return response()->json([
                    'message' => $errorMessage
                ], $res->status());
            }

            $redeem = RedeemService::createVoucherRedeemRecord([
                'user_id' => $userId,
                'voucher_code' => $code,
                'currency_id' => $currency->id,
                'amount' => $result['amount'],
                'claimed_at' => Carbon::createFromTimestamp($result['timestamp']),
                'status' => 'success'
            ]);

            $language = !is_null($user->app_language) ? $user->app_language : 'en';

            $locale = UtilityService::loadLocale($language);

            $notification = NotificationService::sendPushNotification([
                'fcm_token' => $user->fcm_token,
                'title' => $locale->t('notification.title_4'),
                'body' => $locale->t('notification.description_7'),
                'target_user_id' => $user->id
            ], 'redeem',  $redeem->id);

            UtilityService::createLocaleMapperRecord([
                'title_key' => 'notification.title_4',
                'content_key' => 'notification.description_7',
                'reference_table' => 'notification',
                'reference_id' => $notification->id
            ]);

            return response()->json([
                'message' => 'Successfully redeem giftpace voucher with code ' . $code
            ], 200);
        } catch (Throwable $th) {
            return response()->json([
                'message' => 'Fail to redeem voucher with code ' . $code
            ], 404);
        }
    }

    public function findRedeemRecordById($id)
    {
        try {
            $redeem = RedeemService::findRedeemRecordById($id);

            return response()->json($redeem);
        } catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 400);
        }
    }
}
