<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Bank;
use App\Models\BankCountry;
use App\Models\BankAccount;
use App\Services\AccountService;
use App\Services\BankService;
use App\Services\CountryService;
use App\Services\UtilityService;
use Illuminate\Support\Facades\DB;
use Throwable;
use App\Models\CountryCurrency;


class AppBankController extends BaseController
{
    public function bankCountryWithBankAccounts(Request $request)
    {

        $this->validate($request, [
            'currency' => ['required', 'integer'],
            'country' => ['required', 'integer']
        ]);

        $currencyId = $request->input('currency');
        $countryId = $request->input('country');

        $bankCountries = BankCountry::where('bank_country.country_id', $countryId)
            ->with('bankAccount', function ($query) use ($currencyId, $countryId) {
                $query->where('bank_accounts.country_id', $countryId)
                    ->where('bank_accounts.currency_id', $currencyId);
            })
            // ->where('bank_accounts.country_id', $countryId)
            // ->where('bank_accounts.currency_id', $currencyId)
            ->get();

        return response()->json($bankCountries);

        // BankAccount::where('currency_id', $currencyId)
        // ->where('country_id', $countryId)
        // ->with('')
        // ->get();
    }

    public function findAllBankAvailableCountries(Request $request)
    {
        $country_ids = BankService::findAllBankAvailableCountries();

        return $country_ids;
    }

    public function findAllBankAvailableCountriesByCurrency($currencyId)
    {
        $country_ids = CountryCurrency::with([
            'country' => function ($query) {
                $query->select('id', 'name', 'iso_code_2');
            },
            'currency' => function ($query) {
                $query->select('id', 'currency_name', 'iso_code');
            },

        ])->whereHas('country', function ($query) {
            $query->where('is_enabled', true);
        })->where('currency_id', $currencyId)
          ->get();

        return $country_ids;
    }

    public function findAllBanks()
    {
        $banks = BankService::findAllBanks();

        return $banks;
    }

    public function findAllBankWithBankCountries()
    {
        $banks = BankService::findAllBankWithBankCountries();

        return $banks;
    }

    // public function findAllAvailableBankAccounts(Request $request)
    // {

    //     $this->validate($request, [
    //         'currency_id' => ['required', 'integer'],
    //         'country_id' => ['integer']
    //     ]);

    //     $currencyId =  $request->input('currency_id');
    //     $countryId =  $request->input('country_id');
    //     $version = $request->header('version');

    //     if(!$version) {
    //         $bankAccounts = BankService::findAllBankAccountsByCurrencyAndCountry($currencyId, $countryId);
    //         $bankAccounts = $bankAccounts->toArray();

    //         $totalBankAccounts = count($bankAccounts);

    //         // If total bank accounts is less than 3, take the total count of the records
    //         $totalBankAccountToDisplay = $totalBankAccounts < 3 ? $totalBankAccounts : 3;

    //         $filteredBankAccounts = array();

    //         if($totalBankAccounts > 0) {
    //             while(count($filteredBankAccounts) < $totalBankAccountToDisplay) {
    //                 $randomIndex = rand(0, $totalBankAccounts - 1);

    //                 if(array_key_exists($randomIndex, $bankAccounts)) {
    //                     $selectedBankAccount = $bankAccounts[$randomIndex];

    //                     // Insert selected index to new array
    //                     array_push($filteredBankAccounts, $selectedBankAccount);

    //                     // Remove selected index to avoid duplicate
    //                     unset($bankAccounts[$randomIndex]);
    //                 }
    //             }
    //         }

    //         return response()->json($filteredBankAccounts);
    //     }
    //     else {
    //         $bankAccounts = BankService::findAllBankAccountsByLowestScore($currencyId);
                
    //         return response()->json($bankAccounts);
    //     }
    // }

    public function findAllAvailableBankAccounts(Request $request)
    {

        $this->validate($request, [
            'currency_id' => ['required', 'integer'],
            'country_id' => ['integer']
        ]);

        $currencyId =  $request->input('currency_id');
        $countryId =  $request->input('country_id');
        // $version = $request->header('version');

        // if(!$version) {
            $bankAccounts = BankService::findAllBankAccountsByCurrencyAndCountry($currencyId, $countryId);
            // $bankAccounts = $bankAccounts->toArray();

            // $totalBankAccounts = count($bankAccounts);

            // // If total bank accounts is less than 3, take the total count of the records
            // $totalBankAccountToDisplay = $totalBankAccounts < 3 ? $totalBankAccounts : 3;

            // $filteredBankAccounts = array();

            // if($totalBankAccounts > 0) {
            //     while(count($filteredBankAccounts) < $totalBankAccountToDisplay) {
            //         $randomIndex = rand(0, $totalBankAccounts - 1);

            //         if(array_key_exists($randomIndex, $bankAccounts)) {
            //             $selectedBankAccount = $bankAccounts[$randomIndex];

            //             // Insert selected index to new array
            //             array_push($filteredBankAccounts, $selectedBankAccount);

            //             // Remove selected index to avoid duplicate
            //             unset($bankAccounts[$randomIndex]);
            //         }
            //     }
            // }

            return response()->json($bankAccounts);
        // }
        // else {
        //     $bankAccounts = BankService::findAllBankAccountsByLowestScore($currencyId);
                
        //     return response()->json($bankAccounts);
        // }
    }



    public function findAllBankAccountsByLowestScore(Request $request)
    {

        $this->validate($request, [
            'currency_id' => ['required', 'integer'],
        ]);

        $currencyId =  $request->input('currency_id');

        $bankAccounts = BankService::findAllBankAccountsByLowestScore($currencyId);
    
        return response()->json($bankAccounts);
    }


    public function findCountryCurrencyById(Request $request)
    {

        $countryId = $request->query('country_id');

        $currencies =  CountryService::findCountryCurrencyById($countryId);

        return response()->json($currencies);
    }
}
