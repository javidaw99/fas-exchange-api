<?php

namespace App\Http\Controllers\ApiV2;


use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Throwable;
use App\Services\AddressService;
use App\Services\UserService;


class AppAddressController extends BaseController
{
    public function createAddress(Request $request)
    {

      
       $address = [];
       $address['address_1'] = $request->input('address_1');
       $address['address_2'] = $request->input('address_2');
       $address['city'] = $request->input('city');
       $address['postcode'] = $request->input('postcode');
       $address['state_id'] = $request->input('state_id');
       $address['country_id'] = $request->input('country_id');

       $created = AddressService::createAddress($address);
   
        if($created){
            $user["address_id"] = $created;
            $updateUser = UserService::updateUserById($user);

            if($updateUser){
                return response()->json([
                    'message' => "Successfully update user's address.",
                    'user' => $updateUser
                    
                ], 200);
            }else{
                return response()->json([
                    'message' => "Failed to update User for Address.",
                   
                ], 400);  
            }

        }else{
            return response()->json([
                'message' => "Failed to create Address"
            ], 400);
        }

 
    }
    public function updateAddressById(Request $request)
    {

      
       $address = [];
       $address['address_1'] = $request->input('address_1');
       $address['address_2'] = $request->input('address_2');
       $address['city'] = $request->input('city');
       $address['postcode'] = $request->input('postcode');
       $address['state_id'] = $request->input('state_id');
       $address['country_id'] = $request->input('country_id');
       $user = auth()->user();
       $addressId = $user->address_id;
       $userId = $user->id;
    

       $updated = AddressService::updateAddressById($address,$addressId);

       $userData = UserService::findOneUserById($userId);
   
     
        if($updated){
            return response()->json([
                'message' => "Successfully update user's address.",
                'user' => $userData
                
            ], 200);
        }else{
            return response()->json([
                'message' => "Nothing updated",
                
            ], 202);  
        }

    
 
    }
}

