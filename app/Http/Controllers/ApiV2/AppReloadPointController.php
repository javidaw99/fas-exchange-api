<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Services\MediaService;
use App\Services\ReloadPointService;

use Throwable;

class AppReloadPointController extends BaseController
{
    public function createReloadPoint(Request $request){

        $user = auth()->user();

        $type = $request->input('type');
        $bankAccountId = $request->input('bank_account_id');
        $bankId = $request->input('bank_id');
        $amount = $request->input('amount');
        $status = $request->input('status');
        $userId = $user->id;

        $payload = [
            'type' => $type,
            'bank_account_id' => $bankAccountId,
            'bank_id'=> $bankId,
            'user_id' => $userId,
            'amount' => $amount,
            'status' => $status,
        ];

        $reloadPoint = ReloadPointService::createReloadPoint($payload);

      return response()->json([
            'message' => "Successfully topup point.",
            
        ], 200);

    }
    public function uploadReloadReceipt(Request $request)
    {
        try {

            $this->validate($request, [
                'filename' => 'required',
                'path' => 'required',
                'extension' => 'required',
                'mime' => 'required',
                'type' => 'required',
            ]);
            $user = auth()->user();

            $filename = $request->input('filename');
            $path = $request->input('path');
            $extension = $request->input('extension');
            $mime = $request->input('mime');
            $type = $request->input('type');
            $reference_id = $request->input('referenceid');

            // Add image to S3
            $storage = Storage::disk();
            // $filePath = 'media' . '/' . $user->id . '/' . 'reload';

            $filePath = 'banner';
            // $file = $request->file('file');
            $file = $request->file('image');

            $upload = $storage->putFileAs($filePath, $file, $filename);

            // Add image to DB
            $item = MediaService::createMedia($filename, $path, $extension, $mime, $reference_id, 'reload_points', $type);

            // FasTransferService::updateFasTransferStatus($reference_id,2);
            return response()->json([
                $item->id
            ], 200);

        }
        catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 404);
        }
    }
}
