<?php

namespace App\Http\Controllers\ApiV2;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\OnboardingService;
use App\Models\Onboarding;
use App\Models\OnboardingDetails;
use Illuminate\Support\Facades\DB;

class AppOnboardingController extends BaseController
{
    public function index(Request $request)
    {

        try{

             $record = OnboardingService::getAllOnboardingData();


            return json_decode($record);

        } catch(Throwable $e) {
            return response()->json(['message' => $e], 400);
        }
    }
    
}
