<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Services\MediaService;
use App\Services\UtilityService;

class AppBannerController extends BaseController
{
    public function findAllBanners(Request $request)
    {
        $records = MediaService::findAllBanners();

        $records = $records
        ->where('is_enabled', 1)
        ->orderBy('banners.sequence', 'asc')
        ->get();

        foreach ($records as $banner) {
            if (!is_null($banner->path)) {
                $file_url = UtilityService::generateTemporaryFileUrl($banner->path);
                $banner->source = $file_url;
            }
        }

        return response()->json([
            'data' => $records
        ]);

    }

}
