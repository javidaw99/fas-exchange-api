<?php

namespace App\Http\Controllers\ApiV2;
use App\Services\TransferPointService;

use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Throwable;

class AppTransferPointController extends BaseController
{
    public function createTransferPoint(Request $request){


      $data = [
        'type' => $request->input('type'),
        'point_margin_rate_id' =>  $request->input('point_margin_rate_id'), 
        'amount' =>  $request->input('amount'), 
        'converted_amount' => $request->input('converted_amount'),
        'point_rate_margin' => $request->input('point_rate_margin'),
        'to_user_id' => $request->input('to_user_id'),
        'user_id' => $request->input('user_id'),
        'from_transfer_points_id' => $request->input('from_transfer_points_id'),
        'user_bank_account_id' => $request->input('user_bank_account_id'),
        'commission_rate' => $request->input('commission_rate'),
        'user_account_id' => $request->input('user_account_id'),
        'convert_rate' => $request->input('convert_rate')

      ];

        $transferPoint = TransferPointService::createTransferPoint($data);


        if ($transferPoint) {
            return response()->json([
                'message' => 'Successfully transfer',
                'data'=> $transferPoint->id
            ]);
        } else {
            return response()->json([
                'message' => 'Something wrong'
            ], 500);
        }
    }
}
