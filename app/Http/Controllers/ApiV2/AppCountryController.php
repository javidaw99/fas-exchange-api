<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use App\Models\CountryCurrency;
use App\Services\AccountService;
use App\Services\CountryService;
use App\Services\UtilityService;

class AppCountryController extends BaseController
{
    public function countryWithCurrencies(Request $request)
    {

        $type = $request->input('type');

        if ($type === 'all') {
            $countries_currencies = Country::with([
                'currency:id,currency_name,iso_code'
            ])
                ->where('is_enabled', 1)
                ->get();

            return response()->json($countries_currencies);
        } else {
            $countries_currencies = Country::with([
                'currency:id,currency_name,iso_code'
            ]);

            $fields = array(
                'name',
                'iso_code_2',
            );

            $records = UtilityService::modelQueryBuilder($countries_currencies, $request, true, $fields);

            return response()->json($records);
        }
    }

    public function findCountryCurrencyById($id)
    {
        $currencies =  CountryService::findCountryCurrencyById($id);

        return response()->json($currencies);
    }

    public function findCurrencyByEnabledCurrency()
    {
        $countries = CountryCurrency::select('country_id')->distinct()->get();

        $countryIds = $countries->map(function ($item) {
            return $item->country_id;
        })->all();

        $countries = Country::whereIn(
            'id',
            $countryIds
        )
        ->where('is_enabled', 1)
        ->get();

        return response()->json($countries);
    }
}
