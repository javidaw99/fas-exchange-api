<?php

namespace App\Http\Controllers\ApiV2;

use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\CaseService;
use App\Services\AccountService;
use App\Models\BankCountry;
use App\Http\Controllers\ApiV2\AppExchangeRateController;
use App\Models\Cases;
use App\Models\Status;

class AppCaseController extends BaseController
{
    public function userCases(Request $request)
    {

        $now = Carbon::now();

        $month = $request->query('month');
        $year = $request->query('year');

        if (is_null($month) || is_null($year)) {
            $month = $now->month;
            $year = $now->year;
        }

        $cases = CaseService::getUserCasesByMonthAndYear($month, $year);

        return response()->json($cases);
    }

    public function exchangeInitialise(Request $request)
    {
        $userId = $request->input('user_id');
        $appExchangeRateController = new AppExchangeRateController;

        $accounts = AccountService::getUserSourceBalanceWithCurrency($userId);

        $exchangeRates = [];

        if (count($accounts) > 0) {
            $request->request->add([
                'from_currency_id' => $accounts[0]->currency_id
            ]);

            $exchangeRates = $appExchangeRateController->appExchangeRateList($request, false);
        }

        return response()->json([
            'accounts' => $accounts,
            'exchange_rates' => $exchangeRates
        ]);
    }

    public function transferInitialise()
    {

        $user = auth()->user();

        $sourceBalance = AccountService::getUserSourceBalanceWithCurrency($user->id);
        $destinationCountry = AccountService::getDestinationCountry();
        $recipientBanks = AccountService::getRecipientBanks();

        $array = [
            'accounts' => $sourceBalance,
            'destinations' => $destinationCountry,
            'banks' => $recipientBanks
        ];

        return response()->json($array);
    }

    public function reloadInitialise()
    {

        $country = AccountService::getDestinationCountry();
        $bankCountries = BankCountry::with('bankAccount')->get();

        $array = [
            'countries' => $country,
            'banks' => $bankCountries,
        ];

        return response()->json($array);
    }

    public function checkLatestCaseStatusByType(Request $request, $type)
    {
        $version = $request->header('version');

        if(!$version) {
            $pattern = '/(^reload$|^transfer$)/';
        }
        else {
            $pattern = '/(^reloads$|^transfers$)/';
        }

        preg_match($pattern, $type, $matchedType);

        if (!count($matchedType)) {
            return response()->json([
                'message' => 'Invalid request'
            ], 400);
        }


        $user = auth()->user();

        $case = Cases::where([
            'reference_table' => $type,
            'user_id' => $user->id,
        ])->orderByDesc('created_at')->first();

        if (!empty($case)) {
            $status = Status::TYPES[$case->status];

            preg_match('/pending/', $status, $output_array);

            if (count($output_array)) {
                return response()->json([
                    'message' => 'Your last request is still pending or processing. Please wait for it to finish.'
                ], 400);
            }
        }

        return response()->json([
            'message' => 'You are eligible to add fund'
        ], 200);
    }
}
