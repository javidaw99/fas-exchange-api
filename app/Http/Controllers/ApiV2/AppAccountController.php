<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\AccountService;
use App\Services\ExchangeRateService;
use App\Services\NotificationService;
use App\Services\CaseService;
use App\Services\UtilityService;
use App\Services\SocketService;
use App\Models\Media;
use App\Models\Status;
use App\Services\ReloadService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;

class AppAccountController extends BaseController
{
    public function reload(Request $request)
    {

        $this->validate($request, [
            'currencyId' => ['required', 'integer'],
            'bankAccountId' => ['required', 'integer'],
            'receiptRef' => ['required'],
            'amount' => ['required', 'integer', 'not_in:0'],
            'image' => ['required', 'file']
        ]);

        $user = auth()->user();

        $latestCaseStatus = AccountService::getLatestUserCaseStatus($user);

        if ($latestCaseStatus == 1 || $latestCaseStatus == 2) {
            return response()->json([
                'message' => 'Your last request is still pending or processing. Please wait for it to finish.'
            ], 400);
        }

        $currencyId = $request->input('currencyId');
        $bankAccountId = $request->input('bankAccountId');
        $amount = $request->input('amount');
        $receiptRef = $request->input('receiptRef');

        $currency = AccountService::findCurrencyById($currencyId);


        if (is_null($currency)) {
            return response()->json([
                'message' => 'Currency does not exists'
            ], 400);
        }

        $bankAccount = AccountService::findBankAccountById($bankAccountId);

        if (is_null($bankAccount)) {
            return response()->json([
                'message' => 'ME bank account does not exists'
            ], 400);
        }

        //store image into S3
        $storage = Storage::disk();

        $filePath = 'media' . '/' . $user->id . '/' . 'reload';
        $file = $request->file('image');
        $fileName = Str::uuid($file)->toString();

        $upload = $storage->putFileAs($filePath, $file, $fileName . '.' . $file->extension());

        //store image into DB
        $data = [];
        $data['extension'] = $file->extension();
        $data['filename'] = $fileName . '.' . $data['extension'];
        $data['storageId'] = Media::TYPES['primary_repo'];
        $data['path'] = $filePath . '/' . $fileName . '.' . $data['extension'];
        $data['mime'] = $file->getMimeType();

        $storeImage = AccountService::storeImageIntoDB($data);

        /**
         * Create 'reloads' record
         */

        $amount = UtilityService::convertAmountToDecimal($amount, $currency->id);

        $reload = AccountService::createReloadRecord($bankAccountId, $currencyId, $amount, $receiptRef, $storeImage->id, $currency->iso_code);

        $userAccount = AccountService::findUserCurrencyAccountByCurrencyId($user, $currencyId);

        if (is_null($userAccount)) {

            /**
             * Create 'cases' record without currency account if currency account never created before
             */
            $case = AccountService::createCaseRecord($user->id, 'reloads', $reload->id);
        } else {

            /**
             * Create 'cases' record with existing currency account
             */
            $case = AccountService::createCaseRecord($user->id, 'reloads', $reload->id, $userAccount->id);
        }

        if (isset($case->wasRecentlyCreated) && $case->wasRecentlyCreated === true) {

            $userCase = CaseService::getCasesByRefType('reload', null)->find($case->id);
            $userCase = UtilityService::appendAttribute(collect([$userCase]), [
                [
                    'name' => 'formatted_amount',
                    'path' => 'transact.formatted_amount',
                    'hide' => 'transact'
                ]
            ]);

            // SocketService::emitSocketData('/add-reload', $userCase[0]->toArray());

            $notification = NotificationService::sendPushNotificationInBulk([
                'title' => 'New Reload Request',
                'body' => 'New Reload Request',
            ], 'reload', $reload->id);

            return response()->json([
                'message' => 'Processing transaction'
            ]);
        } else {
            return response()->json([
                'message' => 'Something wrong'
            ], 500);
        }
    }

    public function createReloadCase(Request $request)
    {
        $this->validate($request, [
            // 'bank_account_id' => 'required',
            'currency_id' => 'required',
            'user_account_id' => 'required',
            'amount' => 'required|numeric|min:2',
            // 'reference_no' => 'required',
            // 'file' => 'required|file'
        ]);

        $user = auth()->user();

        $bankAccountId = $request->input('bank_account_id');
        $currencyId = $request->input('currency_id');
        $userAccountId = $request->input('user_account_id');
        $amount = $request->input('amount');
        $referenceNo = $request->input('reference_no');
        $userId = $user->id;

        /**
         * TODO:
         * 1. Check if there is existing reloads account that is not approved
         * 2. If existing record is found, throw error to inform user that previous cases has to be confirmed before he/she can request a new one
         * 3. If record not found, new reloads record will be created
         */

        $payload = [
            'bank_account_id' => $bankAccountId,
            'currency_id' => $currencyId,
            'user_account_id' => $userAccountId,
            'user_id' => $userId,
            'amount' => $amount,
            'reference_no' => $referenceNo,
        ];

        $reload = ReloadService::findReloadByInputs($payload);

        // if (!empty($reload)) {
        //     $status = Status::TYPES[$reload->case->status];

        //     preg_match('/pending/', $status, $output_array);

        //     if (count($output_array)) {
        //         return response()->json([
        //             'message' => 'Your same request is still pending or processing. Please wait for it to finish.'
        //         ], 400);
        //     }
        // }

        $reloadId = ReloadService::createReload($payload);

        //copy image into local media storage
        // $storage = storage_path();

        // $storage = Storage::disk();

        // $filePath = 'media' . '/' . $user->id . '/' . 'reload';
        // $file = $request->file('file');
        // $fileName = Str::uuid($file)->toString();

        // $upload = $storage->putFileAs($filePath, $file, $fileName . '.' . $file->extension());

        // //save image to media
        // $data = [];
        // $data['extension'] = $file->extension();
        // $data['filename'] = $fileName . '.' . $data['extension'];
        // $data['path'] = $filePath . '/' . $fileName . '.' . $data['extension'];
        // $data['mime'] = $file->getMimeType();
        // $data['reference_id'] = $reloadId;
        // $data['reference_table'] = 'reloads';
        // $data['type'] = 'receipt';

        // AccountService::storeImageIntoDB($data);

        // $notification = NotificationService::sendPushNotificationInBulk([
        //     'title' => 'New Reload Request',
        //     'body' => 'New Reload Request',
        // ], 'reload', $reloadId);

        return response()->json([
            'reference_id' => $reloadId,
            'message' => 'Successfully created new reload case'
        ], 200);
        
    }

    public function transfer(Request $request)
    {

        $this->validate($request, [
            'accountId' => ['required', 'integer'],
            'countryId' => ['required', 'integer'],
            'bankBranchId' => ['required', 'integer'],
            'bankAccountNo' => ['required'],
            'bankAccountName' => ['required'],
            'amount' => ['required', 'integer', 'not_in:0']
        ]);

        $user = auth()->user();

        $latestCaseStatus = AccountService::getLatestUserCaseStatus($user);

        if ($latestCaseStatus == 1 || $latestCaseStatus == 2) {
            return response()->json([
                'message' => 'Your last request is still pending or processing. Please wait for it to finish.'
            ], 400);
        }

        $accountId = $request->input('accountId');
        $countryId = $request->input('countryId');
        $bankBranchId = $request->input('bankBranchId');
        $bankAccountNo = $request->input('bankAccountNo');
        $amount = $request->input('amount');
        $purpose = $request->input('purpose');
        $recipientAccName = $request->input('bankAccountName');

        $userAccount = AccountService::findUserCurrencyAccountById($user, $accountId);

        if (is_null($userAccount)) {
            return response()->json([
                'message' => 'Currency account does not exists'
            ], 400);
        }

        /**
         * Get country and its respected bank branch
         */
        $country = AccountService::getCountriesWithBankBranchByCountryIdAndBankBranchId($countryId, $bankBranchId);

        if (is_null($country)) {
            return response()->json([
                'message' => 'Country or bank branch in the country does not exists'
            ], 400);
        }

        $latestTransact = AccountService::getCurrencyAccountLatestTransaction($userAccount);

        if (is_null($latestTransact)) {
            return response()->json([
                'message' => 'Something wrong. Currency account exist but no transaction.'
            ], 400);
        }

        /**
         * If balance after deduction is less than 0, not good
         */
        $amount = UtilityService::convertAmountToDecimal($amount, $userAccount->currency_id);

        $balance = AccountService::calculateNewTransactionBalanceByDeduct($latestTransact, $amount);

        if ($balance < 0) {
            return response()->json([
                'message' => 'Amount exceeds account balance.'
            ], 400);
        }

        /**
         * Create 'transfers' record
         */

        $transfer = AccountService::createTransferRecord($userAccount->currency_id, $amount, $purpose, $bankAccountNo, $countryId, $country->banks->first()->id, $recipientAccName);

        if (!isset($transfer->wasRecentlyCreated) || $transfer->wasRecentlyCreated === false) {
            return response()->json([
                'message' => 'Something wrong. Unable to create transfer record.'
            ], 500);
        }


        /**
         * Create 'cases' record with created 'transfers' record
         */
        $case = AccountService::createCaseRecord($user->id, 'transfers', $transfer->id, $userAccount->id);

        if (isset($case->wasRecentlyCreated) && $case->wasRecentlyCreated === true) {

            $userCase = CaseService::getCasesByRefType('transfer', null)->find($case->id);
            $userCase = UtilityService::appendAttribute(collect([$userCase]), [
                [
                    'name' => 'formatted_amount',
                    'path' => 'transact.formatted_amount',
                    'hide' => 'transact'
                ]
            ]);

            // SocketService::emitSocketData('/add-transfer', $userCase[0]->toArray());

            $notification = NotificationService::sendPushNotificationInBulk([
                'title' => 'New Transfer Request',
                'body' => 'New Transfer Request',
            ], 'transfer', $transfer->id);

            return response()->json([
                'message' => 'Processing transaction'
            ]);
        } else {
            return response()->json([
                'message' => 'Something wrong'
            ], 500);
        }
    }

    public function exchange(Request $request)
    {
        $this->validate($request, [
            'from_currency_id' => ['required', 'integer'],
            'to_currency_id' => ['required', 'integer'],
            'amount' => ['required', 'integer', 'not_in:0']
        ]);

        $user = auth()->user();

        $fromCurrencyId = $request->input('from_currency_id');
        $toCurrencyId = $request->input('to_currency_id');
        $fromAmount = $request->input('amount');

        $fromAmount = UtilityService::convertAmountToDecimal($fromAmount, $fromCurrencyId);

        $latestCaseStatus = AccountService::getLatestUserCaseStatus($user);

        if ($latestCaseStatus == 1 || $latestCaseStatus == 2) {
            return response()->json([
                'message' => 'Your last request is still pending or processing. Please wait for it to finish.'
            ], 400);
        }

        /* 
            Check source account existance
        */

        $sourceAccount = AccountService::findUserCurrencyAccountByCurrencyId($user, $fromCurrencyId);

        if (empty($sourceAccount)) {
            return response()->json(['message' => 'Currency account not exist'], 400);
        }

        /* 
            Check source account latest transaction
        */

        $sourceAccountLatestTransact = AccountService::getCurrencyAccountLatestTransaction($sourceAccount);

        if (is_null($sourceAccountLatestTransact)) {
            return response()->json([
                'message' => 'Something wrong. Source currency account exist but no transaction.'
            ], 400);
        }

        /* 
            Check source account balance
        */

        $balance = AccountService::calculateNewTransactionBalanceByDeduct($sourceAccountLatestTransact, $fromAmount);

        if ($balance < 0) {
            return response()->json([
                'message' => 'Amount exceeds account balance.'
            ], 400);
        }

        /* 
            Check existing exchange rate record
        */

        $exchangeRate = ExchangeRateService::checkExistingExchangeRateRecord([
            'from_currency_id' => $sourceAccount->currency_id,
            'to_currency_id' => $toCurrencyId,
            'date' => Carbon::now()->format('Y-m-d')
        ]);

        if ($exchangeRate->isEmpty()) {
            return response()->json([
                'message' => 'There is no exchange rate record found for today'
            ], 400);
        }

        /* 
            Check if target account is already existed, if not create new record
        */

        $targetAccount = AccountService::findUserCurrencyAccountByCurrencyId($user, $toCurrencyId);

        if (empty($targetAccount)) {
            $targetAccount = AccountService::createAccount([
                'user_id' => auth()->user()->id,
                'currency_id' => $toCurrencyId
            ]);
        }

        /* 
            Calculate exchange rate
        */

        $toAmount = AccountService::calculateExchangeRate(
            $fromAmount,
            $exchangeRate->first(),
            $fromCurrencyId,
            $toCurrencyId
        );

        /* 
            Create exchange record
        */

        $exchange = AccountService::createExchangeRecord([
            'from_currency_id' => $sourceAccount->currency_id,
            'to_currency_id' => $targetAccount->currency_id,
            'exchange_rate_id' => $exchangeRate->first()->id,
            'from_account_id' => $sourceAccount->id,
            'to_account_id' => $targetAccount->id,
            'from_amount' => $fromAmount,
            'to_amount' => $toAmount,
        ]);

        /* 
            Create case record
        */

        $case = AccountService::createCaseRecord($user->id, 'exchange', $exchange->id, $sourceAccount->id, $targetAccount->id, 3);

        /* 
            Create transaction record for source account
        */

        $sourceAccountNewBalance = $sourceAccountLatestTransact->new_balance - $fromAmount;

        CaseService::createTransactionRecord([
            'user_id' => $user->id,
            'account_id' => $sourceAccount->id,
            'case_id' => $case->id,
            'currency_id' => $exchange->from_currency_id,
            'old_balance' => $sourceAccountLatestTransact->new_balance,
            'new_balance' => $sourceAccountNewBalance,
            'rate' => $exchangeRate->first()->rate,
        ]);

        /* 
            Create transaction record for target account
        */

        $targetAccountLatestTransact = AccountService::getCurrencyAccountLatestTransaction($targetAccount);

        if (is_null($targetAccountLatestTransact)) {
            $currentBalance = 0;
        } else {
            $currentBalance = $targetAccountLatestTransact->new_balance;
        }

        $targetAccountNewBalance = $currentBalance + $toAmount;

        CaseService::createTransactionRecord([
            'user_id' => $user->id,
            'account_id' => $targetAccount->id,
            'case_id' => $case->id,
            'currency_id' => $exchange->to_currency_id,
            'old_balance' => $currentBalance,
            'new_balance' => $targetAccountNewBalance,
            'rate' => $exchangeRate->first()->rate,
        ]);

        return response()->json([
            'message' => 'Exchange successful'
        ]);
    }

    public function getExchangeConvertedAmount(Request $request)
    {
        $this->validate($request, [
            'from_currency_id' => ['required', 'integer'],
            'to_currency_id' => ['required', 'integer'],
            'amount' => ['required', 'numeric', 'not_in:0']
        ]);

        $fromCurrencyId = $request->input('from_currency_id');
        $toCurrencyId = $request->input('to_currency_id');
        $amount = $request->input('amount');

        $amount = UtilityService::convertAmountToDecimal($amount, $fromCurrencyId);

        $exchangeRate = ExchangeRateService::checkExistingExchangeRateRecord([
            'from_currency_id' => $fromCurrencyId,
            'to_currency_id' => $toCurrencyId,
            'date' => Carbon::now()->format('Y-m-d')
        ]);

        if ($exchangeRate->isEmpty()) {
            return response()->json([
                'message' => 'There is no exchange rate record found for today'
            ], 400);
        }

        $targetAmount = AccountService::calculateExchangeRate(
            $amount,
            $exchangeRate->first(),
            $fromCurrencyId,
            $toCurrencyId
        );

        return response()->json([
            'target_amount' => $targetAmount
        ]);
    }
}
