<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use App\Services\AccountService;
use App\Services\CurrencyKeypadService;
use App\Services\UtilityService;
use function _\map;

class AppCurrencyKeypadController extends BaseController
{
    public function getAllCurrencyKeypads($currencyId)
    {
        $currencyKeypad = CurrencyKeypadService::findCurrencyKeypadByCurrencyId(
            $currencyId
        );

        return response()->json($currencyKeypad);
    }

}
