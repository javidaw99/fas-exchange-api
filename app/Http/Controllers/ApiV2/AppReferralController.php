<?php

namespace App\Http\Controllers\ApiV2;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\ReferralService;
use Exception;

class AppReferralController extends BaseController
{
    public function addGroup(Request $request) {

        $this->validate($request, [
            'withdraw_rate' => ['required', 'integer'],
            'exchange_rate' => ['required', 'integer'],
            'group_name' => ['required']
        ]);

        $withdrawRate = $request->input('withdraw_rate');
        $exchangeRate = $request->input('exchange_rate');
        
        $user = auth()->user();

        DB::beginTransaction();

        try {

            $selfGroup = ReferralService::getSelfReferralGroup($user->id);

            if (is_null($selfGroup)) {
                throw new Exception('User does not belong to any referral group.', 422);
            }

            if ($withdrawRate > $selfGroup->withdrawal_commission_rate || $exchangeRate > $selfGroup->exchange_commission_rate) {
                throw new Exception('Rate exceed given rate', 400);
            }

            ReferralService::createReferralGroup($user->id, $request->input('group_name'), $withdrawRate, $exchangeRate);
            
            DB::commit();

            return response()->json([
                'message' => 'Successfully create referral group.'
            ], 200);

        } catch (Exception $e) {
            DB::rollBack();

            if ($e->getCode() == 0) {
                abort(500, $e->getMessage());
            } else {
                return response()->json([
                    'message' => $e->getMessage()
                ], $e->getCode());   
            }
        }
    }
    
    public function updateGroup(Request $request) {

        $this->validate($request, [
            'group_id' => ['required', 'integer'],
            'group_name' => ['required'],
            'exchange_rate' => ['required', 'integer'],
            'withdraw_rate' => ['required', 'integer'],
        ]);

        $groupId = $request->input('group_id');
        $groupName = $request->input('group_name');
        $exchangeRate = $request->input('exchange_rate');
        $withdrawRate = $request->input('withdraw_rate');
        
        $user = auth()->user();

        DB::beginTransaction();

        try {

            $selfGroup = ReferralService::getSelfReferralGroup($user->id);

            if ($exchangeRate > $selfGroup->exchange_commission_rate || $withdrawRate > $selfGroup->withdrawal_commission_rate) {
                throw new Exception('Rates cannot be more than given rates.', 422);
            }

            $userGroupToBeEdited = ReferralService::getUserReferralGroupById($user->id, $groupId);
    
            if (is_null($userGroupToBeEdited)) {
                throw new Exception('Group does not exist.', 422);
            }

            $groupUsers = ReferralService::getReferralTreeByGroup($groupId)
                        ->pluck('referrals')
                        ->collapse()
                        ->pluck('referralTree.descendant_user_id');

            if ($groupUsers->isEmpty()) {
                $newGroupEcr = $exchangeRate;
                $newGroupWcr = $withdrawRate;
            } else {

                $minRates = ReferralService::getMaxCommissionRatesFromReferralGroups($groupUsers);

                if (
                    $exchangeRate < $minRates->deductable_exchange_commission_rate ||
                    $withdrawRate < $minRates->deductable_withdrawal_commission_rate
                ) {
                    throw new Exception('Unable to set commission rates due the amount of distributed commission rates from children exceeded.', 422);
                }

                $newGroupEcr = $exchangeRate;
                $newGroupWcr = $withdrawRate;
            }

            $updated = ReferralService::updateReferralGroup($userGroupToBeEdited, [
                'name' => $groupName,
                'exchange_commission_rate' => number_format($newGroupEcr, 4),
                'withdrawal_commission_rate' => number_format($newGroupWcr, 4)
            ]);

            DB::commit();

            if ($updated) {
                return response()->json([
                    'message' => 'Successfully updated group setting.'
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Nothing updated.'
                ], 200); 
            }

        } catch (Exception $e) {

            DB::rollBack();

            if ($e->getCode() == 0) {
                abort(500, $e->getMessage());
            } else {
                return response()->json([
                    'message' => $e->getMessage()
                ], $e->getCode());   
            }
        }

    }
    
    public function groupList() {

        $user = auth()->user();

        return ReferralService::getReferralGroupsByOwner($user->id);
    }

    public function groupDetails($id) {

        $user = auth()->user();

        return ReferralService::getUserReferralGroupById($user->id, $id);
    }
    
    public function parent() {

        $user = auth()->user();

        return ReferralService::getReferralParent($user->id);
    }
    
    public function referrals() {

        $user = auth()->user();

        return ReferralService::getReferrals($user->id);
    }
    
    public function referralDetails($id) {

        $user = auth()->user();
        
        try {

            $refDetails = ReferralService::getReferralDetails($user->id, $id);

            if (is_null($refDetails)) {
                throw new Exception('User is not part of any referrals.', 400);
            }

            return $refDetails;
            
        } catch (Exception $e) {
            if ($e->getCode() == 0) {
                abort(500, $e->getMessage());
            } else {
                return response()->json([
                    'message' => $e->getMessage()
                ], $e->getCode());   
            }
        }
    }
 
}
