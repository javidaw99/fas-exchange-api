<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\MetricService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use function _\map;
use Carbon\Carbon;

class MetricController extends BaseController
{
    public function metricsInitialise(Request $request)
    {

        $totalTransaction = MetricService::findAllUserTransactionByDate();
        $pendingTransaction = MetricService::findAllUserTransactionByPending();
        $reloadPending = MetricService::findReloadUserTransactionByPending();
        $transferPending = MetricService::findTransferUserTransactionByPending();
        $totalPairTransferTransaction = MetricService::findPairTransferUserTransactionByCurrency();
        $dailyTransferTransaction = MetricService::findDailyTransferTransactionByCurrency();
     

        $array = [
            'total_transaction' => $totalTransaction,
            'pending_transaction' => $pendingTransaction,
            'reload_pending' => $reloadPending,
            'transfer_pending' => $transferPending,
            'pair_transfer' => $totalPairTransferTransaction,
            'daily_transfer'=> $dailyTransferTransaction,
    
        ];

        return response()->json($array);
    }

    public function findReloadTransactionByDate(Request $request){

        $this->validate($request, [
            'date_type' => ['required']
        ]);
        $dateType =  $request->query('date_type');
        if($dateType == "day"){
            $date = Carbon::today()->subDays(6)->startOfDay();
            $date2 = Carbon::today()->endOfDay();
        }elseif($dateType == "week"){
            $date = Carbon::today()->subWeeks(6)->startOfWeek();
            $date2 = Carbon::today()->endOfWeek();
        }elseif($dateType == "month"){
            $date = Carbon::today()->subMonths(6)->startOfMonth();
            $date2 = Carbon::today()->endOfMonth();
        }elseif($dateType == "year"){
            $date = Carbon::today()->subYears(6)->startOfYear();
            $date2 = Carbon::today()->endOfYear();
        };

        $startDate = $date->toDateTimeString();
        $endDate = $date2->toDateTimeString();
      
        $res = MetricService::findReloadTransactionByDate($startDate, $endDate,$dateType);
        $dates = MetricService::getSevenDateType($dateType);

        $currencies  = collect($res)
        ->mapToGroups(function($item, $key) {
            
            return [$item->from_currency_code => $item];
        })
        ->map(function($item, $key) use($dates){
          
            $newDates = $dates;
            // dd($item);
            foreach($newDates as &$date) {
                $dateType = $date->date_type;
                
                $foundItem = collect($item)->first(function($currencyDate) use ($dateType) {
                    return $currencyDate->date_type == $dateType;
                });


                if(!empty($foundItem)) {
                    $date = collect($date)->merge($foundItem)->all();
                }
            }


            return  $newDates;
        })->all();
       

        return $currencies;
    }

    public function findTransferTransactionByDate(Request $request){

        $this->validate($request, [
            'date_type' => ['required']
        ]);
        $dateType =  $request->query('date_type');
        if($dateType == "day"){
            $date = Carbon::today()->subDays(6)->startOfDay();
            $date2 = Carbon::today()->endOfDay();
        }elseif($dateType == "week"){
            $date = Carbon::today()->subWeeks(6)->startOfWeek();
            $date2 = Carbon::today()->endOfWeek();
        }elseif($dateType == "month"){
            $date = Carbon::today()->subMonths(6)->startOfMonth();
            $date2 = Carbon::today()->endOfMonth();
        }elseif($dateType == "year"){
            $date = Carbon::today()->subYears(6)->startOfYear();
            $date2 = Carbon::today()->endOfYear();
        };

        $startDate = $date->toDateTimeString();
        $endDate = $date2->toDateTimeString();
      
        $res = MetricService::findTransferTransactionByDate($startDate, $endDate,$dateType);
        $dates = MetricService::getSevenDateType($dateType);

        $currencies  = collect($res)
        ->mapToGroups(function($item, $key) {
            
            return [$item->from_currency_code => $item];
        })
        ->map(function($item, $key) use($dates){
          
            $newDates = $dates;
            // dd($item);
            foreach($newDates as &$date) {
                $dateType = $date->date_type;
                
                $foundItem = collect($item)->first(function($currencyDate) use ($dateType) {
                    return $currencyDate->date_type == $dateType;
                });


                if(!empty($foundItem)) {
                    $date = collect($date)->merge($foundItem)->all();
                }
            }


            return  $newDates;
        })->all();
       

        return $currencies;
    }

    public function getTransactionTrends(Request $request, $isFromApi = true)
    {
        $year = $request->query('year');
        $currency = $request->query('currency');

        $records = MetricService::getTransactionTrends($year, $currency);

        $months = MetricService::getListOfMonths();

        $intialValuesByMonth = map($months, function ($item) {
            return [
                'month' => $item,
                'count' => 0
            ];
        });

        $intialValuesByMonth = collect($intialValuesByMonth)->pluck('count', 'month');

        $reload = $intialValuesByMonth->merge($records['reload']);
        $reload = map($reload, function ($value) {
            return $value;
        });

        $transfer = $intialValuesByMonth->merge($records['transfer']);
        $transfer = map($transfer, function ($value) {
            return $value;
        });

        $exchange = $intialValuesByMonth->merge($records['exchange']);
        $exchange = map($exchange, function ($value) {
            return $value;
        });

        $response = [
            'label' => $months,
            'data' => [
                'reload' => $reload,
                'transfer' => $transfer,
                'exchange' => $exchange
            ],
        ];

        if ($isFromApi) {
            return response()->json($response);
        } else {
            return $response;
        }
    }
}
