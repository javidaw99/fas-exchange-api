<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\DeleteUserRequestService;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Services\UtilityService;
use Throwable;

class DeleteUserRequestController extends BaseController
{
    public function handleDeleteUserRequest(Request $request)
    {
        try {

            $this->validate($request, [
                'username' => 'required',
                'topkash_id' => 'required',
                'phone_no' => 'required',
                'email' => 'required',
                'password' => 'required',
                'reason' => 'required',
            ]);

            $username = $request->input('username');
            $topkashId = $request->input('topkash_id');
            $phoneNo = $request->input('phone_no');
            $email = $request->input('email');
            $password = $request->input('password');
            $reason = $request->input('reason');

            $user = UserService::getUserByUsername($username);

            if(!$user) {
                $isValid = 0;
                DeleteUserRequestService::createDeleteUserRequest($isValid, $username, $topkashId, $phoneNo, $email, $reason);
                return response()->json([
                    'message' => 'Request Successfully Created'
                ], 200);
            }

            if($user->role_id != 2) {
                $isValid = 0;
                DeleteUserRequestService::createDeleteUserRequest($isValid, $username, $topkashId, $phoneNo, $email, $reason);
                return response()->json([
                    'message' => 'Request Successfully Created'
                ], 200);
            }

            // check credentials
            if (Hash::check($request->input('password'), $user->password) 
                && $user->topkash_id == $topkashId 
                && $user->phone_no == $phoneNo
                && $user->email == $email) 
            {
                $isValid = 1;
                DeleteUserRequestService::createDeleteUserRequest($isValid, $username, $topkashId, $phoneNo, $email, $reason);

            } else {
                $isValid = 0;
                DeleteUserRequestService::createDeleteUserRequest($isValid, $username, $topkashId, $phoneNo, $email, $reason);

            }

            return response()->json([
                'message' => 'Request Successfully Created'
            ], 200);

        }
        catch (Throwable $th) {
            return response()->json([
                'message' => $th->getMessage()
            ], 404);
        }
    }

    public function findAllDeleteUserRequests(Request $request)
    {
        $queryParams = $request->all();

        $records = DeleteUserRequestService::findAllDeleteUserRequest();

        if (isset($queryParams['created_at'])) {
            $fromDate = $queryParams['created_at'][0];
            $toDate = $queryParams['created_at'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);
            if (isset($queryParams['localTimeOffset'])) {
                $finalFromDate = $fromCarbon->startOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->addMinutes($queryParams['localTimeOffset'])->format('Y-m-d H:i:s');
            }
            else 
            {
                $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
                $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');
            }

            $records->whereBetween('delete_user_requests.created_at', [$finalFromDate, $finalToDate]);
        }

        $records->orderBy('delete_user_requests.id', 'desc');

        $response = UtilityService::modelQueryBuilder($records, $request, [], null, false);

        return response()->json($response);
    }

}
