<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use App\Services\AccountService;
use App\Services\UtilityService;
use App\Services\CurrencyService;

use NumberFormatter;

class CurrencyController extends BaseController
{
    
    public function currencies(Request $request) {
        
        $currencies = CurrencyService::findAllCurrencies()->orderBy('iso_code');
    
        $records = UtilityService::modelQueryBuilder($currencies, $request);

        return response()->json($records);

        // return CurrencyService::findAll();
    }

    public function addCurrency (Request $request) {

        $this->validate($request, [
            'name' => ['required', 'string'],
            'code' => ['required', 'string'],
            'minimum_amount' => ['required', 'numeric'],
            // 'locale' => ['required','string']  
        ]);

        $currencyName = $request->input('name');
        $currencyCode =  $request->input('code');
        // $localeCode =  $request->input('locale');
        $flag ='public/flags/'. $currencyCode .'.png';
        $minimumAmount =  $request->input('minimum_amount');


        try {
            $currencyExisted = AccountService::findCurrencyByCode($currencyCode);

            if (!$currencyExisted) {


                $data = [
                    'currency_name' => $currencyName,
                    'currency_code' => $currencyCode, 
                    'flag' => $flag, 
                    'minimum_amount' => $minimumAmount
                ];

                $currency = AccountService::createNewCurrency($data);

                if (isset($currency->wasRecentlyCreated) && $currency->wasRecentlyCreated === true) {
                    return response()->json([
                        'message' => 'Successfully add new currency'
                    ]);
                } else {
                    return response()->json([
                        'message' => 'Something wrong'
                    ], 500);
                }
            } else {
             
                return response()->json([
                    'message' => 'Currency is already existed'
                ], 400);
            }
           
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to add this record'], 400);
        }  
   
    }

    public function getCurrencyById($id)
    {
        $currency = AccountService::findCurrencyById($id);

        return response()->json(
            $currency
        );
    }

    public function deleteCurrency ($id) {

        try {
          
            $currency = AccountService::findCurrencyById($id);
            $country = $currency->country()->first();
            if(is_null($currency)) {
                return response()->json([
                    'message' => 'Something wrong'
                ], 500);
            } else {
                $currency->delete();

                if(!is_null($country)) {
                    $currency->country()->detach($country->id);
                }
           
                return response()->json([
                    'message' => 'Successfully delete the currency'
                ]);
            }
          
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to delete this record'], 400);
        }  
   
    }

    public function updateCurrency ($id, Request $request) {

        $this->validate($request, [
            'name' => ['required', 'string'],
            // 'code' => ['required', 'string'],
            'minimum_amount' => ['required', 'numeric'],
        ]);

        try {
            $currencyExisted = AccountService::findCurrencyById($id);

            if ($currencyExisted) {

                $currencyExisted->currency_name = $request->input('name');
                // $currencyExisted->iso_code = $request->input('code');
                $currencyExisted->minimum_amount = $request->input('minimum_amount');

                $currencyExisted -> save();
    

                return response()->json(['message' => 'Successfully update the currency'], 200);

             
            } else {
             
                return response()->json([
                    'message' => 'The currency does not exist'
                ], 400);
            }
           
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to update this record'], 400);
        }  
   
    }

    // public function findAllCurrencies(Request $request) {

    //     $currencies = CurrencyService::findAllCurrencies()->orderBy('iso_code');
    
    //     $records = UtilityService::modelQueryBuilder($currencies, $request);

    //     return response()->json($records);
    // }
    
    public function findCurrencyByCountryId(Request $request)
    {
        $this->validate($request, [
            'country_id' => ['required', 'integer'],
        ]);

        $countryId = $request->input('country_id');

        return CurrencyService::findCurrencyByCountryId($countryId);
    }

    public function findCurrencyFilterOptionsByType($type)
    {
        $currency = CurrencyService::findCurrencyFilterOptionsByType($type);


        if($type == 'all') {

            $currency = CurrencyService::findAllCurrencyFilterOptions();

            $currency = collect($currency)->unique('name')->map(function ($value) {
                return [
                    'id' => $value->id,
                    'type' => 'all',
                    'name' => $value->name,
                    'iso_code' => $value->iso_code,
                    'locale' => $value->locale,
                    'flag' => $value->flag
                ];
            })->values();
        }

        return $currency;
    }
}
