<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Reload;
use App\Models\Status;
use Illuminate\Support\Facades\DB;

class ReloadService
{

    public static function findReloadRecordById($id)
    {
        $reload = DB::select('SELECT * 
                        FROM reloads_view 
                        WHERE id = ?', [$id]);

        return $reload[0];
    }

    public static function checkReloadReferenceNumberUniqueness($refenceNumber)
    {
        $count = Reload::where([
            'reference_no' => $refenceNumber
        ])->count();

        return $count < 1;
    }

    public static function findReloadByInputs($payload)
    {
        $reload = Reload::where([
            'bank_account_id' => $payload['bank_account_id'],
            'currency_id' => $payload['currency_id'],
            'user_account_id' => $payload['user_account_id'],
            'user_id' => $payload['user_id'],
            'reference_no' => $payload['reference_no'],
        ])->first();

        return $reload;
    }

    public static function createReload($payload)
    {
        $reload = Reload::create([
            'bank_account_id' => $payload['bank_account_id'],
            'currency_id' => $payload['currency_id'],
            'user_account_id' => $payload['user_account_id'],
            'user_id' => $payload['user_id'],
            'amount' => $payload['amount'],
            'reference_no' => $payload['reference_no'],
        ]);

        return $reload->id;
    }

    public static function findAllReloadsReport($queryParams)
    {
        $reloads = Reload::leftJoin('cases', 'reloads.id', 'cases.reference_id')
            ->leftJoin('transactions', 'transactions.case_id', 'cases.id')
            ->leftJoin('users', 'cases.assigned_user_id', 'users.id')
            ->with([
                'currency:id,currency_name,iso_code',
                'bankAccount' => function ($query) {
                    $query->with(['bankCountry' => function ($query) {
                        $query->with([
                            'bank:id,name',
                            'country:id,name,iso_code_2'
                        ]);
                    }]);
                },
                'user:id,topkash_id,username',
            ])
            ->select(
                'reloads.*', 
                'cases.status', 
                'cases.processed_at',
                'users.username as assigned_to_username',
                'transactions.doc_id', 
                'transactions.type_id'
            )
            ->where([
                ['transactions.is_requested', 1],
                ['transactions.reference_table', 'reloads'],
            ]);

        if(isset($queryParams['submission_date'])) {
            $fromDate = $queryParams['submission_date'][0];
            $toDate = $queryParams['submission_date'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);

            $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
            $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');

            $reloads->whereBetween('reloads.created_at', [$finalFromDate, $finalToDate]);
        }

        if(isset($queryParams['currency'])) {
            $reloads->whereHas('currency', function($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['currency']);
            });
        }

        if(isset($queryParams['recipient_bank_country'])) {
            $reloads->whereHas('bankAccount', function($q) use ($queryParams) {
                $q->whereHas('bankCountry', function($q) use ($queryParams) {
                    $q->whereHas('country', function($q) use ($queryParams) {
                        $q->where('name', $queryParams['recipient_bank_country']);
                    });
                });
            });
        }

        if(isset($queryParams['status'])) {
            $statusId = Status::TYPES_REVERT[strtolower($queryParams['status'])];

            $reloads->where('cases.status', $statusId);
        }

        if(isset($queryParams['user'])) {
            $reloads->where('users.username', $queryParams['user']);
        }

        $reloads = $reloads->orderBy('transactions.doc_id', 'desc')->get();

        $reloads = $reloads->map(function ($item) {
            $status = $item->status ? ucfirst(Status::TYPES[$item->status]) : 'N/A';

            return [
                'reload_id' => UtilityService::transactionIdWithPrefix($item->doc_id, $item->type_id),
                'submission_date' => Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A'),
                'customer_topkash_id' => $item->user->topkash_id ?? "-",
                'customer_username' => $item->user->username ?? "-",
                'currency_iso' => $item->currency->iso_code,
                'currency_name' => $item->currency->iso_code . ' - ' . $item->currency->currency_name,
                'amount' => $item->amount,
                'recipient_bank_country' => $item->bankAccount->bankCountry->country->name ?? "-",
                'recipient_bank' => $item->bankAccount->bankCountry->bank->name ?? "-",
                'recipient_bank_acc_no' => $item->bankAccount->acc_no ?? "-",
                'recipient_bank_acc_name' => $item->bankAccount->acc_name ?? "-",
                'recipient_reference_no' => $item->reference_no,
                'assigned_to_username' => $item->assigned_to_username ?? "-",
                'processed_date' => $item->processed_at ? Carbon::createFromFormat('Y-m-d H:i:s', $item->processed_at)->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A') : '-',
                'status' => $status,
            ];
        });

        return $reloads;
    }
}
