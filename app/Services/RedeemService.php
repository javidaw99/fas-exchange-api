<?php

namespace App\Services;

use App\Models\Redeem;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RedeemService
{
    public static function createVoucherRedeemRecord($data)
    {
        $redeem = Redeem::create([
            'user_id' => $data['user_id'],
            'voucher_code' => $data['voucher_code'],
            'currency_id' => $data['currency_id'],
            'amount' => isset($data['amount']) ? (double)$data['amount'] : null,
            'claimed_at' => isset($data['claimed_at']) ? $data['claimed_at'] : null,
            'status' => $data['status']
        ]);

        return  $redeem;
    }

    public static function findVoucherRedeemByUserIdAndVoucherCode($userId, $voucherCode, $status)
    {
        $redeem = Redeem::where([
            'voucher_code' => $voucherCode,
            'user_id' => $userId,
            'status' => $status
        ])->first();

        return $redeem;
    }

    public static function findRedeemRecordById($id)
    {
        $redeem = DB::table('redeems')->where('redeems.id', $id)
            ->leftJoin('currencies','currencies.id', 'redeems.currency_id')
            ->leftJoin('transactions', 'transactions.reference_id', 'redeems.id')
            ->select(
                'redeems.*',
                'currencies.id as currency_id', 
                'currencies.iso_code as currency_code', 
                'currencies.currency_name as currency_name',
                DB::raw('CONCAT("TG", LPAD(transactions.doc_id, 8,0)) AS doc_id'),
            )
            ->first();

        return $redeem;
    }

    public static function findAllRedeemsReport($queryParams)
    {
        $redeems = Redeem::leftJoin('transactions', 'transactions.reference_id', 'redeems.id')
            ->with([
                'currency:id,currency_name,iso_code',
                'user:id,topkash_id,username',
            ])
            ->select('redeems.*', 'transactions.doc_id', 'transactions.type_id')
            ->where('status', 'success');

        if(isset($queryParams['redemption_date'])) {
            $fromDate = $queryParams['redemption_date'][0];
            $toDate = $queryParams['redemption_date'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);

            $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
            $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');

            $redeems->whereBetween('redeems.created_at', [$finalFromDate, $finalToDate]);
        }

        if(isset($queryParams['currency'])) {
            $redeems->whereHas('currency', function($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['currency']);
            });
        }

        $redeems = $redeems->orderBy('transactions.doc_id', 'desc')->get();

        $redeems = $redeems->map(function ($item) {
            return [
                'redeem_id' => UtilityService::transactionIdWithPrefix($item->doc_id, $item->type_id),
                'redemption_date' => $item->created_at->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A'),
                'customer_topkash_id' => $item->user->topkash_id ?? "-",
                'customer_username' => $item->user->username ?? "-",
                'currency_iso' => $item->currency->iso_code,
                'currency_name' => $item->currency->iso_code . ' - ' . $item->currency->currency_name,
                'amount' => $item->amount,
                'redemption_code' => $item->voucher_code
            ];
        });

        return $redeems;
    }

    public static function findAllRedeems()
    {
        $redeems = DB::table('redeems_view');
       
        return  $redeems;
    }

    public static function findRedeemById($id)
    {
        $redeem = DB::select(
            'SELECT * 
            FROM redeems_view 
            WHERE reference_id = ?',
            [$id]
        );

        return $redeem[0];
    }
}
