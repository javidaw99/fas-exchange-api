<?php

namespace App\Services;

use App\Models\Account;
use App\Models\User;
use App\Models\FasTransferRequest;

class FasTransferRequestService
{

    public static function createFasTransferRequest($info)
    {
        $fasTransferRequest = FasTransferRequest::create([
            'user_id' => $info['user_id'],
            // 'user_bank_account_id' => $info['user_bank_account_id'],
            'user_account_id' => $info['user_account_id'],
            'from_currency_id' => $info['from_currency_id'],
            'to_currency_id' => $info['to_currency_id'],
            'total_amount' => $info['total_amount'],
            'total_convert_amount' => $info['total_convert_amount'],
            'final_amount' => $info['final_amount'],

            'status' => $info['status'],
            'daily_limit' => $info['daily_limit'],
            'exchange_margin_rate_id' => $info['exchange_margin_rate_id'],

            'exchange_rate_id' => $info['exchange_rate_id'],

        ]);

        return $fasTransferRequest->id;
    }


    
    public static function findAllFasTransferRequest($userId,$userAccountId)
    {

        $fasTransferRequest = FasTransferRequest::with(
            ['to_currency','from_currency','exchange_rate']
        )
            ->where('user_id', $userId)
            ->where('user_account_id',$userAccountId)
            ->orderBy('created_at', 'desc')
            ->get();

            // dd($fasTransferRequest);

        return $fasTransferRequest;
    }


    public static function findOneFasTransferRequest($id)
    {

        $fasTransferRequest = FasTransferRequest::with(
            ['fas_transfers'=>function ($query) {
                $query->with([
                'bank_account' => function($q) {
                    $q->with(["bankCountry" => function($que){
                        $que->with(["bank:id,name"]);
                    }]);
                }, 
                'media']);
            },
            'fas_transfer_convert'=>function ($query) {
                $query->with(['user_bank_account' => function($q) {
                    $q->with(["bankInfo:id,name"]);
                }]);
            },
            'to_currency','from_currency','exchange_rate','exchange_margin_rate',
            'user_bank_account'=>function ($query) {
                $query->with(['bankInfo']);
            }]
        )
            ->where('id', $id)
            ->first();


            // dd($fasTransferRequest);
        return $fasTransferRequest;
    }


}
