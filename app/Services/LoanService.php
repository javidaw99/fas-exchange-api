<?php

namespace App\Services;

use App\Models\LoanApplication;
use App\Models\LoanApplicant;
use App\Models\LoanPackage;

class LoanService
{
    public static function createLoanPackage($payload)
    {

        $loanPackage = LoanPackage::create([
            'name' => $payload['name'],
            'type' => $payload['type'],
            'level' => $payload['level'],
            'currency_id' => $payload['currency_id'],
            'loan_amount' => $payload['loan_amount'],
            'loan_day' => $payload['loan_day'],
            'received_credit' => $payload['received_credit'],
            'first_time_penalty' => $payload['first_time_penalty'],
            'daily_penalty_percentage' => $payload['daily_penalty_percentage'],
            'exp' => $payload['exp'],
            'status' => $payload['status'],
        ]);

        return $loanPackage->id;
    }

    public static function findOneLoanPackageById($id)
    {
        $loanPackage = LoanPackage::where('id', $id)->with(['currency' => function ($query) {
            $query->select('id', 'currency_name', 'iso_code', 'flag');
        }])
            ->select([
                'id',
                'name',
                'type',
                'level',
                'currency_id',
                'loan_amount',
                'received_credit',
                'first_time_penalty',
                'daily_penalty_percentage',
                'exp',
                'status',
                'created_at'
            ])->first();
        return $loanPackage;
    }

    public static function findAllLoanPackages()
    {

        $loanPackages = LoanPackage::with(['currency' => function ($query) {
            $query->select('id', 'currency_name', 'iso_code', 'flag');
        }])
            ->select([
                'id',
                'name',
                'type',
                'level',
                'currency_id',
                'loan_amount',
                'received_credit',
                'first_time_penalty',
                'daily_penalty_percentage',
                'exp',
                'status',
                'created_at'
            ]);

        return $loanPackages;
    }

    public static function updateLoanPackageById($data, $loanPackageId)
    {

        $loanPackage = LoanPackage::find($loanPackageId);

        $loanPackage->name = $data['name'];
        $loanPackage->type = $data['type'];
        $loanPackage->level = $data['level'];
        $loanPackage->currency_id = $data['currency_id'];
        $loanPackage->loan_amount = $data['loan_amount'];
        $loanPackage->loan_day = $data['loan_day'];
        $loanPackage->received_credit = $data['received_credit'];
        $loanPackage->first_time_penalty = $data['first_time_penalty'];
        $loanPackage->daily_penalty_percentage = $data['daily_penalty_percentage'];
        $loanPackage->exp = $data['exp'];
        $loanPackage->name = $data['name'];
        $loanPackage->status = $data['status'];

        if ($loanPackage->isClean()) {
            return false;
        } else {
            $loanPackage->save();
            return true;
        }
    }

    public static function deleteLoanPackageById($id)
    {
        $record = LoanPackage::find($id);
        if (empty($record)) {
            return false;
        }
        $record->delete();

        return true;
    }

    public static function createLoanApplication($payload)
    {

        $loanApplication = LoanApplication::create($payload);

        return $$loanApplication->id;
    }

    public static function createLoanApplicant($payload)
    {

        $loanApplicant = LoanApplicant::create($payload);

        return $loanApplicant->id;
    }
}
