<?php

namespace App\Services;

use App\Models\Transaction;
use App\Models\UserAccount;
use App\Services\ExchangeRateService;
use App\Services\ProcessingFeeBaseFixedRateService;
use App\Services\ProcessingFeeTieredPercentageRateService;
use Illuminate\Support\Facades\DB;
use App\Models\Status;

class TransactionService
{

    public static function findUserTransactionsById($userId)
    {

        $transactions = DB::select('SELECT *
                        FROM user_transactions_view 
                        WHERE user_id = ?
                        ORDER BY created_at DESC
                        ', [$userId]);

        foreach ($transactions as $transaction) {
            $transaction->status = Status::TYPES[$transaction->status];
        }

        return  $transactions;
    }

    public static function findUserTransactionsHistoryByCurrency($userId, $currencyId, $userAccountId, $limit, $page)
    {

        $offset = $limit * ($page - 1);

        $transactions = DB::select('SELECT *
                        FROM user_transactions_view 
                        WHERE user_id = ?
                        AND (from_currency_id = ? OR to_currency_id = ?)
                        AND user_account_id = ?
                        ORDER BY created_at DESC
                        LIMIT ?
                        OFFSET ?
                        ', [$userId, $currencyId, $currencyId, $userAccountId, $limit, $offset]);


        foreach ($transactions as $transaction) {

            if ($transaction->status) {
                $transaction->status = Status::TYPES[$transaction->status];
            } else {
                $transaction->status = Status::TYPES['3'];
            }
        }

        return  $transactions;
    }

    public static function findUserTransactionsHistory($userId, $limit, $page)
    {

        $offset = $limit * ($page - 1);
        $transactions = DB::select('SELECT *
                        FROM user_transactions_history_view 
                        WHERE user_id = ?
                        ORDER BY created_at DESC
                        LIMIT ?
                        OFFSET ?
                        ', [$userId, $limit, $offset]);


        foreach ($transactions as $transaction) {

            if ($transaction->status) {
                $transaction->status = Status::TYPES[$transaction->status];
            } else {
                $transaction->status = Status::TYPES['3'];
            }
        }

        return  $transactions? $transactions : [];
    }
    // public static function findUserTransactionsByUserId($userId, $currencies)
    // {

    //     $transactions = DB::table('user_transactions_history_view')
    //         ->where('user_id', $userId);

    //     if (count($currencies) > 0) {
    //         $transactions->where(function ($q) use ($currencies) {
    //             $q->whereIn('from_currency_id', $currencies)
    //                 ->orWhereIn('to_currency_id', $currencies);
    //         });
    //     }
    //     return  $transactions;
    // }
    public static function findAllUserTransactionsByUserId($userId)
    {
        $transactions = DB::table('user_transactions_history_view')
            ->where('user_id', $userId);

        return  $transactions;
    }
    public static  function findAllUserTransactions($id, $topkash_id, $username, $currency, $type, $created_at)
    {
        $transactions = DB::table('user_transactions_history_view')
            ->whereNotIn('status', [1,2]);


            if (isset($id)) {
                $transactions->where('reference_id', '=', $id);
            }

            if (isset($topkash_id)) {
                $transactions->where('user_topkash_id', '=', $topkash_id);
            }

            if (isset($username)) {
                $transactions->where('reference_username', 'like', '%' .$username. '%');
            }

            if (count($currency) > 0) {
                $transactions->whereIn('from_currency_code',$currency);
            }

            if (count($type) > 0) {
                $transactions->whereIn('transaction_type', $type);
            }

            if (count($created_at) > 0) {
                $transactions->whereBetween('created_at', $created_at);
            }

            // if (count($request['currency']) > 0) {
            // $transactions->where(function ($q) use ($currencies) {
            //     $q->whereIn('from_currency_id', $currencies)
            //         ->orWhereIn('to_currency_id', $currencies);
            // });
        
        // ->where('reference_username', 'LIKE', '%'.$username.'%');

        // if($referenceTable){
        //     $transactions->where('reference_username',$referenceTable);
        // }

        // if (count($status) > 0) {
        //     $transactions->whereIn('status', $status);
        // }

        // if (count($type) > 0) {
        //     $transactions->whereIn('transaction_type', $type);
        // }


        // if (count($currencies) > 0) {
        //     $transactions->where(function ($q) use ($currencies) {
        //         $q->whereIn('from_currency_id', $currencies)
        //             ->orWhereIn('to_currency_id', $currencies);
        //     });
        // }

        return  $transactions;
    }

    public static function findAllTransactions()
    {
        
        $transactions = DB::table('all_transaction_view');
       
        return  $transactions;
    }

    public static function findAllRedeemTransactions()
    {
        
        $transactions = DB::table('all_transaction_view')->where('transaction_type', '=','redeem');
       
        return  $transactions;
    }


    public static function findLatestTransactionByUserAndCurrencyId($userId, $currencyId)
    {

        $userAccount = UserAccount::where([
            'user_id' => $userId,
            'currency_id' => $currencyId
        ])->first();


        if (!isset($userAccount)) {
            return;
        }

        $transaction = Transaction::where([
            'user_id' => $userId,
            'user_account_id' => $userAccount->id
        ])->latest()->first();

        return $transaction;
    }

    public static function findUserTransactionsByReferenceId($id, $type)
    {
        $transactions = DB::select('SELECT *
                        FROM user_transactions_history_view 
                        WHERE reference_id = ?
                        AND transaction_type = ?
                        ORDER BY created_at DESC
                        ', [$id, $type]);

        return  $transactions;
    }

    public static function findLastestUserTransactionsByUserId($id)
    {
        $transactions = DB::table('all_transaction_view')
        ->where('user_id', '=', $id)
        ->orderBy('id', 'desc')
        ->take(10)
        ->get();

        return  $transactions;
    }
}
