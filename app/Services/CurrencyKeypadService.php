<?php

namespace App\Services;

use App\Models\CurrencyKeypad;

class CurrencyKeypadService
{

    public static function findAll()
    {
        return CurrencyKeypad::all();
    }

    public static function findCurrencyKeypadByCurrencyId($currencyId)
    {
        return CurrencyKeypad::where('currency_id', $currencyId)
        ->orderBy('amount', 'asc')
        ->get();
    }

    public static function createCurrencyKeypad($currencyKeypadInfo)
    {
        $newCurrencyKeypad = CurrencyKeypad::create([
            'currency_id' => $currencyKeypadInfo['currency_id'],
            'amount' => $currencyKeypadInfo['amount'],
            'amount_label' => $currencyKeypadInfo['amount_label']
        ]);

        return $newCurrencyKeypad;
    }

}
