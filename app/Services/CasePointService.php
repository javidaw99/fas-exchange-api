<?php

namespace App\Services;

use App\Models\Cases;
use App\Models\CasePoint;

use App\Models\Reload;
use App\Models\Transfer;
use App\Models\Transaction;
use App\Models\CaseSecureSafe;
use App\Services\UtilityService;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use function _\map;

class CasePointService
{


    public static function findCaseByUserId($reference_table, $userId, $attributes = ['*'])
    {
        $case = CasePoint::with(
            ['casePoint'=>function($query){
                $query->with(['bankAccount','bank','media']);
             },
           
            ]
        )
        ->where([
            ['reference_table', $reference_table],
            ['user_id', $userId],
            ['status', '<>', 3]
        ])
        ->latest()
            ->select($attributes)
            ->first();

        return $case;
    }

    public static function updateCaseStatueById($id)
    {

        $case = CasePointService::findCaseByUserId('reload_points', $id);        ;

        $case->status = 2;
    
        $case->save();
        
        return response()->json(['message' => 'Successfully update bank account number'], 200);

        
    }





}
