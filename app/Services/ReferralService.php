<?php

namespace App\Services;

use App\Models\ReferralGroup;
use App\Models\ReferralTree;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ReferralService {

    public static function getSelfReferralGroup($userId)
    {
        return ReferralGroup::whereHas('referrals.referralTree',
            function ($query) use ($userId) {
                $query->where('referral_trees.descendant_user_id', $userId);
            })
            ->first();
        
    }

    public static function createReferralGroup($userId, $groupName, $withdrawRate, $exchangeRate)
    {
        ReferralGroup::create([
            'ancestor_user_id' => $userId,
            'name' => $groupName,
            'code' => Str::uuid(),
            'withdrawal_commission_rate' => $withdrawRate,
            'exchange_commission_rate' => $exchangeRate,
        ]);
    }

    public static function getUserReferralGroupById($userId, $groupId) {
        return ReferralGroup::where('ancestor_user_id', '=', $userId)->find($groupId);
    }

    public static function getReferralTreeByGroup($groupId) {
        return ReferralGroup::with('referrals.referralTree:id,descendant_user_id')
        ->where('referral_groups.id', $groupId)
        ->get(['id']);
    }

    public static function getMaxCommissionRatesFromReferralGroups($groupOwnerIds) {
        return ReferralGroup::whereIn('ancestor_user_id', $groupOwnerIds)
        ->first([
            DB::raw('MAX(exchange_commission_rate) as deductable_exchange_commission_rate'),
            DB::raw('MAX(withdrawal_commission_rate) as deductable_withdrawal_commission_rate')
        ]);
    }
    public static function updateReferralGroup($referralGroupModel, $data) {

        $referralGroupModel->name = $data['name'];
        $referralGroupModel->exchange_commission_rate = $data['exchange_commission_rate'];
        $referralGroupModel->withdrawal_commission_rate = $data['withdrawal_commission_rate'];

        if ($referralGroupModel->isClean()) {
            return false;
        } else {
            $referralGroupModel->save();
            return true;
        }
    }
    public static function getReferralGroupsByOwner($userId) {

        return ReferralGroup::where('ancestor_user_id', '=', $userId)->get();
    }

    public static function getReferralParent($userId) {

        $parent = ReferralTree::with([
            'parentUser:id,fullname',
            'referral.referralGroup:id,name,exchange_commission_rate,withdrawal_commission_rate'
        ])
        ->where([
            ['descendant_user_id', '=', $userId],
            ['depth', '=', 1],
        ])
        ->first(['id', 'ancestor_user_id']);

        return $parent;
    }

    public static function getReferrals($userId) {
        return ReferralTree::with('childUser:id,fullname,email,phone_no')
            ->where([
                ['ancestor_user_id', '=', $userId],
                ['depth', '=', 1]
            ])
            ->get([
                'id',
                'ancestor_user_id',
                'descendant_user_id',
                'depth'
            ]);
    }

    public static function getReferralDetails($userId, $childUserId) {

        $referralUser = ReferralTree::whereHas('referral.referralGroup', function ($query) use ($userId, $childUserId) {
                            $query->where('ancestor_user_id', '=', $userId);
                        })
                        ->with('childUser:id,fullname,email,phone_no')
                        ->where('descendant_user_id', '=', $childUserId)
                        ->first();
        
        if (!empty($referralUser)) {

            $children = ReferralTree::where('ancestor_user_id', '=', $childUserId)->get();

            $childrenUserIds = $children->filter(function ($value, $key) use ($childUserId) {
                                return $value['descendant_user_id'] != $childUserId;
                            })
                            ->values()
                            ->pluck('descendant_user_id');

            $childUsers = User::whereIn('id', $childrenUserIds)
                        ->get([
                            'id',
                            'fullname'
                        ]);
            
            return [
                'user' => $referralUser->childUser,
                'children' => $childUsers
            ];

        } else {
            return null;
        }
    }
}