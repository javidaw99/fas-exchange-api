<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Reload;
use App\Models\ReloadPoint;

use App\Models\Status;
use Illuminate\Support\Facades\DB;


class ReloadPointService
{

    public static function createReloadPoint($payload)
    {
        $reloadPoint = ReloadPoint::create([
            'type' => $payload['type'],
            'bank_account_id' => $payload['bank_account_id'],
            'bank_id' => $payload['bank_id'],
            'user_id' => $payload['user_id'],
            'amount' => $payload['amount'],
            'status' => $payload['status']
        ]);

        return $reloadPoint->id;
    }




}