<?php

namespace App\Services;

use App\Constants\ExcelConstant;
use \libphonenumber\PhoneNumberUtil;
use \libphonenumber\PhoneNumberFormat;
use App\Models\Country;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\SmsLog;
use App\Models\LocaleMapper;
use App\Models\Transaction;
use Brick\Money\Money;
use Pkly\I18Next\I18n;
use Pkly\I18Next\Plugin\JsonLoader;
use Illuminate\Support\Facades\DB;

class UtilityService
{

	/**
	 * Create an instance for PhoneNumberUtil. Get iso_code based on passed country_id. 
	 * Parse phone number using the country iso_code. Remove prefix + sign from parsed number.
	 */
	public static function sanitisePhoneNumber($country_id, $phone_no)
	{
		$phoneUtil = PhoneNumberUtil::getInstance();
		$country = Country::where('id', $country_id)->first();

		$phoneNoProto = $phoneUtil->parse($phone_no, $country->iso_code_2);

		$finalPhoneNo = $phoneUtil->format($phoneNoProto, PhoneNumberFormat::E164);
		$finalPhoneNo = preg_replace('/^\+/i', '', $finalPhoneNo);

		return $finalPhoneNo;
	}

	public static function validatePhoneNumber($phone_no, $region)
	{
		$phoneUtil = PhoneNumberUtil::getInstance();

		$isValid = $phoneUtil->isPossibleNumber($phone_no, $region);

		return $isValid;
	}

	public static function validateEmailAddress($email)
	{
		$validator = Validator::make(array(
			'email' => $email
		), [
			'email' => 'required|email:rfc,dns'
		]);

		return !$validator->fails();
	}

	/*
	* Reusable function to handle pagination, filtering and sorting
	* 
	* $model           : Eloquent model
	* $request         : Request params (page, page_size, sort, search, from_date)

		| Params        | Description                                                   | Example
		|--------------------------------------------------------------------------------------------------------
		| page          | Specify the current page number                               | page=1   
		| page_size     | Specify the number of record to return in every page          | page_size=10 //will return 10 records
		| sort          | Specify what field to sort and in what order                  | sort=username_desc or sort=transact.recipientBankCountry.iso_code_asc (for nested sorting)
		| search        | Specify the keyword to search                                 | search=singapore
		| from_date     | Specify the date that we want to filter. Format: YYYY-MM-DD   | from_date=2022-01-01
						
	* $searchingFields : Array of fields that we want to search. Required if searching is applied. Example: ['fullname', 'email']
	* $dateField       : Date field to filter. Required if date filtering is applied. Example: 'effective_date'

	* Paginate will only be applied when 'page' and 'page_size' is being passed in query params

		Example (with paginate): http://localhost:8000/api/banks?page=1&page_size=10&search=singapore

		{
		"current_page": 1,
		"data": [
			{
				"id": 2,
				"name": "Bank Of Singapore",
				"created_at": "2022-09-07T11:42:12.000000Z"
			}
		],
		"first_page_url": "http://localhost:8000/api/banks?page=1",
		"from": 1,
		"last_page": 1,
		"last_page_url": "http://localhost:8000/api/banks?page=1",
		"links": [
			{
				"url": null,
				"label": "pagination.previous",
				"active": false
			},
			{
				"url": "http://localhost:8000/api/banks?page=1",
				"label": "1",
				"active": true
			},
			{
				"url": null,
				"label": "pagination.next",
				"active": false
			}
		],
		"next_page_url": null,
		"path": "http://localhost:8000/api/banks",
		"per_page": "10",
		"prev_page_url": null,
		"to": 1,
		"total": 1
	}

		Example (without paginate): http://localhost:8000/api/banks?search=singapore

		[
		{
			"id": 2,
			"name": "Bank Of Singapore",
			"created_at": "2022-09-07T11:42:12.000000Z"
		},
		]
	*/

	public static function modelQueryBuilder($model, $request, $searchingFields =  [], $dateField = null, $isOrderByParentCreatedAt = true)
	{
		/* 
		Apply searching 
		*/


		if ($request->has('search') && count($searchingFields) > 0) {
			$keyword = $request->query('search');
			$model->where(function ($query) use ($searchingFields, $keyword) {
				foreach ($searchingFields as $field) {
					$query->orWhere($field, 'LIKE', '%' . $keyword . '%');
				}
			});
		}

		/* 
		Apply date filter 
		*/

		if ($request->has('from_date') && $request->has('to_date') && $dateField) {
			$model->whereBetween($dateField, [$request->query('from_date'), $request->query('to_date')]);
		}

		$sort['field'] = null;
		$sort['order'] = null;

		if ($isOrderByParentCreatedAt) {
			$model->orderBy('created_at', 'desc');
		}

		/* 
		Apply sorting 
		*/

		if ($request->has('sort')) {
			$req = $request->query('sort');

			$sort = self::extractSortingFieldAndValue($req);
		}

		if ($sort['field'] && $sort['order']) {
			$model->reorder()->orderBy($sort['field'], $sort['order']);
		}

		/* 
		Apply pagination 
		*/

		if ($request->has('page') && $request->has('page_size')) {
			$page_size = $request->query('page_size');
			$model = $model->paginate($page_size);
		} else {
			$model = $model->get();
		}

		// /* 
		// Apply sorting 
		// */

		// if ($request->has('sort')) {
		// 	$req = $request->query('sort');

		// 	$sort = self::extractSortingFieldAndValue($req);
		// }

		// if (method_exists($model, 'hasPages'))
		// //To detect whether the model is a paginate object or normal collection
		// {
		// 	//Perform sorting without losing the paginate info

		// 	if ($sort['field'] && $sort['order']) {
		// 		$sortedRecords = $model->getCollection()->sortBy([[$sort['field'], $sort['order']]])->values();
		// 		$model = $model->setCollection($sortedRecords);
		// 	}
		// } else {

		// 	if ($sort['field'] && $sort['order']) {
		// 		$model = collect($model)->sortBy([[$sort['field'], $sort['order']]])->values();
		// 	}
		// }

		// dd($model);

		return $model;
	}

	public static function extractSortingFieldAndValue($sort)
	{
		$field = preg_replace("/_(desc|asc)/", "", $sort);

		preg_match("/desc|asc/i", $sort, $matches);

		$order = $matches[0];

		return [
			'field' => $field,
			'order' => $order
		];
	}


	public static function generateCookieDomain($request)
	{
		$origin = preg_replace('/(^https?:\/\/)|(:\d{4,}$)/', '', $request->headers->get('origin'));

		// From eva.holiaodev.com
		// To ['eva', 'holiaodev', 'com']
		$domainNameArray = explode('.', $origin);

		// Take last 2 items
		// ['holiaodev', 'com']
		$domainNameArray = array_slice($domainNameArray, -2, 2);

		// holiaodev.com
		$domain = join('.', $domainNameArray);

		// If detect not localhost add . before domain name
		preg_match('/^localhost$/', $domain, $matchedArray);
		if (!count($matchedArray)) {
			$domain = '.' . $domain;
		}

		return $domain;
	}

	public static function appendAttribute($collection, $attributes = [])
	{
		/*
				To append custom attribute into collection.
				Only applicable for Models that use or define $append property

				Params: 
				1. $collection = Record must be a collection or paginate type of object
				2. $attributes = An array of attributes that we want to append
				Example: 
				[
					[
						'name' => 'formatted_amount', //Attribute name
						'path' => 'transact.formatted_amount', //The path to access the attribute. Use dot if it is nested
						'hide' => 'transact' //Attribute that we want to hide. (Optional)
					]
				]
			*/

		if (method_exists($collection, 'hasPages'))
		//To detect whether the model is a paginate object or normal collection
		{
			$appendedCollection = $collection->getCollection()->each(function ($item) use ($attributes) {
				//Remap attribute path into "$item->foo->bar" form
				foreach ($attributes as $attribute) {
					$path = array_reduce(explode('.', $attribute['path']), function ($o, $p) {
						return $o->$p;
					}, $item);

					$item->setAttribute($attribute['name'], $path)
						->makeHidden($attribute['hide']);
				}
			});

			return $collection->setCollection($appendedCollection);
		} else {
			return $collection->each(function ($item) use ($attributes) {
				foreach ($attributes as $attribute) {
					$path = array_reduce(explode('.', $attribute['path']), function ($o, $p) {
						return $o->$p;
					}, $item);

					$item->setAttribute($attribute['name'], $path)
						->makeHidden($attribute['hide']);
				}
			});
		}
	}

	public static function generateTemporaryFileUrl($filePath, $duration = 5) //Set to 5 minutes by default
	/*
			Generate temporary url for accessing file from s3 storage.

			filePath = Path to files. eg: media/12/reload/test.jpg
			duration = Time duration to access the file. (in minutes)
		*/
	{


		$fileUrl = Storage::temporaryUrl(
			$filePath,
			Carbon::now()->addMinutes($duration)
		);

		// $fileUrl = Storage::url(
		// 	$filePath
		// );

		// $url = Storage::url('file.jpg');

		return $fileUrl;
	}

	public static function convertAmountToDecimal($amount, $currencyId)
	{
		$currency = AccountService::findCurrencyById($currencyId);

		$convertedAmount = self::formatNumberToCurrency($amount, $currency->iso_code);

		//Remove currency symbol
		$output = preg_replace('/[^0-9"."]/', '', $convertedAmount);

		return $output;
	}

	public static function formatNumberToCurrency($amount, $isoCode, $locale = null)
	{
		preg_match("/\./", $amount, $matches);

		if (count($matches) > 0) {
			$money = Money::of($amount, $isoCode);
		} else {
			$money = Money::ofMinor($amount, $isoCode);
		}

		if ($locale) {
			return $money->formatTo($locale);
		} else {
			$formatter = new \NumberFormatter('en_US', \NumberFormatter::CURRENCY);
			$formatter->setSymbol(\NumberFormatter::CURRENCY_SYMBOL, $isoCode);
			$formatter->setSymbol(\NumberFormatter::MONETARY_GROUPING_SEPARATOR_SYMBOL, ',');
			$formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, 2);

			return $money->formatWith($formatter);
		}
	}

	public static function checkIsPhoneNumberExisted($phone_no)
	{

		$phoneNumberExist = User::where('phone_no', $phone_no)
			->whereNull('deleted_at')
			->count();

		return $phoneNumberExist ? TRUE : FALSE;
	}

	public static function generateOtpCode()
	{
		$otpCode = rand(100000, 999999);
		return $otpCode;
	}

	public static function createOtpRecord($otp_info)
	{
		$phoneNo = $otp_info['phone_no'];
		$otpCode = $otp_info['otp_code'];
		$purpose = $otp_info['purpose'];

		$expiredAt = Carbon::now()->addMinute()->getTimestamp();

		$smsLogs = SmsLog::insert([
			'phone_no' => $phoneNo,
			'otp_code' => $otpCode,
			'purpose' => $purpose,
			'expired_at' => $expiredAt,
		]);

		if ($smsLogs) {
			return $expiredAt;
		} else {
			return false;
		}
	}

	public static function findSmsLogsByPhoneNo($phoneNumber, $purpose)
	{
		$otpCode = SmsLog::where('phone_no', $phoneNumber)
			->where('purpose', $purpose)
			->latest('id')
			->first();

		return $otpCode;
	}


	public static function verifyOtpCode($phoneNumber, $otpCode)
	{

		$isOtpCodeExisted = SmsLog::where('phone_no', $phoneNumber)
			->where('otp_code', $otpCode)
			->count();

		if ($isOtpCodeExisted) {

			$currentTimeStamp = Carbon::now()->getTimestamp();

			SmsLog::where('phone_no', $phoneNumber)
				->where('otp_code', $otpCode)
				->update(['expired_at' => $currentTimeStamp]);

			return true;
		}

		return false;
	}

	public static function loadLocale($locale)
	{
		I18n::get([
			'lng' =>  $locale,
			'fallbackLng' => 'en'
		])
			->useModule(new JsonLoader([
				'json_resource_path'    => '../config/translations/' . $locale . '.json'
			]))
			->init();

		return I18n::get();
	}

	public static function createLocaleMapperRecord($data)
	{
		$localeMapper = LocaleMapper::create([
			'title_key' => $data['title_key'],
			'content_key' => $data['content_key'],
			'dynamic_values' => isset($data['dynamic_values']) ? $data['dynamic_values'] : null,
			'reference_table' => $data['reference_table'],
			'reference_id' => $data['reference_id'],
		]);

		$tableName = $data['reference_table'] . 's';
		$refId = $data['reference_id'];

		DB::table($tableName)
			->where('id', $refId)
			->update(['locale_mapper_id' => $localeMapper->id]);
	}

	public static function transactionIdWithPrefix($id, $type)
	{
		return Transaction::PREFIX[$type] . str_pad($id, 8, 0, STR_PAD_LEFT);
	}

	/**
	 * Report Section (Start)
	 */

	public static function setTableTitle($worksheet, $rowNumber, $titleArray)
	{
		foreach ($titleArray as $key => $value) {
			$cell = ExcelConstant::COLUMN_CHARACTERS[$key] . $rowNumber;
			$worksheet->setCellValue($cell, $value);
			$worksheet->getStyle($cell)->applyFromArray(ExcelConstant::TABLE_HEADER_STYLE);
		}
	}

	public static function setTableData($worksheet, $rowNumber, $data)
	{
		foreach ($data as $item) {
			UtilityService::setTableRow($worksheet, $rowNumber, $item);
			$rowNumber++;
		}
	}

	public static function setTableRow($worksheet, $rowNumber, $row)
	{
		$counter = 0;
		foreach ($row as $value) {

			$cell = ExcelConstant::COLUMN_CHARACTERS[$counter] . $rowNumber;
			$worksheet->setCellValue($cell, $value);
			$counter++;
		}
	}

	public static function setNumberingFormat($worksheet, $column, $from, $to, $format)
	{
		$worksheet->getStyle($column . $from . ':' . $column . $to)
			->getNumberFormat()
			->setFormatCode($format);
	}

	public static function setHorizontalAlignment($worksheet, $column, $from, $to, $direction)
	{
		$worksheet->getStyle($column . $from . ':' . $column . $to)
			->getAlignment()
			->setHorizontal(ExcelConstant::HORIZONTAL_ALIGNMENT[$direction]);
	}

	public static function setWidthToAllColumns($worksheet, $numOfColumns)
	{
		for($i = 0; $i < $numOfColumns; $i++) {
            $column = ExcelConstant::COLUMN_CHARACTERS[$i];
            $worksheet->getColumnDimension($column)->setAutoSize(true);
        }
	}

	/**
	 * Report Section (End)
	 */
}
