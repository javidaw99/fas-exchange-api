<?php

namespace App\Services;

use App\Models\Onboarding;
use App\Models\OnboardingDetails;
use Illuminate\Support\Facades\DB;
use GrahamCampbell\Markdown\Facades\Markdown;
use App\Models\Media;

class OnboardingService
{

    public static function getAllOnboardingData()
    {
    

        $record = DB::table('onboardings')
        ->leftJoin('onboarding_details','onboardings.id','onboarding_details.onboarding_id')
        ->leftJoin('medias', 'onboarding_details.id', 'medias.reference_id')
        ->where('medias.reference_table', 'onboarding_details')
        ->where('is_published',1)
        ->get();

        foreach($record as $item){
            $imageUrl = UtilityService::generateTemporaryFileUrl($item->path); 
            $item->path = $imageUrl;
        };

        return $record; 
    }
}