<?php

namespace App\Services;

use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as FirebaseNotification;
use App\Models\Notification;
use App\Models\Role;
use App\Models\User;
use App\Services\UserService;


use Exception;
use Throwable;

class NotificationService
{
    public static function sendPushNotification(
        $payload,
        $type = null,
        $refId = null
    ) {
       
            $fcm_token = $payload['fcm_token'];
            $title = $payload['title'];
            $content = $payload['body'];
            $user_id = $payload['target_user_id'];
            $user = UserService::findUserById($user_id);

            $messaging = app('firebase.messaging');

            $notification = Notification::create([
                'title' => $title,
                'content' => $content,
                'user_id' => $user_id,
                'ref_id' => $refId,
                'ref_type' => $type,
            ]);

            try {

                if ($user->fcm_token) {
                    if (
                        !$user->fcm_token_status ||
                        $user->fcm_token_status !== 'valid'
                    ) {
                        $fcmTokenStatusResponse = $messaging->validateRegistrationTokens(
                            $fcm_token
                        );

                        $fcmTokenStatus = collect($fcmTokenStatusResponse)
                            ->filter(function ($value) {
                                return !empty($value);
                            })
                            ->keys()
                            ->implode(',');

                        UserService::updateFcmTokenStatus(
                            $user->id,
                            $fcmTokenStatus
                        );

                 
                    }    

                    $message = CloudMessage::withTarget('token', $fcm_token)
                        ->withNotification(
                            FirebaseNotification::create($title, $content)
                        )
                        ->withData([
                            'action' => 'view-notification',
                            'type' => $type,
                            'nid' => $notification->id,
                            'id' => $refId,
                        ]);

                    $messaging->send($message);

                }

            return $notification;
        } catch (\Throwable $th) {
            UserService::updateFcmTokenStatus($user->id, 'invalid');

            return $notification;
        }
    }

    public static function sendPushNotificationInBulk(
        $payload,
        $type = null,
        $refId = null,
        $isSkipCreateRecord = false
    ) {
        try {
            $title = $payload['title'];
            $content = $payload['body'];

            $messaging = app('firebase.messaging');

            $allDashboardUser = User::whereIn('role_id', [
                Role::TYPES['customer_service'],
                Role::TYPES['superadmin'],
            ])->get();

            foreach ($allDashboardUser as $dashboardUser) {

                $notificationId = null;

                if(!$isSkipCreateRecord) {
                    $notification = Notification::create([
                        'title' => $title,
                        'content' => $content,
                        'user_id' => $dashboardUser->id,
                        'ref_id' => $refId,
                        'ref_type' => $type,
                    ]);

                    $notificationId = $notification->id;
                }

                if ($dashboardUser->fcm_token) {
                    if (
                        !$dashboardUser->fcm_token_status ||
                        $dashboardUser->fcm_token_status !== 'valid'
                    ) {
                        $fcmTokenStatusResponse = $messaging->validateRegistrationTokens(
                            $dashboardUser->fcm_token
                        );

                        $fcmTokenStatus = collect($fcmTokenStatusResponse)
                            ->filter(function ($value) {
                                return !empty($value);
                            })
                            ->keys()
                            ->implode(',');

                        UserService::updateFcmTokenStatus(
                            $dashboardUser->id,
                            $fcmTokenStatus
                        );

                        if ($fcmTokenStatus && $fcmTokenStatus === ' valid') {

                            NotificationService::handleSendPushNotificationToBoUser($dashboardUser->id, $dashboardUser->fcm_token,  $title, $content, $type, $notificationId, $refId );
                        }
                    } else {
           
                        NotificationService::handleSendPushNotificationToBoUser($dashboardUser->id, $dashboardUser->fcm_token,  $title, $content, $type, $notificationId, $refId );

                    }
                }
            }

            // May remove after make sure notification flow is stable
            // $allDashboardUser = User::whereIn('role_id', [Role::TYPES['customer_service'], Role::TYPES['superadmin']])
            //     ->whereNotNull('fcm_token')
            //     ->get();

            // // $notificationData = $allDashboardUser->map(function ($item) use ($title, $content) {
            // //     return [
            // //         'user_id' => $item->id,
            // //         'title' => $title,
            // //         'content' => $content,
            // //     ];
            // // })->all();

            // // $notification = Notification::insert($notificationData);

            // $message = CloudMessage::new();
            // $message = $message->withNotification(FirebaseNotification::create($title, $content))
            //     ->withData(['action' => 'view-notification', 'type' => $type, 'nid' => $notification->id, 'id' => $refId]);

            // $deviceTokens = $allDashboardUser->filter(function ($item, $key) use ($messaging) {
            //     if($item->fcm_token) {
            //         $fcmTokenStatus = null;
            //         if (!$item->fcm_token_status || $item->fcm_token_status !== 'valid') {
            //             $fcmTokenStatusResponse = $messaging->validateRegistrationTokens($item->fcm_token);

            //             $fcmTokenStatus = collect($fcmTokenStatusResponse)->filter(function ($value) {
            //                 return !empty($value);
            //             })->keys()->implode(',');

            //             UserService::updateFcmTokenStatus($item->id, $fcmTokenStatus);
            //         }

            //         if (($item->fcm_token_status && $item->fcm_token_status === 'valid') || ($fcmTokenStatus && $fcmTokenStatus === 'valid')) {

            //             return $item;
            //         }
            //     }

            // })
            // ->map(function ($item, $key) {
            //     return $item->fcm_token;
            // })
            // ->all();

            // if(count($deviceTokens) > 0) {
            //     $sendReport = $messaging->sendMulticast($message, $deviceTokens);
            // }

            // $unknownTargets = $sendReport->unknownTokens();
            // $invalidTargets = $sendReport->invalidTokens();

            // foreach ($unknownTargets as $unknownTarget) {
            //     UserService::updateFcmTokenStatusByFcmToken($unknownTarget, 'unknown');
            // }

            // foreach ($invalidTargets as $invalidTarget) {
            //     UserService::updateFcmTokenStatusByFcmToken($invalidTarget, 'invalid');
            // }

            // return $sendReport;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public static function handleSendPushNotificationToBoUser ($userId, $fcmToken,  $title, $content, $type, $notificationId, $refId) {
        
        try {

            $messaging = app('firebase.messaging');

            $message = CloudMessage::withTarget(
                'token',
                $fcmToken
            )
            ->withNotification(
                FirebaseNotification::create(
                    $title,
                    $content
                )
            )
            ->withData([
                'action' => 'view-notification',
                'type' => $type,
                'nid' => $notificationId,
                'id' => $refId,
            ]);

            $messaging->send($message);

        } catch (\Throwable $th) {

            UserService::updateFcmTokenStatus($userId, 'invalid');

        }

    }
}
