<?php

namespace App\Services;

use App\Models\UserExchangeQuote;
use Carbon\Carbon;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\Symmetric;
use ParagonIE\HiddenString\HiddenString;
use App\Services\ExchangeRateService;
use App\Services\ProcessingFeeBaseFixedRateService;
use App\Services\ExchangeMarginRateService;
use App\Services\ProcessingFeeTieredPercentageRateService;
use ErrorException;
use Illuminate\Support\Str;

class QuoteService
{

    public int $counter;

    public function __construct(int $counter)
    {
        $this->counter = $counter;
    }
    
    public static function calculateChargesByMode($chargesMode, $fromCurrencyId, $toCurrencyId, $exchangeMarginRateId, $amount)
    {   
        $exchangeMarginRateItem = ExchangeMarginRateService::findOneById($exchangeMarginRateId);
        $chargesModeArray = explode('_', $chargesMode);

        $processingFeeActive = $chargesModeArray[0] === 'PF';
        $exchangRateActive = $chargesModeArray[1] === 'ER';


        // $exchangeMarginRate = 1;
        $exchangeMarginRate = $exchangeMarginRateItem->rate ?? 0;
        $exchangeMarginRate = 1 - (floatval($exchangeMarginRate) / 100);

        if ($fromCurrencyId == $toCurrencyId) {
            $exchangeMarginRate = 1;
        }

        $processingFee = 0;
        $costExchangeRate = 1;
        $exchangeRate = 1;
        $exchangeRateData = null;
        $processingFeeBaseFixedRate = null;
        $processingFeeTieredPercentageRate = null;

        if ($processingFeeActive) {
            $processingFeeBaseFixedRate = ProcessingFeeBaseFixedRateService::findOneByCurrencyIds(
                $fromCurrencyId,
                $toCurrencyId
            );

            $processingFeeTieredPercentageRate = ProcessingFeeTieredPercentageRateService::findOneByCurrencyIds(
                $fromCurrencyId,
                $toCurrencyId,
                $amount
            );

            /**
             * Calculate processing fee, a_n = a+(n-1)*d
             * processing fee = initial value + (amount - 1) * common difference rate
             */
            $initialValue = isset($processingFeeBaseFixedRate) ? $processingFeeBaseFixedRate->amount : 0;
            $commonDifference = isset($processingFeeTieredPercentageRate) ? ($processingFeeTieredPercentageRate->percentage_rate / 100) : (1 / 100);
            $processingFeeBeforeRoundUp = $initialValue + ($amount - 1) * $commonDifference;
            $processingFee = $processingFeeBeforeRoundUp  > 0 ? round($processingFeeBeforeRoundUp, 2) : 0.00;
        }

        if ($exchangRateActive) {

            $exchangeRateData = ExchangeRateService::findOneByCurrencyIds(
                $fromCurrencyId,
                $toCurrencyId
            );

            /**
             * Calculate final converted amount after applied exchange rate
             */
            $costExchangeRate = floatval($exchangeRateData->rate);
            $exchangeRate = floatval($exchangeRateData->rate) * $exchangeMarginRate;
            $exchangeRate = round($exchangeRate, 8);
        }


        /**
         * Calculate the amount to be converted.
         * Exchange rate need to include margin rate
         */
        $toConvertAmountBeforeRoundUp = $amount - $processingFee;
        $toConvertAmount = round($toConvertAmountBeforeRoundUp, 2);

        $convertedAmountBeforeMargin =  $toConvertAmount * $costExchangeRate;
        $convertedAmountAfterMargin =  $toConvertAmount * $exchangeRate;
        $convertedAmount = round($convertedAmountAfterMargin, 2);

        /**
         * To calculate the earnings get from exchange rate difference
         */
        $exchangeRateMarginBeforeRoundUp = $convertedAmountBeforeMargin - $convertedAmount;
        $exchangeRateMargin = round($exchangeRateMarginBeforeRoundUp, 8);

        if ($convertedAmount <= 0) {
            $processingFee = 0;
            $toConvertAmount = 0;
            $exchangeRate = 0;
            $convertedAmount = 0;
        }

        $isSameCurrency = $fromCurrencyId === $toCurrencyId;
      
        return [
            'processing_fee' => $processingFee,
            'to_convert_amount' => $toConvertAmount,
            'exchange_rate' => $exchangeRate,
            'converted_amount' => $convertedAmount,
            'base_fixed_rate_id' => isset($processingFeeBaseFixedRate) ? $processingFeeBaseFixedRate->id : null,
            'tiered_percentage_rate_id' => isset($processingFeeTieredPercentageRate) ? $processingFeeTieredPercentageRate->id : null,
            'exchange_rate_id' => isset($exchangeRateData) ? $exchangeRateData->id : null,
            'cost_exchange_rate' => $costExchangeRate,
            'exchange_rate_margin' => $exchangeRateMargin,
            'exchange_margin_rate_id' => !$isSameCurrency ? $exchangeMarginRateItem->id : null

        ];
    }

    public static function calculateChargesByModeInverted($chargesMode, $fromCurrencyId, $toCurrencyId, $fromAmount)
    {
        $amount = 0;
        $chargesModeArray = explode('_', $chargesMode);

        $processingFeeActive = $chargesModeArray[0] === 'PF';

        $exchangRateActive = $chargesModeArray[1] === 'ER';

        $processingFee = 0;
        $exchangeRate = 1;
        $exchangeRateData = null;
        $processingFeeBaseFixedRate = null;
        $processingFeeTieredPercentageRate = null;

        if ($processingFeeActive) {

            $processingFeeBaseFixedRate = ProcessingFeeBaseFixedRateService::findOneByCurrencyIds(
                $fromCurrencyId,
                $toCurrencyId
            );

            $processingFeeTieredPercentageRate = ProcessingFeeTieredPercentageRateService::findOneByCurrencyIds(
                $fromCurrencyId,
                $toCurrencyId,
                0
            );

            /**
             * Calculate processing fee, a_n = a+(n-1)*d
             * processing fee = initial value + (amount - 1) * common difference rate
             */

            $initialValue = isset($processingFeeBaseFixedRate) ? $processingFeeBaseFixedRate->amount : 0;
            $commonDifference = isset($processingFeeTieredPercentageRate) ? ($processingFeeTieredPercentageRate->percentage_rate / 100) : (1 / 100);
            // $processingFeeBeforeRoundUp = $initialValue + ($amount - 1) * $commonDifference;
            // $processingFee = $processingFeeBeforeRoundUp  > 0 ? round($processingFeeBeforeRoundUp, 2) : 0.00;
        }

        if ($exchangRateActive) {

            $exchangeRateData = ExchangeRateService::findOneByCurrencyIds(
                $fromCurrencyId,
                $toCurrencyId
            );

            /**
             * Calculate final converted amount after applied exchange rate
             */
            $exchangeRate = floatval($exchangeRateData->rate);
        }

        $amountBeforeRoundUp  = (($fromAmount / $exchangeRate) + $initialValue - $commonDifference) / (1 - $commonDifference);
        $amount = $amountBeforeRoundUp  > 0 ? round($amountBeforeRoundUp, 2) : 0.00;

        return $amount;
    }

    public static function getQuoteByCurrencyIds($values)
    {
        $fromCurrencyId = $values['from_currency_id'];
        $toCurrencyId = $values['to_currency_id'];
        $amount = $values['amount'];
        // $userAcountId = isset($values['user_account_id']) ? $values['user_account_id'] : null;
        $userBankAccountId = isset($values['user_bank_account_id']) ? $values['user_bank_account_id'] : null;  // VERY IMPORTANT KIV FOR CONVERT AND SEED FLOW!!!!!
        $inverted = isset($values['inverted']) ? $values['inverted'] : null;

        $userBankAccount = UserService::findBankAccountByBankAccountId($userBankAccountId);

        $processingFeeMode = 'PF';
        $exchangeRateMode = "ER";

        // if same currency, no exchange rate charges will be applied
        if ($fromCurrencyId === $toCurrencyId) {
            $exchangeRateMode = "X" . $exchangeRateMode;
        }


        // if transfer within Topkash environment, no processing fee will be applied
        if (isset($userBankAccount) && $userBankAccount->type === 'TOPKASH') {
            $processingFeeMode = "X" . $processingFeeMode;
        }

        if (!isset($userBankAccountId)) {
            $processingFeeMode = "X" . $processingFeeMode;
        }

        $chargesMode = UserExchangeQuote::MODES[$processingFeeMode . '_' . $exchangeRateMode];

        $exchangeMarginRateItem = ExchangeMarginRateService::findLatestExchangeMarginRate();
        $exchangeMarginRateId = $exchangeMarginRateItem->id ?? null;

        $exchangeRateInfo = self::calculateChargesByMode($chargesMode, $fromCurrencyId, $toCurrencyId, $exchangeMarginRateId, $amount);


        if ($inverted) {
            // For no processing fee mode
            if ($chargesMode === 'XPF_XER' || $chargesMode === 'XPF_ER') {
                $invertedData = [
                    'mode' => $chargesMode,
                    'from_currency_id' => $toCurrencyId,
                    'to_currency_id' => $fromCurrencyId,
                    'amount' => $exchangeRateInfo['converted_amount'],
                    'converted_amount' => $exchangeRateInfo['converted_amount'],
                    'exchange_margin_rate_id' => $exchangeRateInfo['exchange_margin_rate_id'],
                ];

                $exchangeRateInfo = self::getInvertedExchangeRate($invertedData);

                $exchangeRateInfo['amount_inverted'] = $exchangeRateInfo['converted_amount'];
                $exchangeRateInfo['converted_amount'] = floatval($amount);
            } else {

                $convertedAmount = self::calculateChargesByModeInverted($chargesMode, $toCurrencyId, $fromCurrencyId, $amount);

                $exchangeRateInfo = self::calculateChargesByMode($chargesMode, $toCurrencyId, $fromCurrencyId, $exchangeMarginRateId, $convertedAmount);

                $data = [
                    'calculatedAmount' => $exchangeRateInfo['converted_amount'],
                    'amount' => $amount,
                    'chargesMode' => $chargesMode,
                    'toCurrencyId' => $toCurrencyId,
                    'fromCurrencyId' => $fromCurrencyId,
                    'exchangeMarginRateId' => $exchangeMarginRateId,
                    'originalAmount' => $convertedAmount
                ];

                $result = self::adjustment($data, 0);

                $exchangeRateInfo = self::calculateChargesByMode($chargesMode, $toCurrencyId, $fromCurrencyId, $exchangeMarginRateId, $result);

                $exchangeRateInfo['amount_inverted'] = round($result, 2);
            }
        }

        /**
         * Scenario, user may request quotation with new amount and with same from_currency_id and to_currency_id. 
         * Filter, based on from_currency_id, to_currency_id, user_id and expired_at > current timestamp
         * If quote record existed, generate token with existing expired_at value
         * Else, new quote record will be generated, and new token will utilised the new record's id and expired_at value
         */
        $user = auth()->user();

        if ($inverted) {
            $result = self::getExpiredAtAndQuoteId($toCurrencyId, $fromCurrencyId, $user->id, $chargesMode, $exchangeRateInfo);
        } else {
            $result = self::getExpiredAtAndQuoteId($fromCurrencyId, $toCurrencyId, $user->id, $chargesMode, $exchangeRateInfo);
        }

        $quoteId = $result['quoteId'];
        $expiredAt = $result['expiredAt'];

        $toEncryptData = array(
            'quote_id' => $quoteId,
            'expired_at' => $expiredAt
        );

        $toEncryptDataJson = json_encode($toEncryptData);
        $encryptionKey = KeyFactory::loadEncryptionKey('encryption.key');
        $quoteToken = Symmetric\Crypto::encrypt(
            new HiddenString(
                $toEncryptDataJson
            ),
            $encryptionKey
        );

        return [
            'amount' => $amount,
            'amount_inverted' => $exchangeRateInfo['amount_inverted'] ?? null,
            'processing_fee' =>  $exchangeRateInfo['processing_fee'], // Similar like Wise's processing rate
            'to_convert_amount' => $exchangeRateInfo['to_convert_amount'],
            'exchange_rate' => $exchangeRateInfo['exchange_rate'],
            'converted_amount' => $exchangeRateInfo['converted_amount'],
            'quote_token' => $quoteToken,
            'expired_at' => $expiredAt
        ];
    }

    private static function getExpiredAtAndQuoteId($fromCurrencyId, $toCurrencyId, $userId, $chargesMode, $exchangeRateInfo)
    {
        $userExchangeQuoteRecord = UserExchangeQuoteService::findOneByCurrencyIdsUserIdModeAndExpiredAt(
            $fromCurrencyId,
            $toCurrencyId,
            $userId,
            $chargesMode
        );


        if ($userExchangeQuoteRecord) {

            if (!$userExchangeQuoteRecord->is_transferred) {

                $expiredAt = $userExchangeQuoteRecord->expired_at;
                $quoteId = $userExchangeQuoteRecord->id;
            } else {

                $expiredAt = Carbon::now()->addHour()->getTimestamp();

                $userExchangeQuote = UserExchangeQuoteService::createUserExchangeQuote([
                    'from_currency_id' =>  $fromCurrencyId,
                    'to_currency_id' =>  $toCurrencyId,
                    'base_fixed_rate_id' => $exchangeRateInfo['base_fixed_rate_id'],
                    'tiered_percentage_rate_id' => $exchangeRateInfo['tiered_percentage_rate_id'],
                    'exchange_rate_id' => $exchangeRateInfo['exchange_rate_id'],
                    'exchange_margin_rate_id' => $exchangeRateInfo['exchange_margin_rate_id'],
                    'user_id' => $userId,
                    "expired_at" => $expiredAt,
                    "quote_uuid" =>  Str::uuid(),
                    "mode" => $chargesMode,
                ]);

                $quoteId = $userExchangeQuote->id;
            }
        } else {
            /**
             * Get expired_at value, plus 1 hour using UTC
             * Before quote record is generated
             */

            $expiredAt = Carbon::now()->addHour()->getTimestamp();

            $userExchangeQuote = UserExchangeQuoteService::createUserExchangeQuote([
                'from_currency_id' =>  $fromCurrencyId,
                'to_currency_id' =>  $toCurrencyId,
                'base_fixed_rate_id' => $exchangeRateInfo['base_fixed_rate_id'],
                'tiered_percentage_rate_id' => $exchangeRateInfo['tiered_percentage_rate_id'],
                'exchange_rate_id' => $exchangeRateInfo['exchange_rate_id'],
                'exchange_margin_rate_id' => $exchangeRateInfo['exchange_margin_rate_id'],
                'user_id' => $userId,
                "expired_at" => $expiredAt,
                "quote_uuid" =>  Str::uuid(),
                "mode" => $chargesMode,
            ]);

            $quoteId = $userExchangeQuote->id;
        }

        return [
            "expiredAt" => $expiredAt,
            "quoteId" => $quoteId,
        ];
    }

    private static function getInvertedExchangeRate($data)
    {
        $result = self::calculateChargesByMode(
            $data['mode'],
            $data['from_currency_id'],
            $data['to_currency_id'],
            $data['exchange_margin_rate_id'],
            $data['amount']
        );

        $result['to_convert_amount'] = $data['amount'];
        $result['converted_amount'] = $data['converted_amount'];

        return $result;
    }

    public static function verifyQuoteToken($values)
    {

        $token = $values['token'];
        $fromCurrencyId = $values['from_currency_id'];
        $toCurrencyId = $values['to_currency_id'];


        try {
            $encryptionKey = KeyFactory::loadEncryptionKey('encryption.key');
            $quoteToken = Symmetric\Crypto::decrypt(
                $token,
                $encryptionKey
            )->getString();

            $decryptedQuoteToken = json_decode($quoteToken);
        } catch (\Throwable $th) {

            return response()->json(['message' => 'Invalid Token'], 400);
        }

        $user = auth()->user();

        $userExchangeQuoteRecord = UserExchangeQuoteService::findOneByCurrencyIdsUserIdQuoteUuidAndExpiredAt(
            $fromCurrencyId,
            $toCurrencyId,
            $user->id,
            $decryptedQuoteToken->quote_id
        );

        if ($userExchangeQuoteRecord) {

            $expiredAt = $userExchangeQuoteRecord->expired_at;
            $currentTimeStamp = Carbon::now()->getTimestamp();


            $qkeyEncryptionKey = KeyFactory::loadEncryptionKey('quote_uuid.key');
            $qkey = Symmetric\Crypto::encrypt(
                new HiddenString(
                    $userExchangeQuoteRecord->quote_uuid
                ),
                $qkeyEncryptionKey
            );

            if ($expiredAt > $currentTimeStamp) {

                return [
                    'q_key' => $qkey,
                ];
            } else {
                return response()->json(['message' => 'Token expired'], 400);
            }
        } else {
            return response()->json(['message' => 'Record not found'], 400);
        }
    }

    public static function adjustment($data, $convertedAmount = 0, $counter = 1)
    {
        $chargesMode =  $data['chargesMode'];
        $toCurrencyId =  $data['toCurrencyId'];
        $fromCurrencyId =  $data['fromCurrencyId'];
        $exchangeMarginRateId =  $data['exchangeMarginRateId'];
        $amount =  $data['amount'];

        // by default average = convertedAmount
        $average = $convertedAmount;

        $result = self::calculateChargesByMode($chargesMode, $toCurrencyId, $fromCurrencyId, $exchangeMarginRateId, $average);

        $calculatedAmount =  $result['converted_amount'];

        // print_r('Original amount:' . $data['originalAmount'] . PHP_EOL);
        // print_r('Average: ' . $average . PHP_EOL);

        // print_r('Calculated amount:' . $calculatedAmount . PHP_EOL);
        // print_r('To match: ' . floatval($amount) . PHP_EOL);


        // if not equal, call recursive function to calculate again
        if (abs(floatval($amount) - $calculatedAmount) > 0.005) {

            $average =  $data['originalAmount'] - (($data['originalAmount'] - $convertedAmount) / 2);

            // print_r($counter . PHP_EOL);
            // print_r('-----------' . PHP_EOL);

            return self::adjustment($data, $average, $counter + 1);
        } else {

            // stop recursive function and return result to replace amount inverted
            return $convertedAmount;
        }
    }
}
