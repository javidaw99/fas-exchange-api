<?php

namespace App\Services;

use App\Models\DeleteUserRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use function _\map;

class DeleteUserRequestService
{
    public static function createDeleteUserRequest($isValid, $username, $topkashId, $phoneNo, $email, $reason)
    {
        DeleteUserRequest::create([
            'is_valid' => $isValid,
            'username' => $username,
            'topkash_id' => $topkashId,
            'phone_no' => $phoneNo,
            'email' => $email,
            'reason' => $reason,
        ]);
    }

    public static function findAllDeleteUserRequest()
    {
        $records = DB::table('delete_user_requests')
        ->leftJoin('users','users.id', '=', 'delete_user_requests.assigned_user_id')
        ->select('delete_user_requests.*', 'users.username as assigned_user_username');

        return $records;
    }

}
