<?php

namespace App\Services;

use App\Models\Account;
use App\Models\User;
use App\Models\FasTransferConvert;

class FasTransferConvertService
{

    public static function createFasTransferConvert($info)
    {
        $fasexchangeConvert = FasTransferConvert::create([
            'fas_transfer_request_id' => $info['fas_transfer_request_id'],
            'user_bank_account_id' => $info['user_bank_account_id'],
            'currency_id'=>$info['currency_id'],
            'amount' => $info['amount'],
            'day' => $info['day'],
            // 'exchange_rate_id' => $info['exchange_rate_id'],
        ]);

        return $fasexchangeConvert->id;
    }

    public static function findFasTransferConvertById($id)
    {

        $fasTransferConvert = FasTransferConvert::where('id', $id)->first();


        return $fasTransferConvert;
    }
    public static function updateFasTransferConvertStatus($id, $status)
    {

        $fasTransferConvert = FasTransferConvertService::findFasTransferConvertById($id);

        $fasTransferConvert->status = $status;

        $fasTransferConvert->save();
    }
    

}
