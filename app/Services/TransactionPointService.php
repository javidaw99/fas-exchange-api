<?php

namespace App\Services;

use App\Models\TransactionPoint;

use Illuminate\Support\Facades\DB;

class TransactionPointService
{

    public static function findLatestTransactionPointByUserId($userId)
    {


        $transactionPoint = TransactionPoint::where([
            'user_id' => $userId,
        ])
        ->orderBy('created_at','DESC')

        ->orderBy('id','DESC')->first();;


        return $transactionPoint;
    }

    public static function findTransactionPointByUserId($userId)
    {
        // \DB::enableQueryLog(); 
        $transactionPoint =  DB::table('all_point_transaction_view')
         ->where([
            'user_id' => $userId,
        ])->get();
        // $transactionPoint = TransactionPoint::with([
        //     'transactionPoint' => function ($query) {
        //         $query->with(['user:id,username']);
             
        //     },
        //     'toUser' => function ($query) {
        //         $query->select('id', 'username');
        //     },
        //     // 'fromUser' => function ($query) {
        //     //     $query->select('id', 'username');
        //     // },
        // ])
        // ->where([
        //     'user_id' => $userId,
        // ])
        // ->orderBy('created_at','DESC')

        // ->orderBy('id','DESC')->get();

        // dd(\DB::getQueryLog());


        return $transactionPoint;
    }
}