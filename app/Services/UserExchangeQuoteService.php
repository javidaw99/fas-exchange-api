<?php

namespace App\Services;

use App\Models\UserExchangeQuote;
use Carbon\Carbon;

class UserExchangeQuoteService
{
    public static function createUserExchangeQuote($data)
    {
        $userExchangeQuote = UserExchangeQuote::create($data);

        return $userExchangeQuote;
    }

    public static function findOneByCurrencyIdsUserIdModeAndExpiredAt(
        $fromCurrencyId,
        $toCurrencyId,
        $userId,
        $chargesMode
    ) {
        $currentTimeStamp = Carbon::now()->getTimestamp();

        $userExchangeQuote = UserExchangeQuote::where('from_currency_id', $fromCurrencyId)
            ->where('to_currency_id', $toCurrencyId)
            ->where('user_id', $userId)
            ->where('mode', $chargesMode)
            ->where('expired_at', '>=', $currentTimeStamp)
            ->first();

        return $userExchangeQuote;
    }

    public static function findOneByCurrencyIdsUserIdQuoteUuidAndExpiredAt(
        $fromCurrencyId,
        $toCurrencyId,
        $userId,
        $quote_id
    ) {

        $userExchangeQuote = UserExchangeQuote::where('from_currency_id', $fromCurrencyId)
            ->where('to_currency_id', $toCurrencyId)
            ->where('user_id', $userId)
            ->where('user_id', $userId)
            ->where('id', $quote_id)
            ->first();

        return $userExchangeQuote;
    }


    public static function findOneByQuoteUuid($quoteUuid)
    {
        $userExchangeQuote = UserExchangeQuote::where('quote_uuid', $quoteUuid)
            ->first();

        return $userExchangeQuote;
    }

    public static function updateIsTransffered($id)
    {

        UserExchangeQuote::where('id', $id)
            ->update(['is_transferred' =>  1]);
    }
}
