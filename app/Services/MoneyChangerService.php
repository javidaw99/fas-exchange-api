<?php

namespace App\Services;

use App\Models\MoneyChanger;
use App\Models\Address;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\MorphTo;




class MoneyChangerService
{
    public static function createNewMoneyChanger($info)
    {
       
        //need to add topkash id as well
        $money_changer = MoneyChanger::create([
            'name' => $info['name'],
            'pic' => $info['pic'],
            'email' => $info['email'],
            'phone' => $info['phone'],
            // 'address_id' => $info['address_id'],
            'currency_available' => $info['currency_available'],
            'emergency_pic' => $info['emergency_pic'],
            'emergency_phone' => $info['emergency_phone'],
            'longitude' => $info['longitude'],
            'latitude' => $info['latitude'],
            'google_map' => $info['google_map'],
            'waze_map' => $info['waze_map'],
            'is_active' => $info['is_active'],
        ]);

        

        return $money_changer->id;
    }

    public static function checkUniqueName($name)
    {
        $moneyChangerCount = MoneyChanger::where('name', $name)
            ->whereNull('deleted_at')
            ->count();

        return $moneyChangerCount ? FALSE : TRUE;
    }
    public static function findAllMoneyChanger()
    {
        return MoneyChanger::whereNotNull('id');
    }

    public static function findMoneyChangerById($id)
    {

        $moneyChanger = MoneyChanger::where('id', $id)
        ->with([
            'address' => function ($query) {
                $query->select('id','reference_id','reference_table','address_1', 'address_2','city', 'postcode','state_id','country_id');
            },
            'operation' => function ($query) {
                $query->select('id','money_changer_id','hours','days','is_closed');
            },
            'users' => function ($query) {
                $query->select('id','username','fullname','money_changer_id');
            }
        ])
        ->first();
   

        return $moneyChanger;
    }

    public static function updateMoneyChangerById($info, $id)
    {
        // print_r($info);
        if ($id) {
            $money_changer = MoneyChanger::find($id);
        } 

        foreach ($info as $key => $value) {
            $money_changer->$key = $info[$key];
        }

        if ($money_changer->isClean()) {
            return false;
        } else {
            $money_changer->save();
            return true;
        }
    }

}
