<?php

namespace App\Services;

use App\Models\Country;
use App\Models\TransactionFilter;
use App\Models\CountryCurrency;

class CountryService
{
    public static function findCountryCurrencyById($countryId)
    {

        $currencies = CountryCurrency::where('country_id', $countryId)
            ->with(['currency' => function ($query) {
                $query->select('id', 'currency_name', 'iso_code', 'flag');
            }])
            ->select(['id', 'country_id', 'currency_id'])
            ->get();


        return $currencies;
    }

    public static function findAllActiveCountriesWithCurrencies()
    {
        $result = Country::where('is_enabled', 1)
            ->with(['currency' => function ($query) {
                $query->select('currencies.id', 'currency_name', 'iso_code');
            }])
            ->select([
                'countries.id', 
                'name as country_name', 
                'iso_code_2 as country_iso_code', 
                'is_enabled'
            ]);

        return $result;
    }

    public static function findCountryWithCurrenciesById($id)
    {
        $result = Country::where('is_enabled', 1)
            ->with(['currency' => function ($query) {
                $query->select('currencies.id', 'currency_name', 'iso_code');
            }])
            ->select([
                'countries.id', 
                'name as country_name', 
                'iso_code_2 as country_iso_code', 
                'is_enabled'
            ])
            ->where('id', $id)
            ->first();

        return $result;
    }

    public static function updateCountryCurrenciesById($id, $currencies)
    {
        $country = Country::find($id);

        $existingCurrencies = $country->currency()->get();
        $exitingCurrencyIds = $existingCurrencies->map(function ($item) {
            return $item->id;
        })->toArray();

        //Remove existing currency
        $country->currency()->detach($exitingCurrencyIds);

        $country->currency()->attach($currencies);
    }

    public static function findCountryFilterOptionsByType($type)
    {

        $country = TransactionFilter::where('transaction_filters.reference_table', 'countries')
            ->where('transaction_filters.type', $type)
            ->whereNull('transaction_filters.deleted_at')
            ->select('transaction_filters.id','type','name', 'iso_code_2 as iso_code')
            ->leftJoin('countries', 'countries.id', 'transaction_filters.reference_id')
            ->get();

        return $country;
    }

    public static function findAllCountryFilterOptions()
    {

        $country = TransactionFilter::where('transaction_filters.reference_table', 'countries')
            ->whereNull('transaction_filters.deleted_at')
            ->select('transaction_filters.id','type','name', 'iso_code_2 as iso_code')
            ->leftJoin('countries', 'countries.id', 'transaction_filters.reference_id')
            ->get();

        return $country;
    }

    public static function findCountryByIsoCode($isoCodes)
    {

        $countryIds = [];

        foreach($isoCodes as $isoCode) {


            $country = Country::where('iso_code_2',  $isoCode)
            ->first();
 
            array_push($countryIds, $country->id);
        }

        return $countryIds;
    }

}
