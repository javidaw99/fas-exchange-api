<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\TransferPoint;
use App\Models\Status;
use Illuminate\Support\Facades\DB;
use function _\map;

class TransferPointService
{

    public static function createTransferPoint($payload){
        $transferPoint = TransferPoint::create([
            'type' => $payload['type'],
            'point_margin_rate_id' => $payload['point_margin_rate_id'],
            'amount' =>  $payload['amount'],
            'converted_amount' => $payload['converted_amount'],
            'point_rate_margin' => $payload['point_rate_margin'],
            'to_user_id' => $payload['to_user_id'],
            'user_id' => $payload['user_id'],
            'from_transfer_points_id' =>  $payload['from_transfer_points_id'],
            'user_bank_account_id' => $payload['user_bank_account_id'],
            'commission_rate' => $payload['commission_rate'],
            'user_account_id' => $payload['user_account_id'],
            'convert_rate' => $payload['convert_rate']
        ]);

       return $transferPoint;

    }




}