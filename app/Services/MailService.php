<?php

namespace App\Services;

use Mailgun\Mailgun;
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


class MailService
{
  public static function sendWelcomeTempPasswordEmail($payload)
  {
    $pattern = '/^https?:\/\/[-a-z]+((:+[0-9]{0,5})|([-.a-z]*\.[a-z]+))/';  //Get mainsite url
    $success = preg_match($pattern, $payload['url'], $match);

    if ($success) {
      // $mainsite = $match[0];
      // header('Access-Control-Allow-Credentials: true');
      // header("Access-Control-Allow-Origin: ".$mainsite);

      $mail = new PHPMailer(true);
      $htmlFile = file_get_contents("../assets/email-templates/welcome-temporary-password.html");
      $htmlFile = str_replace("{URL}", $payload['url'], $htmlFile);
      $htmlFile = str_replace("{FULLNAME}", $payload['fullname'], $htmlFile);
      $htmlFile = str_replace("{USERNAME}", $payload['username'], $htmlFile);
      $htmlFile = str_replace("{IMAGE_URL}", env('AWS_URL') . '/public/topkash-logo.png', $htmlFile);

      //Server settings
      // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
      $mail->isSMTP();                                            //Send using SMTP
      $mail->Host       = env('EMAIL_HOST', '');                     //Set the SMTP server to send through
      $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
      $mail->Username   = env('EMAIL_USERNAME', '');                     //SMTP username
      $mail->Password   = env('EMAIL_PASSWORD', '');                               //SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
      $mail->Port       = env('EMAIL_PORT', '');

      //Recipients
      $mail->setFrom(env('EMAIL_ADDRESS', ''), 'Topkash');
      $mail->addAddress($payload['to'], '');          //Add a recipient

      //Content
      $mail->isHTML(true);                                  //Set email format to HTML
      $mail->Subject = '[No-reply] - Welcome to Topkash';
      $mail->Body    = $htmlFile;

      $mail->send();
      // echo 'Message has been sent';

      return $htmlFile;
    }
    else {
      throw new Exception('Invalid URL');
    }
  }

  public static function sendResetPasswordEmail($payload)
  {
    $pattern = '/^https?:\/\/[-a-z]+((:+[0-9]{0,5})|([-.a-z]*\.[a-z]+))/';  //Get mainsite url
    $success = preg_match($pattern, $payload['url'], $match);

    if ($success) {
      // $mainsite = $match[0];
      // header('Access-Control-Allow-Credentials: true');
      // header("Access-Control-Allow-Origin: ".$mainsite);

      $mail = new PHPMailer(true);
      $htmlFile = file_get_contents("../assets/email-templates/reset-password.html");
      $htmlFile = str_replace("{URL}", $payload['url'], $htmlFile);
      $htmlFile = str_replace("{FULLNAME}", $payload['fullname'], $htmlFile);
      $htmlFile = str_replace("{IMAGE_URL}", env('AWS_URL') . '/public/topkash-logo.png', $htmlFile);

      //Server settings
      // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
      $mail->isSMTP();                                            //Send using SMTP
      $mail->Host       = env('EMAIL_HOST', '');                     //Set the SMTP server to send through
      $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
      $mail->Username   = env('EMAIL_USERNAME', '');                     //SMTP username
      $mail->Password   = env('EMAIL_PASSWORD', '');                               //SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
      $mail->Port       = env('EMAIL_PORT', '');

      //Recipients
      $mail->setFrom(env('EMAIL_ADDRESS', ''), 'Topkash');
      $mail->addAddress($payload['to'], '');          //Add a recipient     

      //Content
      $mail->isHTML(true);                                  //Set email format to HTML
      $mail->Subject = '[No-reply] - Reset your password';
      $mail->Body    = $htmlFile;

      $mail->send();
      // echo 'Message has been sent';

      return $htmlFile;
    }
    else {
      throw new Exception('Invalid URL');
    }
  }

  public static function sendWelcomeTempPasswordEmailUsingMailgun($payload)
  {
    $htmlFile = file_get_contents("../assets/email-templates/welcome-temporary-password.html");

    $pattern = '/{FULLNAME}/i';
    $htmlFile = preg_replace($pattern, $payload['fullname'], $htmlFile);

    $pattern = '/{PASSWORD}/i';
    $htmlFile = preg_replace($pattern, $payload['password'], $htmlFile);

    $pattern = '/{URL}/i';
    $htmlFile = preg_replace($pattern, $payload['url'], $htmlFile);

    $mg = Mailgun::create(env('MAILGUN_API_KEY', ''), env('MAILGUN_API_BASE_URL', ''));

    $mg->messages()->send(env('MAILGUN_DOMAIN', ''), [
      'from'    => 'bob@example.com', //Need to change this later
      'to'      => $payload['to'],
      'subject' => 'Welcome',
      'text'    => $payload['fullname'],
      'html' => $htmlFile
    ]);

    return $htmlFile;
  }

  public static function sendResetPasswordEmailUsingMailgun($payload)
  {
    $htmlFile = file_get_contents("../assets/email-templates/reset-password.html");

    $pattern = '/{FULLNAME}/i';
    $htmlFile = preg_replace($pattern, $payload['fullname'], $htmlFile);

    $pattern = '/{URL}/i';
    $htmlFile = preg_replace($pattern, $payload['url'], $htmlFile);

    $mg = Mailgun::create(env('MAILGUN_API_KEY', ''), env('MAILGUN_API_BASE_URL', ''));

    $mg->messages()->send(env('MAILGUN_DOMAIN', ''), [
      'from'    => 'bob@example.com', //Need to change this later
      'to'      => $payload['to'],
      'subject' => 'Reset Password',
      'text'    => $payload['fullname'],
      'html' => $htmlFile
    ]);
  }
}
