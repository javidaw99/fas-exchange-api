<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\ActivityLog;
use App\Models\Transfer;
use Illuminate\Support\Collection;


class ActivityLogService
{

    public static function findAllActivityLogs()
    {

        $userId = auth()->user()->id;
        $roleId = auth()->user()->role_id;
     
        $activityLogs = ActivityLog::with([
            'user' => function ($query) {
                $query->select('id', 'username');
            },
        ])
        ->leftJoin('transactions', function($join) {
            $join->on('transactions.reference_id', 'activity_logs.reference_id');
            $join->on('transactions.reference_table', 'activity_logs.reference_table');
            $join->where('transactions.is_requested', 1);
        })
        ->select([
            'activity_logs.id',
            'action_type',
            'activity_logs.reference_table',
            'transactions.type_id',
            DB::raw(
                'IF(
                activity_logs.reference_table = "reloads"
                OR activity_logs.reference_table = "transfers" 
                OR activity_logs.reference_table = "redeems", 
                TRANSACTION_ID_WITH_PREFIX(transactions.doc_id, transactions.type_id), 
                NULL
            ) as doc_id'),
            'activity_logs.reference_id',
            'activity_logs.created_at',
            'description',
            'created_by',
        ]);

        if($roleId !== 3) {
            $activityLogs->where('created_by', $userId);
        }

        return $activityLogs;
    }

    public static function findAllByReferenceTableReferenceID($referenceTable, $referenceId)
    {

        $activityLogs = ActivityLog::where([
            ['reference_table', $referenceTable],
            ['reference_id', $referenceId]
        ])
            ->leftJoin('users', 'activity_logs.created_by', 'users.id')
            ->select('activity_logs.*', 'users.username as username')
            ->orderByDesc('activity_logs.created_at')
            ->get();

        return $activityLogs;
    }

    public static function createActivityLog($values)
    {

        $previousValues = $values['model']->getOriginal();
        $currentValues = $values['model']->getAttributes();
        $actionType = ucfirst($values['action_type']);

        $referenceId = null;
        $referenceTable = null;


        if ($actionType === 'Edit' && ($values['subject'] === 'reloads' || $values['subject'] === 'transfers') ){

            $referenceId = $values['reference_id'];
            $referenceTable = $values['subject'];

        } elseif(($actionType === 'Edit' || $actionType === 'Create')  && $values['subject'] === 'addresses'){

            $referenceId = $currentValues['reference_id'];
            $referenceTable = $currentValues['reference_table'];

        } elseif($actionType === 'Edit' && $values['subject'] === 'money_changers'){

            if (isset($previousValues['money_changer_id'])) {
                $referenceId = $previousValues['money_changer_id'];
            }else{
                $referenceId = $currentValues['id'];
            }
            $referenceTable = $values['subject'];


        }else {

            if (isset($previousValues['id'])) {
                $referenceId = $previousValues['id'];
            }
             else {
                $referenceId = $currentValues['id'];
            }
            $referenceTable = $values['subject'];

        }

        $userId = isset(auth()->user()->id) ? auth()->user()->id : $values['user_id'];

        $descriptionArray = [];

        if ($actionType === 'Edit') {

            $columns = $values['columns'];
            $includeColumns = collect($columns['include']);
            $omitColumns = collect($columns['omit']);

            //default omit keys
            $omitKeys = ['id', 'created_at', 'updated_at', 'deleted_at'];

            if ($omitColumns->isEmpty()) {

                // If omit is empty + include is empty
                // omit default omit keys only
                if ($includeColumns->isEmpty()) {

                    $currentValues = collect($currentValues)->except($omitKeys)->all();

                    // If omit is empty + include is not empty
                    // pick only include columns
                } else {

                    $currentValues = collect($currentValues)->only($columns['include'])->all();
                }
            } else {

                // If omit is not empty + include is empty
                // merge omit columns into default omit key's array then only omit
                if ($includeColumns->isEmpty()) {

                    $omitKeys = array_merge($omitKeys, $omitColumns->toArray());

                    $currentValues = collect($currentValues)->except($omitKeys)->all();

                    // If omit is not empty + include is not empty
                    // Check is omit columns existed in include column, if yes, remove from include column then only omit
                } else {

                    $diff = $includeColumns->diff($omitColumns);

                    $currentValues = collect($currentValues)->only($diff->toArray())->all();
                }
            }

            // Loop to check column changes
            foreach ($currentValues as $key => $value) {

                // If key existed in numberColumns, change both previous values and current values to float before comparing
                if (in_array($key, $values['number_columns'])) {

                    if ((float)  $previousValues[$key] != (float)  $currentValues[$key]) {

                        $previousValues[$key] = number_format((float)$previousValues[$key], 2, '.', '');
                        $currentValues[$key] = number_format((float)$currentValues[$key], 2, '.', '');

                        $descriptionString = $key . '[' . $previousValues[$key] . ' > ' . $currentValues[$key] . ']';

                        array_push($descriptionArray, $descriptionString);
                    }
                } else {
                    $isColumnUpdated = $values['model']->wasChanged($key);

                    if ($isColumnUpdated) {

                        $previousValue =  isset($previousValues[$key]) ? $previousValues[$key] : 'null';

                        $descriptionString = $key . '[' . $previousValue . ' > ' . $currentValues[$key] . ']';

                        array_push($descriptionArray, $descriptionString);
                    }
                }
            }
        }

        if (collect($descriptionArray)->isNotEmpty() || isset($values['description'])) {

            $activityLogs = ActivityLog::create([
                'reference_table' => $referenceTable,
                'reference_id' => $referenceId,
                'action_type' => $actionType,
                'action_source' => isset($values['action_source']) ? $values['action_source'] : strtolower($values['action_type']) . " " . strtolower($values['subject']),
                'created_by' =>  $userId,
                'description' => isset($values['description']) ? $values['description'] : implode(",", $descriptionArray)
            ]);

            return $activityLogs;
        }
    }
}
