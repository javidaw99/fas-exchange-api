<?php

namespace App\Services;

use App\Models\ProcessingFeeTieredPercentageRate;

class ProcessingFeeTieredPercentageRateService
{

    public static function createProcessingFeeTieredPercentageRate($data)
    {
        $processingFeeTieredPercentageRateId = ProcessingFeeTieredPercentageRate::insertGetId([
            'from_currency_id' => $data['from_currency_id'],
            'to_currency_id' => $data['to_currency_id'],
            'percentage_rate' => $data['percentage_rate'],
            'from_amount' => $data['from_amount'],
            'to_amount' => $data['to_amount'],
            'date' => $data['date'],
        ]);

        return $processingFeeTieredPercentageRateId;
    }


    public static function findAllProcessingFeeTieredPercentageRates()
    {
        $processingFeeTieredPercentageRates = ProcessingFeeTieredPercentageRate::all();

        return $processingFeeTieredPercentageRates->load('from_currency', 'to_currency');
    }

    public static function findOneByCurrencyIds($from_currency_id, $to_currency_id, $amount)
    {
        $processingFeeBaseFixedRate = ProcessingFeeTieredPercentageRate::where([
            'from_currency_id' => $from_currency_id,
            'to_currency_id' => $to_currency_id,
        ])->where(
            'from_amount',
            '<=',
            $amount
        )->where(
            'to_amount',
            '>=',
            $amount
        )->orderBy('date', 'desc')->first();

        return $processingFeeBaseFixedRate;
    }
}
