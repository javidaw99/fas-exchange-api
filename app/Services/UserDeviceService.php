<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserDevice;
use Carbon\Carbon;

class UserDeviceService
{
    public static function findOrCreateUserDevice($data)
    {
        $record = User::join('devices', 'users.user_device_id', 'devices.id')
            ->whereNotNull('users.user_device_id')
            ->where('users.id', $data['user_id'])
            ->first();

        if(empty($record)) {
            $device = new UserDevice;

            $device->brand = $data['brand'];
            $device->bundle_id = $data['bundle_id'];
            $device->carrier = $data['carrier'];
            $device->device_name = $data['device_name'];
            $device->device_type = $data['device_type'];
            $device->device_id = $data['device_id'];
            $device->ip_address = $data['ip_address'];
            $device->mac_address = $data['mac_address'];
            $device->manufacturer = $data['manufacturer'];
            $device->model = $data['model'];
            $device->storage = $data['storage'];
            $device->sys_build_id = $data['sys_build_id'];
            $device->sys_build_number = $data['sys_build_number'];
            $device->sys_name = $data['sys_name'];
            $device->sys_version = $data['sys_version'];
            $device->unique_id = $data['unique_id'];
            $device->user_agent = $data['user_agent'];
            $device->version = $data['version'];
            $device->save();

            $user = User::find($data['user_id']);
            $user->user_device_id = $device->id;
            $user->save();
        }
        else {

            $device = UserDevice::find($record->user_device_id);

            $device->brand = $data['brand'];
            $device->bundle_id = $data['bundle_id'];
            $device->carrier = $data['carrier'];
            $device->device_name = $data['device_name'];
            $device->device_type = $data['device_type'];
            $device->device_id = $data['device_id'];
            $device->ip_address = $data['ip_address'];
            $device->mac_address = $data['mac_address'];
            $device->manufacturer = $data['manufacturer'];
            $device->model = $data['model'];
            $device->storage = $data['storage'];
            $device->sys_build_id = $data['sys_build_id'];
            $device->sys_build_number = $data['sys_build_number'];
            $device->sys_name = $data['sys_name'];
            $device->sys_version = $data['sys_version'];
            $device->unique_id = $data['unique_id'];
            $device->user_agent = $data['user_agent'];
            $device->version = $data['version'];

            if(!$device->isClean()) {
                $device->save();
            }
        }
    }
}