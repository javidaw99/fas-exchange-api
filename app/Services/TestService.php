<?php

namespace App\Services;

use App\Models\Account;
use App\Models\User;

class TestService
{

    public static function getAllUsers()
    {
        $users = User::select('id', 'username', 'fullname', 'phone_no', 'email')
            ->whereNull('deleted_at')
            ->get();

        return $users;
    }


    public static function findUserById($user_id)
    {

        $user = User::select('id', 'username', 'fullname', 'phone_no', 'email')
            ->where('id', $user_id)
            ->get();

        return $user;
    }

    public static function createDashboardUser($user_info)
    {

        $user = new User();
        $user->username = $user_info['username'];
        $user->fullname = $user_info['fullname'];
        $user->phone_no = $user_info['phone_no'];
        $user->email = $user_info['email'];
        $user->save();


        return $user;
    }

    public static function updateDashboardUser($user_info)
    {

        $user = User::find($user_info['id']);
        $user->username = $user_info['username'];
        $user->fullname = $user_info['fullname'];
        $user->phone_no = $user_info['phone_no'];
        $user->email = $user_info['email'];

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    public static function deleteUserbyId($user_id)
    {

        $user = User::find($user_id);

        if (is_null($user)) {
            return false;
        } else {
            $user->delete();
            return true;
        }
    }

    public static function getAllUsersLeftJoinCountry()
    {

        $userWithCountryInfo = User::leftJoin('countries as c', 'users.country_id', 'c.id')
            ->select('users.id', 'users.username', 'users.fullname', 'users.phone_no', 'users.email', 'c.name as country')
            ->get();


        return $userWithCountryInfo;
    }


    public static function getUserByIdLeftJoinCountry($user_id)
    {

        $userWithCountryInfo = User::where('users.id', $user_id)
            ->leftJoin('countries as c', 'users.country_id', 'c.id')
            ->select('users.id', 'users.username', 'users.fullname', 'users.phone_no', 'users.email', 'c.name as country')
            ->get();

        return $userWithCountryInfo;
    }

    public static function getUsersWithCountry()
    {


        // $userWithCountryInfo = User::with([
        //     'country' => function ($query) {
        //         $query->select('id', 'name', 'iso_code_2');
        //     }
        // ])
        //     ->whereNull('deleted_at')
        //     ->get();

        // return $userWithCountryInfo;

        $user = Account::selectRaw('COUNT(DISTINCT user_id) as total_accounts')
            ->get();


        // $user = Account::select('user_id')
        //     ->distinct()
        //     ->count();

        return $user;
    }


    // public function test()
    // {
    //     $user = Account::select('user_id')
    //         ->distinct()
    //         ->count();

    //     return $user;
    // }
}
