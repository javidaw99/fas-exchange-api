<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class SettingService {
    
    public static function extractGeneralSettingValueByKey($key)
    {
        $setting = DB::table('general_settings')->where('key', $key)->first();

        if(!is_null($setting)) {
            if($setting->type == 'number')
            {
                return (int)$setting->value;
            }
        }
        else {
            return null;
        }
    }
}