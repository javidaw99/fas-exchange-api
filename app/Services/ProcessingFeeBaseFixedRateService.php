<?php

namespace App\Services;

use App\Models\ProcessingFeeBaseFixedRate;

class ProcessingFeeBaseFixedRateService
{

    public static function createProcessingFeeBaseFixedRate($data)
    {
        $processingFeeBaseFixedRate = ProcessingFeeBaseFixedRate::create([
            'from_currency_id' => $data['from_currency_id'],
            'to_currency_id' => $data['to_currency_id'],
            'amount' => $data['amount'],
            'date' => $data['date'],
        ]);

        return $processingFeeBaseFixedRate->id;
    }


    public static function findAllProcessingFeeBaseFixedRates()
    {
        $processingFeeBaseFixedRates = ProcessingFeeBaseFixedRate::all();

        return $processingFeeBaseFixedRates->load('from_currency', 'to_currency');
    }

    public static function findOneByCurrencyIds($from_currency_id, $to_currency_id)
    {
        $processingFeeBaseFixedRate = ProcessingFeeBaseFixedRate::where([
            'from_currency_id' => $from_currency_id,
            'to_currency_id' => $to_currency_id
        ])->orderBy('date', 'desc')->first();

        return $processingFeeBaseFixedRate;
    }
}
