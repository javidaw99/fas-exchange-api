<?php

namespace App\Services;

use App\Models\Account;
use App\Models\User;
use App\Models\FasTransfer;

class FasTransferService
{

    public static function createFasTransfer($info)
    {
        $fasexchange = FasTransfer::create([
            'fas_transfer_request_id' => $info['fas_transfer_request_id'],
            'bank_account_id' => $info['bank_account_id'],
            'amount' => $info['amount'],
            'day' => $info['day'],
            // 'exchange_rate_id' => $info['exchange_rate_id'],
        ]);

        return $fasexchange->id;
    }
    public static function findFasTransferById($id)
    {

        $fasTransfer = FasTransfer::where('id', $id)->first();


        return $fasTransfer;
    }

    public static function updateFasTransferStatus($id, $status)
    {

        $fasTransfer = FasTransferService::findFasTransferById($id);

        $fasTransfer->status = $status;

        $fasTransfer->save();
    }



}
