<?php

namespace App\Services;

use App\Models\Status;
use App\Models\Transfer;
use App\Models\UserAccount;
use App\Models\UserBankAccount;
use Illuminate\Support\Facades\DB;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\Symmetric;
use Carbon\Carbon;
use function _\map;


class TransferService
{
    public static function transfer($values, $userId)
    {
        $qKey = $values['q_key'];
        $amount = $values['amount'];
        $toUserId = isset($values['to_user_id']) ? $values['to_user_id'] : null;
        $userAccountId = $values['user_account_id'];
        $toUserAccountId = isset($values['to_user_account_id'])
            ? $values['to_user_account_id']
            : null;
        $userBankAccountId = isset($values['user_bank_account_id'])
            ? $values['user_bank_account_id']
            : null;

        $encryptionKey = KeyFactory::loadEncryptionKey('quote_uuid.key');


        $decryptedQuoteUuid = Symmetric\Crypto::decrypt(
            $qKey,
            $encryptionKey
        )->getString();   

        $userTransferQuote = UserExchangeQuoteService::findOneByQuoteUuid(
            $decryptedQuoteUuid
        );

        $currencyId = $userTransferQuote['to_currency_id'];

        // SCAN/SEND MEMBER (NO WALLET)
        if (!$toUserAccountId && $toUserId) {

            $userAccountExisted = UserAccount::where([
                'user_id' => $toUserId,
                'currency_id' => $currencyId,
            ])->first();


            if ($userAccountExisted) {
                $toUserAccountId = $userAccountExisted->id;
            } else {
                $newUserAccount = UserAccount::create([
                    'user_id' => $toUserId,
                    'currency_id' => $currencyId,
                ]);

                $toUserAccountId = $newUserAccount->id;
            }

            // SCAN
            if (!$userBankAccountId) {
                $user = auth()->user();
                $userId = $user->id;

                $topkashUserId = $toUserId;

                $topkashUser = UserService::findOneByTopkashUserId($topkashUserId);

                if (!empty($topkashUser)) {
                    $newTopkashUserBankAccount = UserService::createTopkashUserBankAccount(
                        $userId,
                        $topkashUserId
                    );

                    if ($newTopkashUserBankAccount) {
                        $userBankAccountId = $newTopkashUserBankAccount->id;
                    } else {
                        $existedTopkashUserBankAccount = UserBankAccount::where([
                            'topkash_user_id' => $topkashUserId,
                            'user_id' => $userId,
                        ])->first();

                        $userBankAccountId = $existedTopkashUserBankAccount->id;
                    }
                }
            }
        }

        $isTransferRecordExisted = Transfer::where(
            'quote_id',
            $userTransferQuote['id']
        )->count();

        $userBankAccount = UserService::findBankAccountByBankAccountId(
            $userBankAccountId
        );
  
        if (!$isTransferRecordExisted) {
            $fromCurrencyId = $userTransferQuote['from_currency_id'];
            $toCurrencyId = $userTransferQuote['to_currency_id'];
            $exchangeMarginRateId = $userTransferQuote['exchange_margin_rate_id'];
            $chargesMode = $userTransferQuote['mode'];

            $transferRateInfo = QuoteService::calculateChargesByMode(
                $chargesMode,
                $fromCurrencyId,
                $toCurrencyId,
                $exchangeMarginRateId,
                $amount
            );

            // var_dump($transferRateInfo);
            // die();

            // Set to_user_id for TOPKASH transactions
            // $userBankAccount = UserService::find($userBankAccountId);

            if (empty($toUserId)) {
                $toUserId = $userBankAccount->topkash_user_id;
            }

            $type = null;

            if ($toUserId === $userId) {
                $type = 'CONVERT';
            } else {
                $type = 'SEND';
            }

            $transfer = Transfer::create([
                'type' => $type,
                'quote_mode' => $chargesMode,
                'quote_id' => $userTransferQuote['id'],
                'from_currency_id' => $fromCurrencyId,
                'to_currency_id' => $toCurrencyId,
                'exchange_rate_id' => $transferRateInfo['exchange_rate_id'],
                'user_bank_account_id' => $userBankAccountId,
                'base_fixed_rate_id' => $transferRateInfo['base_fixed_rate_id'],
                'tiered_percentage_rate_id' =>
                $transferRateInfo['tiered_percentage_rate_id'],
                // 'amount' => $transferRateInfo['to_convert_amount'],
                'amount' => $amount,
                'processing_fee' => $transferRateInfo['processing_fee'],
                'converted_amount' => $transferRateInfo['converted_amount'],
                'exchange_rate_margin' => $transferRateInfo['exchange_rate_margin'],
                'exchange_margin_rate_id' => $transferRateInfo['exchange_margin_rate_id'],
                'user_id' => $userId,
                'to_user_id' => $toUserId,
                'user_account_id' => $userAccountId,
                'to_user_account_id' => $toUserAccountId,
            ]);

            UserExchangeQuoteService::updateIsTransffered(
                $userTransferQuote['id']
            );

            unset($transfer['exchange_rate_margin']);
            unset($transfer['exchange_margin_rate_id']);
            unset($transfer['user_bank_account_id']);
            unset($transfer['base_fixed_rate_id']);
            unset($transfer['tiered_percentage_rate_id']);

            return $transfer;
        } else {
            return false;
        }
    }

    public static function findTransferById($id)
    {
        $transfer = DB::select(
            'SELECT * 
            FROM transfers_view 
            WHERE id = ?',
            [$id]
        );

        return $transfer[0];
    }

    public static function findAllConverts()
    {
        $transfer = DB::table('transfers_view')
            ->where([
                ['quote_mode', 'like', '%_ER'],
                ['doc_id', 'like', 'CV%'],
                ['is_requested', 1]
            ]);

        return $transfer;
    }

    public static function findAllTransfersByType($type)
    {
        $transfer = DB::table('transfers_view')
            ->where([
                ['type', 'SEND'],
                ['user_bank_account_type', $type]
            ]);

        return $transfer;
    }

    public static function findAllConversionsReport($queryParams)
    {
        // DB::enableQueryLog();

        $conversions = Transfer::leftJoin('transactions', 'transactions.reference_id', 'transfers.id')
            ->with([
                'from_currency:id,currency_name,iso_code',
                'to_currency:id,currency_name,iso_code',
                'user:id,topkash_id,username',
                'exchange_rate:id,rate',
                'exchange_margin_rate:id,rate'
            ])
            ->select('transfers.*', 'transactions.doc_id', 'transactions.type_id')
            ->where([
                ['quote_mode', 'like', '%_ER'],
                ['transactions.type_id', 3], // Conversion type
                ['transactions.is_requested', 1]
            ]);


        if (isset($queryParams['conversion_date'])) {
            $fromDate = $queryParams['conversion_date'][0];
            $toDate = $queryParams['conversion_date'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);

            $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
            $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');

            $conversions->whereBetween('transfers.created_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['from_currency'])) {
            $conversions->whereHas('from_currency', function ($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['from_currency']);
            });
        }

        if (isset($queryParams['to_currency'])) {
            $conversions->whereHas('to_currency', function ($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['to_currency']);
            });
        }


        $conversions = $conversions->orderBy('transactions.doc_id', 'desc')->get();

        $conversions = $conversions->map(function ($item) {
            return [
                'conversion_id' => UtilityService::transactionIdWithPrefix($item->doc_id, $item->type_id),
                'conversion_date' => $item->created_at->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A'),
                'customer_topkash_id' => $item->user->topkash_id ?? "-",
                'customer_username' => $item->user->username ?? "-",
                'from_currency' => $item->from_currency->iso_code,
                'from_amount' => $item->amount,
                'fees' => floatval($item->processing_fee) === 0.0 ? $item->processing_fee : '-',
                'amount_to_convert' => $item->amount,
                'customer_exchange_rate' => ($item->exchange_rate->rate ?? 1) * (1 - (($item->exchange_margin_rate->rate ?? 0) / 100)),
                'to_currency' => $item->to_currency->iso_code,
                'to_amount' => $item->converted_amount,
                'cost_exchange_rate' => $item->exchange_rate->rate ?? "-",
                'margin' => $item->exchange_margin_rate->rate ?? "-",
                'earning_currency' => $item->from_currency->iso_code,
                'earning_amount' => $item->exchange_rate_margin ?? "-",
                'from_currency_name' => $item->from_currency->iso_code . ' - ' . $item->from_currency->currency_name
            ];
        });

        // var_dump(\DB::getQueryLog());
        // die();

        return $conversions;
    }

    public static function findAllTransfersReport($queryParams)
    {
        // DB::enableQueryLog();

        $sends = Transfer::leftJoin('cases', 'transfers.id', 'cases.reference_id')
            ->leftJoin('transactions', 'transactions.case_id', 'cases.id')
            ->leftJoin('users', 'cases.assigned_user_id', 'users.id')
            ->with([
                'from_currency:id,currency_name,iso_code',
                'to_currency:id,currency_name,iso_code',
                'user:id,topkash_id,username',
                'exchange_rate:id,rate',
                'exchange_margin_rate:id,rate',
                'user_bank_account' => function ($query) {
                    $query->with([
                        'country:id,name,iso_code_2',
                        'bankInfo:id,name'
                    ]);
                },
                'source_bank_account' => function ($query) {
                    $query->with([
                        'bankCountry' => function ($query) {
                            $query->with(['bank:id,name']);
                        }
                    ]);
                }
            ])
            ->select(
                'transfers.*',
                'cases.status',
                'cases.processed_at',
                'users.username as assigned_to_username',
                'transactions.doc_id',
                'transactions.type_id'
            )
            ->where([
                ['type', 'SEND'],
                ['transactions.is_requested', 1],
                ['transactions.reference_table', 'transfers']
            ])
            ->whereHas('user_bank_account', function ($query) {
                $query->where('type', '!=', 'TOPKASH');
            });

        if (isset($queryParams['submission_date'])) {
            $fromDate = $queryParams['submission_date'][0];
            $toDate = $queryParams['submission_date'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);

            $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
            $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');


            $sends->whereBetween('transfers.created_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['from_currency'])) {
            $sends->whereHas('from_currency', function ($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['from_currency']);
            });
        }

        if (isset($queryParams['to_currency'])) {
            $sends->whereHas('to_currency', function ($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['to_currency']);
            });
        }

        if (isset($queryParams['recipient_bank_country'])) {
            $sends->whereHas('user_bank_account', function ($q) use ($queryParams) {
                $q->whereHas('country', function ($q) use ($queryParams) {
                    $q->where('name', $queryParams['recipient_bank_country']);
                });
            });
        }

        if (isset($queryParams['user'])) {
            $sends->where('users.username', $queryParams['user']);
        }

        $sends = $sends->orderBy('transactions.doc_id', 'desc')->get();

        $sends = $sends->map(function ($item) {
            $status = $item->status ? ucfirst(Status::TYPES[$item->status]) : 'N/A';

            return [
                'send_id' => UtilityService::transactionIdWithPrefix($item->doc_id, $item->type_id),
                'submission_date' => Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A'),
                'customer_topkash_id' => $item->user->topkash_id ?? "-",
                'customer_username' => $item->user->username ?? "-",
                'from_currency' => $item->from_currency->iso_code,
                'from_amount' => $item->amount,
                'fees' => floatval($item->processing_fee) === 0.0 ? $item->processing_fee : '-',
                'amount_to_convert' => $item->amount,
                'customer_exchange_rate' => ($item->exchange_rate->rate ?? 1) * (1 - (($item->exchange_margin_rate->rate ?? 0) / 100)),
                'to_currency' => $item->to_currency->iso_code,
                'to_amount' => $item->converted_amount,
                'recipient_bank_country' => $item->user_bank_account->country->name ?? "-",
                'recipient_bank' => $item->user_bank_account->bankInfo->name ?? "-",
                'recipient_bank_acc_no' => $item->user_bank_account->reference_bank_account ?? "-",
                'recipient_bank_acc_name' => $item->user_bank_account->reference_name ?? "-",
                'source_bank' => $item->source_bank_account->bankCountry->bank->name ?? "-",
                'source_bank_acc_no' => $item->source_bank_account->acc_no ?? "-",
                'source_bank_acc_name' => $item->source_bank_account->acc_name ?? "-",
                'assigned_to_username' => $item->assigned_to_username ?? "-",
                'processed_date' => $item->processed_at ? Carbon::createFromFormat('Y-m-d H:i:s', $item->processed_at)->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A') : '-',
                'status' => $status,
                'cost_exchange_rate' => $item->exchange_rate->rate ?? "-",
                'margin' => $item->exchange_margin_rate->rate ?? "-",
                'earning_currency' => $item->from_currency->iso_code,
                'earning_amount' => $item->exchange_rate_margin ?? "-",
                'from_currency_name' => $item->from_currency->iso_code . ' - ' . $item->from_currency->currency_name
            ];
        });

        // var_dump(DB::getQueryLog());

        return $sends;
    }

    public static function findAllTransferMembersReport($queryParams)
    {
        $sends = Transfer::leftJoin('transactions', 'transactions.reference_id', 'transfers.id')
            ->with([
                'from_currency:id,currency_name,iso_code',
                'to_currency:id,currency_name,iso_code',
                'user:id,topkash_id,username',
                'to_user:id,topkash_id,username',
                'exchange_margin_rate:id,rate',
                'exchange_rate:id,rate',
                'user_bank_account' => function ($query) {
                    $query->with([
                        'country:id,name,iso_code_2',
                        'bankInfo:id,name'
                    ]);
                },
            ])
            ->select('transfers.*', 'transactions.doc_id', 'transactions.type_id')
            ->where([
                ['type', 'SEND'],
                ['transactions.is_requested', 1],
                ['transactions.type_id', 4]
            ])
            ->whereHas('user_bank_account', function ($query) {
                $query->where('type', '=', 'TOPKASH');
            });


        if (isset($queryParams['submission_date'])) {
            $fromDate = $queryParams['submission_date'][0];
            $toDate = $queryParams['submission_date'][1];
            $fromCarbon = new Carbon($fromDate);
            $toCarbon = new Carbon($toDate);

            $finalFromDate = $fromCarbon->startOfDay()->format('Y-m-d H:i:s');
            $finalToDate = $toCarbon->endOfDay()->format('Y-m-d H:i:s');

            $sends->whereBetween('transfers.created_at', [$finalFromDate, $finalToDate]);
        }

        if (isset($queryParams['from_currency'])) {
            $sends->whereHas('from_currency', function ($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['from_currency']);
            });
        }

        if (isset($queryParams['to_currency'])) {
            $sends->whereHas('to_currency', function ($q) use ($queryParams) {
                $q->where('iso_code', $queryParams['to_currency']);
            });
        }

        $sends = $sends->orderBy('transactions.doc_id', 'desc')->get();

        $sends = $sends->map(function ($item) {
            return [
                'send_id' => UtilityService::transactionIdWithPrefix($item->doc_id, $item->type_id),
                'submission_date' => Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->timezone('Asia/Phnom_Penh')->format('d-m-Y, h:i:s A'),
                'customer_topkash_id' => $item->user->topkash_id ?? "-",
                'customer_username' => $item->user->username ?? "-",
                'from_currency' => $item->from_currency->iso_code,
                'from_amount' => $item->amount,
                'fees' => floatval($item->processing_fee) === 0.0 ? $item->processing_fee : '-',
                'amount_to_convert' => $item->amount,
                'customer_exchange_rate' => ($item->exchange_rate->rate ?? 1) * (1 - (($item->exchange_margin_rate->rate ?? 0) / 100)),
                'to_currency' => $item->to_currency->iso_code,
                'to_amount' => $item->converted_amount,
                'to_user_topkash_id' => $item->to_user->topkash_id ?? "-",
                'to_user_username' => $item->to_user->username ?? "-",
                // 'earning_amount' => $item->exchange_rate_margin ?? "-",
                'from_currency_name' => $item->from_currency->iso_code . ' - ' . $item->from_currency->currency_name,
                'cost_exchange_rate' => $item->exchange_rate->rate ?? "-",
                'margin' => $item->exchange_margin_rate->rate ?? "-",
                'earning_currency' => $item->from_currency->iso_code,
                'earning_amount' => $item->exchange_rate_margin ?? "-",
            ];
        });

        return $sends;
    }
}
