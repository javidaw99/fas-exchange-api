<?php

namespace App\Services;

use App\Models\Cases;
use App\Models\Reload;
use App\Models\Transfer;
use App\Models\Transaction;
use App\Models\CaseSecureSafe;
use App\Services\UtilityService;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use function _\map;

class CaseService
{

    public static function getCaseByTypeAndRefId($reference_table, $reference_id, $attributes = ['*'])
    {
        $case = Cases::lockForUpdate()
            ->where([
            ['reference_table', $reference_table],
            ['reference_id', $reference_id]
        ])
            ->select($attributes)
            ->first();

        return $case;
    }

    public static function findOneByRefTableRefId($reference_table, $reference_id, $user_id)
    {
        $case = Cases::where([
            'reference_table' => $reference_table,
            'reference_id' => $reference_id,
            "user_id" => $user_id
        ])->first();

        return $case;
    }

    public static function getCaseStatusWithStatusName($type, $ref_id)
    {
        $case = Cases::where([
            ['reference_table', $type],
            ['reference_id', $ref_id]
        ])
            ->leftJoin('statuses', 'cases.status', 'statuses.id')
            ->select('cases.*', 'statuses.name as status_name')
            ->first();

        return $case;
    }

    public static function findReloadCaseById($id)
    {
        // $reload = Reload::with([
        //     'currency:id,iso_code',
        //     'bankAccount' => function ($query) {
        //         $query
        //             ->select('bank_accounts.id', 'acc_no', 'acc_name', 'branch_name as bank_name', 'bank_country.id as bank_branch_id', 'bank_country.country_id as country_id')
        //             ->leftJoin('bank_country', 'bank_accounts.bank_country_id', 'bank_country.id', 'bank_country.country_id');
        //     }
        // ])
        //     ->where('id', $id)
        //     ->first();
        $reload = DB::select('SELECT * FROM reload_cases_view WHERE ref_id = ?', [$id]);

        return $reload[0];
    }

    public static function getCasesByRefTypeAndStatus($type, $status) {
        if ($type == 'reload') {
            if($status === 'all') {
                $cases = DB::table('reload_cases_view');
            }
            else {
                $cases = DB::table('reload_cases_view')
                ->where('status', $status);
            }

        } else if ($type == 'transfer') {
            if($status === 'all') {
                $cases = DB::table('transfer_cases_view');
            }
            else {
                $cases = DB::table('transfer_cases_view')
                ->where('status', $status);
            }
        }

        return $cases;
    }

    public static function getCasesByRefType($type)
    {
        $userId = auth()->user()->id;

        if ($type == 'reload') {
            $cases = DB::table('reload_cases_view')
                ->where('status', 1)
                ->orWhere([
                    ['status', 2],
                    ['assigned_user_id', $userId]
                ]);
        } else if ($type == 'transfer') {
            $cases = DB::table('transfer_cases_view')
                ->where('status', 1)
                ->orWhere([
                    ['status', 2],
                    ['assigned_user_id', $userId]
                ]);
        } else if ($type == 'exchange') {
            $refTable = 'exchanges';
        }

        // $cases = Cases::with('assignedUser:id,username')->leftJoin($refTable, 'cases.reference_id', $refTable . '.id')
        //     ->leftJoin('users', 'cases.user_id', 'users.id')
        //     ->leftJoin('accounts', 'cases.user_account_id', 'accounts.id')
        //     ->leftJoin('currencies as acc_currency', 'accounts.currency_id', 'acc_currency.id')
        //     ->select(
        //         'cases.id',
        //         'cases.user_id',
        //         'cases.reference_table',
        //         'cases.reference_id',
        //         'cases.assigned_user_id',
        //         'cases.status',
        //         'cases.created_at',
        //         'users.id as user_id',
        //         'users.fullname as user_fullname',
        //         'users.email as user_email',
        //         'users.phone_no as user_phone_no',
        //         'acc_currency.id as user_account_id',
        //         'acc_currency.iso_code as user_account_currency'
        //     )
        //     ->where('cases.reference_table', $type);

        // if ($type == 'reload') {
        //     $cases->leftJoin('currencies as reload_currency', 'reloads.currency_id', 'reload_currency.id')
        //         ->leftJoin('bank_accounts', 'reloads.bank_account_id', 'bank_accounts.id')
        //         ->leftJoin('bank_country', 'bank_accounts.bank_country_id', 'bank_country.id')
        //         ->addSelect([
        //             'reloads.amount',
        //             'reloads.reference_no',
        //             'reload_currency.id as reload_currency_id',
        //             'reload_currency.iso_code as reload_currency_iso_code',
        //             'bank_accounts.id as bank_account_id',
        //             'bank_accounts.acc_no as bank_account_no',
        //             'bank_accounts.acc_name as bank_account_name',
        //             'bank_country.branch_name as bank_branch_name',
        //         ]);
        // }

        // if ($type == 'transfer') {
        //     $cases->leftJoin('currencies as transfer_currency', 'transfers.to_currency_id', 'transfer_currency.id')
        //         ->leftJoin('countries', 'transfers.recipient_country_id', 'countries.id')
        //         ->leftJoin('bank_country', 'transfers.recipient_bank_country_id', 'bank_country.id')
        //         ->addSelect([
        //             'transfers.amount',
        //             'transfers.purpose',
        //             'transfers.recipient_acc_no',
        //             'transfer_currency.id as transfer_currency_id',
        //             'transfer_currency.iso_code as transfer_currency_iso_code',
        //             'countries.id as recipient_country_id',
        //             'countries.name as recipient_country_name',
        //             'countries.iso_code_2 as recipient_country_iso_code',
        //             'bank_country.id as recipient_bank_id',
        //             'bank_country.branch_name as recipient_bank_name'
        //         ]);
        // }

        // if ($type == 'exchange') {
        //     $cases->leftJoin('currencies as from_currency', 'exchanges.from_currency_id', 'from_currency.id')
        //         ->leftJoin('currencies as to_currency', 'exchanges.to_currency_id', 'to_currency.id')
        //         ->leftJoin('exchange_rates', 'exchanges.exchange_rate_id', 'exchange_rates.id')
        //         ->addSelect([
        //             'from_currency.id as from_currency_id',
        //             'from_currency.iso_code as from_currency_iso_code',
        //             'exchanges.from_amount',
        //             'to_currency.id as to_currency_id',
        //             'to_currency.iso_code as to_currency_iso_code',
        //             'exchanges.to_amount',
        //             DB::raw('IF(exchanges.from_currency_id = exchange_rates.from_currency_id, exchange_rates.rate, 1/exchange_rates.rate) as exchange_rate'),
        //             'exchanges.processing_fee',
        //         ]);
        // }

        // if ($type == 'reload' || $type == 'transfer') {
        //     $cases->where(function ($query) {
        //         $query
        //             ->where('status', 1)
        //             ->orWhere('status', 2);
        //     });
        // }

        // var_dump($queries);
        return $cases;
    }

    public static function findTransferCaseById($id)
    {
        // $transfer = Transfer::with([
        //     'currency:id,iso_code',
        //     'recipientCountry:id,name,iso_code_2',
        //     'recipientBankCountry:id,branch_name as bank_name,bank_id'
        // ])
        //     ->where('id', $id)
        //     ->first();

        $transfer = DB::select('SELECT * FROM transfer_cases_view WHERE ref_id = ?', [$id]);

        if(count($transfer) > 0) {
            return $transfer[0];
        }
        else {
            return null;
        }
    }



    public static function getUserCasesByMonthAndYear($month, $year)
    {
        $id = auth()->user()->id;

        $first = DB::table('cases as cl1')
            ->leftJoin('users as u1', 'cl1.user_id', 'u1.id')
            ->leftJoin('users as u2', 'cl1.assigned_user_id', 'u2.id')
            ->leftJoin('reloads as r', 'cl1.ref_id', 'r.id')
            ->leftJoin('bank_accounts as ba', 'r.bank_acc_id', 'ba.id')
            ->leftJoin('bank_country as bc', 'ba.bank_country_id', 'bc.id')
            ->leftJoin('countries as ct', 'bc.country_id', 'ct.id')
            ->leftJoin('currencies as c', 'r.currency_id', 'c.id')
            ->leftJoin('statuses as s', 'cl1.status', 's.id')
            ->leftJoin('transactions as tsn', 'cl1.id', 'tsn.case_id')
            ->leftJoin('accounts as acc', 'tsn.account_id', 'acc.id')
            ->leftJoin('currencies as c2', 'acc.currency_id', 'c2.id')
            ->select(
                'cl1.id',
                'cl1.user_id',
                'u1.fullname',
                'u1.email',
                'u1.phone_no',
                'cl1.ref_id',
                'cl1.ref_type',
                'cl1.assigned_user_id',
                'u2.fullname as assigned_user_name',
                DB::raw("NULL as from_currency_code"),
                DB::raw("NULL as to_currency_code"),
                DB::raw("NULL as exchange_rate"),
                DB::raw("NULL as to_bank_acc"),
                "bc.branch_name as bank_branch",
                DB::raw("NULL as from_amount"),
                DB::raw("NULL as to_amount"),
                'ba.acc_no',
                'c.iso_code as currency_code',
                'r.amount',
                'r.reference_no',
                'r.media_id',
                DB::raw("NULL as purpose"),
                DB::raw("NULL as recipient_acc_name"),
                'cl1.status',
                's.name as status_name',
                'tsn.id as transaction_id',
                'ct.name as transaction_country',
                'c2.iso_code as account_currency',
                'tsn.account_id',
                'tsn.old_balance',
                'tsn.new_balance',
                'cl1.created_at as cl_created_at'
            )
            ->where('ref_type', 'reloads')
            ->where('cl1.user_id', $id)
            ->where('cl1.status', '!=', 2)
            ->whereMonth('cl1.created_at', $month)
            ->whereYear('cl1.created_at', $year);

        $second = DB::table('cases as cl2')
            ->leftJoin('users as u1', 'cl2.user_id', 'u1.id')
            ->leftJoin('users as u2', 'cl2.assigned_user_id', 'u2.id')
            ->leftJoin('transfers as t', 'cl2.ref_id', 't.id')
            ->leftJoin('countries as ct', 't.recipient_country_id', 'ct.id')
            ->leftJoin('currencies as c', 't.currency_id', 'c.id')
            ->leftJoin('bank_country as bc', 't.recipient_bank_country_id', 'bc.id')
            ->leftJoin('statuses as s', 'cl2.status', 's.id')
            ->leftJoin('transactions as tsn', 'cl2.id', 'tsn.case_id')
            ->leftJoin('accounts as acc', 'tsn.account_id', 'acc.id')
            ->leftJoin('currencies as c2', 'acc.currency_id', 'c2.id')
            ->select(
                'cl2.id',
                'cl2.user_id',
                'u1.fullname',
                'u1.email',
                'u1.phone_no',
                'cl2.ref_id',
                'cl2.ref_type',
                'cl2.assigned_user_id',
                'u2.fullname as assigned_user_name',
                DB::raw("NULL as from_currency_code"),
                DB::raw("NULL as to_currency_code"),
                DB::raw("NULL as exchange_rate"),
                "t.recipient_acc_no as to_bank_acc",
                "bc.branch_name as bank_branch",
                DB::raw("NULL as from_amount"),
                DB::raw("NULL as to_amount"),
                DB::raw("NULL as acc_no"),
                'c.iso_code as currency_code',
                't.amount',
                DB::raw("NULL as reference_no"),
                DB::raw("NULL as media_id"),
                't.purpose',
                "t.recipient_acc_name as recipient_acc_name",
                'cl2.status',
                's.name as status_name',
                'tsn.id as transaction_id',
                'ct.name as transaction_country',
                'c2.iso_code as account_currency',
                'tsn.account_id',
                'tsn.old_balance',
                'tsn.new_balance',
                'cl2.created_at as cl_created_at'
            )
            ->where('ref_type', 'transfers')
            ->where('cl2.user_id', $id)
            ->where('cl2.status', '!=', 2)
            ->whereMonth('cl2.created_at', $month)
            ->whereYear('cl2.created_at', $year);

        $third = DB::table('cases as cl3')
            ->leftJoin('users as u1', 'cl3.user_id', 'u1.id')
            ->leftJoin('users as u2', 'cl3.assigned_user_id', 'u2.id')
            ->leftJoin('exchanges as ex', 'cl3.ref_id', 'ex.id')
            ->leftJoin('exchange_rates as exr', 'ex.exchange_rate_id', 'exr.id')
            ->leftJoin('currencies as c1', 'ex.from_currency_id', 'c1.id')
            ->leftJoin('currencies as c2', 'ex.to_currency_id', 'c2.id')
            ->leftJoin('statuses as s', 'cl3.status', 's.id')
            ->leftJoin('accounts as acc', 'ex.from_account_id', 'acc.id')
            ->leftJoin('transactions as tsn', 'cl3.id', 'tsn.case_id')
            ->select(
                'cl3.id',
                'cl3.user_id',
                'u1.fullname',
                'u1.email',
                'u1.phone_no',
                'cl3.ref_id',
                'cl3.ref_type',
                'cl3.assigned_user_id',
                'u2.fullname as assigned_user_name',
                'c1.iso_code as from_currency_code',
                'c2.iso_code as to_currency_code',
                DB::raw('IF(ex.from_currency_id = exr.from_currency_id, exr.rate, 1/exr.rate) as exchange_rate'),
                DB::raw("NULL as to_bank_acc"),
                DB::raw("NULL as bank_branch"),
                'ex.from_amount',
                'ex.to_amount',
                DB::raw("NULL as acc_no"),
                DB::raw("NULL as currency_code"),
                DB::raw("NULL as amount"),
                DB::raw("NULL as reference_no"),
                DB::raw("NULL as media_id"),
                DB::raw("NULL as purpose"),
                DB::raw("NULL as recipient_acc_name"),
                'cl3.status',
                's.name as status_name',
                'tsn.id as transaction_id',
                DB::raw("NULL as transaction_country"),
                DB::raw('NULL as account_currency'),
                'tsn.account_id',
                'tsn.old_balance',
                'tsn.new_balance',
                'cl3.created_at as cl_created_at',
            )
            ->where('ref_type', 'exchange')
            ->where('cl3.user_id', $id)
            ->whereColumn('tsn.account_id', 'acc.id')
            ->where('cl3.status', '!=', 2)
            ->whereMonth('cl3.created_at', $month)
            ->whereYear('cl3.created_at', $year);

        $records = $first->union($second)->union($third)->orderBy('cl_created_at', 'desc')->paginate(10);

        $processingFee = SettingService::extractGeneralSettingValueByKey('processing_fee');
        $processingFee = 1 - ($processingFee / 100);

        map($records->items(), function ($item) use ($processingFee) {
            $item->exchange_rate = round($item->exchange_rate * $processingFee, 4);
            $item->amount = $item->amount ? UtilityService::formatNumberToCurrency($item->amount, $item->currency_code) : null;
            $item->from_amount = $item->from_amount ? UtilityService::formatNumberToCurrency($item->from_amount, $item->from_currency_code) : null;
            $item->to_amount = $item->to_amount ? UtilityService::formatNumberToCurrency($item->to_amount, $item->to_currency_code) : null;
            return $item;
        });

        return $records;
    }

    public static function createTransactionRecord($data)
    {
        $transaction = new Transaction();

        $transaction->user_id = $data['user_id'];
        $transaction->account_id = $data['account_id'];
        $transaction->case_id = $data['case_id'];
        $transaction->currency_id = $data['currency_id'];
        $transaction->old_balance = $data['old_balance'];
        $transaction->new_balance = $data['new_balance'];
        $transaction->rate = $data['rate'] ? $data['rate'] : null;
        $transaction->save();

        $case = Cases::find($data['case_id']);

        if (empty($case->account_id)) {
            $case->account_id = $data['account_id'];
            $case->save();
        }
    }

    public static function getLatestTransactionRecord($user_id, $account_id)
    {
        $transaction = Transaction::where([
            ['user_id', $user_id],
            ['account_id', $account_id]
        ])
            ->latest()
            ->first();

        return $transaction;
    }

    public static function getTransactionByCaseId($case_id)
    {
        $transaction = Transaction::where('case_id', $case_id)
            ->first();

        return $transaction;
    }

    public static function createCaseSecureSafe($case_id)
    {
        $caseSecureSafe = CaseSecureSafe::create(["case_id" => $case_id]);
        return $caseSecureSafe;
    }

    public static function findAllUserCasesWithPendingStatus() {
        $reloadCount = Cases::where([
            ['status', 1],
            ['reference_table','reloads'],
        ])
        ->count();

        $transferCount = Cases::where([
            ['status', 1],
            ['reference_table','transfers'],
        ])
        ->count();

        return [
            'reload_count' => $reloadCount,
            'transfer_count' => $transferCount
        ];
    }

    public static function handleCaseWithLock($reference_table, $reference_id, $attributes = ['*'], $action, $user)
    {
        return DB::transaction(function () use ($reference_table, $reference_id, $attributes, $action, $user) {

            $case = Cases::lockForUpdate()
            ->where([
                ['reference_table', $reference_table],
                ['reference_id', $reference_id]
            ])
            ->select($attributes)
            ->first();

            if (empty($case)) {
                return response()->json(['message' => 'Case record cannot be found'], 400);
            }
    
            if ($action == 'assign') {
                if ($case->assigned_user_id == $user->id) {
                    return response()->json(['message' => 'This record has been assigned to' . $user->fullname], 200);
                }
                if (!empty($case->assigned_user_id)) {
                    return response()->json(['message' => 'This record has been assigned to other user'], 400);
                } else {
    
                    $case->assigned_user_id = $user->id;
                    $case->status = 2;
                    $case->save();
                }
            } else if ($action == 'move') {
                $assigned_user_id = $request->input('assigned_user_id');
                $user = UserService::findUserById($assigned_user_id);
    
                $case->assigned_user_id = $assigned_user_id;
                $case->status = 2;
                $case->save();
            }

            // $socketEndpoint = '/';

            // if ($reference_table == 'reload') {
            //     $socketEndpoint = '/update-reload';
            // } else if ($reference_table == 'transfers') {
            //     $socketEndpoint = '/update-transfer';
            // }
    
            // SocketService::emitSocketData($socketEndpoint, [
            //     'id' => $case->id,
            //     'data' => [
            //         'assigned_user' => [
            //             'id' => $case->assigned_user_id,
            //             'username' => $user->username
            //         ]
            //     ]
            // ]);

            return response()->json([
                'message' => 'Successfully ' . $action . ' this case to ' . $user->fullname
            ], 200);

        });
    }
}
