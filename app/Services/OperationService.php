<?php

namespace App\Services;

use App\Models\Operation;

use Illuminate\Support\Facades\DB;




class OperationService
{

    public static function createNewOperation($info,$id)
    {
        $operation = Operation::create([
            'days' => $info['days'],
            'hours' => $info['hours'],
            'money_changer_id' => $id,
            'is_closed' => $info['is_closed'],
    
        ]);
    }
    public static function updateOperationById($info,$id)
    {
        $operation = Operation::find($id);
        $operation->hours = $info['hours'];
        $operation->days = $info['days'];
        $operation->is_closed = $info['is_closed'];
      
        if ($operation->isClean()) {
            return false;
        } else {
            $operation->save();
            return true;
        }


      
    }


   

}
