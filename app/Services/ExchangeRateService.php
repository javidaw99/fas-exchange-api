<?php

namespace App\Services;

use App\Models\ExchangeRate;
use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use function _\map;

class ExchangeRateService
{

    public static function checkExistingExchangeRateRecord($payload)
    {
        $record = ExchangeRate::where('date', $payload['date'])
            ->where(function ($query) use ($payload) {
                $query->where([
                    ['from_currency_id', $payload['from_currency_id']],
                    ['to_currency_id', $payload['to_currency_id']]
                ])
                    ->orWhere([
                        ['from_currency_id', $payload['to_currency_id']],
                        ['to_currency_id', $payload['from_currency_id']]
                    ]);
            })
            ->get();

        return $record;
    }

    public static function checkExistingExchangeRateRecordByCode($from_currency_code, $to_currency_code)
    {
        $todayDate = Carbon::now()->format('Y-m-d');
        $fromCurrency = CurrencyService::findOneByCode($from_currency_code);
        $toCurrency = CurrencyService::findOneByCode($to_currency_code);

        $exchangeRate = ExchangeRate::where([
            'date' => $todayDate,
            'from_currency_id' => $fromCurrency->id,
            'to_currency_id' => $toCurrency->id
        ])->first();

        return $exchangeRate;
    }

    public static function checkExistingExchangeRateRecordByIds($from_currency_id, $to_currency_id, $date)
    {

        $exchangeRate = ExchangeRate::where([
            'effective_date' => $date,
            'from_currency_id' => $from_currency_id,
            'to_currency_id' => $to_currency_id
        ])->first();

        return $exchangeRate;
    }

    public static function updateExchangeRateById($id, $value)
    {

        ExchangeRate::where('id', $id)->update([
            'from_currency_id' => $value['from_currency_id'],
            'to_currency_id' => $value['to_currency_id'],
            'rate' => $value['rate'],
            'effective_date' => $value['effective_date'],
        ]);
    }

    public static function createExchangeRate($value)
    {
        ExchangeRate::create($value);
    }

    public static function findOneByCurrencyIds($from_currency_id, $to_currency_id)
    {
        $exchangeRate = ExchangeRate::where([
            'from_currency_id' => $from_currency_id,
            'to_currency_id' => $to_currency_id,
        ])->orderBy('effective_date', 'desc')->first();

        return $exchangeRate;
    }

    public static function findAllExchangeRatesByCurrencyIds($from_currency_id, $to_currency_id)
    {


        $endDate = Carbon::today();
        $startDate = Carbon::today()->subDays(13);

        $exchangeRate = ExchangeRate::where([
            'from_currency_id' => $from_currency_id,
            'to_currency_id' => $to_currency_id,
        ])->whereBetween('effective_date', [$startDate, $endDate])->select(['id', 'rate', 'effective_date'])->orderBy('effective_date')->get();

        return $exchangeRate;
    }

    public static function findOneByCurrencyIdsAndDate($from_currency_id, $to_currency_id, $effective_date)
    {


        $exchangeRate = ExchangeRate::where([
            'from_currency_id' => $from_currency_id,
            'to_currency_id' => $to_currency_id,
            'effective_date' => $effective_date
        ])->select(['id', 'rate', 'effective_date'])->orderBy('effective_date')->first();

        return $exchangeRate;
    }

    public static function findAll()
    {
        return ExchangeRate::all()->load('from_currency', 'to_currency');
    }

    public function getCurrentExistingRecordByDate($date)
    {
        $records = ExchangeRate::leftJoin('currencies as c1', 'exchange_rates.from_currency_id', 'c1.id')
            ->leftJoin('currencies as c2', 'exchange_rates.to_currency_id', 'c2.id')
            ->select(
                'from_currency_id',
                'c1.iso_code as from_currency_iso_code',
                'to_currency_id',
                'c2.iso_code as to_currency_iso_code',
            )
            ->where('effective_date', $date)->get();

        return [
            'total' => $records->count(),
            'records' => $records->toArray()
        ];
    }


    public function getCurrencyCombinationRecord()
    {
        $currencies = Currency::all();
        $currencies = map($currencies, function ($item) {
            return [
                'id' => $item['id'],
                'iso_code' => $item['iso_code']
            ];
        });
        $totalCurrencies = count($currencies);

        for ($i = 0; $i < $totalCurrencies; $i++) {
            for ($j = $i + 1; $j < $totalCurrencies; $j++) {
                $combinations[] = [
                    'from_currency_id' => $currencies[$i]['id'],
                    'from_currency_iso_code' => $currencies[$i]['iso_code'],
                    'to_currency_id' => $currencies[$j]['id'],
                    'to_currency_iso_code' => $currencies[$j]['iso_code'],
                ];
            }
        }

        return [
            'total' => count($combinations),
            'records' => $combinations
        ];
    }

    public static function getInExistentRecordsByDate($existingRecords, $allRecords)
    {
        $newRecords = [];

        foreach ($allRecords as $allRecordsKey => $allRecordsVal) {
            foreach ($existingRecords as $existingRecordsKey => $existingRecordsVal) {
                if (
                    array_diff($existingRecordsVal, $allRecordsVal) ===
                    array_diff($allRecordsVal, $existingRecordsVal)
                ) {
                    unset($allRecords[$allRecordsKey]);
                }
            }
        }

        $newRecords = map($allRecords, function ($item) {
            return $item;
        });

        return [
            'total' => count($newRecords),
            'records' => $newRecords
        ];
    }

    public static function updateExchangeRatesByApi($date)
    {





        /**
         * Create http client with preset headers settings
         */
        $client = new Client(['headers' => [
            'apikey' => 'S6XxXvJlqoI3blN98WvWwTQgoedIHg58',
            'Content-Type' => 'text/plain'
        ]]);

        /**
         * Get all currencies
         */
        $currencies = CurrencyService::findAll()->map(function ($currency) {
            return $currency->only(['id', 'iso_code']);
        });



        /**
         * Start retrieving exchange rate
         */
        foreach ($currencies as $currency) {





            /**
             * Filter to_currency, based on from_currency
             */
            $currencyCodes = $currencies
                ->filter(function ($item) use ($currency) {
                    return $item['iso_code'] != $currency['iso_code'];
                })
                ->map(function ($item) {
                    return $item['iso_code'];
                })
                ->join(',');

            $base =  $currency['iso_code'];
            $currencyId =  $currency['id'];
            $symbols =  urlencode($currencyCodes);

            $res = $client->request('GET', env('EXCHANGE_RATE_API_URL') . '/' . $date . '?symbols=' . $symbols . '&base=' . $base);




            $result = json_decode($res->getBody()->getContents());




            $exchangeRateResult = $result->rates;



            /**
             * Check exchange rate record's existance
             */
            foreach ($exchangeRateResult as $currencyCode => $rate) {

                $toCurrency = CurrencyService::findOneByCode($currencyCode);
                $toCurrencyId = $toCurrency->id;

                $exchangeRate = ExchangeRateService::checkExistingExchangeRateRecordByIds($currencyId, $toCurrencyId, $date);

                /**
                 * Update same day record if found
                 */
                if ($exchangeRate) {

                    $res = ExchangeRateService::updateExchangeRateById($exchangeRate->id, [
                        'from_currency_id' => $currencyId,
                        'to_currency_id' => $toCurrencyId,
                        'rate' => $rate,
                        'effective_date' => $date
                    ]);
                } else {

                    ExchangeRateService::createExchangeRate([
                        'from_currency_id' => $currencyId,
                        'to_currency_id' => $toCurrencyId,
                        'rate' => $rate,
                        'effective_date' => $date
                    ]);
                }
            }
        }
    }
}
