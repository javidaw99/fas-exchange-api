<?php

namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\ConnectionException;

class SocketService {

    public static function verifyOrigin($origin)
    {
        $allowedOrigins = [env('WEBSOCKET_URL'), null];

        $headers = [
            'Access-Control-Allow-Methods'     => 'POST',
        ];

        if (in_array($origin, $allowedOrigins)) {
            $headers['Access-Control-Allow-Origin'] = $origin;
        } else {
            $headers['Access-Control-Allow-Origin'] = $allowedOrigins[0];
            return response('Invalid origin', 401)->withHeaders($headers);
        }

        return $headers;
    }

    public static function verifyToken($jwt, $userId)
    {
        $jwtKey = env('JWT_KEY_SOCKET');
        $decoded = JWT::decode($jwt, new Key($jwtKey, 'HS256'));

        if ($decoded->sub !== $userId) {
            throw new \Exception('Invalid');
        }
    }

    public static function emitSocketData($endpoint, $data)
    {
        try {
            Http::post(env('WEBSOCKET_URL') . $endpoint, $data);
        } catch (ConnectionException $e) {
            // dd($e);
        }
    }
}