<?php

namespace App\Services;

use App\Models\Reload;
use App\Models\Cases;
use App\Models\Transfer;
use App\Models\Role;
use App\Models\User;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

use function _\map;



class MetricService {

    public static function findAllUserTransactionByDate(){

        $date = Carbon::today()->startOfDay();
        $date2 = Carbon::today()->endOfDay();
        $startDate = $date->toDateTimeString();
        $endDate = $date2->toDateTimeString();
        $totalUserTransaction = DB::table('user_transactions_history_view')
        ->whereBetween('created_at',[$startDate,$endDate])
        ->count();
        return $totalUserTransaction;
    }
    public static function findAllUserTransactionByPending(){
 

        $totalUserTransactionPending = DB::table('user_transactions_history_view')
        ->where('status','1')
        ->count();
        $totalReloadUserTransactionPending = DB::table('user_transactions_history_view')
        ->where('status','1')
        ->where('reference_table','reloads')
        ->count();
        $totalTransferUserTransactionPending = DB::table('user_transactions_history_view')
        ->where('status','1')
        ->where('reference_table','transfers')
        ->count();
        $pending = [
			'reload' => $totalReloadUserTransactionPending, 
            'transfer' => $totalTransferUserTransactionPending, 
            'total'=>$totalUserTransactionPending
		];
        return $pending;
    }
    public static function findReloadUserTransactionByPending(){
        $date = Carbon::today()->startOfDay();
        $date2 = Carbon::today()->endOfDay();
        $startDate = $date->toDateTimeString();
        $endDate = $date2->toDateTimeString();

        $todayReloadUserTransactionPending = DB::table('user_transactions_history_view')
        ->whereBetween('created_at',[$startDate,$endDate])
        ->where('reference_table','reloads')
        ->where('status','1')
        ->count();
  
        $totalOtherTransaction = DB::table('user_transactions_history_view')
        ->whereBetween('created_at',[$startDate,$endDate])
        ->where(function ($query) {
            $query->where('reference_table', '=', 'reloads')
            ->where('status','=','3');
        })
        ->count();

        $reloads = [
			'pending' => $todayReloadUserTransactionPending, 
            'success' => $totalOtherTransaction, 
		];
        return $reloads;
    }
    public static function findTransferUserTransactionByPending(){
        $date = Carbon::today()->startOfDay();
        $date2 = Carbon::today()->endOfDay();
        $startDate = $date->toDateTimeString();
        $endDate = $date2->toDateTimeString();

        $todayTransferUserTransactionPending = DB::table('user_transactions_history_view')
        ->whereBetween('created_at',[$startDate,$endDate])
        ->where('status','1')
        ->where('reference_table','transfers')
        ->count();

  

        $totalOtherTransaction = DB::table('user_transactions_history_view')
        ->whereBetween('created_at',[$startDate,$endDate])
        ->where(function ($query) {
            $query->where('reference_table', '=', 'transfers')
            ->where('status','=','3');
        })
        ->count();

        $transfers = [
			'pending' => $todayTransferUserTransactionPending, 
            'success' => $totalOtherTransaction, 
		];
        return $transfers;
    }
    
    public static function findPairTransferUserTransactionByCurrency(){


        $date = Carbon::today()->startOfDay();
        $date2 = Carbon::today()->endOfDay();
        $startDate = $date->toDateTimeString();
        $endDate = $date2->toDateTimeString();

       
        $totalPairTransferCurrency = DB::select('
        SELECT count(*) as count, from_currency_code, to_currency_code
        FROM user_transactions_history_view
        where reference_table = "transfer" 
        and created_at between ? and ?
        group by from_currency_code , to_currency_code
        order by count desc',[$startDate, $endDate]);
    

        return $totalPairTransferCurrency;
    }
    public static function findDailyTransferTransactionByCurrency(){
        $res = DB::select('
        SELECT sum(amount) as total, from_currency_code, created_at
        FROM user_transactions_history_view
        where reference_table = "transfer" 
        group by created_at , from_currency_code');

        return $res;
    }
    public static function getSevenDateType($dateType){
        if($dateType == 'day'){
              $mode = "DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL n $dateType),'%m/%d') as date_type";
              $orderMode = "asc";
        }elseif($dateType =='month'){
            $mode = "MONTHNAME(DATE_SUB(CURDATE(), INTERVAL n $dateType)) as date_type";
            $orderMode = "desc";
        }else{
            $mode = "$dateType(DATE_SUB(CURDATE(), INTERVAL n $dateType)) as date_type";
            $orderMode = "asc";
        }
       

        $res = DB::select("SELECT DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL n $dateType), '%Y-%m-01') AS date, 
        $mode,
        '0.00' AS total,
        '0.00' AS earn
            FROM (
                SELECT 0 n UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3
                UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6
            ) nums
        ORDER BY date_type $orderMode
        ");

       
         return $res;
    }
    public static function findReloadTransactionByDate($startDate, $endDate,$dateType){
        // \DB::enableQueryLog(); 
        if($dateType == 'day'){
            $mode = "DATE_FORMAT(created_at,'%m/%d')";
            $orderMode = "asc";
        }elseif($dateType =='month'){
            $mode = "MONTHNAME(created_at)";
            $orderMode = "desc";
        }else{
            $mode = "$dateType(created_at)";
            $orderMode = "asc";
        }

        $res = DB::select("SELECT sum(amount) as total, from_currency_code, $mode as date_type
            FROM user_transactions_history_view
            WHERE  reference_table = 'reloads'
            AND status = 3
            AND created_at BETWEEN '$startDate' AND '$endDate'
            GROUP BY from_currency_code, date_type
            ORDER BY date_type $orderMode
        ");




 
        // dd(\DB::getQueryLog()); // Show results of log

        return $res;
    }

    public static function findTransferTransactionByDate($startDate, $endDate,$dateType){
        // \DB::enableQueryLog(); 
        if($dateType == 'day'){
            $mode = "DATE_FORMAT(created_at,'%m/%d')";
            $orderMode = "asc";

        }elseif($dateType =='month'){
            $mode = "MONTHNAME(created_at)";
            $orderMode = "desc";

        }else{
            $mode = "$dateType(created_at)";
            $orderMode = "asc";

        }

                $res = DB::select("SELECT sum(amount) as total, sum(processing_fee) as earn, from_currency_code, $mode as date_type
                FROM user_transactions_history_view
                WHERE  reference_table = 'transfers'
                AND status = 3
                AND NOT transaction_type = 'CONVERT'
                AND user_bank_account_id IS NOT NULL
                AND created_at BETWEEN '$startDate' AND '$endDate'
                GROUP BY from_currency_code, date_type
                ORDER BY date_type $orderMode
                ");

 
        // dd(\DB::getQueryLog()); // Show results of log
       
       
        return $res;
    }
    public static function getTotalUsers() {

        $totalUsers = User::where('role_id', Role::TYPES['app_user'])
        ->whereNull('deleted_at')
        ->count();

        return $totalUsers;
    }

    public static function getTotalTransactions() {

        $totalTransactions = Transaction::whereNull('deleted_at')
        ->count();

        return $totalTransactions;
    }

    public static function getTotalTransferRequests() {

        $totalTransfer = Transfer::whereNull('deleted_at')
        ->count();

        return $totalTransfer;
    }

    public static function getTotalReloadRequests() {

        $totalReload = Reload::whereNull('deleted_at')
        ->count();

        return $totalReload;
    }

    public static function getTotalUserByCountry() {

        $totalUserByCountry = User::leftJoin('countries as c', 'users.country_id', 'c.id')
        ->whereNotNull('users.country_id')
        ->select('c.name as name')
        ->selectRaw('count(*) as value')
        ->groupBy('name')
        ->get();

        return $totalUserByCountry;
    }

    public static function getTotalRequestByStatus() {

        $totalRequestByStatus = Cases::leftJoin('statuses as s', 'cases.status', 's.id')
        ->whereNull('cases.deleted_at')
        ->select('s.name as name')
        ->selectRaw('count(*) as value')
        ->groupBy('name')
        ->get();

        return $totalRequestByStatus;
    }

    public static function getTransactionTrends($year = null, $currency = null) {
		
		if(is_null($year)) {
			$year = Carbon::now()->format('Y');
		}

        if(is_null($currency)) {
			$currency = 'all';
		}

        $reload = Cases::selectRaw('DATE_FORMAT(cases.created_at, "%b") as month, COUNT(*) as count')
        ->leftJoin('reloads', 'cases.ref_id', 'reloads.id')
        ->leftJoin('currencies', 'reloads.currency_id', 'currencies.id')
        ->where('ref_type', 'reloads')
        ->whereNull('cases.deleted_at')
		->whereYear('cases.created_at', $year);
        
        $transfer = Cases::selectRaw('DATE_FORMAT(cases.created_at, "%b") as month, COUNT(*) as count')
        ->leftJoin('transfers', 'cases.ref_id', 'transfers.id')
        ->leftJoin('currencies', 'transfers.currency_id', 'currencies.id')
        ->where('ref_type', 'transfers')
        ->whereNull('cases.deleted_at')
		->whereYear('cases.created_at', $year);

        $exchange = Cases::selectRaw('DATE_FORMAT(cases.created_at, "%b") as month, COUNT(*) as count')
        ->leftJoin('exchanges', 'cases.ref_id', 'exchanges.id')
        ->leftJoin('currencies', 'exchanges.from_currency_id', 'currencies.id')
        ->where('ref_type', 'exchange')
        ->whereNull('cases.deleted_at')
		->whereYear('cases.created_at', $year);

        if($currency != 'all') {
            $reload->where('currencies.iso_code', $currency);
            $transfer->where('currencies.iso_code', $currency);
            $exchange->where('currencies.iso_code', $currency);
        }

        $reload = $reload->groupBy('month')->pluck('count', 'month');
        $transfer = $transfer->groupBy('month')->pluck('count', 'month');
        $exchange = $exchange->groupBy('month')->pluck('count', 'month');

        $trends = [
			'reload' => $reload, 
			'transfer' => $transfer,
			'exchange' => $exchange,
		];

        return $trends;
    }

    public static function getUserTrends() {

		$startDate = Carbon::now()->startOfYear()->format('Y-m-d');
		$endDate = Carbon::now()->endOfYear()->format('Y-m-d');

		$user = User::selectRaw('DATE_FORMAT(created_at, "%b") as month , count(*) as count')
		->whereNull('deleted_at')
		->where('role_id', Role::TYPES['app_user'])
		->whereBetween('created_at', [$startDate, $endDate])
		->groupBy('month')->pluck('count', 'month');

		$months = MetricService::getListOfMonths();

        $months = map($months, function($item) {
            return [
                'month' => $item,
                'count' => 0
            ];
        });

		$months = collect($months)->pluck('count', 'month');

		$merged = $months->merge($user);

		$keys = map($merged, function ($value, $key) {
			return $key;
		});

		$values = map($merged, function ($value, $key) {
			return $value;
		});

		$results = [
			'label' => $keys,
			'data' => $values
		];

		return $results;
    }

    public static function getLatestTransactions() {

        $cases = Cases::with([
            'transact' => function (MorphTo $morphTo) {
                $morphTo->morphWith([
                    Reload::class => [
                        'currency:id,iso_code',                    
                    ],
                    Transfer::class => [
                        'currency:id,iso_code',                    
                    ],                
                ]);
            
             }
         ])
        ->where('ref_type', '!=', 'exchange')
        ->leftJoin('users', 'cases.user_id', 'users.id')
        ->leftJoin('statuses', 'cases.status', 'statuses.id')
        ->select(
            'cases.id', 'cases.user_id', 
            'cases.ref_type', 'cases.ref_id', 
            'cases.assigned_user_id', 'cases.created_at', 
            'users.fullname', 'users.email', 
            'users.phone_no', 'statuses.name as status')
        ->orderByDesc('cases.created_at')
        ->limit(10)
        ->get();

        $finalObject = [];

        foreach($cases as $case => $value) {
            
            $reloadObject = [
                'id'=> $value->id,
                'ref_type' => $value->ref_type,
                'ref_id' => $value->ref_id,
                'user_id'=> $value->user_id,
                'from_fullname' => $value->fullname,
                'from_email' => $value->email,
                'from_phone_no' => $value->phone_no,
                'status' => $value->status,
                'created_at' => $value->created_at,
                'transaction' => collect($value->transact)->only(['currency', 'amount', 'formatted_amount'])
            ];
            
            array_push($finalObject, $reloadObject);

        }

        return $finalObject;

    }

    public static function getListOfMonths() {
        $startDate = Carbon::now()->startOfYear()->format('Y-m-d');
		$endDate = Carbon::now()->endOfYear()->format('Y-m-d');

        $period = new CarbonPeriod($startDate, '1 month', $endDate);

		$months = [];

		foreach($period as $item) { 
			$month = $item->format('M');

			array_push($months, $month);
		}

        return $months;
    }

    public static function getYearsBasedOnCaseRecord() {
        $records = Cases::selectRaw('DATE_FORMAT(created_at, "%Y") as year')
            ->distinct()
            ->get();

        $records = map($records, function($item) {
            return $item->year;
        });

        return $records;
    }

}