<?php

namespace App\Services;

use App\Models\Address;
use App\Services\ActivityLogService;

class AddressService
{

    public static function createAddress($data)
    {
        $address = Address::create([
            'address_1' => $data['address_1'],
            'address_2' => $data['address_2'],
            'city' => $data['city'],
            'postcode' => $data['postcode'],
            'state_id' => $data['state_id'],
            'country_id' => $data['country_id'],
            'reference_id' => $data['reference_id'],
            'reference_table' => $data['reference_table'],
        ]);

        return $address->id;
    }


    public static function updateAddressById($data, $id)
    {
        $address = Address::find($id);
        $address->address_1 = $data['address_1'];
        $address->address_2 = $data['address_2'];
        $address->city = $data['city'];
        $address->postcode = $data['postcode'];
        $address->state_id = $data['state_id'];
        $address->country_id = $data['country_id'];

        if ($address->isClean()) {
            return false;
        } else {
            $address->save();
            return true;
        }
    }

    public static function updateAddressByReferenceIdReferenceTable($data)
    {
        $address = Address::where('reference_id', $data['reference_id']) 
        ->where('reference_table', $data['reference_table'])
        ->first();
     
        $address->address_1 = $data['address_1'];
        $address->address_2 = $data['address_2'];
        $address->city = $data['city'];
        $address->postcode = $data['postcode'];
        $address->state_id = $data['state_id'];
        $address->country_id = $data['country_id'];

        if ($address->isClean()) {
            return false;
        } else {
            $address->save();
            return true;
        }
    }

    public static function findAddressByReferenceIdReferenceTable($refId, $refTable) {
        $address = Address::where('reference_id', $refId) 
        ->where('reference_table', $refTable)
        ->first();

        return $address;
    }

    public static function findAddressById($id)
    {
        $address = Address::where('id', $id)
            ->with([
                'state' => function ($query) {
                    $query->select('id', 'name');
                },
                'country' => function ($query) {
                    $query->select('id', 'name');
                },
            ])
            ->select('address_1', 'address_2', 'city', 'postcode', 'state_id', 'country_id')
            ->first();

        return $address;
    }
}
