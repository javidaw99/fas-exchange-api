<?php

namespace App\Services;

use App\Models\Banner;
use App\Models\Media;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use function _\map;

class MediaService
{
    public static function createBanner($name, $mediaId, $type, $sequence, $isEnabled, $navigationStack = null, $navigationScreen = null, $website = null)
    {

        Banner::create([
            'name' => $name,
            'media_id' => $mediaId,
            'type' => $type,
            'sequence' => $sequence,
            'is_enabled' => $isEnabled,
            'navigation_stack' => $navigationStack,
            'navigation_screen' => $navigationScreen,
            'website' => $website,
        ]);
    }

    public static function findAllBanners()
    {
        $records = Banner::leftJoin('medias', 'banners.media_id', 'medias.id')
            ->select('banners.*',  'medias.filename', 'medias.path');

        return $records; 
    }

    public static function findMediaById($id)
    {
        return Media::find($id);
    }

    public static function createMedia($filename, $path, $extension, $mime, $reference_id, $reference_table, $type)
    {
        return Media::create([
            'filename' => $filename,
            'path' => $path,
            'extension' => $extension,
            'mime' => $mime,
            'reference_id' => $reference_id,
            'reference_table' => $reference_table,
            'type' => $type,
        ]);
    }

    public static function findBannerById($id)
    {
        return Banner::find($id);
    }

    public static function findBannerWithPathById($id)
    {
        $banner = Banner::leftJoin('medias', 'banners.media_id', 'medias.id')
            ->select('banners.*',  'medias.filename', 'medias.path')
            ->where('banners.id', $id)
            ->first();

        return $banner;
    }

}
