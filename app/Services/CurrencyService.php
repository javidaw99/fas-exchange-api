<?php

namespace App\Services;

use App\Models\Currency;
use App\Models\TransactionFilter;
use Illuminate\Support\Facades\DB;

class CurrencyService
{

    public static function findAll()
    {
        return Currency::all();
    }

    public static function findOneByCode($currency_code)
    {
        return Currency::where('iso_code', $currency_code)->first();
    }

    public static function findOneById($currency_id)
    {
        return Currency::where('id', $currency_id)->first();
    }

    public static function findAllCurrencies()
    {
        return Currency::whereNotNull('id');
    }

    
    public static function findCurrencyByCountryId($countryId)
    {
        $bankCountry = DB::table('country_currency')
        ->leftJoin('currencies','country_currency.currency_id','=','currencies.id')
        ->where('country_id', $countryId)
        ->where('country_currency.deleted_at', null)
        ->get();
        // ->first();

        return $bankCountry;
    }

    public static function findCurrencyFilterOptionsByType($type)
    {

        $currency = TransactionFilter::where('transaction_filters.reference_table', 'currencies')
            ->where('transaction_filters.type', $type)
            ->whereNull('transaction_filters.deleted_at')
            ->select('transaction_filters.id','transaction_filters.type','currency_name as name','iso_code', 'locale', 'flag')
            ->leftJoin('currencies', 'currencies.id', 'transaction_filters.reference_id')
            ->get();

        return $currency;
    }

    
    public static function findAllCurrencyFilterOptions()
    {

        $currency = TransactionFilter::where('transaction_filters.reference_table', 'currencies')
            ->whereNull('transaction_filters.deleted_at')
            ->select('transaction_filters.id','transaction_filters.type','currency_name as name','iso_code', 'locale', 'flag')
            ->leftJoin('currencies', 'currencies.id', 'transaction_filters.reference_id')
            ->get();

        return $currency;
    }



    public static function findCurrencyByIsoCode($isoCodes)
    {

        $currencyIds = [];

        foreach($isoCodes as $isoCode) {

        $currency = Currency::where('iso_code',  $isoCode)
           ->first();

           array_push($currencyIds, $currency->id);

        }

        return $currencyIds;
    }
}
