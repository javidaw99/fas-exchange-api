<?php

namespace App\Services;

use App\Models\ExchangeMarginRate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use function _\map;

class ExchangeMarginRateService
{
    public static function createExchangeMarginRate($rate)
    {
        ExchangeMarginRate::create([
            'rate' => $rate,
        ]);
    }

    public static function findAllExchangeMarginRate($queryParams)
    {
        $records = DB::table('exchange_margin_rates');

        return $records;
    }

    public static function findLatestExchangeMarginRate()
    {
        $record = DB::table('exchange_margin_rates')->orderBy('id', 'desc')->first();

        return $record;
    }

    public static function findOneById($id)
    {
        $record = DB::table('exchange_margin_rates')->where('id', $id)->first();

        return $record;
    }
}
