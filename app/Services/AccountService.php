<?php

namespace App\Services;

use App\Models\Currency;
use App\Models\BankAccount;
use App\Models\Reload;
use App\Models\Cases;
use App\Models\Country;
use App\Models\Transfer;
use App\Models\Exchange;
use App\Models\Account;
use App\Models\CountryCurrency;
use App\Models\Bank;
use App\Models\BankCountry;
use App\Models\Media;
use App\Models\Redeem;

class AccountService
{


    public static function getLatestUserCaseStatus($user)
    {

        $userCase = $user->cases()->latest()->first();

        if ($userCase !== null) {
            return $userCase->status;
        } else {
            return null;
        }
    }

    public static function findCurrencyById($currencyId)
    {
        return Currency::find($currencyId);
    }

    public static function findCurrencyByCode($currencyCode)
    {

        $currencyCount = Currency::where('iso_code', $currencyCode)
            ->whereNull('deleted_at')
            ->count();

        return $currencyCount ? TRUE : FALSE;
    }

    public static function findCountryCurrencyByCountryId($countryId)
    {

        $countryCurrency = CountryCurrency::where('country_id', $countryId)
            ->whereNull('deleted_at')
            ->count();
        return $countryCurrency ? TRUE : FALSE;
    }

    public static function findCountryById($countryId)
    {
        return Country::find($countryId);
    }

    public static function findBankAccountById($bankAccountId)
    {
        return BankAccount::find($bankAccountId);
    }

    public static function findBankByName($bankName)
    {
        $bank = Bank::where('name', 'like', '%' . $bankName . '%')
            ->whereNull('deleted_at')
            ->get()->toArray();

        return $bank;
    }

    public static function findBankById($bankId)
    {

        return Bank::find($bankId);
    }

    public static function createReloadRecord($bankAccountId, $currencyId, $amount, $receiptRef, $mediaId)
    {

        $reload = Reload::create([
            'bank_acc_id' => $bankAccountId,
            'currency_id' => $currencyId,
            'amount' => $amount,
            'reference_no' => $receiptRef,
            'media_id' => $mediaId
        ]);

        return $reload;
    }

    public static function createTransferRecord($currencyId, $amount, $purpose, $recipientBankAccountNo, $recipientCountryId, $recipientBankCountryId, $recipientAccName)
    {

        $transfer = Transfer::create([
            'currency_id' => $currencyId,
            'amount' => $amount,
            'purpose' => $purpose,
            'recipient_acc_no' => $recipientBankAccountNo,
            'recipient_country_id' => $recipientCountryId,
            'recipient_bank_country_id' => $recipientBankCountryId,
            'recipient_acc_name' => $recipientAccName

        ]);

        return $transfer;
    }

    public static function createExchangeRecord($data)
    {
        $processingFee = SettingService::extractGeneralSettingValueByKey('processing_fee');

        $exchange = Exchange::create([
            'from_currency_id' => $data['from_currency_id'],
            'to_currency_id' => $data['to_currency_id'],
            'exchange_rate_id' => $data['exchange_rate_id'],
            'processing_fee' => $processingFee,
            'from_account_id' => $data['from_account_id'],
            'to_account_id' => $data['to_account_id'],
            'from_amount' => $data['from_amount'],
            'to_amount' => $data['to_amount'],
        ]);

        return $exchange;
    }

    public static function findUserCurrencyAccountByCurrencyId($user, $currencyId)
    {
        return $user->accounts->where('currency_id', $currencyId)->first();
    }
    public static function findUserCurrencyAccountById($user, $accountId)
    {
        return $user->accounts()->find($accountId);
    }

    public static function createCaseRecord($userId, $refType, $refId, $accountId = null, $toAccountId = null, $status = 1)
    {
        $case = Cases::create([
            'user_id' => $userId,
            'account_id' => $accountId,
            'to_account_id' => $toAccountId,
            'ref_type' => $refType,
            'ref_id' => $refId,
            'status' => $status,
        ]);

        return $case;
    }
    public static function getCountriesWithBankBranchByCountryIdAndBankBranchId($countryId, $bankBranchId)
    {

        $country = Country::with([
            'banks' => function ($query) use ($bankBranchId) {
                $query->where('bank_country.id', $bankBranchId);
            }
        ])
            ->whereHas('banks', function ($q) use ($bankBranchId, $countryId) {
                $q->where([
                    ['bank_country.id', '=', $bankBranchId],
                    ['countries.id', '=', $countryId],
                ]);
            })
            ->find($countryId);

        return $country;
    }

    public static function getCurrencyAccountLatestTransaction($account)
    {
        return $account->transactions()->latest()->first();
    }

    public static function calculateNewTransactionBalanceByDeduct($transaction, $amount)
    {
        return $transaction->new_balance - $amount;
    }

    public static function getUserCurrencyAccountsWithLatestTransaction($userId)
    {

        // \DB::enableQueryLog(); 

        $userAccounts = Account::select([
            'accounts.id',
            'currency_id',
            'currencies.iso_code as currency_iso_code'
        ])
            ->leftJoin('currencies', 'accounts.currency_id', 'currencies.id')
            ->where('user_id', $userId)->with([
                // 'transactions' => function ($query) {
                //     $query
                //         ->select([
                //             'transactions.id',
                //             'transactions.account_id',
                //             'transactions.old_balance',
                //             'transactions.new_balance',
                //         ])
                //         ->latest();
                // },
                'cases' => function ($query) {
                    $query
                        ->select([
                            'cases.id',
                            // 'cases.account_id',
                            'cases.reference_type',
                            'cases.ref_id',
                            'cases.status'
                        ])
                        ->latest();
                },
            ])->get();


        $userAccounts = $userAccounts->toArray();

        $userAccounts = collect($userAccounts)->map(function ($item, $key) {

            $lastTransaction = head($item['transactions']) ? head($item['transactions']) : null;
            $lastTransactionRequest = head($item['cases']) ? head($item['cases']) : null;

            $item['last_transaction'] = $lastTransaction;
            $item['last_case'] = $lastTransactionRequest;

            unset($item['transactions']);
            unset($item['cases']);

            return $item;
        });

        // dd(\DB::getQueryLog()); // Show results of log

        return $userAccounts;
    }

    public static function createAccount($data)
    {
        $account = new Account();
        $account->user_id = $data['user_id'];
        $account->currency_id = $data['currency_id'];
        $account->save();

        return $account;
    }

    public static function createNewCurrency($data)
    {
        $currency = Currency::create([

            'currency_name' => $data['currency_name'],
            'iso_code' => $data['currency_code'],
            // 'locale' => $localeCode,
            'flag' => $data['flag'],
            'minimum_amount' => $data['minimum_amount']
        ]);

        return $currency;
    }

    public static function createCountryCurrencyRecord($countryId, $currencyId)
    {

        $reload = CountryCurrency::create([
            'country_id' => $countryId,
            'currency_id' => $currencyId,
        ]);

        return $reload;
    }

    public static function updateCountryEnableStatus($id)
    {
        $country = Country::find($id);

        if ($country->is_enabled == 1) {
            $country->is_enabled = 0;
        } else if ($country->is_enabled == 0) {
            $country->is_enabled = 1;
        }

        if ($country->isClean()) {
            return false;
        } else {
            $country->save();
            return true;
        }
    }

    public static function updateCountryForTransacts($id)
    {
        $country = Country::find($id);

        if ($country->for_transacts == 1) {
            $country->for_transacts = 0;
        } else if ($country->for_transacts == 0) {
            $country->for_transacts = 1;
        }

        if ($country->isClean()) {
            return false;
        } else {
            $country->save();
            return true;
        }
    }

    public static function createNewBank($bankName)
    {
        $bank = Bank::create([
            'name' => $bankName,
        ]);

        return $bank;
    }

    public static function createBankCountry($bankInfo)
    {
        $bank = BankCountry::create([
            'country_id' => $bankInfo['country_id'],
            // 'branch_name' => $bankInfo['branch_name'],
            'bank_id' => $bankInfo['bank_id'],
        ]);

        return $bank;
    }

    public static function createNewBankAccount($data)
    
    {

        $newBankAcc = BankAccount::create([
            'acc_name' => $data['acc_name'],
            'acc_no' => $data['acc_no'],
            'bank_country_id' => (int) $data['bank_country_id'],
            'country_id' => (int) $data['country_id'],
            'currency_id' => (int) $data['currency_id'],
            'status' => (int) $data['status'],
            'minimum_amount' => $data['minimum_amount'],
            'maximum_amount' => $data['maximum_amount'],
        ]);

        return $newBankAcc;
    }

    public static function createNewBankBranch($data)
    {
        $newBankBranch = BankCountry::create([
            'bank_id' => $data['bank_id'],
            'branch_name' => $data['branch_name'],
            'country_id' =>  $data['country_id']
        ]);

        return $newBankBranch;
    }

    public static function findBankCountryById($bankCountryId)
    {
        return BankCountry::find($bankCountryId);
    }

    public static function calculateExchangeRate($amount, $exchangeRates, $fromCurrencyId, $toCurrencyId)
    {
        $processingFee = SettingService::extractGeneralSettingValueByKey('processing_fee');
        $processingFee = 1 - ($processingFee / 100);

        if (
            $exchangeRates->from_currency_id == $fromCurrencyId &&
            $exchangeRates->to_currency_id == $toCurrencyId
        ) {
            $rate = $exchangeRates->rate;
        } else {
            $rate = 1 / $exchangeRates->rate;
        }

        $rate = $rate * $processingFee;

        $finalAmount = round($amount * $rate, 2);

        return $finalAmount;
    }

    public static function storeImageIntoDB($data)
    {
        $media = Media::create([
            'filename' => $data['filename'],
            'path' =>  $data['path'],
            'reference_id' => $data['reference_id'],
            'reference_table' => $data['reference_table'],
            'extension' =>  $data['extension'],
            'type' => $data['type'],
            'mime' =>  $data['mime'],
        ]);

        return $media;
    }

    public static function getUserSourceBalanceWithCurrency($userId)
    {

        $userAccounts = Account::leftJoin('currencies as c', 'accounts.currency_id', 'c.id')
            ->leftJoin('transactions as t', function ($query) {
                $query->on('t.account_id', '=', 'accounts.id')
                    //To get the latest record from transaction table
                    ->whereRaw('t.id IN (select MAX(transact.id) from transactions as transact join accounts as acc on transact.account_id = acc.id group by acc.id)');
            })
            ->select('accounts.id as account_id', 't.new_balance', 'c.iso_code as currency_iso_code', 'c.id as currency_id', 'c.currency_name')
            ->where('accounts.user_id', $userId)
            ->get();

        $userAccounts = $userAccounts->each(function ($item) {
            $formattedNewBalance = UtilityService::formatNumberToCurrency($item->new_balance, $item->currency_iso_code);
            $formattedNewBalance = preg_replace('/[^0-9,"."]/', '', $formattedNewBalance);

            $item->setAttribute('formatted_new_balance', $formattedNewBalance);
        });

        return $userAccounts;
    }


    public static function getDestinationCountry()
    {

        $destinationCountry = Currency::leftJoin('country_currency as cc', 'currencies.id', 'cc.currency_id')
            ->leftJoin('countries as ct', 'ct.id', 'cc.country_id')
            ->where('for_transacts', 1)
            ->whereNotNull('cc.country_id')
            ->select('currencies.id as currencyId', 'currencies.currency_name', 'currencies.iso_code as currency_iso_code', 'ct.id as country_id', 'ct.name as country_name', 'ct.iso_code_2', 'ct.iso_code_3')
            ->get();

        return $destinationCountry;
    }

    public static function getRecipientBanks()
    {

        $recipientBanks = Bank::leftJoin('bank_country as bc', 'banks.id', 'bc.bank_id')
            ->leftJoin('countries as ct', 'ct.id', 'bc.country_id')
            ->whereNotNull('bc.country_id')
            ->select('banks.name as bank_name', 'bc.country_id', 'bc.id as bank_branch_id', 'bc.branch_name', 'ct.name as country_name', 'ct.iso_code_2', 'ct.iso_code_3')
            ->get();

        return $recipientBanks;
    }

    public static function getSourceAccounts()
    {

        $sourceAccounts = BankAccount::leftJoin('bank_country as bc', 'bank_accounts.bank_country_id', 'bc.id')
            ->whereNotNull('bc.country_id')
            ->select('bank_accounts.id as bank_accounts_id', 'bank_accounts.country_id as bank_country_id', 'bank_accounts.country_id', 'bank_accounts.acc_no', 'bank_accounts.acc_name', 'bc.branch_name')
            ->get();

        return $sourceAccounts;
    }
}
