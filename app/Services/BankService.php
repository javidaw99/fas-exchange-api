<?php

namespace App\Services;

use App\Models\BankAccount;
use App\Models\Bank;
use App\Models\Account;
use App\Models\BankCountry;
use App\Models\Country;
use Illuminate\Support\Facades\DB;
use App\Models\CountryCurrency;


class BankService
{
    public static function findAllBankAvailableCountries()
    {

        // DB::connection()->enableQueryLog();
        $country_ids = BankCountry::distinct()->get(['country_id'])->map(function ($item) {
            return $item->country_id;
        });

        // var_dump(DB::getQueryLog());
        $countries = Country::whereIn('id', $country_ids)->get();

        foreach ($countries as $country) {


            foreach ($country->bankCountries as $bankCountry) {
                $bankCountry->load('bankAccounts');
                $bankCountry->load('bank');
            }
        }

        return $countries;
    }

    public static function findAllBankAvailableCountriesByCurrency($currencyId)
    {

        $countryData = CountryCurrency::with([
            'country' => function ($query) {
                $query->distinct()->select('id', 'name', 'iso_code_2');
            },

        ])
            ->where('currency_id', $currencyId)
            ->get();

        $country_ids = $countryData->map(function ($countryCurrency) {
            return [
                'id' => $countryCurrency->country->id,
                'name' => $countryCurrency->country->name,
                'iso_code_2' => $countryCurrency->country->iso_code_2,
            ];
        });

        return $country_ids;
    }

    public static function findAllBanks()
    {

        return Bank::all();
    }

    public static function findAllBankWithBankCountries()
    {

        return Bank::with('bankCountries')->get();
    }

    public static function findAllBankAccountsAvailableCountries()
    {

        // DB::connection()->enableQueryLog();
        $country_ids = BankAccount::distinct()->get(['country_id'])->map(function ($item) {
            return $item->country_id;
        });
        // var_dump(DB::getQueryLog());

        // Will remove after make sure everything is working well
        // $countries = Country::whereIn('id', $country_ids)->get();

        // foreach ($countries as $country) {


        //     foreach ($country->bankAccounts as $bankAccount) {
        //         $bankAccount->load('bankCountry');
        //     }

        $countries = Country::whereIn('id', $country_ids)
            ->with(['bankAccounts' => function ($query) {
                $query->with(['bankCountry' => function ($query) {
                    $query->join('banks', 'banks.id', '=', 'bank_country.bank_id')
                        ->select('bank_country.id', 'bank_country.country_id', 'bank_country.bank_id', 'banks.name as bank_name');
                }]);
            }])->get();
        return $countries;
    }

    public static function findAllBankAccountsByCurrencyAndCountry($currencyId, $countryId)
    {
        $bankAccounts = BankAccount::distinct()
            ->with(['bankCountry' => function ($query) {
                $query->join('banks', 'banks.id', '=', 'bank_country.bank_id')
                    ->select('bank_country.id', 'bank_country.country_id', 'bank_country.bank_id', 'banks.name as bank_name');
            }])
            ->where([
                ['currency_id', $currencyId],
                ['country_id', $countryId],
                ['status', 1]
            ])
            ->whereNull('deleted_at')
            ->get();

        return $bankAccounts;
    }

    public static function findAllBankAccounts()
    {
        // $records = DB::table('bank_accounts');

        $bank_accounts = DB::table('bank_accounts')
        ->leftJoin('bank_country','bank_accounts.bank_country_id','=','bank_country.id')
        ->leftJoin('banks','bank_country.bank_id','=','banks.id')
        ->leftJoin('countries','bank_accounts.country_id','=','countries.id')
        ->leftJoin('currencies','bank_accounts.currency_id','=','currencies.id');

        return $bank_accounts;
    }

    public static function findBankAccountById($bankAccId)
    {
        $bankAcc = DB::table('bank_accounts')
        ->select(
            'bank_accounts.id', 
            'bank_accounts.status', 
            'countries.name', 
            'currencies.iso_code', 
            'currencies.currency_name', 
            'banks.name',
            'bank_accounts.acc_no',
            'bank_accounts.acc_name',
            'bank_accounts.minimum_amount',
            'bank_accounts.maximum_amount',
            )
        ->leftJoin('bank_country','bank_accounts.bank_country_id','=','bank_country.id')
        ->leftJoin('banks','bank_country.bank_id','=','banks.id')
        ->leftJoin('countries','bank_accounts.country_id','=','countries.id')
        ->leftJoin('currencies','bank_accounts.currency_id','=','currencies.id')
        ->where('bank_accounts.id', $bankAccId)
        ->first();

        return $bankAcc;
    }

    public static function updateBankAccountById($bankAccId, $accNo, $accName, $bankId, $status, $minimumAmount, $maximumAmount)
    {
        try {
            $bank = AccountService::findBankAccountById($bankAccId);

            $bank->acc_no = $accNo;

            $bank->acc_name = $accName;

            $bank->status = $status;

            $bank->minimum_amount = $minimumAmount;

            $bank->maximum_amount = $maximumAmount;

            $bankCountryId = BankService::findBankCountryByBankId($bankId)->select('id')->first()->id;

            // $bankCountryId2 = BankService::findBankCountryByBankId($bankId)->select('id')->first()->id;

            // return response()->json(
            //     $bankCountryId
            // );

            $bank->bank_country_id = $bankCountryId;

            if ($bank->isClean()) {
                return response()->json([
                    'message' => 'Nothing updated'
                ], 202);
            } else {
                // return response()->json([
                //     'message' => 'Not Clean'
                // ], 202);

                $bank->acc_no = $accNo;
                $bank->acc_name = $accName;
                $bank->bank_country_id = $bankCountryId;
                $bank->status = $status;
                $bank->minimum_amount = $minimumAmount;
                $bank->maximum_amount = $maximumAmount;
                $bank->save();
                return response()->json(['message' => 'Successfully update bank account number'], 200);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to update this record'], 400);
        }
    }

    public static function findBankCountryByBankId($bankId)
    {
        $bankCountry = DB::table('bank_country')
        ->where('bank_id', $bankId);
        // ->first();

        return $bankCountry;
    }

    public static function findBankCountryByCountryId($countryId)
    {
        $bankCountry = DB::table('bank_country')
        ->leftJoin('banks','bank_country.bank_id','=','banks.id')
        ->where('country_id', $countryId)
        ->where('bank_country.deleted_at', null)
        ->get();
        // ->first();

        return $bankCountry;
    }

    public static function createBankAccount($data)
    {
        try {
            $existedBankCountry = BankCountry::where('country_id', $data['country_id'])
                ->where('bank_id', $data['bank_id'])
                ->whereNull('deleted_at')
                ->first();

            $bankCountry =  $existedBankCountry;


            if (!$existedBankCountry) {

                $bankCountryPayload = [
                    'country_id' => $data['country_id'],
                    'bank_id' => $data['bank_id']
                ];

                $bankCountry = AccountService::createBankCountry($bankCountryPayload);
            }

            $bankAccountExisted = BankAccount::where('acc_no', $data['acc_no'])
                ->where('bank_country_id',  $bankCountry->id)
                ->whereNull('deleted_at')
                ->count();

            if (!$bankAccountExisted) {

                $data['bank_country_id'] = $bankCountry->id;

                $newBankAcc = AccountService::createNewBankAccount($data);

                if ($newBankAcc) {
                    return response()->json([
                        'message' => 'Successfully create bank account'
                    ]);
                } else {

                    return response()->json([
                        'message' => 'Failed to create bank account'
                    ], 400);
                }
            } else {

                return response()->json([
                    'message' => 'Bank existed'
                ], 400);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => 'Not able to add this record'], 400);
        }
    }

    public static function findAllBankAccountsByHighestScore($currencyId)
    {
        $bankAccounts = DB::select('SELECT * FROM bank_account_frequency_amount_view where currency_id = ? order by score limit 3', [$currencyId]);

        return $bankAccounts;
    }


    public static function findAllBankAccountsByLowestScore($currencyId)
    {
        $bankAccounts = DB::select('SELECT * FROM bank_account_frequency_amount_view where currency_id = ? order by score limit 3', [$currencyId]);

        return $bankAccounts;
    }

    public static function findAllBankAccountsByLowestScoreApp($currencyId, $countryId)
    {
        $bankAccounts = BankAccount::distinct()
            ->with(['bankCountry' => function ($query) {
                $query->join('banks', 'banks.id', '=', 'bank_country.bank_id')
                    ->select('bank_country.id', 'bank_country.country_id', 'bank_country.bank_id', 'banks.name as bank_name');
            }])
            ->where([
                ['currency_id', $currencyId],
                ['country_id', $countryId],
                ['status', 1]
            ])
            ->whereNull('deleted_at')
            ->get();

        return $bankAccounts;
    }

}
