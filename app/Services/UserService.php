<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Http;

use App\Models\User;
use App\Models\Role;
use App\Models\TempUser;
use App\Models\UserAccount;
use App\Models\MaritalStatus;
use App\Models\Country;
use App\Models\SecureCodeLog;
use App\Models\UserBankAccount;

use Carbon\Carbon;
use ParagonIE\Halite\Symmetric;
use ParagonIE\Halite\Symmetric\EncryptionKey;
use ParagonIE\HiddenString\HiddenString;
use phpseclib3\Crypt\AES;

class UserService
{
    public static function checkUniqueUsername($username)
    {
        $userCount = User::where('username', $username)
            ->whereNull('deleted_at')
            ->count();

        return $userCount ? FALSE : TRUE;
    }

    public static function checkUniqueEmail($email)
    {
        $userCount = User::where('email', $email)
            ->whereNull('deleted_at')
            ->count();

        return $userCount ? FALSE : TRUE;
    }


    public static function checkUniqueTopkashId($topkashId)
    {
        $userCount = User::where('topkash_id', $topkashId)
            ->whereNull('deleted_at')
            ->count();

        return $userCount ? FALSE : TRUE;
    }


    public static function generateTemporaryPassword()
    {
        $digits = 6;
        $tempPassword = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        $hashedTempPassword = Hash::make($tempPassword);

        return [
            'temp_password' => $tempPassword,
            'hashed_temp_password' => $hashedTempPassword
        ];
    }
    // public static function findOneUserById($user_id)
    // {
    //     $user = User::with([
    //         'maritalStatus',
    //         'address' => function ($query) {
    //             $query->join('countries', 'countries.id', '=', 'addresses.country_id')
    //                 ->join('states', 'states.id', '=', 'addresses.state_id')
    //                 ->select('addresses.*', 'countries.name as country', 'countries.calling_code as calling_code', 'countries.iso_code_2 as iso_code_2', 'states.name as state');
    //         },
    //         'role'
    //     ])
    //         ->select(
    //             'id',
    //             'topkash_id',
    //             'fullname',
    //             'username',
    //             'email',
    //             'app_language',
    //             'marital_status_id',
    //             'phone_no',
    //             'password',
    //             'dob',
    //             'id_type',
    //             'id_number',
    //             'address_id',
    //             'role_id',
    //             'nationality_id',
    //             'app_pin',
    //             'kyc_verify_status',
    //             'gender',
    //             'fcm_token',
    //             'is_force_login',
    //             'biometric_public_key',
    //             'app_pin',
    //             'status',
    //             'created_at'
    //         )
    //         ->where('id', $user_id)
    //         ->first();
    //     return $user;
    // }
    public static function findOneUserById($user_id)
    {
        $user = User::where('id', $user_id)
        ->with([
            'address' => function ($query) {
                $query->select('id','reference_id','reference_table','address_1', 'address_2','city', 'postcode','state_id','country_id');
            },
            'maritalStatus',
            'role'
           
        ])
        ->first();
   

        return $user;
    }
    public static function findAllMaritalStatus()
    {
        return MaritalStatus::all();
    }
    public static function findAllCountryWithState()
    {
        $country = Country::with([
            'state',
        ])
            ->select('id', 'name', 'iso_code_2', 'iso_code_3', 'calling_code')
            ->get();

        return $country;
    }

    public static function getUserByPhoneNo($phone_no)
    {
        $user = User::where('phone_no', $phone_no)
            ->whereNull('deleted_at')
            ->first();

        return $user;
    }

    public static function findUserById($user_id)
    {

        $user = User::where('id', $user_id)->first();


        return $user;
    }
    public static function getUserByUsername($username)
    {
        $user = User::where('username', $username)
            ->whereNull('deleted_at')
            ->first();

        return $user;
    }

    public static function findOneUserByPhoneNo($phone_no)
    {
        $user = User::where('phone_no', $phone_no)
            ->whereNull('deleted_at')
            ->first();

        return $user;
    }
    public static function findOneSecureCodeLog($secureLogInfo)
    {
        $user = SecureCodeLog::where('user_id', $secureLogInfo['user_id'])
            ->where('type', $secureLogInfo['type'])
            ->whereNull('deleted_at')
            ->first();

        return $user;
    }
    public static function createSecureCodeLog($secureLogInfo)
    {

        $user_id = SecureCodeLog::insertGetId([
            'user_id' => $secureLogInfo['user_id'],
            'tac_code' => $secureLogInfo['tac_code'],
            'type' => $secureLogInfo['type'],

        ]);

        return $user_id;
    }
    public static function deleteSecureCodeLog($secureLogInfo)
    {

        SecureCodeLog::where('user_id', $secureLogInfo['user_id'])
            ->where('type', $secureLogInfo['type'])
            ->whereNull('deleted_at')
            ->delete();

        return;
    }
    public static function getUserByEmail($email)
    {
        $user = User::where('email', $email)
            ->whereNull('deleted_at')
            ->first();

        return $user;
    }

    public static function createNewUser($user_info, $role)
    {
       
        //need to add topkash id as well

        $role = Role::where('code', $role)->first();

        $user = User::create([
            'fullname' => $user_info['fullname'],
            'username' => $user_info['username'],
            'email' => $user_info['email'],
            'phone_no' => $user_info['phone_no'],
            // 'password' => $user_info['password'],
            // 'nationality_id' => $user_info['nationality_id'],
            // 'is_temp_password' => $user_info['is_temp_password'],
            'role_id' => $role->id,
            'status' => $user_info['status'],
        ]);

        

        return $user->id;
    }

    public static function createNewAppUser($user_info, $role)
    {
        $role = Role::where('name', $role)->first();

        $user = User::create([
            'topkash_id' => $user_info['topkash_id'],
            'username' => $user_info['username'],
            'password' => $user_info['password'],
            'phone_no' => $user_info['phone_no'],
            'email' => $user_info['email'],
            'secret_key' => $user_info['secret_key'],
            'role_id' => $role->id
        ]);

        return $user->id;
    }

    public static function createNewTempAppUser($user_info, $role)
    {
        $role = Role::where('name', $role)->first();

        $user_id = TempUser::insertGetId([
            'username' => $user_info['username'],
            'password' => $user_info['password'],
            'phone_no' => $user_info['phone_no'],
            'tac_code' => $user_info['tac_code'],
            'email' => $user_info['email'],
            'role_id' => $role->id
        ]);

        return $user_id;
    }

    public static function deleteNewTempAppUser($user_info)
    {
        $email = $user_info['email'];
        $phoneNo = $user_info['phone_no'];
        $username = $user_info['username'];

        TempUser::where('email', $email)
            ->orWhere('phone_no', $phoneNo)
            ->orWhere('username', $username)
            ->first()->delete();

        return;
    }

    public static function getAppTempUserByEmailPhoneNoUsername($user_info)
    {
        $email = $user_info['email'];
        $phoneNo = $user_info['phone_no'];
        $username = $user_info['username'];

        $tempUser = TempUser::where('email', $email)
            ->orWhere('phone_no', $phoneNo)
            ->orWhere('username', $username)
            ->first();

        return $tempUser;
    }

    public static function updateAppTempUserTacCode($user_info)
    {

        $email = $user_info['email'];
        $password = $user_info['password'];
        $phoneNo = $user_info['phone_no'];
        $username = $user_info['username'];
        $tacCode = $user_info['tac_code'];

        TempUser::where('email', $email)
            ->orWhere('phone_no', $phoneNo)
            ->orWhere('username', $username)
            ->update([
                'tac_code' => $tacCode,
                'password' => $password,
                'email' => $email,
                'phone_no' => $phoneNo,
                'username' => $username,
            ]);
    }

    public static function checkTempUserTacCodeUniqueness($tacCode)
    {
        $tempUser = TempUser::where([
            'tac_code' => $tacCode
        ])->count();

        if ($tempUser) {
            return false;
        }

        return true;
    }

    public static function generateUserToken($user_id, $user_role, $platform = 'server')
    {
        $key = env('JWT_KEY');

        if ($platform == 'socket') {
            $key = env('JWT_KEY_SOCKET');
        }

        // Expired in 8 hrs
        $expired_time = Carbon::now()->timestamp + 1209600;

        $payload = [
            'iss' => env('APP_URL', 'http://localhost:8080'),
            'aud' => env('APP_URL', 'http://localhost:8080'),
            'sub' => $user_id,
            'role' => $user_role,
            "exp" => $expired_time,
        ];

        $jwt = JWT::encode($payload, $key, 'HS256');

        return $jwt;
    }

    public static function updatePasswordByUserId($user_info)
    {

        User::where('id',  $user_info['user_id'])
            ->update(['password' =>  $user_info['new_password']]);
    }

    public static function updatePasswordByPhoneNo($phoneNo, $newPassword)
    {
        User::where('phone_no',  $phoneNo)
            ->update(['password' => $newPassword]);
    }


    public static function changePassword($user_info)
    {

        User::where('id',  $user_info['user_id'])
            ->update(['password' =>  $user_info['new_password']]);
    }
    public static function updatePinByUserId($user_details)
    {
        User::where('id', $user_details['user_id'])
            ->update(['app_pin' => $user_details['new_pin']]);
    }

    public static function changeAppPin($user_details)
    {
        $test = User::where('id', $user_details['user_id'])
            ->update(['app_pin' => $user_details['new_pin']]);
    }


    public static function updateUserById($user_info, $user_id)
    {
        if ($user_id) {
            $user = User::find($user_id);
        } else {
            $user = auth()->user();
        }

        foreach ($user_info as $key => $value) {
            $user->$key = $user_info[$key];
        }

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    // $isEditByAdmin & $userId are not used for App site
    public static function updateUser($user_info, $address_id = null, $userId = null)
    {

        if ($userId) {

            $user = User::find($userId);
            $user->fullname = $user_info['fullname'];
            $user->country_id = $user_info['country_id'];
            $user->phone_no = $user_info['phone_no'];
        } else {

            $user = auth()->user();

            if ($user_info) {

                $user->fullname = $user_info['fullname'];
                $user->phone_no = $user_info['phone_no'];
                $user->dob = $user_info['dob'];
                $user->gender = $user_info['gender'];
                $user->marital_status_id = $user_info['marital_status_id'];
                $user->nationality_id = $user_info['nationality_id'];
                $user->id_type = $user_info['id_type'];
                $user->id_number = $user_info['id_number'];
                $user->address_id = $address_id;
            } else {

                $user->address_id = $address_id;
            }
        }

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    public static function updateDashboardUser($user_info, $userId)
    {


        $user = User::find($userId);
        $user->role_id = $user_info['role_id'];
        $user->fullname = $user_info['fullname'];
        $user->phone_no = $user_info['phone_no'];
        $user->email = $user_info['email'];

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    public static function updateUserDetails($user_info, $userId)
    {


        $user = User::find($userId);
        $user->fullname = $user_info['fullname'];
        $user->username = $user_info['username'];

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    public static function retrieveUserInformation($userId)
    {

        $userDetails = User::leftJoin('addresses', 'users.address_id', 'addresses.id')
            ->leftJoin('marital_statuses', 'users.marital_status_id', '=', 'marital_statuses.id')
            ->where('users.id', $userId)

            ->with([
                'address' => function ($query) {
                    $query
                        ->select([
                            'addresses.id',
                            'addresses.address_1',
                            'addresses.address_2',
                            'addresses.city',
                            'addresses.postcode',
                            'addresses.state_id',
                            'states.name as state_name',
                            'addresses.country_id',
                            'countries.name as country_name',
                        ])
                        ->leftJoin('states', 'addresses.state_id', 'states.id')
                        ->leftJoin('countries', 'addresses.country_id', 'countries.id')
                        ->get();
                },
                'maritalStatus' => function ($query) {
                    $query
                        ->select([
                            'marital_statuses.id',
                            'marital_statuses.name',
                        ])
                        ->get();
                },

            ])
            ->get();
        return $userDetails;
    }

    public static function storeTfaSecret($secret, $userId)
    {
        $user = User::find($userId);

        $user->tfa_secret = $secret;

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    public static function getTfaSecret($userId)
    {

        $secret = User::where('id', $userId)
            ->select('tfa_secret')
            ->where('id', $userId)
            ->first();

        return $secret->tfa_secret;
    }

    public static function enableTfa($userId)
    {
        $user = User::where('id', $userId)
            ->select('is_tfa_enabled')
            ->first();

        $secretExist = User::select('tfa_secret')
            ->where('id', $userId)
            ->whereNotNull('tfa_secret')
            ->count();

        if ($user->is_tfa_enabled == 1) {

            $status = 0;
        } else {

            $status = 1;
        }

        $enableTfa = User::where('id',  $userId)
            ->update([
                'is_tfa_enabled' => $status
            ]);

        return [
            'is_tfa_enabled' => $status,
            'is_tfa_secret_enabled' => $secretExist ? TRUE : FALSE
        ];
    }

    public static function checkTfaStatusById($userId)
    {

        $status = User::where('id', $userId)
            ->select('is_tfa_enabled')
            ->first();

        return $status->is_tfa_enabled;
    }

    public static function checkIsSecretExisted($userId)
    {

        $secretExist = User::select('tfa_secret')
            ->where('id', $userId)
            ->whereNotNull('tfa_secret')
            ->count();

        return $secretExist ? TRUE : FALSE;
    }

    public static function resetTfa($userId)
    {

        $reset = User::where('id',  $userId)
            ->update([
                'is_tfa_enabled' => 0,
                'tfa_secret' => null,
            ]);

        return $reset;
    }

    public static function findRoleById($roleId)
    {
        return Role::find($roleId);
    }

    public static function forceLogin()
    {
        User::where('role_id', '!=',  2)
            ->update([
                'is_force_login' => 1,
            ]);
    }

    public static function resetForceLoginById($userId)
    {

        $force_login = User::where('id',  $userId)
            ->update([
                'is_force_login' => 0,
            ]);
    }

    public static function updateResetTimestampAndCreateOtp($currentTime, $phoneNo)
    {
        $user = UserService::findOneUserByPhoneNo($phoneNo);

        if ($user) {
            $user->reset_pwd_timestamp = $currentTime;
            $user->save();

            $otpCode = UtilityService::generateOtpCode();

            $data = [];
            $data['phone_no'] = $phoneNo;
            $data['otp_code'] = $otpCode;

            Http::get(env('ONEWAYSMS_MT_URL'), [
                'apiusername' => env('ONEWAYSMS_API_USERNAME'),
                'apipassword' => env('ONEWAYSMS_API_PASSWORD'),
                'senderid' => "INFO",
                'languagetype' => 1,
                'mobileno' =>  $phoneNo,
                'message' => 'TopKash: Your one-time code for registration is ' . $otpCode . '.'
            ]);

            $createOtpRecord = UtilityService::createOtpRecord($data);

            if ($createOtpRecord) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function updateBiometricPublicKey($userId, $publicKey)
    {

        $addBiometricPublicKey = User::where('id',  $userId)
            ->update([
                'biometric_public_key' => $publicKey,
            ]);


        if ($addBiometricPublicKey) {
            return true;
        } else {
            return false;
        };
    }

    public static function updateAppPinCode($appPin, $user)
    {


        $user->app_pin = $appPin;

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }

    public static function addWallet($data)
    {

        $wallet = UserAccount::create($data);

        return $wallet;
    }

    public static function checkWalletExist($userId, $currencyId)
    {

        $wallet = UserAccount::where([['user_id', $userId], ['currency_id', $currencyId]])->first();

        return $wallet;
    }

    public static function generateTimeStampKey($userSecretKey)
    {
        $userSecretKey = str_replace('-', '', $userSecretKey);
        $secretKey = new HiddenString($userSecretKey);
        $secretKey = new EncryptionKey($secretKey);

        $currentTimeStamp = Carbon::now('UTC');
        $timeStamp = json_encode(array(
            'current_time' => $currentTimeStamp,
        ));

        $timeStampKey =  Symmetric\Crypto::encrypt(
            new HiddenString(
                $timeStamp
            ),
            $secretKey
        );

        return $timeStampKey;
    }

    public static function findAllBankAccounts($userId, $type, $currencyId = null)
    {

        $userBankAccounts = User::where('id', $userId)->first();
        $userBankAccounts = UserBankAccount::where('user_id', $userId)
            ->where('type', $type)
            ->whereNull('deleted_at')
            ->with([
                'bankUser:id,username,fullname',
                'bankInfo:id,name',
                'country:id,name',
                'currency:id,currency_name,iso_code',
                'topkashUser' => function($query) {
                    $query->select('id','username','topkash_id')
                    ->with(['userAccounts']);
                },
            ]);

        if ($currencyId) {
            $userBankAccounts->whereHas('currency', function ($query) use ($currencyId) {
                $query->where('id', $currencyId);
            });
        }

        $userBankAccounts = $userBankAccounts->get();

        return $userBankAccounts;
    }


    public static function createUserBankAccount($values, $userId)
    {

        $userBankAccount = UserBankAccount::create([
            'user_id' => $userId,
            'reference_name' => $values['reference_name'],
            'reference_bank_account' => $values['reference_bank_account'],
            'currency_id' => $values['currency_id'],
            'country_id' => $values['country_id'],
            'bank_id' => $values['bank_id'],
            'type' => $values['type'],
            'maximum_amount' => $values['maximum_amount'],

        ]);


        return  $userBankAccount;
    }

    public static function findBankAccountByBankAccountId($id)
    {
        $userBankAccount = UserBankAccount::where('id', $id)
            ->with([
                'bankInfo:id,name',
                'country:id,name',
                'topkashUser:id,username,topkash_id'
            ])->first();


        return $userBankAccount;
    }
    public static function findUserByTopkashIdAndUsername($username, $topkashId)
    {
        $user = User::where('username', $username)
            ->where('topkash_id', $topkashId)
            ->whereNull('deleted_at')
            ->first();

        return $user;
    }

    public static function findOneByTopkashUserId($topkashUserId)
    {
        $user = User::where([
            'id' => $topkashUserId
        ])->first();

        return $user;
    }

    public static function createTopkashUserBankAccount($userId, $topkashUserId)
    {
        $isExisted = UserBankAccount::where([
            'topkash_user_id' => $topkashUserId,
            'user_id' => $userId,
        ])->count();



        if (!$isExisted) {
            $newUserBankAccount = UserBankAccount::create([
                'topkash_user_id' => $topkashUserId,
                'user_id' => $userId,
                'type' => "TOPKASH"
            ]);

            return $newUserBankAccount;
        }

        return null;
    }

    public static function saveFcmDetails($fcmToken)
    {
        $user = auth()->user();

        $user->fcm_token = $fcmToken;

        UserService::updateFcmTokenStatus($user->id, 'valid');

        if ($user->isClean()) {
            return false;
        } else {
            $user->save();
            return true;
        }
    }


    public static function updateFcmTokenStatus($userId, $status)
    {

        $user = UserService::findUserById($userId);

        $user->fcm_token_status = $status;

        $user->save();
    }

    public static function updateFcmTokenStatusByFcmToken($userFcmToken, $fcmTokenStatus)
    {

        $user = User::where('fcm_token', $userFcmToken)
            ->whereNull('deleted_at')
            ->first();

        $user->fcm_token_status = $fcmTokenStatus;

        $user->save();
    }

    public static function findUserBankAccountById($userBankAccountId)
    {
        $userBankAccount = UserBankAccount::where('id', $userBankAccountId)->first();

        return $userBankAccount;
    }

    public static function updateAppLanguage($userId, $lang)
    {

        $user = UserService::findUserById($userId);

        $user->app_language = $lang;

        if (!$user->isClean()) {
            $user->save();
        }
    }

    public static function generateEncryptedQrCode($userId)
    {

        $user = self::findUserById($userId);

        $userSecretKey = str_replace("-", "", $user->secret_key);

        $userSignature = $userId . "|" . $user->topkash_id . "|" . $user->username;

        $crypto = new AES(env('QRCODE_ENCRYPT_MODE'));
        $crypto->setIV(env('QRCODE_IV'));
        $crypto->setKey($userSecretKey);

        $cipherText = bin2hex($crypto->encrypt($userSignature));

        $qrcodeString = $user->topkash_id . "|" . $cipherText;

        return $qrcodeString;
    }

    public static function verifyEncryptedQrCode($encryptedQrCode)
    {
        $isValid = false;
        $qrcodeArray = explode("|", $encryptedQrCode);

        $user = User::where('topkash_id', $qrcodeArray[0])->first();

        if (!empty($user)) {
            $userSecretKey = str_replace("-", "", $user->secret_key);

            $crypto = new AES(env('QRCODE_ENCRYPT_MODE'));
            $crypto->setIV(env('QRCODE_IV'));
            $crypto->setKey($userSecretKey);

            $decryptString = $crypto->decrypt(hex2bin(($qrcodeArray[1])));

            $values = explode('|', $decryptString);

            $data = [
                'id' => $values[0],
                'topkash_id' => $values[1],
                'username' => $values[2],
            ];

            $isValid = true;
        }

        return [
            'is_valid' => $isValid,
            'user' => $data
        ];
    }


    // User detail encryption for onboarding email

    public static function generateEncryptedUserDetail($user)
    {

        $userSignature = $user->id . "|" . $user->username. "|" . $user->email.  "|" .$user->fullname . "|" .   $currentTimeStamp = Carbon::now()->getTimestamp();

        $crypto = new AES(env('AUTH_ENCRYPT_MODE'));
        // $crypto->disablePadding();
        $crypto->setIV(env('AUTH_IV'));
        $crypto->setKey(env('AUTH_SECRET_KEY'));

        $cipherText = bin2hex($crypto->encrypt($userSignature));


        // dd($cipherText);

        return $cipherText;
    }


    public static function verifyEncryptedUserDetail($encryptedUserDetail)
    {
        $isValid = false;

            $crypto = new AES(env('AUTH_ENCRYPT_MODE'));
            $crypto->setIV(env('AUTH_IV'));
            $crypto->setKey(env('AUTH_SECRET_KEY'));

            $decryptString = $crypto->decrypt(hex2bin($encryptedUserDetail));

            $values = explode('|', $decryptString);


            $data = [
                'id' => $values[0],
                'username' => $values[1],
                'email' => $values[2],
                'fullname' => $values[3],
                'timestamp' => $values[4],
            ];

            $user = User::where([
                ['id', $data['id']],
                ['username', $data['username']],
                ['email', $data['email']],
                ['fullname', $data['fullname']],
                ['is_resetting_pwd', 1],

            ])->first();

            if(!empty($user)) {
                $isValid = true;
            }

        return [
            'is_valid' => $isValid,
            'timestamp' => $data['timestamp'] ?? null,
            'username' => $data['username'] ?? null,
            'is_resetting_pwd' => 1
        ];
    }

    public static function verifyEncryptedNewUserDetail($encryptedUserDetail)
    {
        $isValid = false;

            $crypto = new AES(env('AUTH_ENCRYPT_MODE'));
            $crypto->setIV(env('AUTH_IV'));
            $crypto->setKey(env('AUTH_SECRET_KEY'));

            $decryptString = $crypto->decrypt(hex2bin($encryptedUserDetail));

            $values = explode('|', $decryptString);

            $data = [
                'id' => $values[0],
                'username' => $values[1],
                'email' => $values[2],
                'fullname' => $values[3],
                'timestamp' => $values[4],
            ];

            $user = User::where([
                ['id', $data['id']],
                ['username', $data['username']],
                ['email', $data['email']],
                ['fullname', $data['fullname']],
                ['password', null]

            ])->first();

            if(!empty($user)) {
                $isValid = true;
            }

        return [
            'is_valid' => $isValid,
            'timestamp' => $data['timestamp'] ?? null,
            'username' => $data['username'] ?? null
        ];
    }



    public static function updateIsResetPasswordById($user_id)
    {
        User::where([
            [ 'id', $user_id],
            ['is_resetting_pwd', 0],
        ])
        
        ->update(['is_resetting_pwd' =>  1]);

    }
    public static function findAllUsersByMoneyChangerId($moneyChangerId)
    {
        // print_r("moneyChangerId");

        // print_r($moneyChangerId);
        $users = User::select(['id','username','fullname','money_changer_id'])
            ->where('money_changer_id', $moneyChangerId)
            ->get();
         

            
        return $users;
    }

    public static function findAllUsersByRoleIdEnabled($roleIds)
    {
        $users = User::join('roles', 'users.role_id', 'roles.id')
            ->select('users.role_id','users.id','users.username','users.fullname','users.money_changer_id', 'roles.name as role')
            ->whereIn('role_id', $roleIds);

        return $users;
    }

    public static function findAllUsersByRoleId($roleIds)
    {
        $users = User::with([
            'country' => function ($query) {
                $query->select('id', 'name', 'iso_code_2');
            }
        ])
            ->join('roles', 'users.role_id', 'roles.id')
            ->select('users.*', 'roles.name as role')
            ->whereIn('role_id', $roleIds);

        return $users;
    }

    public static function findUplineUser()
    {

        $user = auth()->user();

        $userData = User::where([
            [ 'id', $user->id],
        ]);

       
        $uplineData = User::where([
            [ 'id', $user->upline_id],
        ])
        ->select('id','username','fullname')
        ->first();
     
        return $uplineData;
    }

    public static function findDownlineUser()
    {

        $user = auth()->user();

        $downLineData = User::where([
            [ 'upline_id', $user->id],
        ])
        ->select('id','username','fullname')
        ->get();

       
  
     
        return $downLineData;
    }
}
