<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Services\MailService;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('test', function () {
    MailService::sendWelcomeTempPasswordEmail([
        "to" => 'richard@barcove.my',
        'url' => env('SIGN_IN_URL_LOCAL'),
        'fullname' => 'Richard',
        'password' => '123445'
    ]);
    return 'test';
});

//dashboard
$router->group([
    'prefix' => 'api',
], function () use ($router) {
    /**
     * Routes that need authentication checking
     */
    $router->group([
        'middleware' => ['jwt', 'dashboard_role', 'block_user', 'force_login']
    ], function () use ($router) {

        $router->post('login-status', function () {
            return response('OK');
        });

        $router->group([
            'prefix' => 'metrics',
        ], function () use ($router) {
            $router->get('initialize', 'MetricController@metricsInitialise');
            $router->get('reload', 'MetricController@findReloadTransactionByDate');
            $router->get('transfer', 'MetricController@findTransferTransactionByDate');


            // $router->get('transaction-trends', 'MetricController@getTransactionTrends');
        });

        $router->group([
            'prefix' => 'reports',
        ], function () use ($router) {
            $router->get('conversion', 'ReportController@generateConversionReport');
            $router->get('send', 'ReportController@generateSendsReport');
            $router->get('send/members', 'ReportController@generateMemberReport');
            $router->get('topup/bank-deposit', 'ReportController@generateTopUpBankDepositReport');
            $router->get('topup/gift-card', 'ReportController@generateTopUpGiftCardReport');
        });

        // // /api/users
        $router->group([
            'prefix' => 'users',
        ], function () use ($router) {

            $router->post('verify-pin', 'UserController@verifyPinNumber');
            $router->post('add-wallet', 'UserController@addWallet');

            $router->get('/', 'UserController@getUsers');
            $router->get('/dashboard-user', 'UserController@getDashboardUsers');
            $router->patch('/user-details', 'UserController@updateUserDetails');
            // $router->patch('/user-address', 'UserController@updateAddressById');
            $router->patch('/address', 'AddressController@updateAddressById');

            $router->post('/address', 'AddressController@createAddress');

            // $router->post('/add', 'UserController@addUser');
            $router->post('/password', 'UserController@changePassword');
            //     // $router->get('/profile', 'UserController@profileAccount');
            $router->patch('/profile', 'UserController@updateProfile');
            //     $router->patch('/update-app-user', 'UserController@updateAppUser');
            $router->patch('/update-dashboard-user', 'UserController@updateDashboardUser');
            //     $router->get('/bank-accounts', 'UserController@bankAccounts');
            $router->post('/save-fcm', 'UserController@saveFcmDetails');
            $router->get('/generate-tfa-qr-code', 'UserController@generateTfaQrCode');
            $router->post('/tfa-verification', 'UserController@tfaVerification');
            $router->get('/tfa-status-by-id', 'UserController@checkTfaStatusById');
            $router->patch('/handle-tfa', 'UserController@handleTfa');
            $router->patch('/reset-tfa', 'UserController@resetTfa');
            $router->get('/roles', 'UserController@getRoles');
            $router->patch('/force-login', 'UserController@forceDashboardUserLoginAgain');
            // $router->get('/history', 'UserController@getHistoryByUserId');
            //     $router->get('/transaction/list', 'UserController@getTransactionsByUserId');
            // $router->get('/transaction/list', 'UserController@findUserTransactionsByUserId');
            $router->get('/transaction/history', 'UserController@findAllUserTransactionsByUserId');
            $router->get('/initialize', 'UserController@findCountryStatesMaritalStatus');

            //----- NEW -----
            $router->get('/customer/enabled', 'UserController@findAllCustomersEnabled');

            $router->get('/profile', 'UserController@findUserProfile');
            $router->get('/{id}', 'UserController@findUserById');
            $router->get('/dashboard/all', 'UserController@findAllDashboardUsers');
            $router->get('/case/assignee', 'UserController@findAllCaseAssignee');
            $router->get('/role/{role_id}', 'UserController@findAllUsersByRoleId');
            $router->get('customer/all', 'UserController@findAllCustomers');
            $router->get('/customer/{id}', 'UserController@findCustomerById');
            $router->patch('customer/{id}', 'UserController@updateCustomerById');
            $router->patch('customer-status/{id}', 'UserController@updateCustomerStatusById');
            $router->patch('/dashboard-user/{id}', 'UserController@updateDashboardUserById');
            $router->post('/add', 'UserController@createUser');
            $router->post('/resend-email/{id}', 'UserController@resendEmailForPasswordLink');
            $router->get('/customer/wallet/{id}', 'UserController@findCustomerWalletsById');

            
        });

        // /api/base-fixed-rates
        $router->group([
            'prefix' => 'base-fixed-rates'
        ], function () use ($router) {
            $router->get('/', 'ProcessingFeeBaseFixedRateController@index');
            $router->get('/list', 'ProcessingFeeBaseFixedRateController@list');
            // $router->get('/', 'ProcessingFeeBaseFixedRateController@findAllProcessingFeeBaseFixedRates');
            $router->post('/add', 'ProcessingFeeBaseFixedRateController@createProcessingFeeBaseFixedRate');
        });

        // /api/tiered-percentage-rates
        $router->group([
            'prefix' => 'tiered-percentage-rates'
        ], function () use ($router) {
            $router->get('/', 'ProcessingFeeTieredPercentageRateController@findAllProcessingFeeTieredPercentageRates');
            $router->post('/', 'ProcessingFeeTieredPercentageRateController@createProcessingFeeTieredPercentageRate');
        });

        $router->group([
            'prefix' => 'quotes'
        ], function () use ($router) {
            $router->post('/', 'QuoteController@calculateRateByCurrencyIds');
            $router->post('/verify', 'QuoteController@verifyQuoteToken');
        });

        $router->group([
            'prefix' => 'transfers'
        ], function () use ($router) {
            $router->post('/', 'TransferController@transfer');
            $router->get('/conversions', 'TransferController@findAllConversions');
            $router->get('/{id}', 'TransferController@findTransferById');
            $router->get('/all/convert', 'TransferController@findAllConverts');
            $router->get('/all/{type}', 'TransferController@findAllTransfersByType');
        });

        $router->group([
            'prefix' => 'transactions'
        ], function () use ($router) {
            $router->post('/', 'TransactionController@findUserTransactionsById');
            $router->get('/old-all', 'TransactionController@findAllUserTransactions');
            $router->get('/all', 'TransactionController@findAllTransactions');
            $router->get('/redeem/all', 'TransactionController@findAllRedeemTransactions');
            $router->get('/redeem/{id}', 'TransactionController@findRedeemById');
            $router->get('/{id}', 'TransactionController@findUserTransactionsByReferenceId');
            $router->get('/latest/{id}', 'TransactionController@findLastestUserTransactionsByUserId');
        });

        // /api/cases
        $router->group([
            'prefix' => 'cases',
        ], function () use ($router) {
            // $router->get('/recents', 'CaseController@getRecentRecords');
            $router->post('/assign', 'CaseController@assignCase');
            $router->get('/transfer', 'CaseController@getTransferRecords'); //To be remove
            $router->get('/transfer/all', 'CaseController@findAllTransfers');
            // $router->get('/transfer/all-convert', 'CaseController@findAllConverts');
            $router->get('/transfer/pending', 'CaseController@findAllPendingTransfers');
            $router->get('/transfer/{id}', 'CaseController@findTransferCaseById');
            // $router->delete('/transfer/{id}', 'CaseController@deleteTransferRecordById');
            $router->put('/transfer/{id}', 'CaseController@updateTransferById');
            $router->get('/reload', 'CaseController@getReloadRecords'); //To be remove
            $router->get('/reload/all', 'CaseController@findAllReloads');
            $router->get('/reload/pending', 'CaseController@findAllPendingReloads');
          
            $router->get('/reload/{id}', 'CaseController@findReloadCaseById');
            // $router->delete('/reload/{id}', 'CaseController@deleteReloadRecordById');
            $router->put('/reload/{id}', 'CaseController@updateReloadById');
            $router->get('/bank-branch-accounts', 'BankController@bankCountryWithBankAccounts');
            // $router->get('/bank-branch', 'BankController@bankCountry');
            $router->get('/statuses', 'CaseController@getStatuses');
            $router->get('/pending', 'CaseController@findAllUserCasesWithPendingStatus');
            // $router->get('/exchange', 'CaseController@getExchangeRecords');
            
            // $router->get('/reload/flagged-bank-account/frequency', 'CaseController@getFlaggedBankAccountByFrequency');
            // $router->get('/reload/flagged-bank-account/amount', 'CaseController@getFlaggedBankAccountByAmount');
           
         

        });


        // /api/exchange-rates
        $router->group([
            'prefix' => 'exchange-rates',
        ], function () use ($router) {
            $router->get('/', 'ExchangeRateController@index');
            $router->get('/list', 'ExchangeRateController@list');
            $router->get('/margin', 'ExchangeRateController@getExchangeRateMargin');
            // $router->post('/margin/add', 'ExchangeRateController@createExchangeRateMargin');
            $router->get('/{id}', 'ExchangeRateController@detail');
            $router->post('/', 'ExchangeRateController@createExchangeRate');
            $router->patch('/api', 'ExchangeRateController@updateRateByApi');
            $router->post('/add/bulk', 'ExchangeRateController@addInBluk');
            $router->put('/{id}', 'ExchangeRateController@update');
            $router->delete('/{id}', 'ExchangeRateController@delete');
            $router->get('/margin', 'ExchangeRateController@findAllExchangeMarginRate');
            $router->post('/margin/add', 'ExchangeRateController@createExchangeMarginRate');
        });

        // /api/currencies
        $router->group([
            'prefix' => 'currencies',
        ], function () use ($router) {
            $router->get('/', 'CurrencyController@currencies');
            $router->get('/filter/{type}', 'CurrencyController@findCurrencyFilterOptionsByType');
            $router->post('/add', 'CurrencyController@addCurrency');
            $router->get('/currencies-country', 'CurrencyController@findCurrencyByCountryId');
            $router->put('/{id}', 'CurrencyController@updateCurrency');
            $router->get('/{id}', 'CurrencyController@getCurrencyById');
            $router->delete('/{id}', 'CurrencyController@deleteCurrency');
            $router->get('/{id}/currency-keypads', 'CurrencyKeypadController@getAllCurrencyKeypads');
            // $router->get('/all', 'CurrencyController@findAllCurrencies');
        });

        // /api/currency-keypads
        $router->group([
            'prefix' => 'currency-keypads',
        ], function () use ($router) {
            $router->post('/add', 'CurrencyKeypadController@createCurrencyKeypad');
        });

        // /api/countries
        $router->group([
            'prefix' => 'countries',
        ], function () use ($router) {
            $router->get('/', 'CountryController@countryWithCurrencies');
            $router->get('/filter/{type}', 'CountryController@findCountryFilterOptionsByType');
            $router->get('/active', 'CountryController@findAllActiveCountries');
            $router->get('/{id}', 'CountryController@findCountryById');
            $router->patch('/{id}', 'CountryController@updateCountryById');
            $router->patch('/update-status/{id}', 'CountryController@updateCountryStatus');
            $router->patch('/update-for-transacts/{id}', 'CountryController@updateCountryForTransacts');
        });

        // /api/banks
        $router->group([
            'prefix' => 'banks',
        ], function () use ($router) {
            $router->get('/', 'BankController@index');
            // $router->get('/', 'BankController@findAllBankAvailableCountries');
            $router->get('/list', 'BankController@findAllBanks');
            $router->post('/add', 'BankController@addBank');
            $router->put('/{id}', 'BankController@updateBank');
            $router->delete('/{id}', 'BankController@deleteBank');
            $router->post('/add-acc', 'BankController@createBankAccount');
            $router->patch('/update-acc', 'BankController@updateBankAccount');
            $router->post('/add-branch', 'BankController@addBankBranch');
            $router->patch('/update-branch', 'BankController@updateBankBranch');
            $router->get('/banks-by-country', 'BankController@findAllBankAvailableCountries');
            $router->get('/bank-accounts', 'BankController@findAllBankAccountsAvailableCountries');
            $router->get('/countries/{currencyId}', 'BankController@findAllBankAvailableCountriesByCurrency');
            $router->get('/all-bank-accounts', 'BankController@findAllBankAccounts');
            $router->get('/bank-accounts/{bankAccId}', 'BankController@findBankAccountById');
            $router->patch('/bank-accounts/update', 'BankController@updateBankAccountById');
            $router->get('/bank-accounts-country', 'BankController@findBankCountryByCountryId');
            $router->get('/flagged-bank-accounts', "BankController@findBankAccountHighFrequencyAndAmount");
            $router->get('/available-bank-accounts', "BankController@findBankAccountLowFrequencyAndAmount");

            // $router->get('get-banks-by-country', 'BankController@getBanksByCountry');
        });

        // /**
        //  * /api/notification
        //  */
        $router->group([
            'prefix' => 'notification',
        ], function () use ($router) {
            //     // /api/notification/fcm
            //     $router->post('fcm', 'NotificationController@sendPushNotification');
            //     $router->post('fcm-bulk', 'NotificationController@sendPushNotificationInBulk');
            $router->get('list', 'NotificationController@getNotifications');
            $router->patch('mark-all-as-read', 'NotificationController@markAllAsRead');
            $router->post('mark-as-read', 'NotificationController@markMessageAsRead');
        });

        // /**
        //  * /api/general-settings
        //  */
        $router->group([
            'prefix' => 'general-settings',
        ], function () use ($router) {
            // /api/notification/fcm
            $router->get('/', 'SettingController@index');
            $router->patch('/{key}', 'SettingController@updateSettingByKey');
        });

        /**
         * /api/onboardings
         */
        $router->group([
            'prefix' => 'onboardings',
        ], function () use ($router) {
            $router->get('/', 'OnboardingController@index');
            $router->get('/{id}', 'OnboardingController@detail');
            $router->post('/add', 'OnboardingController@addOnboarding');
            $router->put('/{id}', 'OnboardingController@update');
            $router->delete('/{id}', 'OnboardingController@delete');
        });

        /**
         * /api/onboarding-details
         */
        $router->group([
            'prefix' => 'onboarding-details',
        ], function () use ($router) {
            $router->get('/', 'OnboardingDetailsController@index');
            $router->get('/{id}', 'OnboardingDetailsController@detail');
            $router->post('/add', 'OnboardingDetailsController@addOnboardingDetails');
            $router->put('/{id}', 'OnboardingDetailsController@update');
            $router->delete('/{id}', 'OnboardingDetailsController@delete');
        });

        // $router->group([
        //     'prefix' => 'onboarding',
        // ], function () use ($router) {
        //     $router->get('/', 'OnboardingController@index');
        //     $router->get('/{id}', 'OnboardingController@detail');
        //     $router->post('/add', 'OnboardingController@addOnboarding');
        //     $router->put('/{id}', 'OnboardingController@update');
        //     $router->delete('/{id}', 'OnboardingController@delete');
        // });

        // $router->group([
        //     'prefix' => 'onboarding-details',
        // ], function () use ($router) {
        //     $router->get('/', 'OnboardingDetailsController@index');
        //     $router->get('/{id}', 'OnboardingDetailsController@detail');
        //     $router->post('/add', 'OnboardingDetailsController@addOnboardingDetails');
        //     $router->put('/{id}', 'OnboardingDetailsController@update');
        //     $router->delete('/{id}', 'OnboardingDetailsController@delete');
        // });

        $router->group([
            'prefix' => 'media',
        ], function () use ($router) {
            $router->get('/banners', 'MediaController@findAllBanners');
            $router->post('/banner/add', 'MediaController@createBanner');
            $router->get('/banner/{id}', 'MediaController@findBannerById');
            $router->put('/banner/{id}', 'MediaController@updateBanner');
            $router->delete('/{id}', 'MediaController@removeMediaById');
            $router->post('/banner-media/add', 'MediaController@createBannerMedia');
        });

        $router->group([
            'prefix' => 'delete-user-request',
        ], function () use ($router) {
            $router->get('/', 'DeleteUserRequestController@findAllDeleteUserRequests');
        });

        
        // /api/activity-logs
        $router->group([
            'prefix' => 'activity-logs',
        ], function () use ($router) {
            $router->get('/', 'ActivityLogController@activityLogs');
            $router->get('/reference-table/{referenceTable}/reference-id/{referenceId}', 'ActivityLogController@findAllByReferenceTableReferenceID');
            $router->post('/', 'ActivityLogController@createActivityLog');
            $router->get('/sections', 'ActivityLogController@findActivityLogSections');
        });

         // /api/money-changer
         $router->group([
            'prefix' => 'money-changer',
        ], function () use ($router) {
            $router->get('/', 'MoneyChangerController@moneyChanger');
            $router->post('/add', 'MoneyChangerController@createMoneyChanger');
            $router->get('/{id}', 'MoneyChangerController@findMoneyChangerById');
            $router->patch('/{id}', 'MoneyChangerController@updateMoneyChangerById');


        });
    });

    $router->group([
        'prefix' => 'delete-user-request',
    ], function () use ($router) {
        $router->post('/', 'DeleteUserRequestController@handleDeleteUserRequest');
    });

    $router->group([
        'prefix' => 'exchange-rates-app',
    ], function () use ($router) {
        $router->get('/', 'ExchangeRateController@findAllExchangeRatesByCurrencyIds');
    });

    $router->group([
        'prefix' => 'webhook',
    ], function () use ($router) {
        $router->post('/giftpace', 'WebhookController@handleGiftpaceRequest');
    });

    /**
     * /api/auth
     */
    $router->group([
        'prefix' => 'auth',
    ], function () use ($router) {
        // /api/auth/register
        // $router->post('register', 'UserController@createAccount');
        // /api/auth/login
        $router->post('login', 'UserController@login');
        // /api/auth/logout
        $router->post('logout', 'UserController@logout');
        // /api/auth/forget-password
        $router->post('/forget-password', 'UserController@forgetPassword');
        $router->get('/check-reset-password-link', 'UserController@checkResetPasswordLinkExpiry');
        $router->post('/reset-password', 'UserController@resetPassword');

        // /api/auth/new-user-password
        $router->get('/check-password-link', 'UserController@checkNewPasswordLink');
        $router->post('/create-new-password', 'UserController@createNewPassword');
    });

    /**
     * /api/loans
     */
    $router->group([
        'prefix' => 'loans',
    ], function () use ($router) {
        $router->post('/package', 'LoanController@createLoanPackage');
        $router->get('/package', 'LoanController@findAllLoanPackages');
        $router->get('/package/{id}', 'LoanController@findOneLoanPackageById');
        $router->put('/package/{id}', 'LoanController@updateLoanPackageById');
        $router->delete('/package/{id}', 'LoanController@deleteLoanPackageById');
    });



    // /api/activity-logs
    // $router->group([
    //     'prefix' => 'activity-logs',
    // ], function () use ($router) {
    //     $router->get('/', 'ActivityLogController@activityLogs');
    //     $router->get('/reference-table/{referenceTable}/reference-id/{referenceId}', 'ActivityLogController@findAllByReferenceTableReferenceID');
    //     $router->post('/', 'ActivityLogController@createActivityLog');
    //     $router->get('/sections', 'ActivityLogController@findActivityLogSections');
    // });
});

//mobile
$router->group([
    'prefix' => 'apiv2',
    'namespace' => 'ApiV2'
], function () use ($router) {

    /**
     * Routes that need authentication checking
     */
    $router->group([
        'middleware' => ['jwt', 'app_role']
    ], function () use ($router) {
        // /apiv2/users
        $router->group([
            'prefix' => 'users',
        ], function () use ($router) {
            //<<<<<<<<<<GET>>>>>>>>>>>
            $router->get('/profile', 'AppUserController@findOneUserById');
            $router->get('/profile/initialize', 'AppUserController@findCountryStatesMaritalStatus');
            $router->get('/profile/accounts', 'AppUserController@findAllBankAccounts');
            $router->get('/profile/accounts/{id}', 'AppUserController@findBankAccountByBankAccountId');
            $router->get('/profile/send-reset-pin', 'AppUserController@sendResetPinOtp');

            //<<<<<<<<PATCH>>>>>>>>>
            $router->patch('/profile', 'AppUserController@updateUserById');
            $router->patch('/profile/password', 'AppUserController@updatePasswordByUserId');
            $router->patch('/profile/address', 'AppAddressController@updateAddressById');
            $router->patch('/profile/pin', 'AppUserController@updatePinByUserId');

            //<<<<<<<<POST>>>>>>>>>
            $router->post('/profile/address', 'AppAddressController@createAddress');
            $router->post('/profile/verify/phone', 'AppUserController@verifyProfilePhoneNumber');
            $router->post('/profile/verify/secure-code', 'AppUserController@verifyProfileSecureCode');
            $router->post('/profile/accounts', 'AppUserController@createUserBankAccount');
            $router->post('/verify-topkash', 'AppUserController@verifyTopkashId');
            $router->post('/profile/accounts/topkash', 'AppUserController@createTopkashUserBankAccount');
            $router->post('/profile/verify-reset-otp', 'AppUserController@verifyResetOtpCode');

            //<<<<<<<<DELETE>>>>>>>>>
            $router->delete('/profile/accounts/{id}', 'AppUserController@deleteBankAccountByBankAccountId');


            // <<<<<<<<<<<<<<<<<<<<<new api for account profile >>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // $router->get('/profile', 'AppUserController@profileAccount');
            // $router->post('/change-pin', 'AppUserController@changePin');
            $router->post('/save-fcm', 'AppUserController@saveFcmDetails');
            $router->patch('/app-language', 'AppUserController@updateAppLanguage');
            // $router->patch('/update-profile', 'AppUserController@updateProfile');
            $router->get('/activities', 'AppCaseController@userCases');
            // $router->post('/change-password', 'AppUserController@changePassword');
            $router->get('/wallets', 'AppUserController@findAllWallets');//
            $router->get('/main/wallet', 'AppUserController@findMainWallet');//

            $router->post('/biometric/public-key/update', 'AppUserController@updateBiometricPublicKey');
            $router->post('/biometric/public-key/verify', 'AppUserController@verifyBiometricPublickey');
            $router->post('/pin/create-or-update', 'AppUserController@createOrUpdateAppPinCode');
            $router->post('/pin/verify', 'AppUserController@verifyAppPinCode');
            $router->post('/biometric/wallet-session-verify', 'AppUserController@walletSessionVerify');
            $router->post('/device-info', 'AppUserController@findOrCreateUserDeviceById');

            $router->get('/upline', 'AppUserController@findUplineUser');//
            $router->get('/downline', 'AppUserController@findDownlineUser');//

        });

        // /apiv2/qrcode
        $router->group([
            'prefix' => 'qrcode',
        ], function () use ($router) {

            $router->post('generate', 'AppQrCodeController@generateQrCode');
            $router->post('verify', 'AppQrCodeController@verifyQrCode');
        });


        // /apiv2/wallets
        $router->group([
            'prefix' => 'profile',
        ], function () use ($router) {
            $router->get('/wallets', 'AppUserController@findAllWallets');
            $router->get('/topkash-users/{topkash_user_id}/wallets', 'AppUserController@findTopkashUserAllWallets');
            $router->post('/wallets', 'AppUserController@createWallet');
        });

        // /apiv2/banners
        $router->group([
            'prefix' => 'banners',
        ], function () use ($router) {
            $router->get('/', 'AppBannerController@findAllBanners');
        });

        // /apiv2/cases
        $router->group([
            'prefix' => 'cases',
        ], function () use ($router) {
            $router->get('/transfer-initialise', 'AppCaseController@transferInitialise');
            $router->get('/reload-initialise', 'AppCaseController@reloadInitialise');
            $router->get('/exchange-initialise', 'AppCaseController@exchangeInitialise');
            $router->get('/type/{type}', 'AppCaseController@checkLatestCaseStatusByType');
        });

        // /apiv2/case-point
        $router->group([
            'prefix' => 'case-points',
        ], function () use ($router) {
            $router->get('/latest', 'AppCasePointController@findCaseByUserId');
            $router->post('/update', 'AppCasePointController@updateCaseStatueById');

        });


        // /apiv2/exchange-rates
        $router->group([
            'prefix' => 'exchange-rates',
        ], function () use ($router) {
            $router->get('/', 'AppExchangeRateController@appExchangeRateList');
            $router->get('/all', 'AppExchangeRateController@findAllExchangeRatesByCurrencyIds');
            $router->get('/date', 'AppExchangeRateController@findOneByCurrencyIdsAndDate');
            $router->get('/rate', 'AppExchangeRateController@findOneByCurrencyIds');

        });

        // /apiv2/currencies
        $router->group([
            'prefix' => 'currencies',
        ], function () use ($router) {
            $router->get('/', 'AppCurrencyController@currencies');
            $router->get('/countries/{id}', 'AppCurrencyController@findCountryCurrencyById');
            $router->get('/{id}', 'AppCurrencyController@findCurrencyById');

        });

        // /apiv2/banks
        $router->group([
            'prefix' => 'banks',
        ], function () use ($router) {
            $router->get('/countries', 'AppBankController@findAllBankAvailableCountries');
            $router->get('/countries/{currencyId}', 'AppBankController@findAllBankAvailableCountriesByCurrency');
            $router->get('/list', 'AppBankController@findAllBankWithBankCountries');
            $router->post('/bank-accounts', 'AppBankController@findAllAvailableBankAccounts');
            // $router->post('/bank-accounts', 'AppBankController@findAllBankAccountsByLowestScore');
        });

        $router->group([
            'prefix' => 'fastransfer',
        ], function () use ($router) {
            $router->get('/request/{id}', 'AppTransferController@findOneFasTransferRequest');
            $router->delete('/receipt/{id}', 'AppTransferController@removeReceiptById');

            $router->post('/', 'AppTransferController@createFasTransferRequest');
            $router->get('/', 'AppTransferController@findAllFasTransferRequest');
            $router->get('/margin', 'AppTransferController@exchangeMarginRate');

            $router->post('/user-bank-account', 'AppTransferController@createFasTransferConvert');

            $router->post('/receipt/add', 'AppTransferController@uploadReceipt');
            $router->post('/receipt/confirm', 'AppTransferController@updateAllFasTransferStatusByDay');

            $router->post('/bank-account', 'AppTransferController@updateUserBankAccount');
            // $router->post('/bank-account/{id}', 'AppTransferController@updateUserBankAccount');

            $router->delete('/bank-account/{id}', 'AppTransferController@removeUserBankAccount');


        });

        $router->post('/reload', 'AppAccountController@createReloadCase');
        $router->post('/transfer', 'AppTransferController@transfer');
        $router->post('/scan-transfer', 'AppTransferController@scanAndTransfer');

        $router->post('/exchange', 'AppAccountController@exchange');
        $router->post('/exchange/converted-amount', 'AppAccountController@getExchangeConvertedAmount');
        $router->post('/bank-branch', 'AppBankController@bankCountryWithBankAccounts');

        /**
         * /apiv2/notification
         */
        $router->group([
            'prefix' => 'notification',
        ], function () use ($router) {

            $router->get('list', 'AppNotificationController@getNotifications');
            $router->post('mark-as-read', 'AppNotificationController@markMessageAsRead');
        });

        /**
         * /apiv2/transactions
         */
        $router->group([
            'prefix' => 'transactions'
        ], function () use ($router) {
            $router->get('/', 'AppTransactionController@findUserTransactionsById');
            $router->get('/history-by-currency', 'AppTransactionController@findUserTransactionsHistoryByCurrency');
            $router->get('/history', 'AppTransactionController@findUserTransactionsHistory');
            $router->get('/latest', 'AppTransactionController@findLatestTransactionByUserAndCurrencyId');
        });

        /**
         * /apiv2/transaction_points
         */
        $router->group([
            'prefix' => 'transaction-points'
        ], function () use ($router) {
         
            $router->get('/latest', 'AppTransactionPointController@findLatestTransactionPointByUserId');
            $router->get('/history', 'AppTransactionPointController@findTransactionPointByUserId');

        });
        /**
         * /apiv2/transfers
         */
        $router->group([
            'prefix' => 'transfers'
        ], function () use ($router) {
            $router->get('/{id}', 'AppTransferController@findTransferRecordById');
        });

        /**
         * /apiv2/transfers-points
         */
        $router->group([
            'prefix' => 'transfer-points'
        ], function () use ($router) {
            $router->post('/', 'AppTransferPointController@createTransferPoint');
        });

        /**
         * /apiv2/reloads
         */
        $router->group([
            'prefix' => 'reloads'
        ], function () use ($router) {
            $router->get('/initiate', 'AppReloadController@initiateReloadsAction');
            $router->get('/{id}', 'AppReloadController@findReloadRecordById');
        });

        /**
         * /apiv2/reload-points
         */
        $router->group([
            'prefix' => 'reload-points'
        ], function () use ($router) {
            $router->post('/', 'AppReloadPointController@createReloadPoint');
            $router->post('/receipt/add', 'AppReloadPointController@uploadReloadReceipt');

        });

        /**
         * /apiv2/redeems
         */
        $router->group([
            'prefix' => 'redeems'
        ], function () use ($router) {
            $router->get('/{id}', 'AppVoucherController@findRedeemRecordById');
        });

        /**
         * /apiv2/countries
         */
        $router->group([
            'prefix' => 'countries'
        ], function () use ($router) {
            $router->get('/currencies/enabled', 'AppCountryController@findCurrencyByEnabledCurrency');
        });

        /**
         * /apiv2/referral
         */
        $router->group([
            'prefix' => 'referral',
        ], function () use ($router) {

            $router->post('/groups/add', 'AppReferralController@addGroup');
            $router->patch('/groups/update', 'AppReferralController@updateGroup');
            $router->get('/groups', 'AppReferralController@groupList');
            $router->get('/groups/{id}', 'AppReferralController@groupDetails');
            $router->get('/parent', 'AppReferralController@parent');
            $router->get('/users', 'AppReferralController@referrals');
            $router->get('/user/{id}', 'AppReferralController@referralDetails');
        });

        /**
         * /apiv2/quotes
         */
        $router->group([
            'prefix' => 'quotes'
        ], function () use ($router) {
            $router->post('/', 'AppQuoteController@calculateRateByCurrencyIds');
            $router->post('/verify', 'AppQuoteController@verifyQuoteToken');
        });

        /**
         * /apiv2/voucher
         */
        $router->group([
            'prefix' => 'voucher'
        ], function () use ($router) {
            $router->post('/redeem', 'AppVoucherController@redeemVoucherByCode');
        });


        /**
         * /apiv2/test
         */
        $router->group([
            'prefix' => 'test',
        ], function () use ($router) {
        });
    });

    $router->get('/countries', 'AppCountryController@countryWithCurrencies');

    //apiv2/onboarding
    $router->group([
        'prefix' => 'onboardings',
    ], function () use ($router) {
        $router->get('/', 'AppOnboardingController@index');
    });

    /**

     * /apiv2/auth
     */
    $router->group([
        'prefix' => 'auth',
    ], function () use ($router) {
        // /apiv2/auth/register
        // $router->post('register', 'UserController@appRegister');

        $router->group([
            'prefix' => 'register',
        ], function () use ($router) {
            $router->post('/otp-code', 'AppUserController@createAppRegisterOtp');
            $router->post('/', 'AppUserController@register');
        });

        // /apiv2/auth/verify
        $router->group([
            'prefix' => 'verify',
        ], function () use ($router) {
            // /apiv2/auth/verify/username
            $router->post('/username', 'AppUserController@verifyAppUsername');

            // /apiv2/auth/verify/email
            $router->post('/email', 'AppUserController@verifyAppEmailAddress');

            // /apiv2/auth/verify/phone
            $router->post('/phone', 'AppUserController@verifyAppPhoneNumber');

            // /apiv2/auth/verify/initiate
            $router->post('/initiate', 'AppUserController@verifyAppInitiateSecureCode');

            // /apiv2/auth/verify/secure-code
            $router->post('/secure-code', 'AppUserController@verifyAppSecureCode');

            // /apiv2/auth/verify/pin
            $router->post('/pin', 'AppUserController@updateAppUserPin');

            $router->post('/reset-otp', 'AppUserController@verifyResetOtpCode');
        });

        // /apiv2/auth/login
        $router->post('login', 'AppUserController@appLogin');
        $router->post('/phone-no-validation', 'AppUserController@checkIsPhoneNoValid');
        $router->post('/send-reset-password', 'AppUserController@sendResetPasswordOtp');
        $router->patch('/reset-password', 'AppUserController@resetPassword');
        $router->post('/pin/create-or-update', 'AppUserController@createOrUpdateAppPinCode');
    });
    $router->get('countries', 'AppCountryController@countryWithCurrencies');
});

// //websocket
// $router->group([
//     'prefix' => 'socket',
// ], function () use ($router) {
//     $router->post('verify', 'SocketController@verifyConnection');
// });
