import * as dotenv from "dotenv";
dotenv.config();

import express from "express";
import cors from "cors";
import { createServer } from "http";
import { Server } from "socket.io";
import axios from "axios";

const PORT = process.env.PORT;
const app = express();

app.use(
    cors({
        origin: process.env.SERVER_URL,
        methods: ["POST"],
    })
);
app.use(express.json());

const httpServer = createServer(app);

const io = new Server(httpServer, {
    /* options */
    cors: {
        origin: process.env.DASHBOARD_URL,
    },
});

httpServer.listen(PORT, (e) => {
    console.log(`Server running at ${PORT}`);
});

io.on("connection", (socket) => {
    console.log("Someone connect");
    socket.on("disconnect", (reason) => {
        console.log("Someone disconnect");
    });
});

io.use(async (socket, next) => {
    try {
        if (!socket.handshake.headers.hasOwnProperty("authorization")) {
            throw "Authorization inexistant";
        }

        if (!socket.handshake.auth.hasOwnProperty("user")) {
            throw "User inexistant";
        }

        const tokenHeader = socket.handshake.headers.authorization;

        if (!tokenHeader.startsWith("Bearer ")) {
            throw "Authorization inexistant";
        }

        const token = tokenHeader.substring(7, tokenHeader.length);
        const userId = socket.handshake.auth.user;

        await axios.post(process.env.SERVER_URL + "/socket/verify", {
            token: token,
            user: userId,
        });

        next();
    } catch (e) {
        let error;
        if (e.hasOwnProperty("response")) {
            if (e.response.data.hasOwnProperty("message")) {
                error = e.response.data.message;
            } else {
                error = e.response.statusText;
            }
        } else {
            error = e;
        }

        next(new Error(error));
    }
});

app.set("io", io);

app.get("/", (req, res) => {
    res.send("Hello World soc!");
});

app.post("/add-reload", (req, res) => {
    const socketIo = req.app.get("io");

    if (Object.keys(req.body).length > 0) {
        socketIo.emit("add-reload", req.body);
        res.send("Successfully emit!");
    } else {
        res.status(400).send("No data");
    }
});

app.post("/remove-reload", (req, res) => {
    const socketIo = req.app.get("io");

    if (Object.keys(req.body).length > 0) {
        socketIo.emit("remove-reload", req.body);
        res.send("Successfully emit!");
    } else {
        res.status(400).send("No data");
    }
});

app.post("/update-reload", (req, res) => {
    const socketIo = req.app.get("io");

    if (Object.keys(req.body).length > 0) {
        socketIo.emit("update-reload", req.body);
        res.send("Successfully emit!");
    } else {
        res.status(400).send("No data");
    }
});

app.post("/add-transfer", (req, res) => {
    const socketIo = req.app.get("io");

    if (Object.keys(req.body).length > 0) {
        socketIo.emit("add-transfer", req.body);
        res.send("Successfully emit!");
    } else {
        res.status(400).send("No data");
    }
});

app.post("/remove-transfer", (req, res) => {
    const socketIo = req.app.get("io");

    if (Object.keys(req.body).length > 0) {
        socketIo.emit("remove-transfer", req.body);
        res.send("Successfully emit!");
    } else {
        res.status(400).send("No data");
    }
});

app.post("/update-transfer", (req, res) => {
    const socketIo = req.app.get("io");

    if (Object.keys(req.body).length > 0) {
        socketIo.emit("update-transfer", req.body);
        res.send("Successfully emit!");
    } else {
        res.status(400).send("No data");
    }
});
