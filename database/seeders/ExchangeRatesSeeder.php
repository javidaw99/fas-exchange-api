<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExchangeRatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exchange_rates')->insert([
            'from_currency_id' => 1,
            'to_currency_id' => 2,
            'created_by' => '3',
            'rate' => 0.301602,
            'date' => date("Y-m-d"),
        ]);
    }
}
