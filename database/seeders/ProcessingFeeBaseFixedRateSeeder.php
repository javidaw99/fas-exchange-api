<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProcessingFeeBaseFixedRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('processing_fee_base_fixed_rates')->insert([
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'amount' => 2.4300,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 4,
                'amount' => 2.1900,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 7,
                'amount' => 3.2700,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 2,
                'to_currency_id' => 1,
                'amount' => 0.6600,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 2,
                'to_currency_id' => 4,
                'amount' => 0.7500,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 2,
                'to_currency_id' => 7,
                'amount' => 1.0900,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 4,
                'to_currency_id' => 1,
                'amount' => 0.6500,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 4,
                'to_currency_id' => 2,
                'amount' => 0.7700,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 4,
                'to_currency_id' => 7,
                'amount' => 0.9600,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 7,
                'to_currency_id' => 1,
                'amount' => 11927.0000,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 7,
                'to_currency_id' => 2,
                'amount' => 13873.0000,
                'date' => date('Y-m-d')
            ],
            [
                'from_currency_id' => 7,
                'to_currency_id' => 4,
                'amount' => 12973.0000,
                'date' => date('Y-m-d')
            ],
        ]);
    }
}
