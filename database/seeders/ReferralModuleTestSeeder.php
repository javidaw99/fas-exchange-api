<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\Person;
use Faker\Provider\ms_MY\PhoneNumber;
use Faker\Provider\Lorem;

class ReferralModuleTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Person($this->faker));
        $this->faker->addProvider(new PhoneNumber($this->faker));
        $this->faker->addProvider(new Lorem($this->faker));
    }

    public function run()
    {

        $userTable = DB::table('user_tests');
        // $userTable = DB::table('users');

        $settings = [
            // 1,
            // 10000,
            // 2,
            // 1,
            // 1,
            // 1

            // 10,
            // 10,
            // 5,
            // 5,
            // 5,
            // 5
        ];

        $settings = array_merge($settings, array_fill(0, 5000, 1));

        $insertedUser = [];

        $insertedGroup = [];

        $counter = 0;

        foreach ($settings as $key => $set) {

            if ($key == 0) {

                for ($i = 0; $i < $set; $i++) {
                    DB::beginTransaction();

                    $fake = $this->faker;
                    $userName = $fake->name();
                    $groupName = $fake->word();

                    $ecr = rand(15, 20);
                    $wcr = rand(15, 20);

                    $userId = $userTable->insertGetId([
                        'username'=> $userName
                    ]);

                    $refTreeId = DB::table('referral_trees')->insertGetId([
                        'ancestor_user_id'=> $userId,
                        'descendant_user_id'=> $userId,
                        'depth' => 0
                    ]);

                    $groupId = DB::table('referral_groups')->insertGetId([
                        'ancestor_user_id'=> 0,
                        'name' => 'founders',
                        'exchange_commission_rate' => $ecr,
                        'withdrawal_commission_rate' => $wcr
                    ]);

                    DB::table('referrals')->insert([
                        'referral_tree_id'=> $refTreeId,
                        'referral_group_id'=> $groupId
                    ]);

                    DB::commit();

                    if (!array_key_exists($key, $insertedUser)) {
                        $insertedUser[$key] = [$userId];
                    } else {
                        $arr = $insertedUser[$key];
                        $newArr = $arr;
                        array_push($newArr, $userId);
                        $insertedUser[$key] = $newArr;
                    }

                    if (!array_key_exists($key, $insertedGroup)) {
                        $insertedGroup[$key] = [$groupId];
                    } else {
                        $arr = $insertedGroup[$key];
                        $newArr = $arr;
                        array_push($newArr, $groupId);
                        $insertedGroup[$key] = $newArr;
                    }

                    $counter++;
                    dump($counter);
                }
            } 
            else {
                $prevLayerUser = $insertedUser[$key - 1];
                $prevLayerGroup = $insertedGroup[$key - 1];

                foreach ($prevLayerUser as $index => $prevUser) {
                    $parentGroupId = $prevLayerGroup[$index];
                    $parentGroup = DB::table('referral_groups')->find($parentGroupId);

                    for ($i = 0; $i < $set; $i++) {

                        DB::beginTransaction();

                        $fake = $this->faker;
                        $userName = $fake->name();
                        $groupName = $fake->word();

                        $userId = $userTable->insertGetId([
                            'username'=> $userName
                        ]);

                        DB::table('referral_trees')->insert([
                            'ancestor_user_id'=> $userId,
                            'descendant_user_id'=> $userId,
                            'depth' => 0
                        ]);

                        $ancestors = DB::table('referral_trees')->where('descendant_user_id', $prevUser)->get();

                        $refTreeId = null;
                        $uplines = [];
                        foreach ($ancestors as $anc) {
                            $addedDepth = $anc->depth + 1;
                            if ($addedDepth > 1) {
                                array_push($uplines, [
                                    'ancestor_user_id'=> $anc->ancestor_user_id,
                                    'descendant_user_id'=> $userId,
                                    'depth' => $addedDepth
                                ]);
                            } else {
                                $refTreeId = DB::table('referral_trees')->insertGetId([
                                    'ancestor_user_id'=> $anc->ancestor_user_id,
                                    'descendant_user_id'=> $userId,
                                    'depth' => $addedDepth
                                ]);
                            }
                        }

                        DB::table('referral_trees')->insert($uplines);

                        $groupId = DB::table('referral_groups')->insertGetId([
                            'ancestor_user_id'=> $prevUser,
                            'name' => $groupName,
                            'exchange_commission_rate' => rand(0, $parentGroup->exchange_commission_rate),
                            'withdrawal_commission_rate' => rand(0, $parentGroup->withdrawal_commission_rate)
                        ]);

                        if (!is_null($refTreeId)) {
                            DB::table('referrals')->insert([
                                'referral_tree_id'=> $refTreeId,
                                'referral_group_id'=> $groupId
                            ]);
                        }

                        DB::commit();

                        if (!array_key_exists($key, $insertedUser)) {
                            $insertedUser[$key] = [$userId];
                        } else {
                            $arr = $insertedUser[$key];
                            $newArr = $arr;
                            array_push($newArr, $userId);
                            $insertedUser[$key] = $newArr;
                        }

                        if (!array_key_exists($key, $insertedGroup)) {
                            $insertedGroup[$key] = [$groupId];
                        } else {
                            $arr = $insertedGroup[$key];
                            $newArr = $arr;
                            array_push($newArr, $groupId);
                            $insertedGroup[$key] = $newArr;
                        }

                        $counter++;
                        dump($counter);
                    }
                }
            }
        }
    }
}
