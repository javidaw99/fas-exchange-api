<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ReloadsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $receipt = ['.jpeg', '.png', '.gif'];
        DB:: table('reloads')-> insert([
            'bank_acc_id'=> 3,
            'currency_id'=>2,
            'amount'=>1000.50,
            'reference_no'=>1234,
            'receipt'=>$receipt[array_rand($receipt)],


        ]); 
    }
}
