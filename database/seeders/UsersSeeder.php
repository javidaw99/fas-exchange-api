<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\Person;
use Faker\Provider\ms_MY\PhoneNumber;

class UsersSeeder extends Seeder
{
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Person($this->faker));
        $this->faker->addProvider(new PhoneNumber($this->faker));
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = $this->faker;
        $username = $user->firstName();
        $phone =
        DB:: table('users')-> insert([
            'username'=> $username,
            'fullname'=> $user->name(),
            'email'=> strtolower($username).'@gmail.com',
            'phone_no'=> $this->faker->mobileNumber(),
            'password'=>Hash::make('password'),
            'role_id'=> 2,
            'country_id'=>1,
        ]);
    }
}
