<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\Address;

class ContriesSeeder extends Seeder
{
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Address($this->faker));
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('countries')-> insert([
                'name'=> $this->faker->country(),
                'iso_code_2'=>Str::random(2),
                'iso_code_3'=>Str::random(3),
        ]);
    }
}
