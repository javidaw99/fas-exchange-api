<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('transactions')-> insert([
            'user_id'=> 2,
            'account_id'=>1,
            'case_id'=> 3,
            'old_balance'=>1000.40,
            'new_balance'=>500.40,
    ]);
    }
}
