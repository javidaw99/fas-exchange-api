<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\Payment;

class BanksSeeder extends Seeder
{
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Payment($this->faker));
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('banks')-> insert([
            'name'=> $this->faker->bank(),
    ]);
    }
}
