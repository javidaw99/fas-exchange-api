<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\Miscellaneous;

class CurrenciesSeeder extends Seeder
{
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Miscellaneous($this->faker));
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('currencies')-> insert([
            'currency_name'=>$this->faker->currencyCode(),
            'iso_code'=>Str::random(3),
        ]);
    }
}
