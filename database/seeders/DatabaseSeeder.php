<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersSeeder::class,
            ContriesSeeder::class,
            RolesSeeder::class,
            CurrenciesSeeder::class,
            CountryCurrencySeeder::class,
            BankCountrySeeder::class,
            BanksSeeder::class,
            BankAccountsSeeder::class,
            StatusesSeeder::class,
            AccountsSeeder::class,
            CasesSeeder::class,
            ReloadsSeeder::class,
            TransactionsSeeder::class,
            TransfersSeeder::class,
            ExchangeRatesSeeder::class,
            ExchangesSeeder::class,
            RequestLogsSeeder::class,
            NotificationsSeeder::class,
        ]);
    }
}
