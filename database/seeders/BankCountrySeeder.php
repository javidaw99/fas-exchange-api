<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\Address;

class BankCountrySeeder extends Seeder
{
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Address($this->faker));
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('bank_country')-> insert([
            'country_id'=> 2,
            'bank_id'=> 1,
            'branch_name'=>$this->faker->state(),
    ]);
    }
}
