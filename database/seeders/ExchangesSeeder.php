<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ExchangesSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('exchanges')-> insert([
            'from_currency_id'=> 2,
            'to_currency_id'=> 1,
            'exchange_rate_id'=> 2,
            'from_account_id'=> 3,
            'to_account_id'=> 1,
            'from_amount'=> 4000,
            'to_amount'=> 2000,
    ]);
    }
}
