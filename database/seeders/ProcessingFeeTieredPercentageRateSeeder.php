<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProcessingFeeTieredPercentageRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('processing_fee_tiered_percentage_rates')->insert([
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.586,
                'from_amount' => 1,
                'to_amount' => 19999,
                'date' => '2023-02-17'
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.586479,
                'from_amount' => 20000,
                'to_amount' => 599999,
                'date' => '2023-02-17'
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.575070958451597,
                'from_amount' => 600000,
                'to_amount' => 699999,
                'date' => '2023-02-17'
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.562576517966454,
                'from_amount' => 700000,
                'to_amount' => 799999,
                'date' => '2023-02-17'
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.553205691507114,
                'from_amount' => 800000,
                'to_amount' => 899999,
                'date' => '2023-02-17'
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.545917273241415,
                'from_amount' => 900000,
                'to_amount' => 999999,
                'date' => '2023-02-17'
            ],
            [
                'from_currency_id' => 1,
                'to_currency_id' => 2,
                'percentage_rate' => 0.54008654008654,
                'from_amount' => 1000000,
                'to_amount' => 1000000,
                'date' => '2023-02-17'
            ],
        ]);
    }
}
