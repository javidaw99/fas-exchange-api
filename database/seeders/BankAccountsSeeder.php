<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory;
use Faker\Provider\ms_MY\PhoneNumber;

class BankAccountsSeeder extends Seeder
{

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new PhoneNumber($this->faker));
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('bank_accounts')-> insert([
            'bank_country_id'=> 2,
            'country_id'=> 2,
            'currency_id'=>1,
            'acc_no'=> $this->faker->imei(),
            'acc_name'=>Str::random(5),
    ]);
    }
}
