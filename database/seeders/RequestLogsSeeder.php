<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RequestLogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('request_logs')-> insert([
            'user_id'=> 2,
            'status_code'=> 1,
            'ip'=>Str::random(10),
            'url'=>Str::random(10),
            'request'=>Str::random(20),
            'response'=>Str::random(10),
            
    ]); 
    }
}
