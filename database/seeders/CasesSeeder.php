<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class CasesSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = ['reload', 'transfer', 'exchange'];

        DB:: table('cases')-> insert([
            'user_id'=> 3,
            'account_id'=> 3,
            'to_account_id'=>1,
            'ref_type'=> $type[array_rand($type)],
            'status'=>1,
            'ref_id'=>1,
            'assigned_user_id'=>2,
    ]); 
    }
}
