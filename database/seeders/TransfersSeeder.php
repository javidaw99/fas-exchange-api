<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TransfersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('transfers')-> insert([
            'currency_id'=> 3,
            'amount'=> 1000.00,
            'purpose'=>Str::random(10),
            'recipient_country_id'=>3,
            'recipient_bank_country_id'=>2,
            'recipient_acc_no'=>1,
            

    ]); 
    }
}
