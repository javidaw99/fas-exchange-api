<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
// use Faker\Factory;
// use Faker\Provider\ms_MY\Company;

class RolesSeeder extends Seeder
{
    // public function __construct()
    // {
    //     $this->faker = Factory::create();
    //     $this->faker->addProvider(new Company($this->faker));
    // }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        $title = ['Admin', 'SuperAdmin', 'User'];
        DB:: table('roles')-> insert([
            'name'=>$title[array_rand($title)],
        ]);
    }
}
