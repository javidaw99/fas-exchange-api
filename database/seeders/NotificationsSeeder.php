<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;



class notificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB:: table('notifications')-> insert([
          'title'=>Str::random(10),
          'content'=>Str::random(10),
          'is_read'=>1,
          'user_id'=>2,
       ]);
    }
}
