<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropOtpCodeAndIsOtpVerifiedColumnInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('users', 'otp_code') && Schema::hasColumn('users', 'is_otp_verified'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->dropColumn('otp_code');
                $table->dropColumn('is_otp_verified');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('otp_code', 255)->after('tfa_secret')->nullable();
            $table->boolean('is_otp_verified')->default('0')->after('otp_code');
        });
       
    }
}
