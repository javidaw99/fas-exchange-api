<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsFromExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exchanges', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('to_user_id');
            $table->string('rate_type', 40);
            $table->renameColumn('from_account_id', 'account_id');
            $table->renameColumn('from_currency_id', 'currency_id');
            $table->renameColumn('from_amount', 'amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exchanges', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('to_user_id');
            $table->dropColumn('rate_type');
            $table->renameColumn('account_id', 'from_account_id');
            $table->renameColumn('currency_id', 'from_currency_id');
            $table->renameColumn('amount', 'from_amount');
        });
    }
}
