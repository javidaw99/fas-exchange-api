<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessingFeeTieredPercentageRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processing_fee_tiered_percentage_rates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('from_currency_id');
            $table->foreignId('to_currency_id');
            $table->decimal('percentage_rate', 12, 6);
            $table->integer('from_amount');
            $table->integer('to_amount');
            $table->date('date');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processing_fee_tiered_percentage_rates');
    }
}
