<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFasTranferRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fas_tranfer_request', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('user_bank_account_id');
            $table->unsignedInteger('user_account_id');
            $table->unsignedInteger('from_currency_id');
            $table->unsignedInteger('to_currency_id');
            $table->string('total_amount', 255);
            $table->string('total_convert_amount', 255);
            $table->boolean('status',4)->nullable();
            $table->string('daily_limit', 255);
            $table->string('exchange_rate_id', 255);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fas_tranfer_request');
    }
}
