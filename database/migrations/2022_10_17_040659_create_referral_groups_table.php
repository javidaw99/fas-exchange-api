<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ancestor_user_id')->index('ancestor_user_id');
            $table->string('name', 255);
            $table->uuid('code');
            $table->decimal('exchange_commission_rate', 7, 4);
            $table->decimal('withdrawal_commission_rate', 7, 4);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_groups');
    }
}
