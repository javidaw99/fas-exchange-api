<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserTransactionsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('DROP VIEW IF EXISTS user_transactions_view');

        DB::unprepared('
        CREATE VIEW `user_transactions_view` AS
        SELECT 
            `txn`.`user_id` AS `user_id`,
            `txn`.`reference_user_id` AS `reference_user_id`,
            `ru`.`fullname` AS `reference_username`,
            `txn`.`reference_table` AS `reference_table`,
            `txn`.`reference_id` AS `reference_id`,
            \'3\' AS `status`,
            `t`.`type` AS `transaction_type`,
            `t`.`user_bank_account_id` AS `user_bank_account_id`,
            `uba`.`reference_name` AS `user_bank_account_name`,
            `uba`.`type` AS `user_bank_account_type`,
            `txn`.`amount` AS `amount`,
            `t`.`from_currency_id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `t`.`to_currency_id` AS `to_currency_id`,
            `ct`.`iso_code` AS `to_currency_code`,
            `txn`.`created_at` AS `created_at`
        FROM
            ((((((`transactions` `txn`
            LEFT JOIN `transfers` `t` ON (`txn`.`reference_id` = `t`.`id`))
            LEFT JOIN `currencies` `cf` ON (`t`.`from_currency_id` = `cf`.`id`))
            LEFT JOIN `currencies` `ct` ON (`t`.`to_currency_id` = `ct`.`id`))
            LEFT JOIN `users` `ru` ON (`txn`.`reference_user_id` = `ru`.`id`))
            LEFT JOIN `users` `u` ON (`txn`.`user_id` = `u`.`id`))
            LEFT JOIN `user_bank_accounts` `uba` ON (`t`.`user_bank_account_id` = `uba`.`id`))
        WHERE
            `txn`.`reference_table` = \'transfer\' 
        UNION ALL SELECT 
            `txn`.`user_id` AS `user_id`,
            `txn`.`reference_user_id` AS `reference_user_id`,
            `ru`.`fullname` AS `reference_username`,
            `txn`.`reference_table` AS `reference_table`,
            `txn`.`reference_id` AS `reference_id`,
            \'3\' AS `status`,
            \'RELOAD\' AS `transaction_type`,
            NULL AS `user_bank_account_id`,
            NULL AS `user_bank_account_name`,
            NULL AS `user_bank_account_type`,
            `txn`.`amount` AS `amount`,
            `r`.`currency_id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `r`.`currency_id` AS `to_currency_id`,
            `cf`.`iso_code` AS `to_currency_code`,
            `txn`.`created_at` AS `created_at`
        FROM
            ((((`transactions` `txn`
            LEFT JOIN `reloads` `r` ON (`txn`.`reference_id` = `r`.`id`))
            LEFT JOIN `currencies` `cf` ON (`r`.`currency_id` = `cf`.`id`))
            LEFT JOIN `users` `ru` ON (`txn`.`reference_user_id` = `ru`.`id`))
            LEFT JOIN `users` `u` ON (`txn`.`user_id` = `u`.`id`))
        WHERE
            `txn`.`reference_table` = \'reload\' 
        UNION ALL SELECT 
            `cs`.`user_id` AS `user_id`,
            `t`.`to_user_id` AS `reference_user_id`,
            `ru`.`fullname` AS `reference_username`,
            `cs`.`reference_table` AS `reference_table`,
            `cs`.`reference_id` AS `reference_id`,
            \'1\' AS `status`,
            `t`.`type` AS `transaction_type`,
            `t`.`user_bank_account_id` AS `user_bank_account_id`,
            `uba`.`reference_name` AS `user_bank_account_name`,
            `uba`.`type` AS `user_bank_account_type`,
            `t`.`amount` AS `amount`,
            `t`.`from_currency_id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `t`.`to_currency_id` AS `to_currency_id`,
            `ct`.`iso_code` AS `to_currency_code`,
            `cs`.`created_at` AS `created_at`
        FROM
            ((((((`cases` `cs`
            LEFT JOIN `transfers` `t` ON (`cs`.`reference_id` = `t`.`id`))
            LEFT JOIN `currencies` `cf` ON (`t`.`from_currency_id` = `cf`.`id`))
            LEFT JOIN `currencies` `ct` ON (`t`.`to_currency_id` = `ct`.`id`))
            LEFT JOIN `users` `ru` ON (`t`.`to_user_id` = `ru`.`id`))
            LEFT JOIN `users` `u` ON (`t`.`user_id` = `u`.`id`))
            LEFT JOIN `user_bank_accounts` `uba` ON (`t`.`user_bank_account_id` = `uba`.`id`))
        WHERE
            `cs`.`reference_table` = \'transfer\'
                AND `cs`.`status` = 1 
        UNION ALL SELECT 
            `cs`.`user_id` AS `user_id`,
            `cs`.`user_id` AS `reference_user_id`,
            `ru`.`fullname` AS `reference_username`,
            `cs`.`reference_table` AS `reference_table`,
            `cs`.`reference_id` AS `reference_id`,
            \'1\' AS `status`,
            \'RELOAD\' AS `transaction_type`,
            NULL AS `user_bank_account_id`,
            NULL AS `user_bank_account_name`,
            NULL AS `user_bank_account_type`,
            `r`.`amount` AS `amount`,
            `r`.`currency_id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `r`.`currency_id` AS `to_currency_id`,
            `cf`.`iso_code` AS `to_currency_code`,
            `cs`.`created_at` AS `created_at`
        FROM
            ((((`cases` `cs`
            LEFT JOIN `reloads` `r` ON (`cs`.`reference_id` = `r`.`id`))
            LEFT JOIN `currencies` `cf` ON (`r`.`currency_id` = `cf`.`id`))
            LEFT JOIN `users` `ru` ON (`r`.`user_id` = `ru`.`id`))
            LEFT JOIN `users` `u` ON (`cs`.`user_id` = `u`.`id`))
        WHERE
            `cs`.`reference_table` = \'reload\'
                AND `cs`.`status` = 1
        ORDER BY `created_at` DESC , `amount`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS user_transactions_view');
    }
}
