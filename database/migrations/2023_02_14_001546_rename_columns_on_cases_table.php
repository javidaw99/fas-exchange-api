<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsOnCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function (Blueprint $table) {
            $table->renameColumn('ref_type', 'reference_type');
            $table->renameColumn('ref_id', 'reference_id');
            $table->renameColumn('reject_reason_text', 'reject_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function (Blueprint $table) {
            $table->renameColumn('ref_type', 'reference_type');
            $table->renameColumn('ref_id', 'reference_id');
            $table->renameColumn('reject_reason', 'reject_reason_text');
        });
    }
}
