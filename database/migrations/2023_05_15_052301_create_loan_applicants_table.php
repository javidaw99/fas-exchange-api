<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_applicants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level')->default(1);
            $table->string('facebook_profile', 255)->nullable();
            $table->string('emergency_contact_no', 45)->nullable();
            $table->foreignId('user_id');
            $table->boolean('is_blacklisted')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_applicants');
    }
}
