<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transactions');
        //
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('case_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('reference_user_id')->comment('For transfer record, transactions will need 2 records: transfer out from sender(- amount), receiver receive money(+ amount). In order to get transaction record, both transaction records\' user id should be different. E.g: User id A\'s amount = -50, User id B\'s amount = +50.')->nullable();
            $table->string('reference_table', 255);
            $table->integer('reference_id');
            $table->foreignId('user_account_id');
            $table->decimal('old_balance', 18, 6)->nullable();
            $table->decimal('new_balance', 18, 6)->nullable();
            $table->decimal('amount', 18, 6)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedInteger('account_id');
            $table->unsignedInteger('currency_id');
            $table->decimal('rate', 18, 4);

            $table->dropColumn('to_user_id');
            $table->dropColumn('reference_table');
            $table->dropColumn('reference_id');
            $table->dropColumn('user_account_id');
        });
    }
}
