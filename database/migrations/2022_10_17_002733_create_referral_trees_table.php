<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateReferralTreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_trees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ancestor_user_id')->index('ancestor_user_id');
            $table->unsignedInteger('descendant_user_id')->index('descendant_user_id');
            $table->unsignedInteger('depth');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

        $users = DB::table('users')->whereNull('deleted_at')->get();
        
        $insertArr = [];

        foreach ($users as $user) {
            $insertArr[] = [
                'ancestor_user_id' => $user->id,
                'descendant_user_id' => $user->id,
                'depth' => 0,
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()')
            ];
        }

        DB::table('referral_trees')->insert($insertArr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_trees');
    }
}
