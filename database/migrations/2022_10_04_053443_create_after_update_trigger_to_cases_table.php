<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfterUpdateTriggerToCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER after_cases_update
        AFTER UPDATE
        ON cases FOR EACH ROW
        BEGIN
            IF (OLD.status <> NEW.status)
            THEN
            INSERT INTO case_logs
             SET id = OLD.id,
                user_id = OLD.user_id,
                account_id = OLD.account_id,
                to_account_id = OLD.to_account_id,
                assigned_user_id = OLD.assigned_user_id,
                ref_id = OLD.ref_id,
                ref_type = OLD.ref_type,
                status = NEW.status,
                reject_reason_text = NEW.reject_reason_text,
                created_at = NOW();
            ELSEIF (OLD.assigned_user_id <> NEW.assigned_user_id)
            THEN
            INSERT INTO case_logs
             SET id = OLD.id,
                user_id = OLD.user_id,
                account_id = OLD.account_id,
                to_account_id = OLD.to_account_id,
                assigned_user_id = NEW.assigned_user_id,
                ref_id = OLD.ref_id,
                ref_type = OLD.ref_type,
                status = OLD.status,
                reject_reason_text = OLD.reject_reason_text,
                created_at = NOW();
            END IF;
            IF (OLD.status <> NEW.status AND OLD.assigned_user_id <> NEW.assigned_user_id)
            THEN
            INSERT INTO case_logs
             SET id = OLD.id,
                user_id = OLD.user_id,
                account_id = OLD.account_id,
                to_account_id = OLD.to_account_id,
                assigned_user_id = NEW.assigned_user_id,
                ref_id = OLD.ref_id,
                ref_type = OLD.ref_type,
                status = NEW.status,
                reject_reason_text = NEW.reject_reason_text,
                created_at = NOW();
            END IF;
        END'
    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_cases_update`');
    }
}
