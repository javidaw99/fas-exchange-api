<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropOperationHoursOperationDaysFromMoneyChangersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('money_changers', function (Blueprint $table) {
            $table->dropColumn('operation_days');
            $table->dropColumn('operation_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('money_changers', function (Blueprint $table) {
            $table->string('operation_days')->nullable();
            $table->string('operation_hours')->nullable();
        });
    }
}
