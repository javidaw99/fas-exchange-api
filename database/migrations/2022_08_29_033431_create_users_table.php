<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 255)->nullable();
            $table->string('fullname', 255);
            $table->string('email', 255);
            $table->string('phone_no', 45);
            $table->string('password', 255);
            $table->boolean('is_temp_password')->default('0');
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('country_id');
            $table->boolean('is_tfa_enabled')->default('0');
            $table->string('tfa_secret')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();

            // $table->foreign('country_id')->references('id')->on('countries');
            // $table->foreign('role_id')->references('id')->on('roles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
