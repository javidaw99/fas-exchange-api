<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserTransactionDetailsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('DROP VIEW IF EXISTS user_transaction_details_view');
        DB::unprepared('
        CREATE VIEW `user_transaction_details_view` AS
        SELECT 
            `txn`.`user_id` AS `user_id`,
            `u`.`fullname` AS `user_fullname`,
            `txn`.`reference_user_id` AS `reference_user_id`,
            `ru`.`fullname` AS `reference_username`,
            `txn`.`reference_table` AS `reference_table`,
            `txn`.`reference_id` AS `reference_id`,
            `t`.`type` AS `transaction_type`,
            `txn`.`user_account_id` AS `user_account_id`,
            `t`.`to_user_account_id` AS `to_user_account_id`,
            `t`.`user_bank_account_id` AS `user_bank_account_id`,
            `uba`.`reference_name` AS `user_bank_account_name`,
            `uba`.`reference_bank_account` AS `user_bank_account_number`,
            `uba`.`bank_id` AS `bank_id`,
            `uba`.`country_id` AS `user_bank_account_country_id`,
            `co`.`name` AS `user_bank_account_country_name`,
            `uba`.`type` AS `user_bank_account_type`,
            `txn`.`amount` AS `amount`,
            `t`.`processing_fee` AS `processing_fee`,
            `t`.`exchange_rate_id` AS `exchange_rate_id`,
            `e`.`rate` AS `exchange_rate`,
            `t`.`converted_amount` AS `converted_amount`,
            `t`.`from_currency_id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `t`.`to_currency_id` AS `to_currency_id`,
            `ct`.`iso_code` AS `to_currency_code`,
            `txn`.`created_at` AS `created_at`
        FROM
            ((((((((((`transactions` `txn`
            LEFT JOIN `user_accounts` `ua` ON (`txn`.`user_id` = `ua`.`user_id`))
            LEFT JOIN `currencies` `c` ON (`ua`.`currency_id` = `c`.`id`))
            LEFT JOIN `transfers` `t` ON (`txn`.`reference_id` = `t`.`id`))
            LEFT JOIN `currencies` `cf` ON (`t`.`from_currency_id` = `cf`.`id`))
            LEFT JOIN `currencies` `ct` ON (`t`.`to_currency_id` = `ct`.`id`))
            LEFT JOIN `users` `ru` ON (`txn`.`reference_user_id` = `ru`.`id`))
            LEFT JOIN `users` `u` ON (`txn`.`user_id` = `u`.`id`))
            LEFT JOIN `user_bank_accounts` `uba` ON (`t`.`user_bank_account_id` = `uba`.`id`))
            LEFT JOIN `countries` `co` ON (`uba`.`currency_id` = `co`.`id`))
            LEFT JOIN `exchange_rates` `e` ON (`t`.`exchange_rate_id` = `e`.`id`))
        WHERE
            `txn`.`reference_table` = \'transfer\' 
        UNION ALL SELECT 
            `txn`.`user_id` AS `user_id`,
            `u`.`fullname` AS `user_fullname`,
            `txn`.`reference_user_id` AS `reference_user_id`,
            `ru`.`fullname` AS `reference_username`,
            `txn`.`reference_table` AS `reference_table`,
            `txn`.`reference_id` AS `reference_id`,
            \'RELOAD\' AS `transaction_type`,
            `txn`.`user_account_id` AS `user_account_id`,
            NULL AS `to_user_account_id`,
            NULL AS `user_bank_account_id`,
            NULL AS `user_bank_account_name`,
            NULL AS `user_bank_account_number`,
            NULL AS `bank_id`,
            NULL AS `user_bank_account_country_id`,
            NULL AS `user_bank_account_country_name`,
            NULL AS `user_bank_account_type`,
            `txn`.`amount` AS `amount`,
            NULL AS `processing_fee`,
            NULL AS `exchange_rate_id`,
            NULL AS `exchange_rate`,
            NULL AS `converted_amount`,
            `r`.`currency_id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `r`.`currency_id` AS `to_currency_id`,
            `cf`.`iso_code` AS `to_currency_code`,
            `txn`.`created_at` AS `created_at`
        FROM
            ((((((`transactions` `txn`
            LEFT JOIN `user_accounts` `ua` ON (`txn`.`user_id` = `ua`.`user_id`))
            LEFT JOIN `currencies` `c` ON (`ua`.`currency_id` = `c`.`id`))
            LEFT JOIN `reloads` `r` ON (`txn`.`reference_id` = `r`.`id`))
            LEFT JOIN `currencies` `cf` ON (`r`.`currency_id` = `cf`.`id`))
            LEFT JOIN `users` `ru` ON (`txn`.`reference_user_id` = `ru`.`id`))
            LEFT JOIN `users` `u` ON (`txn`.`user_id` = `u`.`id`))
        WHERE
            `txn`.`reference_table` = \'reload\'
        ORDER BY `created_at` DESC
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS user_transaction_details_view');
    }
}
