<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTkBankAccountIdAndTkReferenceNoToTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->integer('tk_bank_account_id')->nullable()->after('to_user_account_id');
            $table->string('tk_reference_no', 255)->nullable()->after('tk_bank_account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropColumn('tk_bank_account_id');
            $table->dropColumn('tk_reference_no');
        });
    }
}
