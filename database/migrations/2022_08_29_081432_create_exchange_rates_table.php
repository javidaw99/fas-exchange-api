<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('from_currency_id');
            $table->unsignedInteger('to_currency_id');
            $table->unsignedInteger('created_by');
            $table->decimal('rate',18 ,4);
            $table->date('effective_date')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();

            // $table->foreign('created_by')->references('id')->on('users');
            // $table->foreign('from_currency_id')->references('id')->on('currencies');
            // $table->foreign('to_currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_rates');
    }
}
