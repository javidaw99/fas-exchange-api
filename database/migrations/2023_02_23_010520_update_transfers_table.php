<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transfers');

        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 45);
            $table->string('quote_mode', 45);
            $table->foreignId('quote_id');
            $table->foreignId('from_currency_id');
            $table->foreignId('to_currency_id');
            $table->integer('user_bank_account_id')->nullable();
            $table->foreignId('exchange_rate_id')->nullable();
            $table->foreignId('base_fixed_rate_id')->nullable();
            $table->foreignId('tiered_percentage_rate_id')->nullable();
            $table->decimal('amount', 18, 4);
            $table->decimal('processing_fee', 18, 4);
            $table->decimal('converted_amount', 18, 4);
            $table->foreignId('user_id');
            $table->foreignId('to_user_id')->nullable();
            $table->foreignId('user_account_id');
            $table->foreignId('to_user_account_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function (Blueprint $table) {

            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('recipient_country_id');
            $table->unsignedInteger('recipient_bank_country_id');
            $table->string('purpose', 255)->nullable();
            $table->string('recipient_acc_name', 255);
            $table->string('recipient_acc_no', 255);

            $table->dropColumn('type');
            $table->dropColumn('quote_mode');
            $table->dropColumn('quote_id');
            $table->dropColumn('from_currency_id');
            $table->dropColumn('to_currency_id');
            $table->dropColumn('exchange_rate_id');
            $table->dropColumn('user_bank_account_id');
            $table->dropColumn('base_fixed_rate_id');
            $table->dropColumn('tiered_percentage_rate_id');
            $table->dropColumn('processing_fee');
            $table->dropColumn('converted_amount');
            $table->dropColumn('user_id');
            $table->dropColumn('to_user_id');
            $table->dropColumn('user_account_id');
            $table->dropColumn('to_user_account_id');
        });
    }
}
