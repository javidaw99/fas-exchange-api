<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medias', function (Blueprint $table) {
            $table->foreignId('reference_id')->after('mime');
            $table->string('reference_table', 255)->after('reference_id');
            $table->string('type', 255)->after('reference_table')->comment('type of media: kyc, receipt, icon, background etc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function (Blueprint $table) {
            $table->dropColumn('reference_id');
            $table->dropColumn('reference_table');
            $table->dropColumn('type');
        });
    }
}
