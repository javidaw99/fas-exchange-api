<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cases', function (Blueprint $table) {
            $table->dropColumn('reference_type');
            $table->string('reference_table', 255)->after('reference_id');
            $table->foreignId('user_account_id')->after('reference_table');
            $table->boolean('status', 4)->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cases', function (Blueprint $table) {
            $table->string('reference_type', 64);
            $table->dropColumn('reference_table');
            $table->dropColumn('user_account_id');
            $table->boolean('status', 4)->nullable()->change();
        });
    }
}
