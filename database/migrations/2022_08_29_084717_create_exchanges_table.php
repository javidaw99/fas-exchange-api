<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('from_currency_id');
            $table->unsignedInteger('to_currency_id');
            $table->unsignedInteger('exchange_rate_id');
            $table->unsignedInteger('from_account_id');
            $table->unsignedInteger('to_account_id');
            $table->decimal('from_amount', 18, 4);
            $table->decimal('to_amount', 18, 4);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();

            // $table->foreign('from_currency_id')->references('id')->on('currencies');
            // $table->foreign('to_currency_id')->references('id')->on('currencies');
            // $table->foreign('exchange_rate_id')->references('id')->on('exchange_rates');
            // $table->foreign('from_account_id')->references('id')->on('accounts');
            // $table->foreign('to_account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchanges');
    }
}
