<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReloadCasesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP VIEW IF EXISTS reload_cases_view');

        DB::unprepared('
        CREATE VIEW `reload_cases_view` AS
        SELECT 
            `cases`.`id` AS `id`,
            `cases`.`reference_table` AS `reference_table`,
            `cases`.`reference_id` AS `ref_id`,
            `cases`.`status` AS `status`,
            `cases`.`created_at` AS `created_at`,
            `cases`.`assigned_user_id` AS `assigned_user_id`,
            `assigned_users`.`fullname` AS `assigned_user_fullname`,
            `users`.`id` AS `user_id`,
            `users`.`fullname` AS `user_fullname`,
            `users`.`email` AS `user_email`,
            `users`.`phone_no` AS `user_phone_no`,
            `acc_currency`.`id` AS `user_account_id`,
            `acc_currency`.`iso_code` AS `user_account_currency`,
            `reloads`.`amount` AS `amount`,
            `reloads`.`reference_no` AS `reference_no`,
            `reload_currency`.`id` AS `reload_currency_id`,
            `reload_currency`.`iso_code` AS `reload_currency_iso_code`,
            `bank_accounts`.`id` AS `bank_account_id`,
            `bank_accounts`.`acc_no` AS `bank_account_no`,
            `bank_accounts`.`acc_name` AS `bank_account_name`,
            `bank_country`.`branch_name` AS `bank_branch_name`,
            `medias`.`path` AS `file_path`
        FROM
            (((((((((`cases`
            LEFT JOIN `reloads` ON (`cases`.`reference_id` = `reloads`.`id`))
            LEFT JOIN `users` ON (`cases`.`user_id` = `users`.`id`))
            LEFT JOIN `users` `assigned_users` ON (`cases`.`assigned_user_id` = `assigned_users`.`id`))
            LEFT JOIN `accounts` ON (`cases`.`user_account_id` = `accounts`.`id`))
            LEFT JOIN `currencies` `acc_currency` ON (`accounts`.`currency_id` = `acc_currency`.`id`))
            LEFT JOIN `currencies` `reload_currency` ON (`reloads`.`currency_id` = `reload_currency`.`id`))
            LEFT JOIN `bank_accounts` ON (`reloads`.`bank_account_id` = `bank_accounts`.`id`))
            LEFT JOIN `bank_country` ON (`bank_accounts`.`bank_country_id` = `bank_country`.`id`))
            LEFT JOIN `medias` ON (`medias`.`reference_table` = "reload"
                AND `reloads`.`id` = `medias`.`reference_id`))
        WHERE
            `cases`.`reference_table` = "reload"
                AND (`cases`.`status` = 1
                OR `cases`.`status` = 2)
                AND `cases`.`deleted_at` IS NULL
        ORDER BY `cases`.`created_at`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS reload_cases_view');
    }
}
