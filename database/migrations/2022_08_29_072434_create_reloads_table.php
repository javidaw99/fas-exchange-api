<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reloads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bank_acc_id');
            $table->unsignedInteger('currency_id');
            $table->decimal('amount', 18, 4);
            $table->string('reference_no', 255);
            $table->string('receipt', 255);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();

            // $table->foreign('bank_acc_id')->references('id')->on('bank_accounts');
            // $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reloads');
    }
}
