<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFasTranfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fas_tranfers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transfers_request_id');
            $table->unsignedInteger('bank_account_id');
            $table->unsignedInteger('amount');
            $table->boolean('is_active')->default('0');
            $table->date('paid_at')->nullable();
            $table->boolean('status',4)->nullable();
            $table->string('day', 255);
            $table->string('exchange_rate_id', 255);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fas_tranfers');
    }
}
