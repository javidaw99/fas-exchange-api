<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameExchangeMarginRateColumnForTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->renameColumn('exchange_margin_rate', 'exchange_rate_margin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exchange_margin_rates', function (Blueprint $table) {
            $table->renameColumn('exchange_rate_margin', 'exchange_margin_rate');
        });
    }
}
