<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserExchangeQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_exchange_quotes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('from_currency_id');
            $table->foreignId('to_currency_id');
            $table->foreignId('base_fixed_rate_id')->nullable();
            $table->foreignId('tiered_percentage_rate_id')->nullable();
            $table->foreignId('exchange_rate_id')->nullable();
            $table->foreignId('user_id');
            $table->integer('expired_at');
            $table->uuid('quote_uuid');
            $table->string('mode', 45);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_exchange_quotes');
    }
}
