<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeleteUserRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_user_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_valid');
            $table->string('username', 255);
            $table->string('topkash_id', 255);
            $table->string('phone_no', 45);
            $table->tinyInteger('status')->default(1);
            $table->unsignedInteger('assigned_user_id')->nullable();
            $table->text('reason')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delete_user_requests');
    }
}
