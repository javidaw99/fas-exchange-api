<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExchangeMarginRateColumnsInTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->unsignedInteger('exchange_margin_rate_id')->after('exchange_rate_id');
            $table->decimal('exchange_margin_rate', 21, 8)->after('converted_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropColumn('exchange_margin_rate_id');
            $table->dropColumn('exchange_margin_rate');
        });
    }
}
