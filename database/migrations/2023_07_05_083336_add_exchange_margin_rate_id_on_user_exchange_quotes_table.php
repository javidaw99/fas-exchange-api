<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExchangeMarginRateIdOnUserExchangeQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_exchange_quotes', function (Blueprint $table) {
            $table->unsignedInteger('exchange_margin_rate_id')->nullable()->after('exchange_rate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_exchange_quotes', function (Blueprint $table) {
            $table->dropColumn('exchange_margin_rate_id');
        });
    }
}
