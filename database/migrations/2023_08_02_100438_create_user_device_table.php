<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand', 45)->nullable();
            $table->string('model', 45)->nullable();
            $table->string('sys_version', 45)->nullable();
            $table->string('sys_name', 45)->nullable();
            $table->string('sys_build_id', 128)->nullable();
            $table->string('sys_build_number', 128)->nullable();
            $table->string('bundle_id', 128)->nullable();
            $table->string('carrier', 128)->nullable();
            $table->string('device_id', 128)->nullable();
            $table->string('device_type', 128)->nullable();
            $table->string('device_name', 128)->nullable();
            $table->string('ip_address', 45)->nullable();
            $table->string('mac_address', 128)->nullable();
            $table->string('manufacturer', 128)->nullable();
            $table->string('unique_id', 45)->nullable();
            $table->string('user_agent', 255)->nullable();
            $table->string('version', 45)->nullable();
            $table->string('storage', 45)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_devices');
    }
}
