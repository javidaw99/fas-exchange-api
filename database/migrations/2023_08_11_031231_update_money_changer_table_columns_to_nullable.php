<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMoneyChangerTableColumnsToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('money_changers', function (Blueprint $table) {
            $table->string('latitude', 255)->nullable()->change();
            $table->string('longitude', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('money_changers', function (Blueprint $table) {
            $table->string('latitude', 255)->nullable(false)->change();
            $table->string('longitude', 255)->nullable(false)->change();
        });
    }
}
