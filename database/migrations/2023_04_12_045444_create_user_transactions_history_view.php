<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTransactionsHistoryView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP VIEW IF EXISTS user_transactions_history_view');

        DB::unprepared('
            CREATE VIEW `user_transactions_history_view` AS
            SELECT 
                `txn`.`user_id` AS `user_id`,
                `txn`.`reference_user_id` AS `reference_user_id`,
                `ru`.`username` AS `reference_username`,
                `txn`.`reference_table` AS `reference_table`,
                `txn`.`reference_id` AS `reference_id`,
                CONVERT( \'3\' USING UTF8) AS `status`,
                `t`.`type` AS `transaction_type`,
                `b`.`name` AS `transfer_bank`,
                `t`.`user_bank_account_id` AS `user_bank_account_id`,
                `uba`.`reference_name` AS `user_bank_account_name`,
                `uba`.`type` AS `user_bank_account_type`,
                NULL AS `bank_account_id`,
                NULL AS `branch_name`,
                NULL AS `reload_bank`,
                NULL AS `acc_name`,
                NULL AS `acc_no`,
                NULL AS `reference_no`,
                `txn`.`user_account_id` AS `user_account_id`,
                `txn`.`old_balance` AS `old_balance`,
                `txn`.`new_balance` AS `new_balance`,
                `txn`.`amount` AS `amount`,
                `t`.`processing_fee` AS `processing_fee`,
                `t`.`from_currency_id` AS `from_currency_id`,
                `cf`.`iso_code` AS `from_currency_code`,
                `t`.`to_currency_id` AS `to_currency_id`,
                `ct`.`iso_code` AS `to_currency_code`,
                `txn`.`created_at` AS `created_at`
            FROM
                (((((((`transactions` `txn`
                LEFT JOIN `transfers` `t` ON (`txn`.`reference_id` = `t`.`id`))
                LEFT JOIN `currencies` `cf` ON (`t`.`from_currency_id` = `cf`.`id`))
                LEFT JOIN `currencies` `ct` ON (`t`.`to_currency_id` = `ct`.`id`))
                LEFT JOIN `users` `ru` ON (`txn`.`reference_user_id` = `ru`.`id`))
                LEFT JOIN `users` `u` ON (`txn`.`user_id` = `u`.`id`))
                LEFT JOIN `user_bank_accounts` `uba` ON (`t`.`user_bank_account_id` = `uba`.`id`))
                LEFT JOIN `banks` `b` ON (`uba`.`bank_id` = `b`.`id`))
            WHERE
                `txn`.`reference_table` = \'transfer\' 
            UNION ALL SELECT 
                `txn`.`user_id` AS `user_id`,
                `txn`.`reference_user_id` AS `reference_user_id`,
                `ru`.`username` AS `reference_username`,
                `txn`.`reference_table` AS `reference_table`,
                `txn`.`reference_id` AS `reference_id`,
                CONVERT( \'3\' USING UTF8) AS `status`,
                \'RELOAD\' AS `transaction_type`,
                NULL AS `user_bank_account_id`,
                NULL AS `user_bank_account_name`,
                NULL AS `user_bank_account_type`,
                NULL AS `transfer_bank`,
                `r`.`bank_account_id` AS `bank_account_id`,
                `bc`.`branch_name` AS `branch_name`,
                `b2`.`name` AS `reload_bank`,
                `ba`.`acc_name` AS `acc_name`,
                `ba`.`acc_no` AS `acc_no`,
                `r`.`reference_no` AS `reference_no`,
                `txn`.`user_account_id` AS `user_account_id`,
                `txn`.`old_balance` AS `old_balance`,
                `txn`.`new_balance` AS `new_balance`,
                `txn`.`amount` AS `amount`,
                NULL AS `processing_fee`,
                `r`.`currency_id` AS `from_currency_id`,
                `cf`.`iso_code` AS `from_currency_code`,
                `r`.`currency_id` AS `to_currency_id`,
                `cf`.`iso_code` AS `to_currency_code`,
                `txn`.`created_at` AS `created_at`
            FROM
                (((((((`transactions` `txn`
                LEFT JOIN `reloads` `r` ON (`txn`.`reference_id` = `r`.`id`))
                LEFT JOIN `bank_accounts` `ba` ON (`r`.`bank_account_id` = `ba`.`id`))
                LEFT JOIN `bank_country` `bc` ON (`ba`.`bank_country_id` = `bc`.`id`))
                LEFT JOIN `banks` `b2` ON (`bc`.`bank_id` = `b2`.`id`))
                LEFT JOIN `currencies` `cf` ON (`r`.`currency_id` = `cf`.`id`))
                LEFT JOIN `users` `ru` ON (`txn`.`reference_user_id` = `ru`.`id`))
                LEFT JOIN `users` `u` ON (`txn`.`user_id` = `u`.`id`))
            WHERE
                `txn`.`reference_table` = \'reload\' 
            UNION ALL SELECT 
                `cs`.`user_id` AS `user_id`,
                `t`.`to_user_id` AS `reference_user_id`,
                `ru`.`username` AS `reference_username`,
                `cs`.`reference_table` AS `reference_table`,
                `cs`.`reference_id` AS `reference_id`,
                CONVERT( \'1\' USING UTF8) AS `status`,
                `t`.`type` AS `transaction_type`,
                `t`.`user_bank_account_id` AS `user_bank_account_id`,
                `uba`.`reference_name` AS `user_bank_account_name`,
                `uba`.`type` AS `user_bank_account_type`,
                `b`.`name` AS `transfer_bank`,
                NULL AS `bank_account_id`,
                NULL AS `branch_name`,
                NULL AS `reload_bank`,
                NULL AS `acc_name`,
                NULL AS `acc_no`,
                NULL AS `reference_no`,
                `cs`.`user_account_id` AS `user_account_id`,
                `tc`.`old_balance` AS `old_balance`,
                `tc`.`new_balance` AS `new_balance`,
                `t`.`amount` AS `amount`,
                `t`.`processing_fee` AS `processing_fee`,
                `t`.`from_currency_id` AS `from_currency_id`,
                `cf`.`iso_code` AS `from_currency_code`,
                `t`.`to_currency_id` AS `to_currency_id`,
                `ct`.`iso_code` AS `to_currency_code`,
                `cs`.`created_at` AS `created_at`
            FROM
                ((((((((`cases` `cs`
                LEFT JOIN `transfers` `t` ON (`cs`.`reference_id` = `t`.`id`))
                LEFT JOIN `transactions` `tc` ON (`t`.`id` = `tc`.`reference_id`))
                LEFT JOIN `currencies` `cf` ON (`t`.`from_currency_id` = `cf`.`id`))
                LEFT JOIN `currencies` `ct` ON (`t`.`to_currency_id` = `ct`.`id`))
                LEFT JOIN `users` `ru` ON (`t`.`to_user_id` = `ru`.`id`))
                LEFT JOIN `users` `u` ON (`t`.`user_id` = `u`.`id`))
                LEFT JOIN `user_bank_accounts` `uba` ON (`t`.`user_bank_account_id` = `uba`.`id`))
                LEFT JOIN `banks` `b` ON (`uba`.`bank_id` = `b`.`id`))
            WHERE
                `cs`.`reference_table` = \'transfer\'
                    AND `cs`.`status` = 1 
            UNION ALL SELECT 
                `cs`.`user_id` AS `user_id`,
                `cs`.`user_id` AS `reference_user_id`,
                `ru`.`username` AS `reference_username`,
                `cs`.`reference_table` AS `reference_table`,
                `cs`.`reference_id` AS `reference_id`,
                CONVERT( \'1\' USING UTF8) AS `status`,
                \'RELOAD\' AS `transaction_type`,
                NULL AS `user_bank_account_id`,
                NULL AS `user_bank_account_name`,
                NULL AS `user_bank_account_type`,
                NULL AS `transfer_bank`,
                `r`.`bank_account_id` AS `bank_account_id`,
                `bc`.`branch_name` AS `branch_name`,
                `b2`.`name` AS `reload_bank`,
                `ba`.`acc_name` AS `acc_name`,
                `ba`.`acc_no` AS `acc_no`,
                `r`.`reference_no` AS `reference_no`,
                `cs`.`user_account_id` AS `user_account_id`,
                `t`.`old_balance` AS `old_balance`,
                `t`.`new_balance` AS `new_balance`,
                `r`.`amount` AS `amount`,
                NULL AS `processing_fee`,
                `r`.`currency_id` AS `from_currency_id`,
                `cf`.`iso_code` AS `from_currency_code`,
                `r`.`currency_id` AS `to_currency_id`,
                `cf`.`iso_code` AS `to_currency_code`,
                `cs`.`created_at` AS `created_at`
            FROM
                ((((((((`cases` `cs`
                LEFT JOIN `reloads` `r` ON (`cs`.`reference_id` = `r`.`id`))
                LEFT JOIN `transactions` `t` ON (`r`.`id` = `t`.`reference_id`))
                LEFT JOIN `bank_accounts` `ba` ON (`r`.`bank_account_id` = `ba`.`id`))
                LEFT JOIN `bank_country` `bc` ON (`ba`.`bank_country_id` = `bc`.`id`))
                LEFT JOIN `banks` `b2` ON (`bc`.`bank_id` = `b2`.`id`))
                LEFT JOIN `currencies` `cf` ON (`r`.`currency_id` = `cf`.`id`))
                LEFT JOIN `users` `ru` ON (`r`.`user_id` = `ru`.`id`))
                LEFT JOIN `users` `u` ON (`cs`.`user_id` = `u`.`id`))
            WHERE
                `cs`.`reference_table` = \'reload\'
                    AND `cs`.`status` = 1
            ORDER BY `created_at` DESC , `amount`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS user_transactions_history_view');
    }
}
