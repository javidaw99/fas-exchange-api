<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTransfersView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('DROP VIEW IF EXISTS transfers_view');

        DB::unprepared('
        CREATE VIEW `transfers_view` AS
        SELECT 
            `t`.`id` AS `id`,
            `t`.`type` AS `type`,
            `t`.`quote_mode` AS `quote_mode`,
            `t`.`user_bank_account_id` AS `user_bank_account_id`,
            `uba`.`reference_name` AS `reference_bank_account_name`,
            `uba`.`reference_bank_account` AS `reference_bank_account_no`,
            `uba`.`type` AS `user_bank_account_type`,
            `b`.`name` AS `bank_name`,
            `c`.`name` AS `country_name`,
            `cf`.`id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `ct`.`id` AS `to_currency_id`,
            `ct`.`iso_code` AS `to_currency_code`,
            `t`.`exchange_rate_id` AS `exchange_rate_id`,
            `e`.`rate` AS `exchange_rate`,
            `t`.`base_fixed_rate_id` AS `base_fixed_rate_id`,
            `t`.`tiered_percentage_rate_id` AS `tiered_percentage_rate_id`,
            `t`.`amount` AS `amount`,
            `t`.`processing_fee` AS `processing_fee`,
            `t`.`converted_amount` AS `converted_amount`,
            `t`.`user_id` AS `user_id`,
            `t`.`to_user_id` AS `to_user_id`,
            `u`.`fullname` AS `to_user_fullname`,
            `t`.`user_account_id` AS `user_account_id`,
            `t`.`to_user_account_id` AS `to_user_account_id`,
            `t`.`created_at` AS `created_at`
        FROM
            (((((((`transfers` `t`
            LEFT JOIN `currencies` `cf` ON (`t`.`from_currency_id` = `cf`.`id`))
            LEFT JOIN `currencies` `ct` ON (`t`.`to_currency_id` = `ct`.`id`))
            LEFT JOIN `user_bank_accounts` `uba` ON (`t`.`user_bank_account_id` = `uba`.`id`))
            LEFT JOIN `banks` `b` ON (`uba`.`bank_id` = `b`.`id`))
            LEFT JOIN `countries` `c` ON (`uba`.`country_id` = `c`.`id`))
            LEFT JOIN `exchange_rates` `e` ON (`t`.`exchange_rate_id` = `e`.`id`))
            LEFT JOIN `users` `u` ON (`t`.`to_user_id` = `u`.`id`))
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS transfers_view');
    }
}
