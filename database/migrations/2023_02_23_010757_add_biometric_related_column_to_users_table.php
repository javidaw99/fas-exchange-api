<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBiometricRelatedColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('biometric_public_key', 255)->nullable()->after('tfa_secret');
            $table->string('app_pin', 255)->nullable()->after('biometric_public_key');
            $table->uuid('secret_key')->after('app_pin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('biometric_public_key');
            $table->dropColumn('app_pin');
            $table->dropColumn('secret_key');
        });
    }
}
