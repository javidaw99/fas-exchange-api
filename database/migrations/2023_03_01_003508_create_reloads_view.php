<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateReloadsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('DROP VIEW IF EXISTS reloads_view');

        DB::unprepared('
        CREATE VIEW `reloads_view` AS
        SELECT 
            `r`.`id` AS `id`,
            `r`.`user_id` AS `user_id`,
            `r`.`bank_account_id` AS `bank_account_id`,
            `r`.`currency_id` AS `currency_id`,
            `c`.`iso_code` AS `currency_code`,
            `r`.`amount` AS `amount`,
            `r`.`user_account_id` AS `user_account_id`,
            `m`.`filename` AS `filename`,
            `m`.`path` AS `path`,
            `m`.`mime` AS `mime`,
            `r`.`created_at` AS `created_at`
        FROM
            ((`reloads` `r`
            LEFT JOIN `currencies` `c` ON (`r`.`currency_id` = `c`.`id`))
            LEFT JOIN `medias` `m` ON (`r`.`id` = `m`.`reference_id`
                AND `m`.`reference_table` = \'reload\'))
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS reloads_view');
    }
}
