<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bank_country_id');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('currency_id');
            $table->string('acc_no', 255);
            $table->string('acc_name', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();

            // $table->foreign('bank_country_id')->references('id')->on('bank_country');
            // $table->foreign('country_id')->references('id')->on('countries');
            // $table->foreign('currency_id')->references('id')->on('currencies');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
