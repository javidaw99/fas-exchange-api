<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->foreignId('applicant_id');
            $table->foreignId('package_id');
            $table->foreignId('to_bank_id');
            $table->string('to_bank_account_no', 255);
            $table->foreignId('from_bank_account_id', 255)->nullable();
            $table->integer('status')->default(1);
            $table->integer('assigned_user_id')->nullable();
            $table->foreignId('assigned_staff_id')->nullable();
            $table->integer('loan_daily_penalty_percentage')->nullable();
            $table->decimal('loan_first_time_penalty', 18, 4)->nullable();
            $table->decimal('loan_received_credit', 18, 4)->nullable();
            $table->timestamp('loan_start_date')->nullable();
            $table->timestamp('loan_due_date')->nullable();
            $table->integer('loan_status')->nullable();
            $table->foreignId('currency_id');
            $table->integer('exp');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_applications');
    }
}
