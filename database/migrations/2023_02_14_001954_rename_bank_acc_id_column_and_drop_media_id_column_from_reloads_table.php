<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameBankAccIdColumnAndDropMediaIdColumnFromReloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reloads', function (Blueprint $table) {
            $table->renameColumn('bank_acc_id', 'bank_account_id');
            $table->dropColumn('media_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reloads', function (Blueprint $table) {
            $table->renameColumn('bank_account_id', 'bank_acc_id');
            $table->string('media_id', 255);
        });
    }
}
