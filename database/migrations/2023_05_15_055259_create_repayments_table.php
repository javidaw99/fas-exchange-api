<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayments', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('repaid_amount', 18, 4)->nullable();
            $table->decimal('remaining_amount', 18, 4)->nullable();
            $table->foreignId('currency_id');
            $table->boolean('is_due')->default(0);
            $table->decimal('penalty', 18, 4)->nullable();
            $table->timestamp('repay_date')->nullable();
            $table->foreignId('from_bank_id');
            $table->string('from_bank_accont_no', 255);
            $table->foreignId('loan_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayments');
    }
}
