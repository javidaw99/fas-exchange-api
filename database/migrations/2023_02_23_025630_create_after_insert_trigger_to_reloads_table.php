<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAfterInsertTriggerToReloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('
        CREATE TRIGGER reloads_AFTER_INSERT 
        AFTER INSERT 
        ON reloads
        FOR EACH ROW
        BEGIN
        INSERT INTO cases (user_id, reference_table, reference_id, user_account_id)
        VALUES (NEW.user_id, \'reload\', NEW.id, NEW.user_account_id);
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS reloads_AFTER_INSERT');
    }
}
