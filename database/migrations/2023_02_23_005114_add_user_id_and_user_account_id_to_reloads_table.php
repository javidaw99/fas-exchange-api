<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdAndUserAccountIdToReloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reloads', function (Blueprint $table) {
            $table->integer('user_id')->after('id');
            $table->integer('user_account_id')->after('reference_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reloads', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('user_account_id');
        });
    }
}
