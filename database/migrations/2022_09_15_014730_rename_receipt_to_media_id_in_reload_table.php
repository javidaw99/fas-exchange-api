<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameReceiptToMediaIdInReloadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reloads', function (Blueprint $table) {
            $table->renameColumn('receipt', 'media_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reloads', function (Blueprint $table) {
            $table->renameColumn('media_id', 'receipt');
        });
    }
}
