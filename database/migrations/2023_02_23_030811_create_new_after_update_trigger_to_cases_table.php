<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateNewAfterUpdateTriggerToCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::unprepared('
        CREATE TRIGGER cases_AFTER_UPDATE
        AFTER UPDATE 
        ON cases 
        FOR EACH ROW
        BEGIN
			DECLARE old_balance DECIMAL(18, 4);
			DECLARE new_balance DECIMAL(18, 4);
            DECLARE transaction_amount DECIMAL(18,4);
            
			IF NEW.status = 3 AND OLD.status = 1 AND NEW.reference_table = \'reload\'
			THEN
            SELECT amount INTO transaction_amount FROM reloads WHERE id = NEW.reference_id;
            CALL calculate_latest_transaction_amount(
					transaction_amount, 
					NEW.user_id, 
					NEW.user_account_id, 
					old_balance, 
					new_balance
				);
			INSERT INTO transactions (case_id, user_id, reference_user_id, reference_table, reference_id, user_account_id, old_balance, new_balance, amount)
			VALUES (NEW.id, NEW.user_id, NEW.user_id, NEW.reference_table, NEW.reference_id, NEW.user_account_id, old_balance, new_balance, transaction_amount);
			END IF;
            
            IF NEW.status = 3 AND OLD.status = 1 AND NEW.reference_table = \'transfer\'
			THEN            
            SELECT amount INTO transaction_amount FROM transfers WHERE id = NEW.reference_id;
            CALL calculate_latest_transaction_amount(
					-transaction_amount, 
					NEW.user_id, 
					NEW.user_account_id, 
					old_balance, 
					new_balance
				);
			INSERT INTO transactions (case_id, user_id, reference_user_id, reference_table, reference_id, user_account_id, old_balance, new_balance, amount)
			VALUES (NEW.id, NEW.user_id, NEW.user_id, NEW.reference_table, NEW.reference_id, NEW.user_account_id, old_balance, new_balance, transaction_amount);
			END IF;
            
			END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS cases_AFTER_UPDATE');
    }
}
