<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTransferCasesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP VIEW IF EXISTS transfer_cases_view');

        DB::unprepared('
        CREATE VIEW `transfer_cases_view` AS
        SELECT 
            `cases`.`id` AS `id`,
            `cases`.`reference_table` AS `reference_table`,
            `cases`.`reference_id` AS `ref_id`,
            `cases`.`status` AS `status`,
            `cases`.`assigned_user_id` AS `assigned_user_id`,
            `assigned_users`.`fullname` AS `assigned_user_fullname`,
            `users`.`id` AS `user_id`,
            `users`.`fullname` AS `user_fullname`,
            `users`.`email` AS `user_email`,
            `users`.`phone_no` AS `user_phone_no`,
            `transfers`.`type` AS `type`,
            `transfers`.`quote_mode` AS `quote_mode`,
            `transfers`.`user_bank_account_id` AS `user_bank_account_id`,
            `uba`.`reference_name` AS `reference_bank_account_name`,
            `uba`.`reference_bank_account` AS `reference_bank_account_no`,
            `uba`.`type` AS `user_bank_account_type`,
            `b`.`name` AS `bank_name`,
            `c`.`name` AS `country_name`,
            `cf`.`id` AS `from_currency_id`,
            `cf`.`iso_code` AS `from_currency_code`,
            `ct`.`id` AS `to_currency_id`,
            `ct`.`iso_code` AS `to_currency_code`,
            `transfers`.`exchange_rate_id` AS `exchange_rate_id`,
            `e`.`rate` AS `exchange_rate`,
            `transfers`.`base_fixed_rate_id` AS `base_fixed_rate_id`,
            `transfers`.`tiered_percentage_rate_id` AS `tiered_percentage_rate_id`,
            `transfers`.`amount` AS `amount`,
            `transfers`.`processing_fee` AS `processing_fee`,
            `transfers`.`converted_amount` AS `converted_amount`,
            `cases`.`created_at` AS `created_at`
        FROM
            (((((((((`cases`
            LEFT JOIN `transfers` ON (`cases`.`reference_id` = `transfers`.`id`))
            LEFT JOIN `users` ON (`cases`.`user_id` = `users`.`id`))
            LEFT JOIN `users` `assigned_users` ON (`cases`.`assigned_user_id` = `assigned_users`.`id`))
            LEFT JOIN `currencies` `cf` ON (`transfers`.`from_currency_id` = `cf`.`id`))
            LEFT JOIN `currencies` `ct` ON (`transfers`.`to_currency_id` = `ct`.`id`))
            LEFT JOIN `user_bank_accounts` `uba` ON (`transfers`.`user_bank_account_id` = `uba`.`id`))
            LEFT JOIN `banks` `b` ON (`uba`.`bank_id` = `b`.`id`))
            LEFT JOIN `countries` `c` ON (`uba`.`country_id` = `c`.`id`))
            LEFT JOIN `exchange_rates` `e` ON (`transfers`.`exchange_rate_id` = `e`.`id`))
        WHERE
            `cases`.`reference_table` = "transfer"
                AND (`cases`.`status` = 1
                OR `cases`.`status` = 2)
                AND `cases`.`deleted_at` IS NULL
        ORDER BY `cases`.`created_at`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP VIEW IF EXISTS transfer_cases_view');
    }
}
