<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('type', 255);
            $table->integer('level');
            $table->foreignId('currency_id');
            $table->decimal('loan_amount', 18, 4)->nullable();
            $table->integer('loan_day');
            $table->decimal('received_credit', 18, 4)->nullable();
            $table->decimal('first_time_penalty', 18, 4);
            $table->integer('daily_penalty_percentage');
            $table->integer('exp');
            $table->string('status');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_packages');
    }
}
