<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAllAddressColumnToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('address_1', 255)->nullable()->change();
            $table->string('address_2', 255)->nullable()->change();
            $table->string('city', 255)->nullable()->change();
            $table->string('postcode', 255)->nullable()->change();
            $table->unsignedInteger('state_id')->nullable()->change();
            $table->unsignedInteger('country_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('address_1', 255)->nullable(false)->change();
            $table->string('address_2', 255)->nullable(false)->change();
            $table->string('city', 255)->nullable(false)->change();
            $table->string('postcode', 255)->nullable(false)->change();
            $table->unsignedInteger('state_id')->nullable(false)->change();
            $table->unsignedInteger('country_id')->nullable(false)->change();
        });
    }
}
