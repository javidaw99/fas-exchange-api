<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameExchangeMarginRatesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exchange_margin_rates', function (Blueprint $table) {
            $table->renameColumn('updated_at', 'created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exchange_margin_rates', function (Blueprint $table) {
            $table->renameColumn('created_at', 'updated_at');
        });
    }
}
