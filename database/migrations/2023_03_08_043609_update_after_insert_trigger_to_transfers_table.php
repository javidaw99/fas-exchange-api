<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateAfterInsertTriggerToTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS transfers_AFTER_INSERT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('
        CREATE TRIGGER transfers_AFTER_INSERT
        AFTER INSERT 
        ON transfers 
        FOR EACH ROW
        BEGIN
            DECLARE old_balance DECIMAL(18, 4);
            DECLARE new_balance DECIMAL(18, 4);
            DECLARE ref_old_balance DECIMAL(18, 4);
            DECLARE ref_new_balance DECIMAL(18, 4);

			/*
				If quote_mode = PF_XER or PF_ER, insert record into cases table
			*/
			IF NEW.quote_mode = \'PF_XER\' OR NEW.quote_mode = \'PF_ER\'
            THEN
                INSERT INTO cases (user_id, reference_table, reference_id, user_account_id)
                VALUES (NEW.user_id, \'transfer\', NEW.id, NEW.user_account_id);
            END IF;

			/*
				If quote_mode = XPF_XER or XPF_ER + type = CONVERT or XPF_ER + type = SEND
                insert record into transactions table.
                Call calculate_latest_transaction_amount stored procedure to calculate latest transaction amount
                for both sender and receiver, where sender\'s amount is -amount and receiver\'s amount is +amount.
			*/

            IF NEW.quote_mode = \'XPF_XER\' OR (NEW.quote_mode = \'XPF_ER\' AND NEW.type = \'SEND\')
            THEN
				CALL calculate_latest_transaction_amount(
					-NEW.amount, 
					NEW.user_id, 
					NEW.user_account_id, 
					old_balance, 
					new_balance
				);
                
                INSERT INTO transactions (user_id, reference_user_id, reference_table, reference_id, user_account_id, old_balance, new_balance, amount)
                VALUES (NEW.user_id, NEW.to_user_id, \'transfer\', NEW.id, NEW.user_account_id, old_balance, new_balance, NEW.amount);
                
                CALL calculate_latest_transaction_amount(
					NEW.converted_amount, 
					NEW.to_user_id, 
					NEW.to_user_account_id, 
					ref_old_balance, 
					ref_new_balance
				);
                INSERT INTO transactions (user_id, reference_user_id, reference_table, reference_id, user_account_id, old_balance, new_balance, amount)
                VALUES (NEW.to_user_id, NEW.user_id, \'transfer\', NEW.id, NEW.to_user_account_id, ref_old_balance, ref_new_balance, NEW.converted_amount);
                
            END IF;
            
            IF NEW.quote_mode = \'XPF_ER\' AND NEW.type = \'CONVERT\'
            THEN
				CALL calculate_latest_transaction_amount(
					-NEW.amount, 
					NEW.user_id, 
					NEW.user_account_id, 
					old_balance, 
					new_balance
				);
                INSERT INTO transactions (user_id, reference_user_id, reference_table, reference_id, user_account_id, old_balance, new_balance, amount)
                VALUES (NEW.user_id, NEW.user_id, \'transfer\', NEW.id, NEW.user_account_id, old_balance, new_balance, NEW.amount);
                
                CALL calculate_latest_transaction_amount(
					NEW.converted_amount, 
					NEW.user_id, 
					NEW.to_user_account_id, 
					ref_old_balance, 
					ref_new_balance
				);
                INSERT INTO transactions (user_id, reference_user_id, reference_table, reference_id, user_account_id, old_balance, new_balance, amount)
                VALUES (NEW.user_id, NEW.user_id, \'transfer\', NEW.id, NEW.to_user_account_id, ref_old_balance, ref_new_balance, NEW.converted_amount);
            END IF;
            
        END
        ');
    }
}
