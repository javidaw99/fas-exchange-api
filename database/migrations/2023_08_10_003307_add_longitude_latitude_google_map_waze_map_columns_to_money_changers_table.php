<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLongitudeLatitudeGoogleMapWazeMapColumnsToMoneyChangersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('money_changers', function (Blueprint $table) {
            $table->string('longitude')->after('created_at');
            $table->string('latitude')->after('created_at');
            $table->string('google_map')->nullable()->after('created_at');
            $table->string('waze_map')->nullable()->after('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('money_changers', function (Blueprint $table) {
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
            $table->dropColumn('google_map');
            $table->dropColumn('waze_map');
        });
    }
}
