<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_bank_accounts', function (Blueprint $table) {
            $table->string('reference_name')->nullable()->change();
            $table->string('reference_bank_account')->nullable()->change();
            $table->foreignId('currency_id')->nullable()->change();
            $table->foreignId('country_id')->nullable()->change();
            $table->foreignId('bank_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('user_bank_accounts', function (Blueprint $table) {
            $table->string('reference_name')->change();
            $table->string('reference_bank_account')->change();
            $table->foreignId('currency_id')->change();
            $table->foreignId('country_id')->change();
            $table->foreignId('bank_id')->change();
        });
    }
}
