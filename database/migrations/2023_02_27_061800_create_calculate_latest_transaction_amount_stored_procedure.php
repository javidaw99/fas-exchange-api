<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCalculateLatestTransactionAmountStoredProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE PROCEDURE `calculate_latest_transaction_amount`(
            IN amount DECIMAL(18,4), 
            IN user_id_param INT, 
            IN user_account_id_param INT,  
            OUT old_bal DECIMAL(18, 4), 
            OUT new_bal DECIMAL(18, 4))
            BEGIN
            
                DECLARE row_count INT DEFAULT 0;
                SELECT COUNT(*)
                INTO row_count
                FROM transactions WHERE user_id = user_id_param AND user_account_id = user_account_id_param;
                
                SELECT COALESCE(new_balance, 0) as old_balance, (COALESCE(new_balance, 0) + amount) as new_balance
                INTO old_bal, new_bal
                FROM transactions WHERE user_id = user_id_param AND user_account_id = user_account_id_param
                ORDER BY created_at DESC 
                LIMIT 1;
                
                /*
                    check if record existed, if no record, set old balance = 0 and new balance = amount
                */
                IF (row_count = 0 ) THEN
                  SET old_bal = 0;
                  SET new_bal = amount;
                END IF;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS calculate_latest_transaction_amount');
    }
}
