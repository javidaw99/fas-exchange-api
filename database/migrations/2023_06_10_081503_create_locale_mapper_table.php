<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocaleMapperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locale_mapper', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_key', 255);
            $table->string('content_key', 255);
            $table->json('dynamic_values')->nullable();
            $table->string('reference_table', 255);
            $table->integer('reference_id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locale_mapper');
    }
}
