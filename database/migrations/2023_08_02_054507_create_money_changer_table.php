<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneyChangerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_changers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('pic', 255);
            $table->string('phone', 45);
            $table->string('email', 255);
            $table->unsignedInteger('address_id')->nullable();
            $table->string('currency_available', 255);
            $table->string('operation_days', 255);
            $table->string('operation_hours', 255);
            $table->string('emergency_pic', 255);
            $table->string('emergency_phone', 255);
            $table->boolean('is_active')->default('1');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_changer');
    }
}
