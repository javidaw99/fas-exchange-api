<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('recipient_country_id');
            $table->unsignedInteger('recipient_bank_country_id');
            $table->decimal('amount', 18, 4);
            $table->string('purpose', 255)->nullable();
            $table->string('recipient_acc_no', 255);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->softDeletes();

            // $table->foreign('currency_id')->references('id')->on('currencies');
            // $table->foreign('recipient_country_id')->references('id')->on('countries');
            // $table->foreign('recipient_bank_country_id')->references('id')->on('bank_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
