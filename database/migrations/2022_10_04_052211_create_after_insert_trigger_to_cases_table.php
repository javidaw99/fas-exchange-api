<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfterInsertTriggerToCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER after_cases_insert
            AFTER INSERT
            ON cases FOR EACH ROW
            BEGIN
            INSERT INTO case_logs
            SET id = NEW.id,
                user_id = NEW.user_id,
                account_id = NEW.account_id,
                to_account_id = NEW.to_account_id,
                ref_id = NEW.ref_id,
                assigned_user_id = NEW.assigned_user_id,
                ref_type = NEW.ref_type,
                status = NEW.status,
                reject_reason_text = NEW.reject_reason_text,
                created_at = NOW();
            END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `after_cases_insert`');
    }
}
